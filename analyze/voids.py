#!/usr/bin/env python

######### 8 October 2012 -KS########
def void_size_distribution(sys, max_error = 0.01, use_element_radii = True, sampling_interval = 1000, bins = 200, fix_bin_size = False, bin_size = 0.1):
	'''Computes the pore size distribution of a system according to the algorithm proposed by Bhattacharya and Gubbins, Langmuir 2006 22 7726-7731. Currently untested'''

	from numpy import random
	from GAS.elements import element_radius_list, element_symbol_list
	from math import sqrt, floor
	from numpy import zeros
	from numpy import mean

	#System value checks
	if sys.periodic == []:
		sys.periodic = [False, False, False]
	if sys.lx == 0.0:
		sys.update_box


	#Initializing cumulative histogram function	
	hist = []
	bin_centers = []
	avg_box_length = ((sys.lx+sys.ly+sys.lz)/3.0)
	if avg_box_length == 0.0:
		sys.lx = sys.box[0][1] - sys.box[0][0]
		sys.ly = sys.box[1][1] - sys.box[1][0]
		sys.lz = sys.box[2][1] - sys.box[2][0]
		avg_box_length = ((sys.lx+sys.ly+sys.lz)/3.0)
 	if fix_bin_size == False:
		bin_size = avg_box_length/bins
		#print bin_size
		for a in range(bins):
			hist.append(0.0)
			bin_centers.append(a*bin_size)
	else:
		#bins = 	int(avg_box_length/bin_size)
		
		for a in range(bins):
			hist.append(0.0)
			bin_centers.append(a*bin_size)
	#Staring pore size distribution calculation
	err_convergence = []
	err = 100
	PSD_old = []
	while err > max_error:

		#Computing histogram over one sampling interval
		for a in range(sampling_interval):
			P = [random.uniform(0,1)*sys.lx, random.uniform(0,1)*sys.ly, random.uniform(0,1)*sys.lz]
			#print P
			R = (2*avg_box_length)**2
			#print R
			for b in range(len(sys.id)):
				r = sqrt((P[0]-sys.x[b])**2+(P[1]-sys.y[b])**2+(P[2]-sys.z[b])**2)-element_radius_list[element_symbol_list.index(sys.element[b])]

				if r< 0:
					#print 'Point inside atom: radius not computed'
					break
				if r < R:
					R = r
						
			hist_bin = int(floor(R/bin_size))

			if hist_bin > bins:
				hist_bin = bins
			elif hist_bin < 0:
				hist_bin = 0
			
			for c in range(hist_bin):
				hist[c]+=1.0

		#Computing current pore size distribution, -dH(D)/dD
		PSD = zeros(bins-1)
		
		for i in range(len(PSD)):
			PSD[i] = -(hist[i+1]-hist[i])/bin_size
			if PSD[i] == -0:
				PSD[i]= 0

		#Computing error by comparing to previous 
		raw_err = zeros(bins-1)
		err = 0.0
		
		print PSD

		if PSD_old == []:
			PSD_old = PSD
			err = 100.0 #Dummy value to force us to stay in the while loop
			err_convergence.append(err)

		else:
			for i in range(len(PSD)):
				if PSD[i] == 0 and PSD_old[i]==0:
					raw_err[i] = 0
				elif PSD[i] ==0 and PSD_old[i]!=0:
					raw_err[i]=PSD_old[i]
				else:
					raw_err[i] = (PSD[i]-PSD_old[i])/PSD[i] 
			
			err = mean(raw_err)
			err_convergence.append(err)
			PSD_old = PSD

	return (PSD, hist, bin_centers, err_convergence)


