#!/usr/bin/env python

def pca_of_coord(coord,box,periodic = [True,True,True]):
	'''Uses the mlab.PCA() class to compute the principle components of atoms belonging to the given molecule id. Flatness is computed as the total standard deviation in all three component directions.Expects coordinates as [[x1,y1,z1],[x2,y2,z2]...]'''
#Modified 2 August 2012 to use PCA class instead of prepca
#Modified 16 August to unwrap coordinates

	from matplotlib import mlab
	from numpy import array, dot, empty
	from math import sqrt
	from kirke.analyze import periodic_delta

	avgcoord = [0,0,0]
	lx = box[0][1]-box[0][0]
	ly = box[1][1]-box[1][0]
	lz = box[2][1]-box[2][0]

	N = len(coord)
	if N > 3:
		array_coord = empty([N,3])
		for i in range(N):
			if i>0 :
				#Correcting periodic distances according the previous atoms positions
				if periodic[0]:
					dx = coord[i][0]-coord[i-1][0]
					coord[i][0] = coord[i][0]+periodic_delta(dx,lx)
				if periodic[1]:
					dy = coord[i][1]-coord[i-1][1]
					coord[i][1] = coord[i][1]+periodic_delta(dy,ly)
				if periodic[2]:
					dz = coord[i][2]-coord[i-1][2]
					coord[i][2] = coord[i][2]+periodic_delta(dz,lz)
				array_coord[i][0] = coord[i][0]
				array_coord[i][1] = coord[i][1]
				array_coord[i][2] = coord[i][2]
			else:
				array_coord[0][0] = coord[0][0]
				array_coord[0][1] = coord[0][1]
				array_coord[0][2] = coord[0][2]


		pcacomp = mlab.PCA(array_coord)
		pcaCoeff = pcacomp.Wt.T
		pcaScore = pcacomp.Y[0:2][:].T
		plane_normal = pcaCoeff[2]
		#Flatness is definined as the combined standard deviation
		s = pcacomp.sigma
		m = pcacomp.mu
		f = pcacomp.fracs
		netm = ((N*m[0])+(N*m[1])+(N*m[2]))/(3.0*N)

		#Assuming m in all directions is 0 (i.e., shifted center, as it should be)
		nets = sqrt((3*N*((s[0]**2)+(s[1]**2)+(s[2]**2))/(3*N)))

		flatness = nets
		return (plane_normal,flatness, pcaCoeff, s,f)


def ellipsoid_of_probability(xlist, ylist, zlist, fraction_inside = 0.5):
	'''Assumes a multivariate normal distribution, returns the axis of an ellipsoid that contain the fraction_inside of probability'''
	
	# first we have to find the Mahalanobis distance from the desired contained probabiltiy



	def CDF(a): 
		from scipy.special import erf
		from numpy import pi, exp, sqrt
		return erf(a/sqrt(2.0))-sqrt(2.0/pi)*a*exp(-0.5*a**2.0)

	def function_to_solve(x):
		return CDF(x)-fraction_inside

	from  scipy.optimize import newton
	Mahalanobis_distance = newton(function_to_solve , 1.0 )
	#print Mahalanobis_distance, CDF(Mahalanobis_distance)  This solver seems to work just fine between 0.01 and 0.999, i havent tested further


	########
	from numpy import cov, array, matrix, diag, sqrt
	from numpy.linalg import svd

	covar_mat = cov(array([ xlist, ylist, zlist ]))

	U, S, V = svd(covar_mat, full_matrices=True)
	S =  matrix(diag(sqrt(S)))  # dont forget the sqrt
	sigma = V*S # should contain the vectors for Mahalanobis_distance of 1 in each principle axis

	list_of_vectors = array(sigma).T * Mahalanobis_distance # need to scale them to for the right CDF
	
	return list_of_vectors
		



def compute_kabsch_rotation_matrix(initial_system, final_system):
	'''Two systems with the same atoms go in, and the rotation matrix go from the first to second comes out, they must have the same centroid to start'''
	#maybe fix the centroid thing later...
	from numpy import zeros, matrix, sign, identity
	from numpy.linalg import svd, det
	P = zeros((initial_system.number_of_atoms,3))
	Q = zeros((final_system.number_of_atoms,3))
	
	for i in range(initial_system.number_of_atoms):
		P[i][0] = initial_system.x[i]
		P[i][1] = initial_system.y[i]
		P[i][2] = initial_system.z[i]

		Q[i][0] = final_system.x[i]
		Q[i][1] = final_system.y[i]
		Q[i][2] = final_system.z[i]
	Q = matrix(Q)
	P = matrix(P)
	
	A = P.T*Q
	V, S, Wt = svd(A, full_matrices=True)

	# this fixes the handedness of the coordinate system
	d = sign(det(Wt.T*V.T))
	hand = identity(3)
	hand[2][2] = d 

	R = Wt.T*hand*V.T # the final rotation matrix
	
	return R

