#!/usr/bin/env python

def radial_distribution_function(sys, box = [[],[],[]] , periodic = [False, False , False], element1 = 'all', element2 = 'all', max_radius = 10.0, bins = 40,fix_bin_size = False, bin_size = 1.0 ,verbose = False): #modified by mike on 3/30/2012 #modified by Katie 5/9/2012, fixed and simplified normalization #mike should redo this #switch to initialize bins 26 June 2013 #switch to compute minimum_image_distance 27 Jan 2014
	'''Computes the pair correlation function between element1 and element2, which may both be set to 'all' to compute the total pcf. Bins can be set by size or count. Bin width or count will be determined from the max_radius. DEPRECATED!!! Use cell_radial_distribution_function instead!'''
	
	print '\n\n######## WARNING ########\nThis function is deprecated and redirects to cell_radial_distribution_function!\nPlease switch to calling cell_radial_distribution_function!!!!\n\n'
	from kirke.analyze import cell_radial_distribution_function
	bin_centers, counts = cell_radial_distribution_function(sys=sys, box=box, periodic=periodic, element1=element1, element2=element2, max_radius=max_radius, bins=bins, fix_bin_size=fix_bin_size, bin_size=bin_size, verbose=verbose)

	return bin_centers, counts

#############################################

def coordination_function(sys, box = [[],[],[]] , periodic = [False, False , False], element1 = 'all', element2 = 'all', max_radius = 10.0, bins = 40,fix_bin_size = False, bin_size = 1.0 ,verbose = False): #added by Katie 5/14/2012, based on the radial distribution function #modified 6/26/2013 to use initialize bins
	'''Computes the number of element2 atoms around the element1 atoms as a function of distance. The total coordination function can be calculated by setting both to 'all'. Bins can be set by count or size, and width or count will be determined from the max_radius.'''
	if box == [[],[],[]] :
		sys.update_box_from_positions()
	from math import sqrt,floor,pi
	from kirke.analyze.commonsubfunctions import initialize_bins
	sys.update_lengths_from_box()
	#make some bins
	
	[bin_centers, bin_edges, W, bins, counts] =initialize_bins(max_radius =max_radius, bins = bins,fix_bin_size = fix_bin_size, bin_size = bin_size ,verbose = verbose, initialize_count = True,minimum = 0.0)

	pairs = 0.0

	number_of_element1 = 0.0
	number_of_element2 = 0.0
	mxrsq = max_radius**2

	from kirke.analyze.commonsubfunctions import periodic_delta

	for i in range(sys.number_of_atoms):

		if element1 == sys.element[i] or element1 == 'all':
			number_of_element1 += 1.0
			if verbose and (i % 1000) == 0:
				print 'coordination_function(verbose) %i/%i' %(i,(sys.number_of_atoms-1))

			for j in range(sys.number_of_atoms):
				if element2 == sys.element[j] or element2 == 'all':
					number_of_element2 += 1.0 #Currently counting every loop, ends up as number_of_element1*number_of_element2 -Katie
					if i != j:
						pairs += 1.0 #Added to keep track of pairs for normalization purposes, since it will only count when a pair is evaluated

					rsq = sys.compute_minimum_image_distance(i,j)**2

					#dx = sys.x[i] - sys.x[j]
					#dy = sys.y[i] - sys.y[j]
					#dz = sys.z[i] - sys.z[j]

					#if periodic[0]:
					#	dx = periodic_delta(dx,sys.lx)
					#if periodic[1]:
					#	dy = periodic_delta(dy,sys.ly)
					#if periodic[2]:
					#	dz = periodic_delta(dz,sys.lz)
					
					#rsq = dx**2 + dy**2 + dz**2
					if rsq <= mxrsq and i != j:
						r = sqrt(rsq)
						bin = int(floor(r/W))## place in bin
						if bin > bins -1 : # in that rare mathematical chance r/w = bins aka right on the line
							bin = bins
						#This is where coordination number is different than cumulative dist or radial dist
						# The atom gets added to every bin larger than or equal to its actual position 
						for k in range(bin,bins):
							counts[k] = counts[k] + 1
	if number_of_element1 == 0.0:
		print "coordination_function: Warning, No %s atoms were found." % element1
	elif number_of_element2 == 0.0:
		print "coordination_function: Warning, No %s atoms were found." % element2
	else:
		
		for i in range (bins):
			counts[i] = (counts[i])/(number_of_element1) #Only care about the number of centers

	return bin_centers, counts

#############################################

#Modified 7/29/12 to normalize according to overall number density, not just what's in the slice
#Modified Nov 2 2012 to allow fix_slice_size
#Modified 28 Jan 2013 to reimplement the normalization bug fix.
def TwoD_slice_rdf(sys, dimension, box = [[],[],[]], periodic = [True, True, True], element1 = 'all', element2 = 'all', max_radius = 10.0, bins = 40, fix_bin_size = False, bin_size = 1.0, max_slice_limits= [[],[]],slices=20, fix_slice_size = False, slice_thick = 1.0, verbose = False):
	'''Computes the pair correlation function of a group of atoms projected into a two-dimensional slice. Dimension refers to the direction normal to the slice being taken. Bins can be fixed by count or size. Slices must be set by count, though future version may allow fixing by slice width. Function is normalized according to the total number density of the system, and may not come out to exactly 1 if there are significant density variations in your system.'''

	print 'WARNING : Need to check normalization!! If it uses the same normalization process as cell_radial_distribution_function, it will not normalize to 1.\n'

	from numpy import array
	from numpy import zeros
	from kirke.analyze.commonsubfunctions import periodic_delta, initialize_slice_edges, initialize_bins

	#Dimension refers to the direction normal to the slices being taken
	if box == [[],[],[]] :
		sys.update_box_from_positions()
	from math import sqrt,floor,pi
	sys.update_lengths_from_box()
	lx = sys.lx
	ly = sys.ly
	lz = sys.lz
	#Because I have to define all these things variably in order to make this work in any direction: *1 and *2 refer to the two in-plane directions
	#Modifying to make sure min&max system are used in the direction we're taking slices to avoid empty spaces which can affect number density factors
	if dimension == 'x':
		dim = 0
		p1 = 2
		p2 = 3
		l1 = ly
		l2 = lz
		l3 = max(sys.x)-min(sys.x)
		if max_slice_limits == []:
			max_slice_limits= [min(sys.x),max(sys.x)]

	elif dimension == 'y':
		dim = 1
		p1 = 1
		p2 = 3
		l1 = lx
		l2 = lz
		l3 = max(sys.y)-min(sys.y)
		if max_slice_limits == []:
			max_slice_limits= [min(sys.y),max(sys.y)]

	elif dimension == 'z':
		dim = 2 
		p1 = 1
		p2 = 2
		l1 = lx
		l2 = ly
		l3 = max(sys.z)-min(sys.z)
		if max_slice_limits == []:
			max_slice_limits= [min(sys.z),max(sys.z)]
	ldim = l3
	#Make some bins

	[slice_centers, slice_edges, slice_thick, slices] = initialize_slice_edges (max_slice_limits= max_slice_limits, slices=slices, fix_slice_size = fix_slice_size, slice_thick = slice_thick, verbose = verbose)


	[bin_centers, bin_edges, W, bins] =initialize_bins(max_radius =max_radius, bins = bins,fix_bin_size = fix_bin_size, bin_size = bin_size ,verbose = False, initialize_count = False,minimum = 0.0)

	print 'TwoD_slice_rdf: RDF Bin Width = %f' % W

	counts = []
	count_row = []

	counts = zeros((int(bins),int(slices)))
	raw_counts = zeros((int(bins),int(slices)))
	norm = zeros((int(bins),int(slices)))
	number_of_element1 = 0.0

	mxrsq = max_radius**2
	slice_pairs = zeros(int(slices))


	for s in range(slices):
		slice_atoms = []
		slice_limits = slice_edges[s]
		slice_obj = sys.get_slice(slice_limits,dimension)
		slice_atoms = slice_obj.id

		number_of_element1 = 0.0
		number_of_element2 = 0.0 
		
		if dimension == 'x':
			plane1 = slice_obj.y
			plane2 = slice_obj.z
		elif dimension == 'y':
			plane1 = slice_obj.x
			plane2 = slice_obj.z
		elif dimension == 'z':
			plane1 = slice_obj.x
			plane2 = slice_obj.y
		pairs = 0
		for i in range(len(slice_atoms)):
			if element1 == slice_obj.element[i] or element1 == 'all':
				number_of_element1 +=1.0
				for j in range(len(slice_atoms)):
					if element2 == slice_obj.element[j] or element2 == 'all':
						number_of_element2 += 1.0
						pairs +=1;
						#Note: this is the calculation of projected distance, rather than absolute distance
						d1 = plane1[i] - plane1[j]
						d2 = plane2[i] - plane2[j]

						if periodic[p1]:
							d1 = periodic_delta(d1,l1)
						if periodic[p2]:
							d2 = periodic_delta(d2,l2)
						rsq = d1**2 + d2**2
						if rsq <= mxrsq and i!=j: 
 
							r = sqrt(rsq)

							if r <0.9 and verbose:
								print 'TwoD_rdf: close pair found. %s and %s are % apart' %(i,j,r)

							bin = int(floor(r/W))
							if bin > bins-1 :
								bin = bins-1
							counts[bin][s] +=1
			slice_pairs[s]=pairs
	if element1 == 'all' and element2 == 'all':
		number_of_element1 = len(sys.id)
		number_of_element2 = len(sys.id)
		number_of_pairs = number_of_element2*(number_of_element1-1)/(2*slices)
	elif element1 == element2:
		number_of_element1 = len(sys.get_atom_typeid(element1).id)
		number_of_element2 = len(sys.get_atom_typeid(element2).id)
		number_of_pairs = number_of_element2*(number_of_element1-1)/slices
	else:
		number_of_element1 = len(sys.get_atom_typeid(element1).id)
		number_of_element2 = len(sys.get_atom_typeid(element2).id)
		number_of_pairs = number_of_element1*number_of_element2/slices
	if number_of_element1!=0 and number_of_element2!=0:
		for s in range (slices):
			numdens = number_of_pairs/(l1*l2)
			scaling = slice_pairs[s]/number_of_pairs*100
			for i in range (bins):
				r  = bin_centers[i]
				dr = W
				V = 2.0*pi*r*W#*slice_thickness
				if scaling != 0.0:
					norm[i][s] = V*slice_pairs[s]/(l1*l2)/scaling
					raw_counts[i][s] = counts[i][s]
					counts[i][s] = counts[i][s]/norm[i][s]

	return bin_centers, counts ,slice_edges

