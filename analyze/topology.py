#!/usr/bin/env python

def bond_length_distribution(sys, bond_type = 'all', periodic = [False, False, False], bins = 200, max_radius = 5.0, fix_bin_size = False, bin_size = 0.1, verbose=False, return_list = False):
	'''Computes the bond distribution of either a particular bond type or all bonds in a system. Currently untested'''
	if sys.box == [[],[],[]] :
		sys.update_box_from_positions()
	if sys.number_of_bonds == 0:
		sys.number_of_bonds = len(sys.bonds)

	from math import floor
	from kirke.analyze.commonsubfunctions import initialize_bins
	
	[bin_centers, bin_edges, W, bins, counts] =initialize_bins(max_radius =max_radius, bins = bins,fix_bin_size = fix_bin_size, bin_size = bin_size ,verbose = False, initialize_count = True,minimum = 0.0)

	bond_length_list = []

	for i in range(sys.number_of_bonds):

		if bond_type == sys.bonds[i][1] or bond_type == 'all':
			atom1 = sys.bonds[i][2]
			atom2 = sys.bonds[i][3]

			if sys.id_to_index == []:
				sys.build_index_dict()

			i = sys.id_to_index[atom1]
			j = sys.id_to_index[atom2]

			r = sys.compute_minimum_image_distance_between_two_particles(i,j)

			bond_length_list.append(r)

			bin = int(floor(r/W))## place in bin
			if bin > bins -1 : # in that rare mathematical chance r/w = bins aka right on the line
				bin = bins-1

			counts[bin] = counts[bin] + 1
	if return_list == False:
		return bin_centers, counts
	else:
		return bond_length_list, bin_centers, counts

def bond_angle_distribution(sys, angle_type = 'all', periodic = [False, False, False], use_degrees = True, bins = 180, max_angle = 180.0, min_angle = 0, fix_bin_size = False, bin_size = 1.0):
	'''Computes the bond angle distribution of either a particular angle type or all bonds in a system. Returns values in degrees or radians as specified. Default bin values are intended for degrees. '''
	from math import floor, pi
	from kirke.analyze.commonsubfunctions import initialize_bins

	if sys.box == [[],[],[]] :
		sys.update_box_from_positions()
	if sys.number_of_angles == 0:
		sys.number_of_angles = len(sys.angles)

	if sys.lx == 0.0 and sys.box_mode == 'orthorhombic':
		sys.lx = sys.box[0][1] - sys.box[0][0]
		sys.ly = sys.box[1][1] - sys.box[1][0]
		sys.lz = sys.box[2][1] - sys.box[2][0]

	#make some bins
	[bin_centers, bin_edges, W, bins, counts] =initialize_bins(max_radius = (max_angle - min_angle), bins = bins,fix_bin_size = fix_bin_size, bin_size = bin_size ,verbose = False, initialize_count = True,minimum = 0.0)

	for j in range(len(bin_centers)):	# controlling for non-zero values of min_angle
		bin_centers[j] += min_angle
		bin_edges[j] += min_angle

	for a in range(sys.number_of_angles):

		if angle_type == sys.angles[a][1] or angle_type == 'all':
			atom1 = sys.angles[a][2]
			atom2 = sys.angles[a][3]
			atom3 = sys.angles[a][4]

			if sys.id_to_index == []:
				sys.build_index_dict()

			i = sys.id_to_index[atom1]
			j = sys.id_to_index[atom2]
			k = sys.id_to_index[atom3]

			r = sys.compute_minimum_image_angle(j, i, k)

			if use_degrees == True:
				r = r*57.2957795

			bin = int(floor(r/W))## place in bin
			if bin > bins -1 : # in that rare mathematical chance r/w = bins aka right on the line
				bin = bins

			counts[bin] = counts[bin] + 1

	return bin_centers, counts

def dihedral_angle_distribution(sys, dihedral_angle_type = 'all', periodic = [False, False, False], use_degrees = True, bins = 180, max_angle = 180.0, min_angle = 0, fix_bin_size = False, bin_size = 1.0):
	'''Computes the dihedral angle distribution of either a particular dihedral angle type or all dihedral angles in a system. Returns values in degrees or radians as specified. Default bin values are intended for degrees. Can now accomodate negative values. '''
	from kirke.analyze.commonsubfunctions import initialize_bins
	from math import floor, pi

	if sys.box == [[],[],[]] :
		sys.update_box_from_positions()

	sys.update_lengths_from_box()

	if sys.number_of_dihedrals == 0:
		sys.number_of_dihedrals = len(sys.dihedrals)

	# Make some bins
	[bin_centers, bin_edges, W, bins, counts] =initialize_bins(max_radius = (max_angle - min_angle), bins = bins,fix_bin_size = fix_bin_size, bin_size = bin_size ,verbose = False, initialize_count = True,minimum = 0.0)
	extent_list = []			# 

	for j in range(len(bin_centers)):	# controlling for non-zero values of min_angle
		bin_centers[j] += min_angle
		bin_edges[j] += min_angle

	for a in range(sys.number_of_dihedrals):

		if dihedral_angle_type == sys.dihedrals[a][1] or dihedral_angle_type == 'all':
			atom1 = sys.dihedrals[a][2]
			atom2 = sys.dihedrals[a][3]
			atom3 = sys.dihedrals[a][4]
			atom4 = sys.dihedrals[a][5]

			if sys.id_to_index == []:
				sys.build_index_dict()

			i = sys.id_to_index[atom1]
			j = sys.id_to_index[atom2]
			k = sys.id_to_index[atom3]
			l = sys.id_to_index[atom4]

			# Minimum image vector accomodates triclinics, periodicity
			r = sys.compute_minimum_image_dihedral(i, j, k, l)
		
			if use_degrees == True:
				r *= (180.0/pi)

			bin = int(floor((r-min_angle)/W))## place in bin
			if bin > bins -1 : # in that rare mathematical chance r/w = bins aka right on the line
				bin = bins

			counts[bin] = counts[bin] + 1

	return bin_centers, counts, extent_list

def find_bond_centers(sys, periodic = [False, False, False],bond_type = 'all' ):
	''' Returns a GAS object, bond_centers, of the bond centers according to type'''
	from kirke import GAS
	if sys.number_of_bonds == 0:
		print 'Bonds are needed to find the centers. You may need to run sys.update_number_of_bonds() firsts'
	else:
		bond_centers = GAS()
		bond_centers.box = sys.box
		bond_centers.box_mode = sys.box_mode
		bond_centers.periodic = sys.periodic
		bond_centers.update_lengths_from_box()
		if sys.id_to_index == []:
			sys.build_index_dict()

		if bond_type == 'all':
			#Finding center with no regard for relative sizes or masses.
			#Populating q with ones to indicate
			for a in range(sys.number_of_bonds):
				i = sys.id_to_index[sys.bonds[a][2]]
				j = sys.id_to_index[sys.bonds[a][3]]
				ij = sys.compute_minimum_image_vector_between_two_particles(i,j)
				bond_centers.id.append(sys.bonds[a][0])
				bond_centers.x.append(sys.x[i]+ij[0])
				bond_centers.y.append(sys.y[i]+ij[1])
				bond_centers.z.append(sys.z[i]+ij[2])
				bond_centers.atomic_number.append(sys.bonds[a][1])
			
		else:
			for a in range(sys.number_of_bonds):
				if sys.bonds[a][1] in bond_type:
					i = sys.id_to_index[sys.bonds[a][2]]
					j = sys.id_to_index[sys.bonds[a][3]]
					ij = sys.compute_minimum_image_vector_between_two_particles(i,j)
					bond_centers.id.append(sys.bonds[a][0])
					bond_centers.x.append(sys.x[i]+ij[0])
					bond_centers.y.append(sys.y[i]+ij[1])
					bond_centers.z.append(sys.z[i]+ij[2])
					bond_centers.atomic_number.append(sys.bonds[a][1])
		if bond_centers.box_mode == '':
			bond_centers.box_mode = 'orthorhombic'
		bond_centers.update_number_of_atoms()
		#Update the internal coordinates for bond centers
	return bond_centers
