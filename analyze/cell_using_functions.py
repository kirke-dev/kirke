#!/usr/bin/env python

###Building fast functions that call the cell builder 

def cell_radial_distribution_function(sys, box = [[],[],[]] , periodic = [False, False , False], element1 = 'all', element2 = 'all', max_radius =10.0, bins = 40,fix_bin_size = False, bin_size = 1.0 ,verbose = False):
	#Modified 24 Jan 2014 to use compute_minimum_distance and to debug normalization
	#Modified 19 Mar 2014 to use id_to_index
	# Modified 6 Feb 2015 to prevent self-pair detection because of the new neighbor_list reporting
	'''Uses the neighbor_list by Mike to compute the pair correlation function between element1 and element2, which may both be set to 'all' to compute the total pcf. Bins can be set by size or count. Bin width or count will be determined from the max_radius. '''

	from kirke.analyze.neighbors import neighbor_list

	if box == [[],[],[]] :
		sys.update_box_from_positions()
		box = sys.box

	from math import sqrt,floor,pi
	if sys.lx == 0.0:
		sys.update_lengths_from_box()

	sys.build_index_dict()
	#Generating initial bins

	from commonsubfunctions import initialize_bins
	if verbose:
		print 'cell_radial_distribution_function: Initializing bins'
	[bin_centers, bin_edges, W, bins, counts] = initialize_bins(max_radius, bins, fix_bin_size, bin_size, initialize_count = True)

	#Generating cell lists
	if verbose == True:
		print 'cell_radial_distribution_function: Sorting'
	sys.sort_by('id')
	if verbose == True:
		print 'cell_radial_distribution_function: Making nghblst'

	nghblst = neighbor_list(sys, max_radius, periodic, element1, element2, verbose = False, use_index = True)
#Trying using index based nghblst next to make this better

	if verbose == True:
		print 'cell_radial_distribution_function: Computing distances'

	for index in range(sys.number_of_atoms):
		i = sys.id[index]
		ix = sys.x[index]
		iy = sys.y[index]
		iz = sys.z[index]
		#Iterating over all neighbors
		#Had to fix this because nghblst now auto-includes its own center, which screwed up all the things
		for j in range(1,len(nghblst[index])):
#removed reliance on id_to_index, because something was going wrong.

			jndex = nghblst[index][j]
			jx = sys.x[jndex]
			jy = sys.y[jndex]
			jz = sys.z[jndex]

			r = sys.compute_minimum_image_distance_between_two_particles(index,jndex)

			if r < 0.8 and verbose == True :
				print "cell_radial_distribution_function: Close pair found: %s and %s.(%s and %s) Types: %s and %s, Molecules %s and %s. Distance is %s." %(i, sys.id[jndex],index,jndex, sys.element[index], sys.element[jndex], sys.molecule[index], sys.molecule[jndex], r)

			if r<= max_radius and index!=jndex:
				count_bin = int(floor(r/W)) #floor by default returns a float...
				if count_bin > bins -1:
					count_bin = bins
				counts[count_bin] += 1

	#Normalization
	if verbose == True:
		print 'cell_radial_distribution_function: Computing normalization factors'

	if element1 != 'all':
		number_of_element1 = len(sys.get_atom_typeid_fast(element1).id)
	if element2 != 'all':
		number_of_element2 = len(sys.get_atom_typeid_fast(element2).id) #This was one of the problems: number of element2 was looking at element1.id
	if element1 == 'all' and element2 == 'all':
		number_of_pairs = len(sys.id)*(len(sys.id)-1) #Because I'm double counting pairs, rather than using diagonalization, removed 0.5 factor
	elif element1 == 'all' and element2 != 'all':
		number_of_pairs = (len(sys.id)-1)*number_of_element2
	elif element2 == 'all' and element1 != 'all':
		number_of_pairs = (len(sys.id)-1)*number_of_element1
	elif element2 != 'all' and element1 != 'all':
		number_of_pairs = number_of_element2*number_of_element1	

	#CHECK THE NORMALIZATION FACTOR ONCE DONE WITH THE OTHER ONE

	for b in range(bins):
		sysvol = sys.lx*sys.ly*sys.lz
		V = 4.0/3.0*pi*(bin_edges[b+1]**3-bin_edges[b]**3)
		counts[b] = (counts[b]/V)/(number_of_pairs/sysvol)

	return bin_centers, counts 

###################################
def compute_neighbor_distance_list(sys,  box = [[],[],[]] , periodic = [False, False , False], element1 = 'all', element2 = 'all', max_radius = 10.0, bins = 40,fix_bin_size = False, bin_size = 1.0 ,verbose = False):
	'''Computes pair distances, includes id outputs. Probably going to be used in polymerization. Bins id pairs according to a distance bin'''

	print 'compute_neighbor_distance_list: WARNING Function currently useless'

	from kirke.analyze.neighbors import neighbor_list

	if box == [[],[],[]] :
		sys.update_box_from_positions()
		box = sys.box

	from math import sqrt,floor,pi

	sys.update_lengths_from_box()

	#Generating initial bins

	from commonsubfunctions import initialize_bins
	print 'compute_neighbor_distance_list: Initializing bins'

	[bin_centers, bin_edges, W, bins] = initialize_bins(max_radius, bins, fix_bin_size, bin_size, initialize_count = False)

	pair_lists = []
	for b in range(bins):
		pair_lists.append([])

	print pair_lists

	#Generating cell lists
	print 'compute_neighbor_distance_list: Sorting'
	sys.sort_by('id')
	print 'compute_neighbor_distance_list: Making nghblst'
	nghblst = neighbor_list(sys, max_radius, periodic, element1, element2, verbose)
	print 'compute_neighbor_distance_list: Computing distances'

	for index in range(sys.number_of_atoms):
		i = sys.id[index]
		ix = sys.x[index]
		iy = sys.y[index]
		iz = sys.z[index]
		#Iterating over all neighbors
		for j in nghblst[index]:
			if sys.id_to_index == []:
				sys.build_index_dict()
			jndex = sys.id_to_index[j]

			jx = sys.x[jndex]
			jy = sys.y[jndex]
			jz = sys.z[jndex]

			dx = ix - jx
			dy = iy - jy
			dz = iz - jz
			
			from commonsubfunctions import periodic_delta
			if periodic[0]:
				dx = periodic_delta(dx,sys.lx)
			if periodic[1]:
				dy = periodic_delta(dy,sys.ly)
			if periodic[2]:
				dz = periodic_delta(dz,sys.lz)

			r = sqrt((dx)**2+(dy)**2+(dz)**2)

			#Binning R
			rbin = int(floor(r/W))
			if rbin > bins -1:
				rbin = bins
			pair_lists[rbin].append([index,jndex])

	return bin_centers, pair_lists #, raw_counts


