#!/usr/bin/env python

def write_sliced_distribution(slice_positions, bin_list, dist_values, output_file='outfile', delim='\t'):
	'''Designed to easily convert output from functions lie TwoD_slice_rdf to a well organized matrix, with slices as columns and bins as rows'''
	
	yarn = []
	string = 'Bin Centers'+delim
	
	for i in slice_positions:
		string += '%s' %i+delim
	string +='\n'
	yarn.append(string)
	tick = 0

	for a in dist_values:
		string = '%f' %bin_list[tick] +delim
		tick += 1
		for b in range(len(slice_positions)):
			string += '%f' %a[b] +delim
		yarn.append(string)

	outputid = open(output_file, 'w')
	for a in yarn:
		outputid.write(a)
	outputid.close()

######################################################

def write_2col_distribution( bin_list, dist_values, output_file = 'output_file', delim = '\t', labels = 'Bin Centers \t Counts \n'):
	'''Designed to easily convert the output from basic distribution functions, such as RDF, to an easily read and plotted file.'''

	yarn = labels
	for a in range(len(bin_list)):
		string = '%f'%(bin_list[a]) 
		string += delim
		string += '%f \n' %(dist_values[a])
		yarn += string

	outputid = open(output_file, 'w')
	for a in yarn:
		outputid.write(a)
	outputid.close()

###################################################

def write_generic_columns(output_file, number_of_columns = 1, column_list = [], delim = '\t', labels = 'Unknown \n'):
	'''Designed to write any number of lists of the same length to columns in a file.'''

	#Flag error if columns unequal lengths
	first_column_length = len(column_list[0])
	flag = False
	for a in range(number_of_columns):
		if len(column_list[a])!=first_column_length:
			print 'write_generic_columns: ERROR Column %s is a different length than the first column. Please try a different way to write to file' %a
			flag = True
			break

	if flag == False:

		yarn = labels
		string = ''
		for a in range(first_column_length):
			for b in range(number_of_columns):
				string += '%s' %column_list[b][a]
				string += delim
			string += '\n'
		yarn += string

		outputid = open(output_file, 'w')
		for a in yarn:
			outputid.write(a)
		outputid.close()

	else:
		print 'write_generic_columns: ERROR! Fix your columns! They are of unequal lengths!\n'


