def sorted_neighbor_list(sys, cutoff_radius, periodic = [],element1 = 'all', element2 = 'all' ,verbose = False, use_index = False): 
	'''Determines the nearest neighbor list according to cutoff_radius. ''' # this one uses cell lists and is super sexy fast.

	if periodic == []:
		periodic = sys.periodic
	if periodic[0] or periodic[1] or periodic[2]:
		if sys.box_mode != 'orthorhombic' and  sys.box_mode != 'triclinic':
			print 'sorted_neighbor_list: ERROR This system is periodic but the box_mode is not set. The neighbor_list function hurt itself in your confusion.'
	print "sorted_neighbor_list: Updating Basis and origin"

	if sys.box_mode == 'orthorhombic':
		sys.update_basis_and_origin_for_orthorhombic()

	# picking cell 
	from kirke.analyze.cell_builder_functions import compute_cells_to_fit_radius_inside, cell_list
	from kirke.analyze.commonsubfunctions import periodic_delta
	cells = compute_cells_to_fit_radius_inside(sys,cutoff_radius)
	cells_list_array = cell_list(sys,cells)

	if verbose:
		print 'cells: ', cells 

	neighbor_list = []
	atomcellindex = [0,0,0]

	rcutsq = cutoff_radius*cutoff_radius
	from math import floor,sqrt
	from operator import itemgetter

	print 'sorted_neighbor_list: Iterating though atoms to find neighbors'
	for i in range(sys.number_of_atoms):
		if verbose:
			print '%i/%i' %(i+1,(sys.number_of_atoms))
		if use_index == False:
			neighbor_list.append([sys.id[i]])
		else:
			neighbor_list.append([i])
		
		tuple_list = []
		if element1 == sys.element[i] or element1 == 'all' or element1==sys.atomic_number[i]:
			# we will check atom i, now we just need to figure out the cells
			S = (sys.sx[i], sys.sy[i], sys.sz[i])
			for dim in range(3):
				atomcellindex[dim] = int(floor(S[dim]*cells[dim]))
			# loop over the 3x3x3 cell block
			for ci in range(atomcellindex[0]-1, atomcellindex[0]+2):
				for cj in range(atomcellindex[1]-1, atomcellindex[1]+2):
					for ck in range(atomcellindex[2]-1, atomcellindex[2]+2):
						# loop over atoms in the cell
						for j in cells_list_array[ci%cells[0]][cj%cells[1]][ck%cells[2]]:

							if element2 == sys.element[j] or element2 == 'all' or element2==sys.atomic_number[j]:
								ds1 = sys.sx[j] - sys.sx[i]
								ds2 = sys.sy[j] - sys.sy[i]
								ds3 = sys.sz[j] - sys.sz[i]

								if periodic[0]:
									ds1 = periodic_delta(ds1,1.0)
								if periodic[1]:
									ds2 = periodic_delta(ds2,1.0)
								if periodic[2]:
									ds3 = periodic_delta(ds3,1.0)
								ds = [ds1,ds2,ds3]

								dr = sys.matrix_vector_product_3D(sys.basis,ds)
								rsq = sys.dot_product_3D(dr,dr)
								if rsq <= rcutsq and i !=j :
									tuple_list.append((j,sqrt(rsq)))	
		tuple_list.sort(key = itemgetter(1))
		#print tuple_list
		for tup in tuple_list:
			if use_index == False:
				neighbor_list[i].append(sys.id[tup[0]])
			else:
				neighbor_list[i].append(tup[0])
		if verbose:
			print len(neighbor_list[i])-1

	return neighbor_list
