#!/usr/bin/env python

def neighbor_list_from_bonds(sys, element1 = 'all', element2 = 'all', verbose = False, use_index = False): 
	'''Determines the nearest neighbor list according to sys.bonds. ''' 

	if sys.number_of_bonds == 0:
		print "neighbor_list_from_bonds: WARNING Current number of bonds is 0. This function requires bonds. Either generate bonds using manipulate.topology functions, or use neighbor_list and cutoff criteria"

	if sys.id_to_index == []:
		sys.build_index_dict()

	neighbor_list = []
	from math import floor

	print 'neighbor_list: Iterating though atoms to find neighbors'
	for i in range(sys.number_of_atoms):
		if verbose:
			print 'neighbor_list(verbose): %i/%i' %(i+1,(sys.number_of_atoms))
		if use_index == False:
			neighbor_list.append([sys.id[i]])
		else:
			neighbor_list.append([i])

	for b in range(sys.number_of_bonds):
		atom_1 = sys.bonds[b][2]
		atom_2 = sys.bonds[b][3]

		i = sys.id_to_index[atom_1]
		j = sys.id_to_index[atom_2]

		if i==j:
			print "neighbor_list_from_bonds: Something thinks it's bonded to itself. Check indexing and bonds"

		if element1 == sys.element[i] or element1 == 'all' or element1==sys.atomic_number[i]:
			if element2 == sys.element[j] or element2 == 'all' or element2==sys.atomic_number[j]:
				if use_index == False:
					neighbor_list[i].append(atom_2)	
					neighbor_list[j].append(atom_1)
				else:
					neighbor_list[i].append(j)	
					neighbor_list[j].append(i)
		if verbose:
			print len(neighbor_list[i])-1
	return neighbor_list

def neighbor_list(sys, cutoff_radius, periodic = [],element1 = 'all', element2 = 'all', verbose = False, use_index = False): 
	'''Determines the nearest neighbor list according to cutoff_radius. INCLUDES THE CENTER ATOMS?''' # this one uses cell lists and is super sexy fast.

	if periodic == []:
		periodic = sys.periodic
	if periodic[0] or periodic[1] or periodic[2]:
		if sys.box_mode != 'orthorhombic' and  sys.box_mode != 'triclinic':
			print 'neighbor_list: ERROR This system is periodic but the box_mode is not set. The neighbor_list function hurt itself in your confusion.'
	if verbose:
		print "neighbor_list: Updating Basis and origin"

	if sys.box_mode == 'orthorhombic':
		sys.update_basis_and_origin_for_orthorhombic()

	if sys.id_to_index == []:
		sys.build_index_dict()

	# picking cell 
	from kirke.analyze.cell_builder_functions import compute_cells_to_fit_radius_inside, cell_list
	from kirke.analyze.commonsubfunctions import periodic_delta
	cells = compute_cells_to_fit_radius_inside(sys,cutoff_radius)
	cells_list_array = cell_list(sys,cells)

	if verbose:
		print 'cells: ', cells 

	neighbor_list = []
	atomcellindex = [0,0,0]

	rcutsq = cutoff_radius*cutoff_radius
	from math import floor
	if verbose:
		print 'neighbor_list: Iterating though atoms to find neighbors'

	for i in range(sys.number_of_atoms):
		if verbose:
			print 'neighbor_list(verbose): %i/%i' %(i+1,(sys.number_of_atoms))
		if use_index == False:
			neighbor_list.append([sys.id[i]])
		else:
			neighbor_list.append([i])

		if element1 == sys.element[i] or element1 == 'all' or element1==sys.atomic_number[i]:
			# we will check atom i, now we just need to figure out the cells
			S = (sys.sx[i], sys.sy[i], sys.sz[i])
			for dim in range(3):
				atomcellindex[dim] = int(floor(S[dim]*cells[dim]))
			# loop over the 3x3x3 cell block
			for ci in range(atomcellindex[0]-1, atomcellindex[0]+2):
				for cj in range(atomcellindex[1]-1, atomcellindex[1]+2):
					for ck in range(atomcellindex[2]-1, atomcellindex[2]+2):
						# loop over atoms in the cell
						for j in cells_list_array[ci%cells[0]][cj%cells[1]][ck%cells[2]]:

							if element2 == sys.element[j] or element2 == 'all' or element2==sys.atomic_number[j]:
								ds1 = sys.sx[j] - sys.sx[i]
								ds2 = sys.sy[j] - sys.sy[i]
								ds3 = sys.sz[j] - sys.sz[i]

								if periodic[0]:
									ds1 = periodic_delta(ds1,1.0)
								if periodic[1]:
									ds2 = periodic_delta(ds2,1.0)
								if periodic[2]:
									ds3 = periodic_delta(ds3,1.0)
								ds = [ds1,ds2,ds3]

								dr = sys.matrix_vector_product_3D(sys.basis,ds)
								rsq = sys.dot_product_3D(dr,dr)
								if rsq <= rcutsq and i !=j and sys.id[i]!=sys.id[j]:

									if use_index == False:
										neighbor_list[i].append(sys.id[j])
									else:
										neighbor_list[i].append(j)	#This is currently indicies, not ids
		if verbose:
			print len(neighbor_list[i])-1

	return neighbor_list


def cluster_list(id_list,neighbor_list, compute_number_distribution = False):
	'''Uses a list of atom ids and the neighbor list to compute clusters in the structure. Can also compute the distribution of cluster sizes. '''

	print" WARNING: cluster_list may not be compatible with updates to neighbor_list. Try updated_cluster_list, unless working with flx_nghblst files"
	unclaimed_index_set = set([])
	for a in id_list:
		unclaimed_index_set.add(a)
	#print unclaimed_index_set
	##

	cluster_list_list = []
	more_atoms = True

	while more_atoms:
		#print len(unclaimed_index_set)
		first_index_of_cluster = list(unclaimed_index_set)[0]

		#print first_index_of_cluster
		checked_set = set([])
		unchecked_set = set([first_index_of_cluster])

		more_to_add = True
		while more_to_add:

			add_set = set([])
			for a in unchecked_set:
				index_of_a = id_list.index(a)
				add_set =  add_set | (set(neighbor_list[index_of_a])) # set operations

			checked_set = checked_set | unchecked_set # set operations
			unchecked_set = add_set - checked_set # set operations

			if len(add_set) == 0:
				more_to_add = False
		##
		cluster_list_list.append(list(checked_set))

		unclaimed_index_set = unclaimed_index_set - checked_set # set operations

		if len(unclaimed_index_set) == 0:
			more_atoms = False

	if compute_number_distribution:
		maxsize = 1
		for a in cluster_list_list: # find max size
			if len(a)> maxsize:
				maxsize = len(a)

		count_list = []
		for i in range(maxsize):   # make bins
			count_list.append(0.0)

		for a in cluster_list_list: #count for each bin
			count_list[len(a)-1] += 1
		
		return (cluster_list_list,count_list)
	else:
		return cluster_list_list

def updated_cluster_list(new_neighbor_list, compute_number_distribution = False):
	'''Uses neighbor list, assuming the neighbor_list includes to center atom id to compute clusters in the structure. Can also compute the distribution of cluster sizes. '''

	unclaimed_index_set = set([])
	id_list = []
	neighbor_list = []
	for a in new_neighbor_list:
		unclaimed_index_set.add(a[0])
		id_list.append(a[0]) #look, now we have one!
		neighbor_list.append(a[1:]) #And returning the neighbor list to its previous incarnation, with no center
	#print unclaimed_index_set
	##

	cluster_list_list = []
	more_atoms = True

	while more_atoms:
		#print len(unclaimed_index_set)
		first_index_of_cluster = list(unclaimed_index_set)[0]

		#print first_index_of_cluster
		checked_set = set([])
		unchecked_set = set([first_index_of_cluster])

		more_to_add = True
		while more_to_add:

			add_set = set([])
			for a in unchecked_set:
				index_of_a = id_list.index(a)
				add_set =  add_set | (set(neighbor_list[index_of_a])) # set operations

			checked_set = checked_set | unchecked_set # set operations
			unchecked_set = add_set - checked_set # set operations

			if len(add_set) == 0:
				more_to_add = False
		##
		cluster_list_list.append(list(checked_set))

		unclaimed_index_set = unclaimed_index_set - checked_set # set operations

		if len(unclaimed_index_set) == 0:
			more_atoms = False

	if compute_number_distribution:
		maxsize = 1
		for a in cluster_list_list: # find max size
			if len(a)> maxsize:
				maxsize = len(a)

		count_list = []
		for i in range(maxsize):   # make bins
			count_list.append(0.0)

		for a in cluster_list_list: #count for each bin
			count_list[len(a)-1] += 1
		
		return (cluster_list_list,count_list)
	else:
		return cluster_list_list

