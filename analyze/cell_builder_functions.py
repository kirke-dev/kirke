#!/usr/bin/env python

def compute_cells_to_fit_radius_inside(sys,radius):
	# i have such a hard time visualizing this, I've added several descriptions
	#1 if a cell length is half the diameter of interest wide (or greater), then it will always fit in a 3x3x3 box, importantly all interactions of with the radius of as a range will only occur in that 3x3x3 box
	#2 Inscribed circle 
	#3 in the end, the cell wall interplaner spacing will be slightly larger than the radius
	#4 finds the maximum number of cells in each direction that still keep the interplaner spacing greater than some radius
	cells = [1,1,1]
	from math import pi,sqrt,floor
	for i in range(3):
		B = sys.compute_reciprocal_basis_vector(i)
		distance = (2.0*pi) / sqrt(B[0]**2.0 + B[1]**2.0 + B[2]**2.0)
		cells[i] = int(floor(distance/radius))
		if cells[i] < 1:
			cells[i] = 1
	return cells



def compute_cells_to_have_maximized_spacing_less_than_radius(sys,radius):
	# this is for the case where you want all your grid points to be just under some radius apart.
	# this was made so radial analysis could ensure roughly 1 to 1 correspondece between radial spacing and grid spacing.
	cells = [1,1,1]
	from math import sqrt,ceil
	for i in range(3):
		A = sys.get_basis_vector(i)
		distance = sqrt(A[0]**2.0 + A[1]**2.0 + A[2]**2.0)
		cells[i] = int(ceil(distance/radius))
	return cells




def compute_cells_to_fit_radius_outside(sys,radius):
	from kirke.analyze.cell_builder_functions import compute_cells_to_fit_radius_inside
	nmin = compute_cells_to_fit_radius_inside(sys ,radius = radius)
	#print nmin
	from numpy import array, dot, sqrt, ceil
	A0 = array(sys.get_basis_vector(0))
	A1 = array(sys.get_basis_vector(1))
	A2 = array(sys.get_basis_vector(2))
	
	case0 = A0/nmin[0] + A1/nmin[1] + A2/nmin[2]
	case0 = dot(case0,case0)
	
	case1 = A0/nmin[0] + A1/nmin[1] - A2/nmin[2]
	case1 = dot(case1,case1)

	case2 = A0/nmin[0] - A1/nmin[1] + A2/nmin[2]
	case2 = dot(case2,case2)

	case3 = A0/nmin[0] - A1/nmin[1] - A2/nmin[2]
	case3 = dot(case3,case3)

	cases = [case0, case1, case2, case3]
	
	for i in range(4):
		if cases[i] == max(cases):
			largest_case = i
	
	scale_factor = sqrt(cases[largest_case]/(radius*radius))
	n0 = int(ceil(scale_factor*nmin[0]))
	n1 = int(ceil(scale_factor*nmin[1]))
	n2 = int(ceil(scale_factor*nmin[2]))
	cells = (n0, n1, n2)

	return cells


def cell_list(sys,cells,verbose = False):

	if sys.sx == [] :
		if verbose:
			print "cell_list(verbose) No internal coordinates present. Creating internal coordinate lists."
		sys.fill_s_lists()
		sys.update_internal_from_external()
	elif set(sys.sx) == set([0.0]):
		sys.update_internal_from_external()

	cell_array = []
	for i in range(cells[0]):
		cell_array.append([])
		for j in range(cells[1]):
			cell_array[i].append([])
			for k in range(cells[2]):
				cell_array[i][j].append([])

	from math import floor
	cellf = [0.0,0.0,0.0]
	atomcellindex = [0,0,0]

	for atom in range(sys.number_of_atoms):
		S = (sys.sx[atom], sys.sy[atom], sys.sz[atom])
		for dim in range(3):
			cellf[dim] = S[dim]*cells[dim]
			atomcellindex[dim] = int(floor(cellf[dim]))

		cell_array [atomcellindex[0]%cells[0]] [atomcellindex[1]%cells[1]] [atomcellindex[2]%cells[2]].append(atom)

	return cell_array

