
def compute_temperature(sys, boltzmann_constant = 1.0):

	if boltzmann_constant == 1.0:
		print 'compute_temperature: Warning, boltzmann_constant is set to 1.0, try these:'
		print 'compute_temperature: Boltzmann constant is 1.3807E-23 J/K
		print 'compute_temperature: or is 0.83145 amu*Ang^2/(ps^2*K)

	summation = 0.0
	for atom in range(sys.number_of_atoms):
		vsqr = sys.vx[atom]**2 + sys.vy[atom]**2 + sys.vz[atom]**2
		mass = sys.masses[atom]
		summation += vsqr*mass

	temperature = summation/(3.0*boltzmann_constant*sys.number_of_atoms )
	return temperature
