#!/usr/bin/env python

#Extracting some of the common subroutines from analysis functions in order to simply the code
#None of these actually interact with the system object, but do useful little things we need frequently

##########
def periodic_delta(dx,lx):
	''' Periodic corrector function. Requires dx and lx, where dx and lx are a matching distance and dimension length pair.Returns delta values'''
	from math import floor
	return ( dx - lx * floor(dx/lx - 0.5) - lx )

######################
def get_hist_list(list_to_hist, bins = 10,fix_bin_size = False, fix_bin_count = True, bin_size = 1.0, min_bin = 0.0, floor = False ):
	''' Computes a histogram of the given list with a fixed number of bins or fixed bin size. '''
#Modified 17 August for fixed bin sizes
	from math import floor
	if bins == []:
		bins = 10

	max_value = max(list_to_hist)
	min_value = min(list_to_hist)
	value_range = (max_value-min_value)
	hist = []
	if fix_bin_size == False:
		W = (value_range*1.0)/(bins*1.0) 
		bin_edges = [min_value]
		#print 'Bin Width = %f' % W

	elif fix_bin_size == True and fix_bin_count == True:
		W = bin_size
		bins = bins
		bin_edges = [min_bin]
		min_value = min_bin
		#print 'Bin extent: %s to %s' %(min_bin, min_bin+W*bins)

	else:
		W = bin_size
		bins = int(floor(max_value-min_value)+1)
		bin_edges = [min_value]
		#print 'Bin Count: = %s' %bins

	bin_centers = []
	counts = []
	bin_floor = []
	for i in range(bins):
		counts.append(0.0)
		bin_centers.append(W*(i+0.5)+min_value)
		bin_edges.append(W*(i+1)+min_value)
		bin_floor.append(W*i+min_value)


	for i in list_to_hist:
	#Shifting so min_value is effectively zero
		I = i-min_value
		bin = int(floor(I/W))
		if bin > bins-1 :
			bin = bins-1
		elif bin < 0: #Modified 21 Feb 2015 to deal with low end skew
			bin = 0
		counts[bin] = counts[bin] +1
	if floor == False:
		return (bin_centers, counts)
	else:
		return (bin_floor, counts)


def initialize_bins(max_radius =10.0, bins = 40,fix_bin_size = False, bin_size = 1.0 ,verbose = False, initialize_count = False,minimum = 0.0):
	'''This function initializes a bin center and bin edges list. Returns width, bin center and edges, and is indicated, initializes a counts variable list as well.  Assumes no negative bins'''
	from math import floor
	min_bins = 2

	if fix_bin_size == False:
		if bins < min_bins:
			bins = min_bins
		W = ((max_radius-minimum)*1.0)/(bins*1.0)
	else:
		bins = int(floor((max_radius-minimum)/(bin_size*1.0)))
		if bins < min_bins:
			bins = min_bins
		W = ((max_radius-minimum)*1.0)/(bins*1.0) #need to have even bins for my sanity

	bin_centers = []
	counts = []
	pairs = 0.0
	bin_edges = [0.0]
	for i in range (bins):
		counts.append(0.0)
		bin_centers.append(minimum+W*(i+0.5))
		bin_edges.append(minimum+W*(i+1))

	if initialize_count == True:
		return bin_centers, bin_edges, W, bins, counts
	else:
		return bin_centers, bin_edges, W, bins

def initialize_slice_edges (max_slice_limits= [], slices=20, fix_slice_size = False, slice_thick = 1.0, verbose = False):
	'''Initializes a slice center and slice edges list with an arbitrary minimium position. Can also be used when you need bins of negative values.'''
	from math import floor
	if verbose:
		print max_slice_limits, fix_slice_size, slices

	if fix_slice_size == True: #This previously said false. This was exactly backwards and fucked things up. Sorry.
		slices = int(floor((max_slice_limits[1]-max_slice_limits[0])/(slice_thick*1.0)))
		slice_thick = slice_thick *1.0
	else:
		slice_thick = (max_slice_limits[1]-max_slice_limits[0])*1.0/(slices*1.0)

	slice_centers = []
	slice_edges = []#[max_slice_limits[0],slice_thick*(i)+max_slice_limits[0] ]

	for i in range(slices):
		slice_centers.append(slice_thick*(i+0.5)+max_slice_limits[0])
		slice_edges.append([slice_thick*(i)+max_slice_limits[0],slice_thick*(i+1)+max_slice_limits[0]])

	return slice_centers, slice_edges, slice_thick, slices
	



