#!/usr/bin/env python

def molecule_size_dist(system):
	'''Computes the distribution of molecule sizes from the molecule list.'''
	molecule_size_dist = []
	#Modified 7/25/12
	for i in range(0,max(system.molecule)+1):
		current_molecule = system.get_molecule(i)
		molecule_size_dist.append(current_molecule.number_of_atoms)	

	return molecule_size_dist

def molecule_weight_dist(system):
	'''Computes the mass of each molecule in the system, returns list and average. And by computes the mass, I mean uses the magic of things get_subset was already doing.'''
	molecule_weight_list = []

	for i in range(0,max(system.molecule)+1):
		current_molecule = system.get_molecule(i)
		molecule_weight_list.append(current_molecule.mass)

	return molecule_weight_list

def molecular_dispersity(system, exclude_list = []):
	'''Computes the IUPAC dispersity (d-stroke) [https://en.wikipedia.org/wiki/Dispersity] by calling molecule_weight_dist. Returns dispersity, weight average, number average and the complete list of weights. Averaging removes size zero molecules'''

	weights = molecule_weight_dist(system)

	molecule_count = 0
	number_average = 0.0
	weight_average = 0.0

	for w in range(len(weights)):
		if weights[w]!=0 and w not in exclude_list:
			molecule_count+=1
			number_average+=weights[w] 	#sum(N*M)/sum(N)
			weight_average+=weights[w]*weights[w]	#sum(N*M*M)/sum(N*M)

	weight_average = weight_average/number_average 
	number_average = number_average/molecule_count

	dispersity = weight_average/number_average

	return dispersity, number_average, weight_average, molecule_count


#####################
#Modified 2/25/2014 to force the center back into a periodic box
#UPDATE 3/25/15 to computer center of molecule_mass in internal coordinates
def center_of_molecule_mass(sys, molecule_id, internal = False, wrapped = False):
	'''Compute the center of mass for the given molecule ID. '''
	from kirke.manipulate.unwrap import unwrap_coordinates

	current_molecule = sys.get_molecule(molecule_id)

	mass_mol = []
	mass_x = []
	mass_y = []
	mass_z = []
	
	if wrapped == False:
		unwrap_coordinates(current_molecule,use_nx=False)
	else :
		pass
	
	if internal == True:
		for j in range(current_molecule.number_of_atoms):
			sx = current_molecule.sx[j]*current_molecule.masses[j]
			sy = current_molecule.sy[j]*current_molecule.masses[j]
			sz = current_molecule.sz[j]*current_molecule.masses[j]
			mass_x.append(sx)
			mass_y.append(sy)
			mass_z.append(sz)
			mass_mol.append(current_molecule.masses[j])
	else:
	

		for j in range(current_molecule.number_of_atoms):

			mx = current_molecule.x[j]*current_molecule.masses[j]
			my = current_molecule.y[j]*current_molecule.masses[j]
			mz = current_molecule.z[j]*current_molecule.masses[j]
			mass_x.append(mx)
			mass_y.append(my)
			mass_z.append(mz)
			mass_mol.append(current_molecule.masses[j])

	if mass_mol == []:
		center_of_molecule_mass = [0.0,0.0,0.0,0.0]

	else:

		x_center = sum(mass_x)/sum(mass_mol)
		y_center = sum(mass_y)/sum(mass_mol)
		z_center = sum(mass_z)/sum(mass_mol)

		if sys.periodic == []:
			sys.periodic = [True, True, True]

		if sys.box == []:
			sys.update_box_from_positions()

		#Forcing the COM into the box
#		if sys.box_mode == 'orthorhombic':
#			if x_center < sys.box[0][0] and sys.periodic[0]==True:
#				dx = sys.box[0][0]-x_center
#				x_center = sys.box[0][1]-dx
#			elif x_center > sys.box[0][1] and sys.periodic[0] == True:
#				dx = x_center - sys.box[0][1]
#				x_center = sys.box[0][0]+dx

#			if y_center < sys.box[1][0] and sys.periodic[1]==True:
#				dy = sys.box[1][0]-y_center
#				y_center = sys.box[1][1]-dy
#			elif y_center > sys.box[1][1] and sys.periodic[1] == True:
#				dy = y_center - sys.box[1][1]
#				y_center = sys.box[1][0]+dy

#			if z_center < sys.box[2][0] and sys.periodic[2]==True:
#				dz = sys.box[2][0]-z_center
#				z_center = sys.box[2][1]-dz
#			elif z_center > sys.box[2][1] and sys.periodic[2] == True:
#				dz = z_center - sys.box[2][1]
#				z_center = sys.box[2][0]+dz

		center_of_molecule_mass = [x_center, y_center, z_center, sum(mass_mol)]
		
	return center_of_molecule_mass

def radius_of_gyration(system, molecule_id):
	'''Computes radius of gyration from the atoms belongs to the given molecule.'''
	from math import sqrt
	from kirke.manipulate.unwrap import unwrap_coordinates
	from numpy import mean

	radius_of_gyration = []
	center_of_mass = center_of_molecule_mass(system, molecule_id)
	current_molecule = system.get_molecule(molecule_id)

	# Automatically unwraps coordinates, even if n lists are empty
	current_molecule = unwrap_coordinates(current_molecule,use_nx=False)

	if current_molecule.number_of_atoms < 2:
		radius_of_gyration = 0.0
	else:
		mean_x = mean(current_molecule.x)
		mean_y = mean(current_molecule.y)
		mean_z = mean(current_molecule.z)
		radius_sq = []

		for i in range(current_molecule.number_of_atoms):
			rsq = (current_molecule.x[i]-mean_x)**2+(current_molecule.y[i]-mean_y)**2+(current_molecule.z[i]-mean_z)**2
			radius_sq.append(rsq)
		radius_of_gyration = sqrt(sum(radius_sq)/(len(current_molecule.x)-1))

	return radius_of_gyration, center_of_mass
	
#####################
def molecule_axes(sys, mol_id):
	'''Finding the size of molecule in the box axes. A future version might do the principle component axes'''

	from math import sqrt
	from kirke.manipulate.unwrap import unwrap_coordinates

	current_molecule = system.get_molecule(molecule_id)
	current_molecule = unwrap_coordinates(current_molecule,use_nx=False)

	molecule_dimensions = [[],[],[]]

	if current_molecule.number_of_atoms < 2:
		molecule_dimensions = [0,0,0]

	else:
		mol_lx = max(current_molecule.x)-min(current_molecule.x)
		mol_ly = max(current_molecule.y)-min(current_molecule.y)
		mol_lz = max(current_molecule.z)-min(current_molecule.z)

	molecule_dimensions = [mol_lx, mol_ly, mol_lz]

	return molecule_dimensions

#################
def pca_of_molecule(sys, mol_id):
	'''Uses the mlab.PCA() class to compute the principle components of atoms belonging to the given molecule id. Flatness is computed as the total standard deviation in all three component directions.'''
#Modified 2 August 2012 to use PCA class instead of prepca
#Modified 16 August to unwrap coordinates

	from matplotlib import mlab
	from numpy import array, dot, empty
	from math import sqrt

	avgcoord = [0,0,0]
	current_molecule = sys.get_molecule(mol_id)
	N = current_molecule.number_of_atoms
	if N > 3:
		coord = empty([len(current_molecule.id),3])
		if sys.nx == []:
			for i in range(len(current_molecule.id)):
				coord[i][0]=current_molecule.x[i]
				coord[i][1]=current_molecule.y[i]
				coord[i][2]=current_molecule.z[i]
				if i>0 :
					#Correcting periodic distances according the previous atoms positions
					if periodic[0]:
						dx = coord[i][0]-coord[i-1][0]
						coord[i][0] = coord[i][0]+periodic_delta(dx,sys.lx)
					if periodic[1]:
						dy = coord[i][1]-coord[i-1][1]
						coord[i][1] = coord[i][1]+periodic_delta(dy,sys.ly)
					if periodic[2]:
						dz = coord[i][2]-coord[i-1][2]
						coord[i][2] = coord[i][2]+periodic_delta(dz,sys.lz)
		else:
			for i in range(len(current_molecule.id)):
				coord[i][0]=current_molecule.x[i]+current_molecule.nx[i]*sys.lx
				coord[i][1]=current_molecule.y[i]+current_molecule.ny[i]*sys.ly
				coord[i][2]=current_molecule.z[i]+current_molecule.nz[i]*sys.lz

		pcacomp = mlab.PCA(coord)
		pcaCoeff = pcacomp.Wt.T
		pcaScore = pcacomp.Y[0:2][:].T
		plane_normal = pcaCoeff[2]
		#Flatness is definined as the combined standard deviation
		N = current_molecule.number_of_atoms
		s = pcacomp.sigma
		m = pcacomp.mu
		f = pcacomp.fracs
		netm = ((N*m[0])+(N*m[1])+(N*m[2]))/(3.0*N)

		#Assuming m in all directions is 0 (i.e., shifted center, as it should be)
		nets = sqrt((3*N*((s[0]**2)+(s[1]**2)+(s[2]**2))/(3*N)))

		flatness = nets
		return (plane_normal,flatness, pcaCoeff, s,f)

#################
def pca_of_molecule_temp(sys, mol_id):
	'''Uses the mlab.PCA() class to compute the principle components of atoms belonging to the given molecule id. Flatness is computed as the total standard deviation in all three component directions.'''
#Modified 2 August 2012 to use PCA class instead of prepca
#Modified 16 August to unwrap coordinates

	from matplotlib import mlab
	from numpy import array, empty
	from math import sqrt
	from kirke.manipulate import rotate_coordinates, center_coordinates, unwrap_coordinates
	from kirke.write import write_XYZ

	current_molecule = sys.get_molecule(mol_id)

	unwrap_coordinates(current_molecule)
	coord = empty([len(current_molecule.id),3])
	for a in range(len(current_molecule.x)):
		
		coord[a]=[current_molecule.x[a],current_molecule.y[a],current_molecule.z[a]]

	avgcoord = [0,0,0]

	pcacomp = mlab.PCA(coord)
	pcaCoeff = pcacomp.Wt.T
	pcaScore = pcacomp.Y[0:2][:].T
	plane_normal = pcaCoeff[2]
	#Testing new flatness: rotate coordinates to new plane system, compute average distance to the xy plane (i.e., sqrt(z^2)
	rot_mol = rotate_coordinates(current_molecule,pcacomp.Wt)
	
	center_coordinates(current_molecule)
	zflat = pcacomp.sigma[0] 
	#zflat = 0;
	#for j in rot_mol.z: 
	#	zflat += j**2
	#zflat = sqrt(zflat/len(rot_mol.z))

	return (plane_normal,zflat)

#################
