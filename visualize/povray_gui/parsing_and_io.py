
class camera:
	def __init__(self):
		from numpy import array
		self.aspect_ratio = 1.33 # just putting in some functional defaults
		self.projection = 'perspective'

		self.right = array([self.aspect_ratio, 0.0, 0.0])
		self.up = array([0.0, 1.0, 0.0]) # this vector should always be a unit vector

		self.look_point =  array([0.0, 0.0, 0.0])
		self.camera_point =  array([0.0, 0.0, -1.0])

		self.look = array([0.0, 0.0, 0.0])
		self.update_look() # so that it looks the right way
		self.angle = 67.0

	def update_look(self):
		self.look = self.look_point - self.camera_point

	def array_to_string(self,an_array):
		output_string = '<'
		for i in range(len(an_array)-1): #last item doesnt need comma
			output_string = output_string + '%f, ' % an_array[i]

		output_string = output_string + '%f>' % an_array[len(an_array)-1] #last item needs carrot
		return output_string


	def format_for_POV(self):
		output = ['camera\n{\n']
		output.append(' %s\n' % self.projection)
		output.append(' angle %f\n' % self.angle)
		output.append(' location %s\n' % self.array_to_string(self.camera_point))
		output.append(' look_at %s\n' %  self.array_to_string(self.look_point))
		output.append(' up %s\n' %       self.array_to_string(self.up))
		output.append(' sky %s\n' %      self.array_to_string(self.up))
		output.append(' right %s\n' %    self.array_to_string(self.right))
		output.append('}\n')
		return output

	def print_camera_info(self):
		print 'Your camera is now:'
		lines = self.format_for_POV()
		for thing in lines:
			print thing,
		from camera_transformations import check_handedness, check_orthogonality
		print 'Which is %s handed with and orthogonality of %f.'% (check_handedness(self), check_orthogonality(self))

#{
# orthographic angle 30
# location <14.791309, 21.608848, -29.900679>
#  look_at <14.791309, 21.608848, 5.980136>
#  up <-0.330108, 0.943943, -0.000000>
#  right <0.943943, 0.330108, -0.000000>
#}


def write_and_render(file_name ,camera_lines, pov_lines, height, width, quality = 4):
	pov_name = file_name+'.pov'
	img_name = file_name+'.png'

	fid = open(pov_name,'w')
	for line in camera_lines:
		fid.write(line)
	for line in pov_lines:
		fid.write(line)
	fid.close()
	from os import system
	command = 'povray Input_File_Name=%s Output_File_Name=%s +H%i +W%i +Q%i Display=off' % (pov_name, img_name, height, width, quality) 
	system(command)

	# flags from avogadro  +V +D +FN +Q9 +P +UD +UL +UV +A +AM2 +UA
	#
	#+V or Verbose=bool	Output verbose status messages on the progress 
	#			of the rendering.
	#+D[0][GHT] or Display=bool  Palette=char	Display the rendering in
	#			progress, optionally specifying the palette...
	#+FN			forces the format to P*N*G, replacing the N 
	#			changes the format.
	#+Qn or Quality=integer	Render at quality n. Qualities range from 0 for 
	#			rough images and 9 for complete ray-tracing and 
	#			textures, and 10 and 11 add radiosity.
	#+P or Pause_When_Done=bool	If previewing, pause when the rendering 
	#			is complete before closing the window.
	#+UD or Draw_Vistas=bool	Draw vista rectangles before rendering 
	#			has been deprecated.
	#UL or Light_Buffer=bool	Use light buffer to speed up rendering 
	#			has been deprecated.
	#UV or Vista_Buffer=bool	Use vista buffer to speed up rendering 
	#			has been deprecated.
	#+A0.n or Antialias=bool Antialias_Threshold=integer	Do  antialiasing
	#			on the pixels until the difference between 
	#			adjacent pixels is less that 0.n, or the maximum
	#			recursion depth is reached.
	#+AMn or Sampling_Method=integer	Specify the method of 
	#			antialiasing used, non-adaptive  (n  =  1),  or 
	#			adaptive antialiasing (n = 2).
	#+UA or Output_Alpha=bool	Use alpha channel for transparency mask.


def parse_file(file_name):

	fid = open(file_name,'r')
	lines = fid.readlines()
	line_num = 0
	while 'camera' not in lines[line_num]: 
		line_num += 1
	###########
	cam_start = line_num
	

	
	while '}' not in lines[line_num]: 
		line_num += 1

	cam_end = line_num

	cam_lines = lines[cam_start:cam_end+1]
	#print cam_lines

	my_cam = camera()

	for line in cam_lines:
		if 'perspective' in line:
			my_cam.projection = 'perspective'
		if 'angle' in line:
			sline = line.split()
			my_cam.angle = float(sline[1]) # terribly inflexible but will work for now 
		if 'orthographic' in line:
			sline = line.split()
			i = 0

			for thing in sline:
				if  'orthographic' == thing:
					start = i
				i+=1
			my_cam.projection = sline[start] + ' '+ sline[start+1]+' '+sline[start+2]
	
		if 'right' in line:
			sline = line.split()
			i = 0

			for thing in sline:
				if  'right' == thing:
					start = i
				i+=1
			#print sline[start]
			x = float(sline[start+1].split('<')[1].split(',')[0])
			y = float(sline[start+2].split(',')[0])
			z = float(sline[start+3].split('>')[0])
			#print x
			#print y
			#print z
			my_cam.right[0] = x
			my_cam.right[1] = y
			my_cam.right[2] = z

		if 'look_at' in line:
			sline = line.split()
			i = 0

			for thing in sline:
				if  'look_at' == thing:
					start = i
				i+=1
			#print sline[start]
			x = float(sline[start+1].split('<')[1].split(',')[0])
			y = float(sline[start+2].split(',')[0])
			z = float(sline[start+3].split('>')[0])
			#print x
			#print y
			#print z
			my_cam.look_point[0] = x
			my_cam.look_point[1] = y
			my_cam.look_point[2] = z


		if 'location' in line:
			sline = line.split()
			i = 0

			for thing in sline:
				if  'location' == thing:
					start = i
				i+=1
			#print sline[start]
			x = float(sline[start+1].split('<')[1].split(',')[0])
			y = float(sline[start+2].split(',')[0])
			z = float(sline[start+3].split('>')[0])
			#print x
			#print y
			#print z
			my_cam.camera_point[0] = x
			my_cam.camera_point[1] = y
			my_cam.camera_point[2] = z

		if 'up' in line:
			sline = line.split()
			i = 0

			for thing in sline:
				if  'up' == thing:
					start = i
				i+=1
			#print sline[start]
			x = float(sline[start+1].split('<')[1].split(',')[0])
			y = float(sline[start+2].split(',')[0])
			z = float(sline[start+3].split('>')[0])
			#print x
			#print y
			#print z
			my_cam.up[0] = x
			my_cam.up[1] = y
			my_cam.up[2] = z
	from camera_transformations import mag, unit_vector

	my_cam.aspect_ratio = mag(my_cam.right)/mag(my_cam.up)
	my_cam.up = unit_vector(my_cam.up)
	my_cam.right = unit_vector(my_cam.right) * my_cam.aspect_ratio
	my_cam.update_look()


	pov_lines = [] 
	for line in lines[0:cam_start]:
		pov_lines.append(line)
	for line in lines[cam_end+1: len(lines)]:
		pov_lines.append(line)

	return my_cam, pov_lines
	#return file_camera, file_lines

