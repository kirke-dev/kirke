
def povray_gui(file_name, temporary_file_name = 'temporary', split_screen = False ,image_height = 512):

	###################
	button_space_height = 110 #px

	from parsing_and_io import camera, write_and_render, parse_file
	my_cam, pov_lines = parse_file(file_name)

	if split_screen:
		screen_width = 1920
		screen_height = 1000
		eye_angle = 1.5
		image_height = screen_height - button_space_height
		image_width = screen_width/2
	else:
		#image_height = 512 #now default option 
		image_width = int(image_height * my_cam.aspect_ratio)
################################################################


	from Tkinter import Tk, Frame, Button, BOTH, Label

	from PIL import Image
	from PIL import ImageTk


	root = Tk()
	#root.geometry("250x150+300+300")
	root.title("Kirke Face")


	## testing the camera for sanity
	from camera_transformations import check_handedness, check_orthogonality, repair_orthogonality

	print 'Your camera is %s handed with an orthogonality of %f'  % (check_handedness(my_cam), check_orthogonality(my_cam))
	if check_orthogonality(my_cam) < 0.95:
		#input("\nYour camera orthogonality is poor, press ENTER to attempt repair:\n")
		print "\nYour camera orthogonality is poor, attempting repair.\n"
		repair_orthogonality(my_cam)
	####


	if split_screen == False:
		frame_center = Frame(root)
		frame_center.pack(side='top',fill = "both", expand = "yes")

		########### first image
		write_and_render(file_name = temporary_file_name,
			camera_lines = my_cam.format_for_POV(),
			pov_lines = pov_lines,
			height = image_height,
			width = image_width )
		my_cam.print_camera_info()

		rendering_center = ImageTk.PhotoImage(Image.open(temporary_file_name+".png"))
		image_box_center = Label(frame_center, image = rendering_center)
		image_box_center.pack(side = "top", fill = "both", expand = "yes")
	


	else:
		########### first image
		from camera_transformations import create_stereo_pair
		my_left, my_right = create_stereo_pair(my_cam, eye_angle = eye_angle)
		#left
		write_and_render(file_name = temporary_file_name+'_left',
			camera_lines = my_left.format_for_POV(),
			pov_lines = pov_lines,
			height = image_height,
			width = image_width )
		#right
		write_and_render(file_name = temporary_file_name+'_right',
			camera_lines = my_right.format_for_POV(),
			pov_lines = pov_lines,
			height = image_height,
			width = image_width )
		print 'Your camera is %s handed with an orthogonality of %f'  % (check_handedness(my_cam), check_orthogonality(my_cam))

		#left
		frame_left = Frame(root)
		frame_left.pack(side='left',fill = "both", expand = "yes")
		rendering_left = ImageTk.PhotoImage(Image.open(temporary_file_name + "_left.png"))
		image_box_left = Label(frame_left, image = rendering_left)
		image_box_left.pack(side = "top", fill = "both", expand = "yes")
		#right
		frame_right = Frame(root)
		frame_right.pack(side='right',fill = "both", expand = "yes")
		rendering_right = ImageTk.PhotoImage(Image.open(temporary_file_name + "_right.png"))
		image_box_right = Label(frame_right, image = rendering_right)
		image_box_right.pack(side = "top", fill = "both", expand = "yes")
		# both info
		print 'Left Camera:'
		my_left.print_camera_info()
		print 'Right Camera:'
		my_right.print_camera_info()
		print 'Center Camera:'
		my_cam.print_camera_info()



###################### defining the function the renders and updates on button click
	if split_screen == False:
		def render_for_button():
			from time import time
			t0 = time()
			from parsing_and_io import write_and_render
			write_and_render(file_name = temporary_file_name ,
				camera_lines = my_cam.format_for_POV(),
				pov_lines = pov_lines,
				height = image_height,
				width = image_width )
			rendering_new = ImageTk.PhotoImage(Image.open(temporary_file_name + ".png"))
			image_box_center.configure(image = rendering_new)
			image_box_center.image = rendering_new
			#display infor
			my_cam.print_camera_info()
			print 'Refresh Time (sec): %f'%(time()-t0)
	else:
		def render_for_button():
			from time import time
			t0 = time()
			from parsing_and_io import write_and_render
			my_left, my_right = create_stereo_pair(my_cam, eye_angle = eye_angle)
			#left
			write_and_render(file_name = temporary_file_name + '_left',
				camera_lines = my_left.format_for_POV(),
				pov_lines = pov_lines,
				height = image_height,
				width = image_width )
			rendering_new = ImageTk.PhotoImage(Image.open(temporary_file_name + "_left.png"))
			image_box_left.configure(image = rendering_new)
			image_box_left.image = rendering_new
			#right
			write_and_render(file_name = temporary_file_name + '_right',
				camera_lines = my_right.format_for_POV(),
				pov_lines = pov_lines,
				height = image_height,
				width = image_width )
			rendering_new = ImageTk.PhotoImage(Image.open(temporary_file_name + "_right.png"))
			image_box_right.configure(image = rendering_new)
			image_box_right.image = rendering_new
			# both info
			print 'Left Camera:'
			my_left.print_camera_info()
			print 'Right Camera:'
			my_right.print_camera_info()
			print 'Center Camera:'
			my_cam.print_camera_info()
			print 'Refresh Time (sec): %f'%(time()-t0)
#############################################


	def create_button_box(parent_frame):
	
		button_box = Frame(parent_frame, height=button_space_height, width=image_width)
		button_box.pack_propagate(0) # don't shrink
		button_box.pack()


		from camera_transformations import tilt_up_down, tilt_right_left, rotate, zoom_in_out
		from camera_transformations import translate_in_out, translate_right_left, translate_up_down

		h = 1.0/5.0
		w = 1.0/8.0
		############### TILT ############

		tilt_label = Label(button_box,text= 'Tilt' )
		tilt_label.pack()
		tilt_label.place(rely = 0*h, relx = 0*w, relheight = h, relwidth = 3*w)

		##############
		def tilt_up_for_button():
			tilt_up_down(my_cam,angle = 5) 
			render_for_button()

		tilt_up_button = Button(button_box, text="Up",command=tilt_up_for_button)
		tilt_up_button.pack()
		tilt_up_button.place(rely = h, relx = w, relheight = h, relwidth = w)


		#########
		def tilt_down_for_button():
			tilt_up_down(my_cam,angle = -5) 
			render_for_button()

		tilt_down_button = Button(button_box, text="Down",command=tilt_down_for_button)
		tilt_down_button.pack()
		tilt_down_button.place(rely = 4*h, relx = w, relheight = h, relwidth = w)

		#########

		def tilt_right_for_button():
			tilt_right_left(my_cam, angle = 5)
			render_for_button()

		tilt_right_button = Button(button_box, text="Right",command=tilt_right_for_button)
		tilt_right_button.pack()
		tilt_right_button.place(rely = 2*h, relx = 2*w, relheight = 2*h, relwidth = w)

		########
		def tilt_left_for_button():
			tilt_right_left(my_cam, angle = -5)
			render_for_button()

		tilt_left_button = Button(button_box, text="Left",command=tilt_left_for_button)
		tilt_left_button.pack()
		tilt_left_button.place(rely = 2*h, relx = 0*w, relheight = 2*h, relwidth = w)




		##################### Rotate ########

		rotate_label = Label(button_box,text= 'Rotate' ) # povray is fucking this up.
		rotate_label.pack()
		rotate_label.place(rely = 3*h, relx = 3*w, relheight = h, relwidth = 2*w)
		#########
		def rotate_clockwise_for_button():
			rotate(my_cam, angle = -5.0)
			render_for_button()


		rotate_clockwise_button = Button(button_box, text="+",command=rotate_clockwise_for_button)
		rotate_clockwise_button.pack()
		rotate_clockwise_button.place(rely = 4*h, relx = 4*w, relheight = h, relwidth = w)


		#########
		def rotate_anticlockwise_for_button():
			rotate(my_cam, angle = 5.0)
			render_for_button()

		rotate_anticlockwise_button = Button(button_box, text="-", command=rotate_anticlockwise_for_button)
		rotate_anticlockwise_button.pack()
		rotate_anticlockwise_button.place(rely = 4*h, relx = 3*w, relheight = h, relwidth = w)



		##################### Zoom ########

		zoom_label = Label(button_box,text= 'Zoom' )
		zoom_label.pack()
		zoom_label.place(rely = 0*h, relx = 3*w, relheight = h, relwidth = 2*w)

		######

		def zoom_in_for_button():
			zoom_in_out(my_cam, factor = -0.1)
			render_for_button()

		zoom_in_button = Button(button_box, text="+",command=zoom_in_for_button)
		zoom_in_button.pack()
		zoom_in_button.place(rely = h, relx = 4*w, relheight = h, relwidth = w)

		######

		def zoom_out_for_button():
			zoom_in_out(my_cam, factor = +0.1)
			render_for_button()

		zoom_out_button = Button(button_box, text="-",command=zoom_out_for_button)
		zoom_out_button.pack()
		zoom_out_button.place(rely = h, relx = 3*w, relheight = h, relwidth = w)




		########## Translate #############

		translate_label = Label(button_box,text= 'Translate' )
		translate_label.pack()
		translate_label.place(rely = 0*h, relx = 5*w, relheight = h, relwidth = 3*w)


		######
		def translate_in_for_button():
			translate_in_out(my_cam, factor = 0.1)
			render_for_button()

		translate_in_button = Button(button_box, text="In",command = translate_in_for_button)
		translate_in_button.pack()
		translate_in_button.place(rely = 2*h, relx = 6*w, relheight = h, relwidth = w)

		######
		def translate_out_for_button():
			translate_in_out(my_cam, factor = -0.1)
			render_for_button()

		translate_out_button = Button(button_box, text="Out",command = translate_out_for_button)
		translate_out_button.pack()
		translate_out_button.place(rely = 3*h, relx = 6*w, relheight = h, relwidth = w)



		######
		def translate_right_for_button():
			translate_right_left(my_cam, factor = 0.1)
			render_for_button()

		translate_right_button = Button(button_box, text="right",command = translate_right_for_button)
		translate_right_button.pack()
		translate_right_button.place(rely = 2*h, relx = 7*w, relheight = 2*h, relwidth = w)



		######
		def translate_left_for_button():
			translate_right_left(my_cam, factor = -0.1)
			render_for_button()

		translate_left_button = Button(button_box, text="left",command = translate_left_for_button)
		translate_left_button.pack()
		translate_left_button.place(rely = 2*h, relx = 5*w, relheight = 2*h, relwidth = w)


		######
		def translate_up_for_button():
			translate_up_down(my_cam, factor = 0.1)
			render_for_button()

		translate_up_button = Button(button_box, text="Up",command = translate_up_for_button)
		translate_up_button.pack()
		translate_up_button.place(rely = 1*h, relx = 6*w, relheight = 1*h, relwidth = w)


		######
		def translate_down_for_button():
			translate_up_down(my_cam, factor = -0.1)
			render_for_button()

		translate_down_button = Button(button_box, text="Down",command = translate_down_for_button)
		translate_down_button.pack()
		translate_down_button.place(rely = 4*h, relx = 6*w, relheight = 1*h, relwidth = w)

	if split_screen == False:
		create_button_box(frame_center)
	else:
		create_button_box(frame_left)
		create_button_box(frame_right)

	##################
	root.mainloop()  



if __name__ == '__main__':
	from sys import argv
	if len(argv) > 1:
		fname = argv[1]
	else:
		print "NO FILE GIVEN!"

	if len(argv) > 2:
		if argv[2] == '3D' or argv[2] == '3d':
			split_screen = True
	else:
		split_screen = False

	povray_gui(file_name = fname, split_screen = split_screen)


