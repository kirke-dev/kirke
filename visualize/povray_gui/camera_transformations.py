

def mag(vector):
	from numpy import dot, sqrt
	return sqrt(dot(vector,vector))

def unit_vector(vector):
	return vector/mag(vector)
##################

def tilt_up_down(camera, angle = 5):#up is positive angle
	from numpy import sin, cos, pi
	theta = angle*pi/180.0

	new_up   = camera.up*cos(theta) + unit_vector(camera.look)*sin(theta)
	new_look = camera.look*cos(theta) - camera.up*sin(theta)*mag(camera.look)
	new_camera_point = camera.look_point - new_look
	
	camera.up = new_up
	camera.camera_point = new_camera_point
	camera.update_look()

def tilt_right_left(camera, angle = 5):# tilt to the right is positive
	from numpy import sin, cos, pi
	sgn = 1.0
	if check_handedness(camera) == 'right': #swaps sign for right handed cameras so the buttons still make sense
		sgn = -1.0
	theta = sgn*angle*pi/180.0


	new_right = camera.right*cos(theta) + mag(camera.right)*unit_vector(camera.look)*sin(theta)
	new_look  = camera.look*cos(theta) - mag(camera.look)*unit_vector(camera.right)*sin(theta)
	new_camera_point = camera.look_point - new_look
	
	camera.right = new_right
	camera.camera_point = new_camera_point
	camera.update_look()

###################
def rotate(camera, angle = 5): #positive angles rotate the camera counter clockwise relative to its current oreiantation
	from numpy import sin, cos, pi
	theta = angle*pi/180.0

	new_up   = camera.up*cos(theta) - unit_vector(camera.right)*sin(theta)
	new_right = ( camera.up*sin(theta) + unit_vector(camera.right)*cos(theta) ) * mag(camera.right) 

	camera.up = new_up
	camera.right = new_right


#######################
def translate_up_down(camera, factor = 0.1): # up is the positive
	displacement = factor*mag(camera.look)*unit_vector(camera.up)

	camera.look_point = camera.look_point     + displacement
	camera.camera_point = camera.camera_point + displacement 

def translate_right_left(camera, factor = 0.1): # right is the positive
	sgn = 1.0	
	if check_handedness(camera) == 'right':
		sgn = -1.0
	displacement = sgn*factor*mag(camera.look)*unit_vector(camera.right)

	camera.look_point = camera.look_point     + displacement
	camera.camera_point = camera.camera_point + displacement 


def translate_in_out(camera, factor = 0.1): # in is the positive
	displacement = factor*mag(camera.look)*unit_vector(camera.look)

	camera.look_point = camera.look_point     + displacement
	camera.camera_point = camera.camera_point + displacement 

#################

def zoom_in_out(camera, factor = 0.1 ): # positive zooms out
	if factor >= 0.0:  # these lines make sure that a zoom of +10% is countered perfectly with a zoom of -10%
		f = 1.0 + factor
	else:
		f = 1.0/(1.0-factor)

	new_look = camera.look*f
	new_camera_point = camera.look_point - new_look

	camera.camera_point = new_camera_point
	camera.update_look()


############ 
def check_handedness(camera):
	L = camera.look
	U = camera.up
	R = camera.right
	from numpy import cross, dot
	test = dot(cross(L,U), R)
	if test < 0.0:
		return 'right'
	elif test > 0.0:
		return 'left'
	else:
		print "You've messed up badly, your camera is unphysical!"





def check_orthogonality(camera): # the closer to 1 the better
	from numpy import cross, dot
	L = unit_vector(camera.look)
	U = unit_vector(camera.up)
	R = unit_vector(camera.right)
	test = abs(dot(cross(L,U), R))
	return test


def repair_orthogonality(camera): # the closer to 1 the better
	handed = check_handedness(camera)
	from numpy import cross, dot
	L = unit_vector(camera.look)
	U = unit_vector(camera.up)
	R = unit_vector(camera.right)
	camera.up = unit_vector( U - dot(L, U) * L ) #assumes camera is a unit vector from reading in, uses projection
	new_right = unit_vector(cross(L,camera.up)) * camera.aspect_ratio
	if handed == 'left':
		new_right = -new_right
	camera.right = new_right


def create_stereo_pair(camera, eye_angle = 1.5):
	from copy import deepcopy
	camera_left  = deepcopy(camera)
	camera_right = deepcopy(camera)

	tilt_right_left(camera_left, angle = -eye_angle/2.0)
	tilt_right_left(camera_right, angle = eye_angle/2.0)
	#becuase horizontal interlcing
	camera_left.aspect_ratio = camera_left.aspect_ratio*2.0 # twice as far in same image size
	camera_right.aspect_ratio = camera_right.aspect_ratio*2.0

	camera_left.right = camera_left.right*2.0
	camera_right.right = camera_right.right*2.0
	
	return camera_left, camera_right

