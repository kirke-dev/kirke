#!/usr/bin/env python


######## End K. Sebeck

def fill_q_list(self,q = 0.0): # name change for consistancy by mike on 3/28/2012
	'''Fills the q attribute with given value. Default vale is 0.0'''
	new_q_list = []
	for i in range(self.number_of_atoms):
		new_q_list.append(q)
	self.q = new_q_list

def fill_molecule_list(self,molecule = 0):
	'''Fills the molecule attribute with the given value. Default is 0.'''
	new_m_list = []
	for i in range(self.number_of_atoms):
		new_m_list.append(molecule)
	self.molecule = new_m_list

def fill_v_lists(self,v = 0.0):
	'''Fills the v attribute with the given value. Default value is 0.0'''
	new_v_list = []
	for i in range(self.number_of_atoms):
		new_v_list.append(v)
	from copy import copy 
	self.vx = new_v_list
	self.vy =  copy(new_v_list)
	self.vz =  copy(new_v_list)

def fill_s_lists(self,s = 0.0): # mike 9-8-2012
	'''Fills the internal coordinate attributes with the given value. Default value is 0.0'''
	new_s_list = []
	for i in range(self.number_of_atoms):
		new_s_list.append(s)
	from copy import copy 
	self.sx =  new_s_list
	self.sy =  copy(new_s_list)
	self.sz =  copy(new_s_list)

def fill_position_lists(self,x = 0.0, y = 0.0, z = 0.0): # mike 11-13-2012
	'''Fills the external coordinate attributes with the given value. Default value is 0.0'''
	new_x_list = []
	new_y_list = []
	new_z_list = []
	for i in range(self.number_of_atoms):
		new_x_list.append(x)
		new_y_list.append(y)
		new_z_list.append(z)
	self.x =  new_x_list
	self.y =  new_y_list
	self.z =  new_z_list

def fill_n_lists(self,n = 0): # mike 9-8-2012
	'''Fills the periodic image list with the given value. Default value is 0.'''
	new_n_list = []
	for i in range(self.number_of_atoms):
		new_n_list.append(n)
	from copy import copy 
	self.nx =  new_n_list
	self.ny =  copy(new_n_list)
	self.nz =  copy(new_n_list)

def fill_masses_list(self,masses = 0): # mike 9-8-2012
	'''Fills the atom masses list with the given value. Default value is 0.'''
	new_masses_list = []
	for i in range(self.number_of_atoms):
		new_masses_list.append(masses)
	self.masses = new_masses_list

def fill_id_list(self, start = 1):
	'''Fills the id list starting from the given value. Default value is 1.'''
	new_id_list = []
	for i in range( self.number_of_atoms):
		new_id_list.append(i+start)
	self.id = new_id_list

def fill_selective_dynamics_list(self, selective_dynamic = [True,True,True]):
	new_sd_list = []
	for i in range( self.number_of_atoms):
		new_sd_list.append(selective_dynamic)
	self.selective_dynamics = new_sd_list
