#!/usr/bin/env python

def update_element_list(self):
	'''Updates element from atomic_number by referencing element_symbol_list'''
	from kirke.elements import element_symbol_list
	new_element_list = []		
	for a in self.atomic_number:
		new_element_list.append(element_symbol_list[a])
	self.element = new_element_list

def update_atomic_number_list(self):
	'''Updates atomic_number from element by referencing element_symbol_list.Non-real elements are assigned 0'''
	from kirke.elements import element_symbol_list
	new_atomic_number_list = []
	for a in self.element:
		if a in element_symbol_list:
			new_atomic_number_list.append(element_symbol_list.index(a))
		else:
			new_atomic_number_list.append(0)
	self.atomic_number = new_atomic_number_list
#####
def update_masses_from_element(self): #Katie 18 March 2013
	'''Fills the masses list from element_mass_list. Autocalls update_element_list if empty, but prints a warning.'''
	from kirke.elements import element_mass_list, element_symbol_list
	new_masses_list = [] 
	if self.element == []:
		self.update_element_list()
		print 'update_masses_from_element: WARNING Elements were updated from atomic_number by referencing element_symbol_list. Please check that this is correct for your system.'
	for a in self.element:
		new_masses_list.append(element_mass_list[element_symbol_list.index(a)])
	self.masses = new_masses_list

def update_type_masses(self): #Katie 18 March 2013
	'''Fills the type_masses list from masses, uses 0.0 for unused types'''
	if self.masses == []:
		self.update_masses_from_element()
	if self.atomic_number ==[]:
		self.update_atomic_number_list()
	new_type_masses_list = []
	unique_atomic_number_list = list(set(self.atomic_number))
	Natom_types = max(unique_atomic_number_list)

	for atomic_number in range(1,Natom_types):#
		for a in range(len(self.atomic_number)):
			type_mass = 0.0
			if self.atomic_number[a] == atomic_number:
				type_mass = self.masses[a]
				break
		new_type_masses_list.append(type_mass)

	type_masses = new_type_masses_list
#####
def convert_number_to_atomic_number(self,translation_dict):
	'''Uses the given translation dictionary to update atomic_number. If you get errors from this function, please check the formating of your dictionary first.'''
	for i in range(len(self.atomic_number)):
		self.atomic_number[i] = translation_dict[ self.atomic_number[i] ]
	self.update_element_list()

def convert_atomic_number_to_element(self,translation_dict):
	'''Uses the given translation dictionary to update element. If you get errors from this function, please check the formating of your dictionary first.'''
	self.element = []
	for i in range(len(self.atomic_number)):
		self.element.append( translation_dict[self.atomic_number[i] ])
	self.update_atomic_number_list()

def convert_number_to_element(self,translation_dict):
	'''Uses the given translation dictionary to update element. If you get errors from this function, please check the formating of your dictionary first.This version does not call update_atomic_number_list. In the case of LAMMPS, this preserves the type assignments'''
	self.element = []
	for i in range(len(self.atomic_number)):
		self.element.append( translation_dict[self.atomic_number[i] ])

def convert_element_to_number(self, translation_dict): #New 12 Feb 2013 to allow write_LAMMPS to work with defaulting to atom_type
	'''Uses the given translation dictionary to update atomic_number from element. If you get errors from this function, please check the formatting of your dictionary first.'''
	for i in range(len(self.element)):
		self.atomic_number[i] = translation_dict[self.element[i]]

def update_box_from_positions(self,padding = [0.0,0.0,0.0]):
	'''Updates box, lx, ly, and lz with a given amount of padding around the max and min system position values. Default padding is [0.0, 0.0, 0.0]'''
	if self.box_mode == 'orthorhombic':
		self.box[0] = [min(self.x),max(self.x) + padding[0]]
		self.box[1] = [min(self.y),max(self.y) + padding[1]]
		self.box[2] = [min(self.z),max(self.z) + padding[2]]
		self.lx = self.box[0][1] - self.box[0][0]
		self.ly = self.box[1][1] - self.box[1][0]
		self.lz = self.box[2][1] - self.box[2][0]
	else:
		print 'Warning, update_box_from_positions is not defined for non-orthorhombic systems.'

def update_lengths_from_box(self): # added 3/27/2012 by mike
	''' Updates lx, ly, and lz from box.'''
	if self.box_mode == 'orthorhombic':
		self.lx = self.box[0][1] - self.box[0][0]
		self.ly = self.box[1][1] - self.box[1][0]
		self.lz = self.box[2][1] - self.box[2][0]
	else:
		print 'Warning, update_lengths_from_box is not and cannot be defined for non-orthorhombic systems.'


######


def update_basis_and_origin_for_orthorhombic(self):
	'''Creates the basis matrix and origin for orthorhombic boxes.'''
	if self.box_mode != 'orthorhombic':
		print "update_basis_and_origin_for_orthorhombic: ERROR Your box_mode is not orthorhombic, you shouldn't do this."
	h = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	omega = [0.0,0.0,0.0]
	for i in range(3):
		h[i][i] = self.box[i][1] - self.box[i][0]
		omega[i] = self.box[i][0]
	self.basis = h
	self.origin = omega
	

def update_box_mode_to_triclinic_from_orthorhombic(self):#mike 11/27/2012
	'''' Carries the other info too.'''
	if self.box == [[],[],[]]:
		print "update_box_mode_to_triclinic_from_orthorhombic: ERROR Your box is not set, failure imminent."
	#self.update_lengths_from_box()
	self.box_mode = 'triclinic'
	self.origin = [self.box[0][0],self.box[1][0],self.box[2][0]]
	self.update_basis_from_lengths_and_angles(
		a = self.box[0][1] - self.box[0][0], 
		b = self.box[1][1] - self.box[1][0], 
		c = self.box[2][1] - self.box[2][0])
	self.update_internal_from_external()
	

def rewrap_internal(self):
	''' Forces all of the atoms into the box in all actively periodic directions'''
	if self.periodic[0]:
		for i in range(self.number_of_atoms):
			self.sx[i] = self.internal_wrap(self.sx[i])
	if self.periodic[1]:
		for i in range(self.number_of_atoms):
			self.sy[i] = self.internal_wrap(self.sy[i])
	if self.periodic[2]:
		for i in range(self.number_of_atoms):
			self.sz[i] = self.internal_wrap(self.sz[i])


def update_external_from_internal(self):
	''' Updates external coordinates from internal coordinates'''
	if len(self.sx) != len(self.x):
		self.fill_position_lists() #make sure there are lists to put into
	if self.box_mode == 'triclinic':
		r = [0.0,0.0,0.0]
		for i in range(self.number_of_atoms):
			s = [self.sx[i], self.sy[i], self.sz[i]]
			r =  self.vector_vector_add_3D(self.matrix_vector_product_3D(self.basis, s) , self.origin )
			self.x[i] = r[0]
			self.y[i] = r[1]
			self.z[i] = r[2]
	elif self.box_mode == 'orthorhombic':
		self.update_lengths_from_box()
		for i in range(self.number_of_atoms):
			self.x[i] = self.sx[i]*self.lx + self.box[0][0]
			self.y[i] = self.sy[i]*self.ly + self.box[1][0]
			self.z[i] = self.sz[i]*self.lz + self.box[2][0]
	else:
		print 'update_external_from_internal: Error, No box_mode set!'

def update_internal_from_external(self):
	''' Updates internal coordinates from external coordinates'''
		#When getting unclear errors, run update_number_of_atoms()
	if len(self.sx) != len(self.x):
		self.fill_s_lists() #make sure there are lists to put into	
	if self.box_mode == 'triclinic':
		s = [0.0,0.0,0.0]
		h_inv = self.compute_basis_inverse()
		for i in range(self.number_of_atoms):
			r = [self.x[i],self.y[i],self.z[i]]
			s = self.matrix_vector_product_3D(h_inv , self.vector_vector_subtract_3D(r, self.origin))
			self.sx[i] = s[0]
			self.sy[i] = s[1]
			self.sz[i] = s[2]
	elif self.box_mode == 'orthorhombic':
		self.update_lengths_from_box()
		for i in range(self.number_of_atoms):
			self.sx[i] = (self.x[i] - self.box[0][0])/self.lx
			self.sy[i] = (self.y[i] - self.box[1][0])/self.ly
			self.sz[i] = (self.z[i] - self.box[2][0])/self.lz
	else:
		print 'update_internal_from_external: Error, No box_mode set!'


