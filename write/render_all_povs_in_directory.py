#!/usr/bin/env python

def render_all_povs_in_directory(directory = '', width = 800, height = 600,
		cores = 4, submit_as_job = False, comment = ''):  # this powerfull tool can render all the povs in a directory
	'''If you chose not to render during write_POV, this will render ALL .pov files in the given directory.'''
	from os import listdir,getcwd,chdir
	from os.path import isfile
	
	if comment == '':
		comment = 'POVRAY-Series'

	cores = int(cores)# if this raises an error, hit someone for being dumb

	if directory == '':
		directory = getcwd()

	chdir(directory)
	directory = getcwd()

	dirlist = listdir(directory)

	# this removes things that dont end in .pov
	i = 0
	while i < len(dirlist):
		sectioned = dirlist[i].split('.')
		suffix = sectioned.pop()
		if suffix == 'POV' or suffix == 'pov':
			i+=1
		else:
			dirlist.pop(i)



	# this removes the directories
	i = 0
	while i < len(dirlist):
		if not(isfile(dirlist[i])):
			dirlist.pop(i)
		else:
			i+=1
	
	dirlist.sort() # this makes things better for sanity reasons, and should help nyx out

	# now to calculate the segments to write to the shell scripts

	tasks = len(dirlist)/cores
	remainder = len(dirlist)-cores*tasks
	
	segment_list = [0]
	previous_index = 0
	for i in range(1,cores+1):
		index = previous_index + tasks
		if i <= remainder:
			index+=1
		segment_list.append(index)
		previous_index = index
	
	shell_script_list = []
	list_of_number_of_renders = []
	for i in range(cores):
		shell_name = 'shell_script_for_core_%i.sh' % i 
		shell_script_list.append(shell_name)

		fid = open(shell_name,'w')
		files_for_this_core = dirlist[ segment_list[i] :segment_list[i+1]]

		list_of_number_of_renders.append(len(files_for_this_core))
		for file_name in files_for_this_core:
			fid.write('nice -n 5 povray Input_File_Name=%s +H%i +W%i display=false\n' 
				% (file_name , height, width) )

	fid.close()

	if submit_as_job == False:
		from os import system
		for name in shell_script_list:
			system('nohup sh %s &' % name)

	else: ######## this section is for submitting to nyx
		print "render_all_povs_in_directory: Feature not fully tested yet."
		from time import time
		from os import system,getlogin
		time_list = []
		for i in range(cores):  # figuring out how long each render should take
			file_name = dirlist[segment_list[i]]
			print 'render_all_povs_in_directory: '+file_name
			to=time()
			system('nice -n 5 povray Input_File_Name=%s +H%i +W%i display=false'% (file_name , height, width))
			time_list.append(time()-to) #1.5 factor for safety 
		
		max_time = max(list_of_number_of_renders) * 1.5 * max(time_list)

		pbs_script_list = []
		for i in range(cores): 
			pbs_name = 'pbs_script_for_core_%i.pbs' % i 
			pbs_script_list.append(pbs_name)

			fid2 = open(pbs_name,'w')
		
			fid2.write('#!/bin/sh\n'
				+ '#PBS -N %s-%i\n' % (comment,i)
				+ '#PBS -l nodes=1:ppn=1,pmem=128mb,walltime=00:%i:00\n' % (int(max_time/60.0))
				+ '#PBS -q route\n'
				+ '#PBS -M %s@umich.edu\n' % (getlogin())
				+ '#PBS -m abe\n'
				+ '#PBS -V\n\n'
				+ 'cd %s\n\n' % directory
				+ 'sh %s\n\n' % shell_script_list[i] )
			fid2.close()

		for pbs in pbs_script_list:
			system('qsub %s' % pbs)

