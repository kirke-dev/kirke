#!/usr/bin/env python

#I'm not touching this with a 20ft pole 18 March 2013 ---Katie
def write_DAE(sys,file_name = '',radius_scale = 1.0):
	''' Writes a collada file. '''

	from time import time
	start_time = time()
	from kirke.elements import default_element_rgb_colors, element_symbol_list, element_name_list, element_radius_list
	from os import getlogin
	from time import strftime


	if  file_name=='':
		file_name = sys.file_name + '.dae'

	new_dae_file=open(file_name,'w')


	def unique(items):
		found = set([])
		keep = []

		for item in items:
			if item not in found:
				found.add(item)
				keep.append(item)
		return keep

	def write_list_of_lines(file_id,list_of_lines):
		for a in list_of_lines:
			file_id.write(a+'\n')

	def write_int_array(file_id,list_of_numbers):
		for a in list_of_numbers:
			file_id.write('%i ' % a) #does not seem to care about the space at the end, so i'll leave it to make the code simple

	def write_float_array(file_id,list_of_numbers):
		for a in list_of_numbers:
			file_id.write('%.3f ' % a)

	number_of_elements = len(element_symbol_list)

	unique_atomic_number_set = set(unique(sys.atomic_number))
	#print unique_atomic_number_list

	#######################################3
	header = [
		'<?xml version="1.0" encoding="UTF-8" ?>',
		'<COLLADA xmlns="http://www.collada.org/2005/11/COLLADASchema" version="1.4.1">'
		]
	write_list_of_lines(new_dae_file,header)
	new_dae_file.write('\n')
	###############################################################
	asset_lines = [
		'    <asset>',
		'        <contributor>',
		'            <authoring_tool>GAS Library by Mike Waters</authoring_tool>',
		'            <author>%s</author>'% getlogin(),
		'        </contributor>',
		'        <created>%s</created>' %  strftime("%a, %d %b %Y %H:%M:%S"),
		'        <unit meter="1.0" name="meter" />',
		'        <up_axis>Z_UP</up_axis>',
		'    </asset>'
		]
	write_list_of_lines(new_dae_file,asset_lines)
	new_dae_file.write('\n')
	###########################################################

	light_lines = [
		'    <library_lights>',
		'        <light id="Lamp_light" name="Lamp">',
		'            <technique_common>',
		'                <ambient>',
		'                    <color>1 1 1</color>',
		'                </ambient>',
		'            </technique_common>',
		'        </light>',
		'    </library_lights>']
	write_list_of_lines(new_dae_file,light_lines)
	new_dae_file.write('\n')

	##########################################################################################
	new_dae_file.write('    <library_effects>'+'\n')
	for i in range(number_of_elements):
		if i in unique_atomic_number_set:
			effect_lines = [
				'        <effect id="ID%i" name="%s">' %(i+number_of_elements,element_name_list[i]),
				'            <profile_COMMON>',
				'                <technique sid="common">',
				'                    <phong>',
				'                        <emission>',
				'                            <color sid="emission">0 0 0 1</color>',
				'                        </emission>',
				'                        <ambient>',
				'                            <color sid="ambient">0 0 0 1</color>',
				'                        </ambient>',
				'                        <diffuse>',
				'                            <color>%f %f %f 1.0000000</color>'
					% ( default_element_rgb_colors[i][0] , default_element_rgb_colors[i][1] , default_element_rgb_colors[i][2]) ,
				'                        </diffuse>',
				'                        <specular>',
				'                            <color sid="specular">0.5 0.5 0.5 1</color>',
				'                        </specular>',
				'                        <shininess>',
				'                            <float sid="shininess">50</float>',
				'                        </shininess>',
				'                        <index_of_refraction>',
				'                            <float sid="index_of_refraction">1</float>',
				'                        </index_of_refraction>',
				'                    </phong>',
				'                </technique>',
				'            </profile_COMMON>',
				'        </effect>'
				]
			write_list_of_lines(new_dae_file,effect_lines)

	new_dae_file.write('    </library_effects>'+'\n')
	new_dae_file.write('\n')
	##########################################################################

	new_dae_file.write('    <library_materials>'+'\n')
	for i in range(number_of_elements):
		if i in unique_atomic_number_set:	
			material_lines=[
				'        <material id="ID%i" name="%s">' % (i,element_symbol_list[i] ),
				'            <instance_effect url="#ID%i" />'% (i+number_of_elements),
				'        </material>'
				]
			write_list_of_lines(new_dae_file,material_lines)

	new_dae_file.write('    </library_materials>'+'\n')
	new_dae_file.write('\n')




	################################### this section writes the actual geometry


	write_list_of_lines(new_dae_file,['    <library_geometries>'])

	from collada import position_list,triangle_list,normal_list  #>>>>> this line imports the template<<<<<<


	#########create/allocate new position list
	new_position_list = []
	for a in position_list:
		new_position_list.append(0.0)# basically allocates

	###### loop over atoms
	for i in range(sys.number_of_atoms):
		j = 310 + 10*i # this keeps the IDs from conficting with element materials
		# the index j will be multiplied by 10 to give an extra place for the sub IDs
		write_list_of_lines(new_dae_file,[
			'        <geometry id="ID%i">' % (j) ,
			'            <mesh>',
			'                <source id="ID%i">' % ( j+1 ) 
		] )

		##### writes the vertex positions in the float_array
		new_dae_file.write('                    <float_array id="ID%i" count="%i">' % (j+2, len(position_list)) )


	

		residual_factor = 0.0254000	# collada files have units and always list the conversion factor to meters.
						# When i made the template sphere, it was in google sketchup which despite
						# my indicating that i wanted to work in meters, it stored in inches.
						# this conversion factor renormalizes the unit sphere to radius ~ 1.0
						# remaking the template could give some computational
						# speed up and eleminate the need for this factor
	
		element_radius = element_radius_list[ sys.atomic_number[i] ] #looks up element radius

		for k in range(len(position_list)/3): # [x0 ,y0 ,z0 ,x1, y1, z1, ...]
			l = k*3
			#i'dth atom in the system, l'dth vertex triplet [x, y, z]
			new_position_list[l]   = sys.x[i] + radius_scale * residual_factor * element_radius * position_list[l]   
			new_position_list[l+1] = sys.y[i] + radius_scale * residual_factor * element_radius * position_list[l+1]
			new_position_list[l+2] = sys.z[i] + radius_scale * residual_factor * element_radius * position_list[l+2]

		write_float_array(new_dae_file,new_position_list)

		new_dae_file.write('</float_array>\n')
	
		# finish the first source section

		write_list_of_lines(new_dae_file,[
			'                    <technique_common>',
			'                        <accessor count="%i" source="#ID%i" stride="3">' % (len(position_list)/3, j+2),
			'                            <param name="X" type="float" />',
			'                            <param name="Y" type="float" />',
			'                            <param name="Z" type="float" />',
			'                        </accessor>',
			'                    </technique_common>',
			'                </source>'
			] )

		############# second source section, normal vectors
		new_dae_file.write('                <source id="ID%i">\n                    <float_array id="ID%i" count="%i">' 
			% ( j+3, j+4, len(normal_list) ) )

		write_float_array(new_dae_file,normal_list)
		new_dae_file.write('</float_array>\n')
	
		# finish the second source section with the accessor

		write_list_of_lines(new_dae_file,[
			'                    <technique_common>',
			'                        <accessor count="%i" source="#ID%i" stride="3">' % (len(normal_list)/3, j+4),
			'                            <param name="X" type="float" />',
			'                            <param name="Y" type="float" />',
			'                            <param name="Z" type="float" />',
			'                        </accessor>',
			'                    </technique_common>',
			'                </source>'
			] )

		############## vertices section 

		write_list_of_lines(new_dae_file,[
			'                <vertices id="ID%i">' % (j + 5),
			'                    <input semantic="POSITION" source="#ID%i" />'% (j + 1),
			'                    <input semantic="NORMAL" source="#ID%i" />'% (j + 3),
			'                </vertices>'
			] )

		##### triangles section 

		write_list_of_lines(new_dae_file,[
			'                <triangles count="%i" material="Atoms_and_Stuff">'  % ( len(triangle_list)/3 ),
			'                    <input offset="0" semantic="VERTEX" source="#ID%i" />' % (j + 5)
			] )

		new_dae_file.write('                    <p>')
		write_int_array(new_dae_file,triangle_list)
		new_dae_file.write('</p>\n')
		new_dae_file.write('                </triangles>\n')
	
		############ finish this atoms geometry section
		write_list_of_lines(new_dae_file,[
			'            </mesh>',
			'            <extra><technique profile="MAYA"><double_sided>1</double_sided></technique></extra>',
			'        </geometry>'
			] )

		############################ finishes the actual geometry section and the addition of new ID tags

	new_dae_file.write('    </library_geometries>\n')
	new_dae_file.write('\n')
	last_used_ID_tag_number = (sys.number_of_atoms-1)*10 + 5 + 1000 ### this will be good to know for
			# adding any geometry later, so that there are no ID tag number conflicts
	

	########################################################## this section aligns materials to geometries


	write_list_of_lines(new_dae_file,[
		'    <library_visual_scenes>',
		'        <visual_scene id="ID300">',	# visual scene is tagged at <<300>> to be after 
							# the elements data but before atoms to avoid conflicts
							# i see no reason to make it a variable since there is only one.
		
		])
	#print sys.number_of_atoms
	for i in range(sys.number_of_atoms):
		j = 310 + 10*i # this keeps the IDs from conficting with element materials
		# the index j will be multiplied by 10 to give an extra place for the sub IDs
		geom_list = [
			'            <node id="Atoms_%i" type="NODE">' % i,
			'                <instance_geometry url="#ID%i">' % (j),
			'                    <bind_material>',
			'                        <technique_common>',
			'                            <instance_material symbol="Material2" target="#ID%i"/>' % (element_symbol_list.index(sys.element[i])),
			'                        </technique_common>',
			'                    </bind_material>',
			'                </instance_geometry>',
			'            </node>'
			]

		write_list_of_lines(new_dae_file,geom_list)


	################## for the ambient light
	camera_z = (max(sys.z)-min(sys.z))*5.0+min(sys.z)
	write_list_of_lines(new_dae_file,[
		'            <node id="Lamp" type="NODE">',
		'                <translate sid="location">0.0 0.0 %f</translate>' % camera_z,
		'                <scale sid="scale">1 1 1</scale>',
		'                <instance_light url="#Lamp-light"/>',
		'            </node>'])

	#########
	
	write_list_of_lines(new_dae_file,[
		'        </visual_scene>',
		'    </library_visual_scenes>'
		])
	new_dae_file.write('\n')

	############# writes the scene ###########################################3

	write_list_of_lines(new_dae_file,[
		'    <scene>',
		'        <instance_visual_scene url="#ID300" />',
		'    </scene>'
		] )
	################### Finish the file

	new_dae_file.write('</COLLADA>')
	new_dae_file.close()

	print "write_DAE: Write time for '%s' was %.1f Seconds" %( file_name , time()-start_time )

