#!/usr/bin/env python

#fast xyz split VERY FAST

def fast_XYZ_split(file_name,output_name = ''):
	''' Splits frames from an appened xyz file into multiple files. '''

	if output_name == '':
		frame_file_name = file_name.split('.')
		suffix = frame_file_name.pop()
		for b in frame_file_name:
			output_name = sys.file_name + b + '.'
	else:
		suffix = 'xyz'

	fid = open(file_name,'r')

	start_line_list = []
	i = 0
	for a in fid:
		if a == 'Atoms\n':
			start_line_list.append(i - 1) # the starting line of each block 
		i += 1
	fid.close()	
	frames = len(start_line_list)-1
	#done parsing the file, now for dumping it
	
	fid_in = open(file_name,'r')
	frame_i = 0
	i = 0
	fid2 = open(output_name+str(frame_i) + '.' + suffix,'w')

	for line in fid_in:
		fid2.write(line)
		if frame_i < frames: #keeps frame_i from leaving the list
			if i == (start_line_list[frame_i+1]-1):
				fid2.close()
				frame_i += 1
				fid2 = open(output_name+str(frame_i) + '.' + suffix,'w')
		i+=1

	fid2.close()
	
	return frames+1
