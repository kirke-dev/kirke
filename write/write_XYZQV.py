#!/usr/bin/env python

def write_XYZQV(sys,file_name = '', use_element = False):#Default_to_type
	'''Writes a simple single frame .xyzqv file.'''
	if  file_name=='':
		file_name = sys.file_name + '.xyzqv'

	header = '%s\nAtoms' %  sys.number_of_atoms

	ws = ''
	
	if use_element == True:
		for i in range(sys.number_of_atoms):

			line = '\n%s\t%s\t%s\t%s' % (sys.element[i],sys.x[i],sys.y[i],sys.z[i]) +\
				'\t%s\t%s\t%s\t%s' % ( sys.q[i],sys.vx[i],sys.vy[i],sys.vz[i])

			ws = ws + line
	else:
		for i in range(sys.number_of_atoms):

			line = '\n%s\t%s\t%s\t%s' % (sys.atomic_number[i],sys.x[i],sys.y[i],sys.z[i]) +\
				'\t%s\t%s\t%s\t%s' % ( sys.q[i],sys.vx[i],sys.vy[i],sys.vz[i])
			ws = ws + line
	
	fid = open(file_name,'w')
	fid.write(header+ws)
	fid.close()

	return file_name

