#!/usr/bin/env python

#Added by Katie 21 August 2012
#Only capable of simple files, I haven't figured out the many bells and whistles of Gaussian yet
#Like PDB, must use element, not type
def write_basic_gauss_input(sys, output_file = 'gauss.out', link_commands = '', route_section = '# HF/6-31G(d)', title = 'GAS_Gauss', charge = 0, spin_multiplicity = 1, bonds = False, angles = False):
	'''This is my attempt to generate very simple Gaussian input files from a GAS object. Not all Gaussian input file sections are currently supoprted, because I don't understand them yet. Defaults to Hartree-Fock 6031G(d) set if not otherwise given a route_section.  Please don't include formating flag, such as blank lines in the header for string variables'''
	
	if len(sys.id) > 50:
		print 'write_basic_gauss_input: You have %s atoms: This may be slow to converge.' %len(sys.id)
	if len(sys.id) > 100:
		print 'write_basic_gauss_input: Seriously, this may not run.'
	if len(sys.id) > 1000:
		print "write_basic_gauss_input: You're crazy. I will still generate the file, but don't blame me if it can't converge in your lifetime."
	
	lines = route_section + link_commands + '\n\n' + title + '\n\n'+ str(charge) + ' ' + str(int(spin_multiplicity)) + '\n'
	
	for a in range(len(sys.id)):
		lines +='%s %.6f %.6f %.6f \n' %(sys.element[a], sys.x[a], sys.y[a], sys.z[a])

	lines += '\n'

	if bonds:
		for b in range(len(sys.bonds)):
			lines +='%i %i \n' %(sys.bonds[b][2], sys.bonds[b][3])
		lines += '\n'
	if angles:
		for an in range(len(sys.angles)):
			lines += '%i %i %i \n' %(sys.angles[an][2], sys.angles[an][3], sys.angles[an][4])
		lines +='\n'


	fid = open(output_file, 'w')
	for a in lines:
		fid.write(a)
	fid.close
