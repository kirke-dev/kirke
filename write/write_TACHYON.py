#!/usr/bin/env python

def write_TACHYON(sys,file_name = '', image_name = '', render = True, coloring = {}, height = 1024, width = 1024,
		camera_position = [], camera_direction = [-1,0,0], background_color = [0,0,0],
		display = False, light_position = [0,0,0], finish = {},radius_scale = 1.0, radius = {},
		comment = '',show_charge = False, qmax = 0.0, qmin = 0.0, x_offset = 1.50,light_at_camera = False,
		rotate = 0, with_bonds = False, bond_scale = 0.8, quality = 'low', comment_size = 32, threads = 1):
	'''Tachyon Rendering, it is a whole mess of stuff'''

	##### taking care of defaults

	if finish == {}:
		for i in range(126):
			finish[i]='dull' 

	if len(coloring) < len(finish):
		from GAS.elements import default_element_rgb_colors
		newcoloring = default_element_rgb_colors
		for i in coloring:
			newcoloring[i] = coloring[i]  #basicaly replaces the defaults with yours 
		coloring = newcoloring

	if radius == {}:
		from GAS.elements import element_radius_list
		radius = element_radius_list

	if file_name=='':
		file_name = sys.file_name + '.tachyon'


	#camera position guesssing 
	if camera_position == []:
		# this is guessing for you, assumes you have no idea what the camera position is
		maxy = max(sys.y)
		miny = min(sys.y)
		maxz = max(sys.z)
		minz = min(sys.z)
		maxx = max(sys.x)
		midy = (maxy+miny)/2		
		midz = (maxz+minz)/2
		camera_position = [0,0,0] 
		camera_offset = x_offset*max( [abs(maxy-miny), abs(maxz-minz)])

		camera_position[0] = camera_offset + maxx
		camera_position[1] = midy
		camera_position[2] = midz
		light_offset = camera_offset*4.0/3.0 
		light_position = [maxx + light_offset, maxy + light_offset/3.0, maxz + light_offset/3.0]
		camera_direction = [-1,0,0]

	#### Quality setting
	AA = 0 
	RD = 4
	shade_quality = '-mediumshade' #this is a command line flag
	if quality == 'high':
		AA = 16 # multiples of 2 I think 
		RD = 16
		shade_quality = '-fullshade'

	def interpolate_color(val,v1,c1,v2,c2):
		#If value1 has color1 and value2 has color2
		#what color should val have? thats what this function does
		fraction2 = (val-v1)/(v2-v1)
		color = [0.0, 0.0, 0.0]
		for i in range(3):
			color[i] = (c2[i]-c1[i]) * fraction2 + c1[i] 
		return color

	if show_charge == True:
		if qmax == 0.0:
			qmax = max(sys.q)
		qmax_color = [0.0,0.0,1.0] # blue
		qzero_color = [1.0,1.0,1.0] #white
		qmin_color = [1.0,0.0,0.0] # red
		if qmin == 0.0:
			qmin = min(sys.q)

	############ begin writting file 
	outfile = open(file_name,'w')

	outfile.write("BEGIN_SCENE\n\n")

	outfile.write("RESOLUTION %i %i\n\n" % (width,height) )

	outfile.write('CAMERA\n')
	outfile.write('\tZOOM 1.0\n')
	outfile.write('\tASPECTRATIO 1.0\n')
	outfile.write('\tANTIALIASING %i\n'%AA)
	outfile.write('\tRAYDEPTH %i\n'%RD)
	outfile.write('\tCENTER %f %f %f\n' %(camera_position[0],camera_position[1],camera_position[2]) )
	outfile.write('\tVIEWDIR %f %f %f\n' %(camera_direction[0],camera_direction[1],camera_direction[2]) )
	outfile.write('\tUPDIR 0 1 0\n')
	outfile.write('END_CAMERA\n\n')

	outfile.write('BACKGROUND %f %f %f\n\n'%(background_color[0],background_color[1],background_color[2]))

	outfile.write('LIGHT\n')
	outfile.write('\tCENTER %f %f %f\n'% (light_position[0],light_position[1],light_position[2]))
	outfile.write('\tRAD 0.1\n')
	brightness = 1.0
	if light_at_camera == True:
		brightness = 0.6
	outfile.write('\tCOLOR %f %f %f\n\n'%(brightness,brightness,brightness))
	######## light in camera direction

	if light_at_camera == True:
		#outfile.write('LIGHT\n')
		#outfile.write('\tCENTER %f %f %f\n'% (camera_position[0],camera_position[1]+0.15,camera_position[2]))
		#outfile.write('\tRAD 0.1\n')
		#outfile.write('\tCOLOR 0.6 0.6 0.6\n\n')
		outfile.write('Directional_Light Direction %f %f %f  Color 0.6 0.6 0.6\n\n' %(camera_direction[0],camera_direction[1],camera_direction[2]))

	###3333############### textures definitions
	from GAS.elements import element_name_list
	for i in finish:# i is atomic number
		element_tex = finish[i]
		element_name = element_name_list[i]
		element_color = coloring[i]  ## IF you error on this line, You do not have enough colors for your finish. We are working on it
		if element_tex == 'dull':
			outfile.write('TEXDEF %s\n'% element_name)
			outfile.write('\tAMBIENT 0.25\n')
			outfile.write('\tDIFFUSE 0.8\n')
			outfile.write('\tSPECULAR 0.0\n') # reflectivity?
			outfile.write('\tOPACITY 1.0\n')
			#outfile.write('\tPHONG PLASTIC 0.8 PHONG_SIZE 45\n')
			outfile.write('\tCOLOR %f %f %f\n'% (element_color[0],element_color[1],element_color[2]))
			outfile.write('\tTEXFUNC 0\n\n')
			
		if element_tex == 'metal':
			outfile.write('TEXDEF %s\n'% element_name)
			outfile.write('\tAMBIENT 0.08\n')
			outfile.write('\tDIFFUSE 0.4\n')
			outfile.write('\tSPECULAR 0.4\n')
			outfile.write('\tOPACITY 1.0\n')
			outfile.write('\tPHONG PLASTIC 0.3 PHONG_SIZE 10\n')
			outfile.write('\tCOLOR %f %f %f\n'% (element_color[0],element_color[1],element_color[2]))
			outfile.write('\tTEXFUNC 0\n\n')

#		if element_tex == 'glass':  ### we need a refraction ability here
#			#print "Not done yet"
#			outfile.write('TEXDEF %s\n'% element_name)
#			outfile.write('\tAMBIENT 0.10\n')
#			outfile.write('\tDIFFUSE 0.4\n')
#			outfile.write('\tSPECULAR 0.4\n')
#			outfile.write('\tOPACITY 0.95\n')
#			outfile.write('\tPHONG PLASTIC 0.8 PHONG_SIZE 10\n')
#			outfile.write('\tCOLOR %f %f %f\n'% (element_color[0],element_color[1],element_color[2]))
#			outfile.write('\tTEXFUNC 0\n\n')

	###333333############ spheres

	for i in range(sys.number_of_atoms):
		ith_atomic_number = sys.atomic_number[i]
		ith_radius = radius[ith_atomic_number] * radius_scale
		element_name = element_name_list[ith_atomic_number]
		if show_charge == True:
			if sys.q[i] >= 0.0:
				color = interpolate_color(sys.q[i], 0.0, qzero_color, qmax ,qmax_color)
			else:
				color = interpolate_color(sys.q[i], 0.0, qzero_color, qmin ,qmin_color)
			outfile.write('SPHERE CENTER\t%f\t%f\t%f\tRAD\t%f\tTEXTURE\tAMBIENT 0.25\tDIFFUSE 0.8\tSPECULAR 0.0\tOPACITY 1.0\tCOLOR\t%f\t%f\t%f\tTEXFUNC 0\n' % (sys.x[i],sys.y[i],sys.z[i],ith_radius, color[0], color[1], color[2]) )
		else:
			outfile.write('SPHERE CENTER\t%f\t%f\t%f\tRAD\t%f\t%s\n'%(sys.x[i],sys.y[i],sys.z[i],ith_radius,element_name))
		

	############## bond cyclinders

	if with_bonds == True:
		outfile.write('\n')
		sys.sort_by_id()
		if any(sys.periodic):
			if sys.box_mode == 'orthorhombic':
				sys.update_lengths_from_box()
				print 'The box mode is %s.' % sys.box_mode 
			elif sys.box_mode == 'triclinic':
				print 'The box mode is %s.' % sys.box_mode  ### this isn't needed but i need to make sure the user sets a box mode
			else:
				print "WARNING, YOU ARE USING PERIODIC BOUNDARY CONDITIONS WITHOUT A 'box_mode' !"
		


		for bond in sys.bonds:
			id1 = bond[2]
			id2 = bond[3]
			index1 = sys.get_index_with_binary_search_from_id(id1)
			index2 = sys.get_index_with_binary_search_from_id(id2)
			
			element_name1 = element_name_list[sys.atomic_number[index1]]
			element_name2 = element_name_list[sys.atomic_number[index2]]
			r1 = radius_scale*radius[sys.atomic_number[index1]]
			r2 = radius_scale*radius[sys.atomic_number[index2]] 
			midx = (sys.x[index2] - sys.x[index1]) * r1/(r1+r2) + sys.x[index1] # this nicely centers the bond mid point between the atoms
			midy = (sys.y[index2] - sys.y[index1]) * r1/(r1+r2) + sys.y[index1]
			midz = (sys.z[index2] - sys.z[index1]) * r1/(r1+r2) + sys.z[index1]
			rmax = max([r1,r2])	
			rmin = min([r1,r2])
			from math import exp
			cylinder_radius = bond_scale*rmin*(2.0-exp(-(rmax/rmin)/2.0))/2.0  # this is a function mike came up with to make pretty bonds

			draw_bond = True # we are looking for the contary below
					
			if sys.box_mode == 'triclinic':
				if sys.periodic[0] == True:
					absdx = abs(sys.sx[index2] - sys.sx[index1])
					if absdx > 0.5:
						draw_bond = False
				if sys.periodic[1] == True:
					absdy = abs(sys.sy[index2] - sys.sy[index1])
					if absdy > 0.5:
						draw_bond = False
				if sys.periodic[2] == True:
					absdz = abs(sys.sz[index2] - sys.sz[index1])
					if absdz > 0.5:
						draw_bond = False
			else:
				if sys.periodic[0] == True:
					absdx = abs(sys.x[index2] - sys.x[index1])
					if absdx > (sys.lx*0.5):
						draw_bond = False
				if sys.periodic[1] == True:
					absdy = abs(sys.y[index2] - sys.y[index1])
					if absdy > (sys.ly*0.5):
						draw_bond = False
				if sys.periodic[2] == True:
					absdz = abs(sys.z[index2] - sys.z[index1])
					if absdz > (sys.lz*0.5):
						draw_bond = False

			if draw_bond == True: ## this is true if the bond is not cut by periodic bondary conditions
				################### cylinder 1
				outfile.write('FCYLINDER BASE\t%f\t%f\t%f\tAPEX\t%f\t%f\t%f\tRAD\t%f\t%s\n' %
					(midx,midy,midz,sys.x[index1],sys.y[index1],sys.z[index1],cylinder_radius,element_name1 ))

				##################### cylinder 2
				outfile.write('FCYLINDER BASE\t%f\t%f\t%f\tAPEX\t%f\t%f\t%f\tRAD\t%f\t%s\n' %
					(midx,midy,midz,sys.x[index2],sys.y[index2],sys.z[index2],cylinder_radius,element_name2 ))

	#################################### 
			
	outfile.write('\nEND_SCENE')
	outfile.close()
	####### done writting file!

	if render==True:
		
		if image_name == '':
			image_name = (file_name.split('.')[0]) + '.png'

		command = 'tachyon %s %s -format PNG -o %s ' % (file_name, shade_quality, image_name)  ##<---- important shit here

		if threads > 1:   #multicore
			command = 'mpiexec -n %i ' % threads + command 

		if rotate != 0:    # rotating image, has to go before commenting
			rotate_command = 'convert -rotate %i %s %s ' % (rotate, image_name, image_name)
			command = command + "&&" + rotate_command

		if comment !='': ### commenting
			comment_command = "convert %s  -fill white  -undercolor '#00000090'  -gravity Northwest -pointsize %i -annotate +5+5 ' %s ' %s" % (image_name,comment_size,comment,image_name)
			command = command + ' && ' + comment_command

		from os import system
		print '\nPrint Executing Command "%s"\n' % command
		system(command)  #### actually passes the command
		if display==True:
			system ('gpicview %s' %image_name)

	return file_name

