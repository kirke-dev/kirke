#!/usr/bin/env python

def write_UC(sys,file_name = '',with_charge = False, with_origin = False):
	'''Writes a unitcell file'''
	if  file_name=='':
		file_name = sys.file_name + '.uc'
	if sys.box_mode == 'orthorhombic':
		sys.update_lengths_from_box()
		a = sys.lx
		b = sys.ly
		c = sys.lz
		alpha = 90.0
		beta =  90.0
		gamma = 90.0
		originx = sys.box[0][0]
		originy = sys.box[1][0]
		originz = sys.box[2][0]
	elif sys.box_mode == 'triclinic':
		LandA = sys.compute_lengths_and_angles_from_basis()
		a,     b,    c     = LandA[0][0], LandA[0][1], LandA[0][2]
		alpha, beta, gamma = LandA[1][0], LandA[1][1], LandA[1][2]
		originx, originy, originz = sys.origin[0], sys.origin[1], sys.origin[2]
	else:
		print "write_UC: The box_mode is not set to 'triclinic' or 'orthorhombic', this will fail.\n"
	
	#if originx != 0.0 or originy != 0.0 or originz != 0.0:
	#	if with_origin!=True:
	#		print "Warning, You have an origin but are not writting it to file."

	fid = open(file_name,'w')
	fid.write("%f\t%f\t%f"%(a,b,c))
	fid.write("\n%f\t%f\t%f"%(alpha,beta,gamma))
	if with_origin:
		fid.write("\n%f\t%f\t%f"%(originx, originy, originz))
	else:
		print 'write_UC: The system is being moved to compensate for the origin not being written.\n'
		from kirke.manipulate import move
		move(sys,[-originx,-originy,-originz])

	if with_charge:
		for i in range(sys.number_of_atoms):
			fid.write('\n%s\t%f\t%f\t%f\t%f' % (sys.element[i], sys.sx[i], sys.sy[i], sys.sz[i], sys.q[i] ) )
	else:

		for i in range(sys.number_of_atoms):
			fid.write('\n%s\t%f\t%f\t%f' % (sys.element[i], sys.sx[i], sys.sy[i], sys.sz[i] ) )

	fid.close()

	return file_name

