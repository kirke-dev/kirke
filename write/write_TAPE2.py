#!/usr/bin/env python

def write_TAPE2(sys,file_name = ''):
	''' Writes tape2 files from a tape2 class object only.'''
	print "write_TAPE2: This only accepts the TAPE2 class for now. Sorry! Writes the atoms using the wrapbody, if you modifiy the body make sure to update the wrapbody"
	if file_name=='':
		file_name = sys.file_name + '.tape2'

	fid=open(file_name,'w')

	#writes the atoms using the wrapbody, if you modifiy the body make sure to update the wrapbody
	for a in sys.wrapbody:

		ws = ''
		for b in a: 
			ws+= '%2.10e' % b + ' '
		ws=ws+'\n'
		fid.write(ws)

	#starts tail
	for a in sys.unparced_tail[0:6]:
		fid.write(a)

	#stops tail for the box
	for a in sys.box:
	
		ws = ''
		for b in a: 
			ws+= '%2.10e' % b + ' '
		ws=ws+'\n'
		fid.write(ws)
		
	#continues tail afterbox
	for a in sys.unparced_tail[9:]:
		fid.write(a)


	fid.close()


	return file_name

