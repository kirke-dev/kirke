#!/usr/bin/env python

def write_PDB(sys, file_name= '',title = '', author = ''): #This one has to assume element because that's what PDB requires
	'''Writes basic PDB files. Does not support some of the more complicated protein options'''

	#The pdb format expects everything in order according to id, so we premptively sort things

	sys.sort_by('id')

	if author == '':
		from os import getlogin
		author = getlogin()

	if file_name == '':
		file_name = sys.file_name + '.pdb'

	if title == '':
		title = sys.file_name + ' was the source file for this automatic conversion to an PDB file'

	header = 'HEADER\t'+ title + '\n' + 'REMARK   1 The author was ' + author+'\n' 

	leadin = ''

	body = ''
	if sys.element == []:
		sys.update_element_list()

	for i in range(sys.number_of_atoms):
		current_line = 'ATOM  '
		symbol_string =  sys.element[i].upper()
	
		#This is a space sensitive collection of crazy
		#Any given section can be left blank, and all missing indices are space
		#Columns 1-6 "ATOM  "
		#Col 7-11 Integer ID, right aligned
		id_string = str(sys.id[i])
		if len(id_string) == 1:
			current_line+='    '+id_string
		elif len(id_string) == 2:
			current_line += '   '+id_string
		elif len(id_string) == 3:
			current_line += '  '+id_string
		elif len(id_string) == 4:
			current_line += ' '+id_string
		elif len(id_string) == 5:
			current_line += id_string
		else:
			print "I'm sorry, our current write_PDB function doesn't know what to do with this atom ID"

		#Col 13-16 atom name, left aligned (CAPS)

		if len(symbol_string) == 1:
			current_line+=' '+symbol_string+'   '
		elif len(symbol_string) ==2:
			current_line+=' '+symbol_string+'  '
		elif len(symbol_string) == 3:
			current_line+=' '+symbol_string+' '
		elif len(symbol_string) == 4:
			current_line+=' '+symbol_string
		else:
			print "I'm sorry, write_PDB cannot handle your element name. Please use something else."

		#Col 17 character alternate location, use space
		#Col 18-20 resNAME (always sys)
		#Col 22 ignore
		current_line += ' SYS '

		#Col 23-26 Integer residue sequence number (i.e., mol id)

		mol_string = str(sys.molecule[i])
		if len(mol_string) == 1:
			current_line+='   '+mol_string+'    '
		elif len(mol_string) == 2:
			current_line+='  '+mol_string+'    '
		elif len(mol_string) == 3:
			current_line+=' '+mol_string+'    '
		elif len(mol_string) == 4:
			current_line+=mol_string+'    '
		else:
			print "I'm sorry, write_PDB cannot handle your molecule_id."

		#Col 31-38 Real(8.3) x

		string_x = '{:8.3f}'.format(sys.x[i])

		if len(string_x) == 1:
			current_line+='       '+string_x
		elif len(string_x) == 2:
			current_line+='      '+string_x
		elif len(string_x) == 3:
			current_line+='     '+string_x
		elif len(string_x) == 4:
			current_line+='    '+string_x
		elif len(string_x) == 5:
			current_line+='   '+string_x
		elif len(string_x) == 6:
			current_line+='  '+string_x
		elif len(string_x) == 7:
			current_line+=' '+string_x
		elif len(string_x) == 8:
			current_line+=string_x
		else:
			print "I'm sorry, write_PDB cannot handle your x value. Consider changing your units?"

		#Col 39-46 Real(8.3) y

		string_y = '{:8.3f}'.format(sys.y[i])
		
		if len(string_y) == 1:
			current_line+='       '+string_y
		elif len(string_y) == 2:
			current_line+='      '+string_y
		elif len(string_y) == 3:
			current_line+='     '+string_y
		elif len(string_y) == 4:
			current_line+='    '+string_y
		elif len(string_y) == 5:
			current_line+='   '+string_y
		elif len(string_y) == 6:
			current_line+='  '+string_y
		elif len(string_y) == 7:
			current_line+=' '+string_y
		elif len(string_y) == 8:
			current_line+=string_y
		else:
			print "I'm sorry, write_PDB cannot handle your y value. Consider changing your units?"
		#Col 47-54 Real(8.3) z

		string_z = '{:8.3f}'.format(sys.z[i])
		
		if len(string_z) == 1:
			current_line+='       '+string_z
		elif len(string_z) == 2:
			current_line+='      '+string_z
		elif len(string_z) == 3:
			current_line+='     '+string_z
		elif len(string_z) == 4:
			current_line+='    '+string_z
		elif len(string_z) == 5:
			current_line+='   '+string_z
		elif len(string_z) == 6:
			current_line+='  '+string_z
		elif len(string_z) == 7:
			current_line+=' '+string_z
		elif len(string_z) == 8:
			current_line+=string_z
		else:
			print "I'm sorry, write_PDB cannot handle your z value. Consider changing your units?"
		#Col 55-60 Real (6.2) occupancy (1.0)
		#Col 61-66 Real(6.2) tempFactor (0.0)
		#Col 77-78 String element sysmbol, right aligned 
		#Col 79-80 LString(2) integer charge, which we are ignoring for reasons
		current_line+='                          '

		current_line +='\n'
		body+=current_line


	connections = ''

	#This is essentially the neighbor_list for each atom, an hence both 1 2 and 2 1 must be included. Oy vey. Double bonds show up as neighbors twice
	if sys.number_of_bonds > 0:

		from kirke.analyze import neighbor_list_from_bonds
		nghblst = neighbor_list_from_bonds(sys, element1 = 'all', element2 = 'all', verbose = False, use_index = False)

		for a in nghblst:

			connect_line = 'CONECT'

			center = str(a[0])
			neighbors = a[1:]
			neighbors.sort()
			
			if len(center) == 1:
				connect_line +='    '+center
			elif len(center)==2:
				connect_line +='   '+center
			elif len(center) == 3:
				connect_line +='  '+center
			elif len(center) == 4:
				connect_line +=' '+center
			elif len(center) == 5:
				connect_line +=center

			for n in neighbors:
				ngh = str(n)
				if len(ngh) == 1:
					connect_line+='    '+ngh
				elif len(ngh) == 2:
					connect_line += '   '+ngh
				elif len(ngh) == 3:
					connect_line += '  '+ngh
				elif len(ngh) == 4:
					connect_line += ' '+ngh
				elif len(ngh) == 5:
					connect_line += ' '+ngh
			connect_line+='\n'

			connections+=connect_line

	footer = 'TER\nEND'

	
	fid = open(file_name,'w')
	fid.write(header + leadin + body + connections + footer)



	return file_name
