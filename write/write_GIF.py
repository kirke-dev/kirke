#!/usr/bin/env python

def write_GIF(frames, file_name = '',
	pov_file_name = '',image_file_name = '' , render = True, coloring = {}, height = 600, width = 800,
	camera_position = [], camera_look_at_point = [0,0,0], background_color = [0,0,0],
	display = False, light_position = [0,0,0], finish = {},radius_scale = 1.0,
	show_charge = False, qmax = 0.0, qmin = 0.0, x_offset = 1.20,
	remove_temporary_directory = False,
	comments = [],	
	temporary_directory = '',
	delay = 8,
	verbose = True):

	'''Like write_POV, only it automatically compiles a GIF.'''
	if file_name =='':
		file_name = frames[0].file_name + ".gif"

	if pov_file_name == '':
		pov_file_name = frames[0].file_name

	if image_file_name =='':
		image_file_name = frames[0].file_name


	from os import system
	from os import getcwd, chdir
	
	
	WD = getcwd()
	if temporary_directory == '':				
		temporary_directory = WD+'/GIF_renders'
	system('mkdir '+ temporary_directory)
	

	show_comments = True

	if len(comments) == 0:
		show_comments = False

	if len(comments) < len(frames):
		show_comments = False

	chdir(temporary_directory) #change into working directory since povray is dumb
	
	imagei = 0
	
	for i in range(len(frames)):

		sys = frames[i]
		
		if show_comments:	
			comment = comments[i]
		else:
			comment = ''

		pov_file = write_POV(sys, background_color = background_color,
			file_name  = pov_file_name + '_%05d_.pov' % imagei ,
			image_name = image_file_name + '_%05d_.png' % imagei ,
			render = render, # makes it write the .pov file and calls povray 
			height = height, 
			width = width, 
			camera_position = camera_position,
			display = display , # this makes povray not display the progress
			camera_look_at_point = camera_look_at_point,
			light_position = light_position,
			finish = finish, 
			coloring = coloring, 
			radius_scale = radius_scale,
			show_charge = show_charge,
			comment = comment,
			qmax = qmax,
			qmin = qmin,
			verbose = verbose)

		imagei += 1

	
	chdir(WD) #change back from render directory 
	png_file_temp = temporary_directory+'/'+ image_file_name + '_*_.png' #formating png filename with wildcard


	if len(comments) > len(frames):
		print 'write_GIF: Warning, more comments then frames in the animation.'
	if len(comments) < len(frames):
		print 'write_GIF: Comments were ignored since there were less than the number of frames.'


	if not render:
		print 'write_GIF: Rendering was disabled for some reason, so there may not be any PNG file to animate.'
	
	
	print 'write_GIF: Converting series of images to one gif animation, this may take a while...'
	system('convert -delay %d -loop 0 %s %s' % (delay,  png_file_temp ,file_name) )


	return file_name

