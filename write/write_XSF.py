

def write_XSF(sys, file_name = ''): 
	if  file_name=='':
		file_name = sys.file_name + '.xsf'
		
	fid = open(file_name,'w')

	fid.write('#'+sys.comment+'\n')

	if sys.box_mode == 'triclinic' or  sys.box_mode == 'orthorhombic':
		fid.write('CRYSTAL\n')
		fid.write('PRIMVEC\n')
		if sys.box_mode == 'triclinic':
			sys.update_external_from_internal()
			for i in range(3):
				vector = sys.get_basis_vector(i)
				for j in range(3):
					fid.write('  %.10f'% vector[j])
				fid.write('\n')
		else: #orthorombic mode
			
			fid.write('  %.10f  0.0000000000  0.0000000000\n' % (sys.box[0][1]-sys.box[0][0]) )
			fid.write('  0.0000000000  %.10f  0.0000000000\n' % (sys.box[1][1]-sys.box[1][0]) )
			fid.write('  0.0000000000  0.0000000000  %.10f\n' % (sys.box[2][1]-sys.box[2][0]) )

		fid.write('\n')
		fid.write('PRIMCOORD\n')
		fid.write('%i 1\n'%sys.number_of_atoms)
		for i in range(sys.number_of_atoms):
			fid.write('  %i   %.10f   %.10f   %.10f\n' % (sys.atomic_number[i], sys.x[i], sys.y[i], sys.z[i]))
	else:
		fid.write('ATOMS\n')
		for i in range(sys.number_of_atoms):
			fid.write('  %i   %.10f   %.10f   %.10f\n' % (sys.atomic_number[i], sys.x[i], sys.y[i], sys.z[i]))


	fid.close()
