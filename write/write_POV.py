#!/usr/bin/env python

def write_POV(sys, file_name = '',image_name = '' , 
		height = 512, width = 512, width_height_ratio = 0.0,
		camera_position = [], camera_look_at_point = [0,0,0], up_direction = [0,1,0], camera_style = '',
		background_color = [0,0,0], display = False, render = True,
		light_position = [0,0,0], shadowless = True,light_at_camera = True, write_lights = True,
		coloring = {}, finish = {},radius_scale = 1.0, radius = {},
		rotate = 0, comment = '',comment_size = 32,
		show_charge = False, qmax = 0.0, qmin = 0.0, 
		with_bonds = False, bond_scale = 0.8,
		draw_box = False, box_color = [0.5, 0.5, 0.5], box_scale = 1.0,
		x_offset = 1.20,
		verbose = False,
		transparent = False, antialias = False):
#Still need to work out not auto-useing elements here
	''' This is how you render the pretty pictures of your dreams... well, at least pretty pictures of your system. Really, just read the full manual entry until I copy it back here.'''


	

	from kirke.elements import element_radius_list, default_element_rgb_colors



	def interpolate_color(val,v1,c1,v2,c2):
		#If value1 has color1 and value2 has color2
		#what color should val have? thats what this function does
		fraction2 = (val-v1)/(v2-v1)
		color = []
		for i in range(len(c1)):
			color.append( (c2[i]-c1[i]) * fraction2 + c1[i] )
		return color


	BBRR = 1.9 # dims billiard colors to be similar to regular colors

	######## start of options
	
	if show_charge == True:
		if verbose:
			print 'write_POV: Showing Charges.'
		if qmax == 0.0:
			qmax = max(sys.q)
		qmax_color = [0.0,0.0,1.0]
		qzero_color = [1.0,1.0,1.0]
		qmin_color = [1.0,0.0,0.0]
		if qmin == 0.0:
			qmin = min(sys.q)

	if coloring == {}:
		coloring = default_element_rgb_colors

	if finish == {}:
		for i in range(126):
			finish[i]='dull' 

	if finish == '':
		finish = {}
		for i in range(126):
			
			finish[i]='dull'

	if finish == 'dull':
		finish = {}
		for i in range(126):
			finish[i]='dull'

	if finish == 'glass':
		finish = {}
		for i in range(126):
			finish[i]='glass'

	if finish == 'metal':
		finish = {}
		for i in range(126):
			finish[i]='metal'

	if finish == 'frosted':
		finish = {}
		for i in range(126):
			finish[i] = 'frosted'

	if finish == 'irid':
		finish = {}
		for i in range(126):
			finish[i] = 'irid'
			
	if finish == 'billiard':
		finish = {}
		for i in range(126):
			finish[i] = 'billiard'

	if width_height_ratio == 0.0:
		width_height_ratio = float(width)/float(height)
		#for side by side stereo 3D, set the width_height_ratio to that of the target screen. For example 1920x1200,   width_height_ratio = 16/10 = 1.6 
		# you would then set image for each eye to be width/2 for interlacing screens. This is how you should do it for the Sony KDL 70R55A.


	if radius == {}:
		radius = element_radius_list


	if camera_position == []:
		# this is guessing for you, assumes you have no idea what the camera position is
		maxy = max(sys.y)
		miny = min(sys.y)
		maxz = max(sys.z)
		minz = min(sys.z)
		maxx = max(sys.x)
		midy = (maxy+miny)/2		
		midz = (maxz+minz)/2
		camera_position = [0,0,0] 
		camera_offset = x_offset*max( [abs(maxy-miny), abs(maxz-minz)])

		camera_position[0] = camera_offset + maxx
		camera_position[1] = midy
		camera_position[2] = midz
		light_offset = camera_offset*4.0/3.0 
		light_position = [maxx + light_offset, maxy + light_offset/3.0, maxz + light_offset/3.0]
		camera_look_at_point = [maxx,midy,midz]
		if verbose:
			print 'write_POV: Guessed these things:'
			print 'write_POV: camera_position : ' , camera_position
			print 'write_POV: camera_look_at_point : ', camera_look_at_point

	if camera_style == '':
		camera_style = 'perspective'  
		# the options are:   perspective | orthographic | fisheye | ultra_wide_angle | omnimax | panoramic | cylinder CylinderType | spherical
		# for orthographic you might have to use 'orthographic angle 30' or similar angle
	if verbose:
		print 'write_POV: camera_style : ', camera_style

	if file_name=='':
		file_name = sys.file_name + '.pov'
	if file_name=='':
		file_name = sys.comment + '.pov'
	if file_name == '':
		print 'write_POV: You did not give me a file_name and I tried to guess one, but it did not work.'

	######################### start of file creation
	fid = open(file_name,'w')
	if verbose:
		print 'write_POV: Creating Header...'


	##### new camera positioner
	Uin = up_direction #up direction
	L = sys.vector_vector_subtract_3D(camera_look_at_point,camera_position) #look direction vector
	A = -width_height_ratio #basically aspect ratio, negative sign for the silly coordinate system

	R = sys.unit_vector_3D(sys.cross_product_3D(L,Uin))
	Ruse = [A*R[0], A*R[1], A*R[2]]
	Uuse = sys.unit_vector_3D(sys.cross_product_3D(R,L)) #just makes sure its perpendicular, might have been off before

	###fucking povray and its gimbal issue
	Lhat = sys.unit_vector_3D(L) #tracking within 1degree of poles
	from math import asin, sqrt, pi
	safe_input = min( sqrt(Lhat[0]*Lhat[0]+Lhat[2]*Lhat[2]), 1.0)
	gimbal_angle = 180/pi*asin(safe_input)
	if gimbal_angle < 0.1:
		print "write_POV: WARNING!, POV-Ray has GIMBAL LOCK ISSUES if the look direction is purely in the Y direction!"

	#################
	# +'global_settings { assumed_gamma 1.0 }\n' \ #adding the gamma makes it too bright, adding the verision removes the background transparency 
	#header = '#version 3.7;\n' \
	#header = 'global_settings { assumed_gamma 1.0 }\n' \
	header = '\n' \
		+ '\n' \
		+ 'background { color rgb <%f, %f, %f> }\n\n' % (background_color[0], background_color[1], background_color[2]) \
		+ '\ncamera\n' \
		+ '{\n' \
		+ ' %s\n'%camera_style \
		+ ' location <%f, %f, %f>\n' % (camera_position[0], camera_position[1], camera_position[2])  \
		+ '  look_at <%f, %f, %f>\n' % (camera_look_at_point[0], camera_look_at_point[1], camera_look_at_point[2] ) \
		+ '  up <%f, %f, %f>\n'    %(Uuse[0], Uuse[1], Uuse[2]) \
		+ '  sky <%f, %f, %f>\n'  %(Uuse[0], Uuse[1], Uuse[2]) \
		+ '  right <%f, %f, %f>\n' %(Ruse[0], Ruse[1], Ruse[2]) \
		+ '}\n\n' 
	if write_lights:
		if shadowless:
			header = header + 'light_source { <%s, %s, %s> color rgb <1.0,1.0,1.0> \n shadowless }\n\n' %(light_position[0], light_position[1], light_position[2])
		else:
			header = header + 'light_source { <%s, %s, %s> color rgb <1.0,1.0,1.0>}\n\n' %(light_position[0], light_position[1], light_position[2])

		if light_at_camera == True:
			if shadowless:
				header = header + 'light_source { <%s, %s, %s> color rgb <0.75,0.75,0.75> \n shadowless }\n\n' %(camera_position[0], camera_position[1], camera_position[2])
			else:
				header + 'light_source { <%s, %s, %s> color rgb <0.75,0.75,0.75>}\n\n' %(camera_position[0], camera_position[1], camera_position[2])

	####### end of options !

	sphere_body = ''
	if verbose:
		print 'write_POV: Creating Spheres...'


	for i in range(sys.number_of_atoms):
		
		sph = ''
		sph = sph + '\nsphere {\n'
		sph = sph + ' <%s, %s, %s>, %s\n' % (sys.x[i], sys.y[i], sys.z[i], radius_scale*radius[sys.atomic_number[i]] )
		sph = sph + '\n'
	    

		if show_charge == True:
			if sys.q[i] >= 0.0:
				color = interpolate_color(sys.q[i], 0.0, qzero_color, qmax ,qmax_color)
			else:
				color = interpolate_color(sys.q[i], 0.0, qzero_color, qmin ,qmin_color)
		else:
			color = coloring[sys.atomic_number[i]]

		thisfinish = finish[sys.atomic_number[i]]

		if thisfinish == 'glass':

			sph = sph + '  pigment { color rgbf <%s,%s,%s,0.95> }\n' %(color[0],color[1],color[2])
			sph = sph + '  finish { phong 0.8 \n'
			sph = sph + '  reflection 0.2} \n'
			sph = sph + ' interior{ ior 1.5} \n'

		elif thisfinish == 'metal':

			sph = sph + '  pigment { color rgb <%s,%s,%s> }\n' %(color[0],color[1],color[2])
			sph = sph + '  finish { ambient 0.1 \n'
			sph = sph + '  diffuse 0.1 \n'
			sph = sph + '  specular 1.0\n'
			sph = sph + '  roughness 0.001\n'
			sph = sph + '  metallic\n'
			#sph = sph + '   \n'
			sph = sph + '  reflection{ 0.5 metallic}} \n' 

		elif thisfinish == 'frosted':

			sph = sph + '  pigment { color rgbf <%s,%s,%s,0.7> }\n' %(color[0],color[1],color[2])
			sph = sph + '  finish { phong 0.7 \n'
			sph = sph + '  reflection 0.2 \n'
			sph = sph + '  roughness 0.2}\n'
			sph = sph + ' interior{ ior 1.5} \n'

		elif thisfinish == 'irid':

			sph = sph + '  pigment { color rgbf <%s,%s,%s,0.1> }\n' %(color[0],color[1],color[2])
			sph = sph + '  finish { phong 0.7 \n'
			sph = sph + '  reflection {0.2 metallic} \n'
			sph = sph + '  diffuse 0.1\n'
			sph = sph + '  irid {0.75 thickness 0.5 turbulence 0.2}} \n'
			sph = sph + ' interior{ ior 1.5} \n'

		elif thisfinish == 'billiard':

			sph = sph + '  pigment { color rgb <%s,%s,%s> }\n' %(color[0]/BBRR,color[1]/BBRR,color[2]/BBRR)
			sph = sph + '  finish { ambient 0.8 diffuse 1 specular 1 roughness 0.005 metallic 0.5} \n'

		else: 

			sph = sph + '  pigment { color rgb <%s,%s,%s> }\n' %(color[0],color[1],color[2])
			
		
		sph = sph + '}\n'
		sphere_body = sphere_body + sph


	sphere_body = sphere_body + '\n'


	fid.write(header  + sphere_body)


########## bonds section
	if with_bonds == True:
		if verbose:
			print 'write_POV: Creating Bond Cylinders...'
		cylinder_body = ''
		sys.sort_by('id')

		#Need to come up with a good if statement here
		sys.update_internal_from_external()
			
			
		for bond in sys.bonds:
			id1 = bond[2]
			id2 = bond[3]
			index1 = sys.get_index_with_binary_search_from_id(id1)
			index2 = sys.get_index_with_binary_search_from_id(id2)

			r1 = radius_scale*radius[sys.atomic_number[index1]]
			r2 = radius_scale*radius[sys.atomic_number[index2]] 
			midx = (sys.x[index2] - sys.x[index1]) * r1/(r1+r2) + sys.x[index1] # this nicely centers the bond mid point between the atoms
			midy = (sys.y[index2] - sys.y[index1]) * r1/(r1+r2) + sys.y[index1]
			midz = (sys.z[index2] - sys.z[index1]) * r1/(r1+r2) + sys.z[index1]
			rmax = max([r1,r2])	
			rmin = min([r1,r2])
			from math import exp
			cylinder_radius = bond_scale*rmin*(2.0-exp(-(rmax/rmin)/2.0))/2.0  # this is a function mike came up with to make pretty bond widths

			draw_bond = True
			#sys.update_lengths_from_box()			

			absdx = abs(sys.sx[index2] - sys.sx[index1])
			absdy = abs(sys.sy[index2] - sys.sy[index1])
			absdz = abs(sys.sz[index2] - sys.sz[index1])
			if sys.periodic[0] == True:
			#	absdx = abs(sys.sx[index2] - sys.sx[index1])
				if absdx > (0.5):
					draw_bond = False
			if sys.periodic[1] == True:
			#	absdy = abs(sys.sy[index2] - sys.sy[index1])
				if absdy > (0.5):
					draw_bond = False
			if sys.periodic[2] == True:
			#	absdz = abs(sys.sz[index2] - sys.sz[index1])
				if absdz > (0.5):
					draw_bond = False

			if absdx == 0.0 and absdy == 0.0 and absdz == 0.0:
				print 'write_POV: Warning, bond with no length skipped.'
				draw_bond = False



			if draw_bond == True:
				#### cylinder 1
				color1 = coloring[sys.atomic_number[index1]]
				if show_charge:
					if sys.q[index1] >= 0.0:
						color1 = interpolate_color(sys.q[index1], 0.0, qzero_color, qmax ,qmax_color)
					else:
						color1 = interpolate_color(sys.q[index1], 0.0, qzero_color, qmin ,qmin_color)
				thisfinish1 = finish[sys.atomic_number[index1]]
				cyl = ''
				cyl = cyl + '\ncylinder {\n'
				cyl = cyl + '<%s, %s, %s>, <%s, %s, %s>, %s\n' % (sys.x[index1], sys.y[index1], sys.z[index1], midx, midy, midz, cylinder_radius )
				cyl = cyl + '\n'
				
				if thisfinish1 == 'glass':
				
					cyl = cyl + '  pigment { color rgbf <%s,%s,%s,0.95> }\n' %(color1[0],color1[1],color1[2])
					cyl = cyl + '  finish { phong 0.8 \n'
					cyl = cyl + '  reflection 0.2} \n'
					cyl = cyl + ' interior{ ior 1.5} \n'
					
				elif thisfinish1 == 'metal':
					cyl = cyl + '  pigment { color rgb <%s,%s,%s> }\n' %(color1[0],color1[1],color1[2])
					cyl = cyl + '  finish { ambient 0.1 \n'
					cyl = cyl + '  diffuse 0.1 \n'
					cyl = cyl + '  specular 1.0\n'
					cyl = cyl + '  roughness 0.001\n'
					cyl = cyl + '  metallic\n'
					cyl = cyl + '  reflection{ 0.5 metallic}} \n' 
					
				elif thisfinish1 == 'frosted':
					cyl = cyl + '  pigment { color rgbf <%s,%s,%s,0.7> }\n' %(color1[0],color1[1],color1[2])
					cyl = cyl + '  finish { phong 0.7 \n'
					cyl = cyl + '  reflection 0.2 \n'
					cyl = cyl + '  roughness 0.2}\n'
					cyl = cyl + ' interior{ ior 1.5} \n'

				elif thisfinish == 'irid':

					cyl = cyl + '  pigment { color rgbf <%s,%s,%s,0.1> }\n' %(color1[0],color1[1],color1[2])
					cyl = cyl + '  finish { phong 0.7 \n'
					cyl = cyl + '  reflection {0.2 metallic} \n'
					cyl = cyl + '  diffuse 0.3\n'
					cyl = cyl + '  irid {0.75 thickness 0.5 turbulence 0.5}} \n'
					cyl = cyl + ' interior{ ior 1.5} \n'
					
				elif thisfinish == 'billiard':

					cyl = cyl + '  pigment { color rgb <%s,%s,%s> }\n' %(color1[0]/BBRR,color1[1]/BBRR,color1[2]/BBRR)
					cyl = cyl + '  finish { ambient 0.8 diffuse 1 specular 1 roughness 0.005 metallic 0.5} \n'

				else: 
					cyl = cyl + '  pigment { color rgb <%s,%s,%s> }\n' %(color1[0],color1[1],color1[2])
					
				cyl = cyl + '}\n'
				cylinder_body = cylinder_body + cyl 

				### cylinder 2
				color2 = coloring[sys.atomic_number[index2]]
				if show_charge:
					if sys.q[index2] >= 0.0:
						color2 = interpolate_color(sys.q[index2], 0.0, qzero_color, qmax ,qmax_color)
					else:
						color2 = interpolate_color(sys.q[index2], 0.0, qzero_color, qmin ,qmin_color)
				thisfinish2 = finish[sys.atomic_number[index2]]
				cyl = ''
				cyl = cyl + '\ncylinder {\n'
				cyl = cyl + '<%s, %s, %s>, <%s, %s, %s>, %s\n' % (sys.x[index2], sys.y[index2], sys.z[index2], midx, midy, midz, cylinder_radius )
				cyl = cyl + '\n'
				if thisfinish2 == 'glass':
					cyl = cyl + '  pigment { color rgbf <%s,%s,%s,0.95> }\n' %(color2[0],color2[1],color2[2])
					cyl = cyl + '  finish { phong 0.8 \n'
					cyl = cyl + '  reflection 0.2} \n'
					cyl = cyl + ' interior{ ior 1.5} \n'
				elif thisfinish2 == 'metal':
					cyl = cyl + '  pigment { color rgb <%s,%s,%s> }\n' %(color2[0],color2[1],color2[2])
					cyl = cyl + '  finish { ambient 0.1 \n'
					cyl = cyl + '  diffuse 0.1 \n'
					cyl = cyl + '  specular 1.0\n'
					cyl = cyl + '  roughness 0.001\n'
					cyl = cyl + '  metallic\n'
					cyl = cyl + '  reflection{ 0.5 metallic}} \n' 
				elif thisfinish2 == 'frosted':
					cyl = cyl + '  pigment { color rgbf <%s,%s,%s,0.7> }\n' %(color2[0],color2[1],color2[2])
					cyl = cyl + '  finish { phong 0.7 \n'
					cyl = cyl + '  reflection 0.2 \n'
					cyl = cyl + '  roughness 0.2}\n'
					cyl = cyl + ' interior{ ior 1.5} \n'
				elif thisfinish == 'irid':

					cyl = cyl + '  pigment { color rgbf <%s,%s,%s,0.1> }\n' %(color2[0],color2[1],color2[2])
					cyl = cyl + '  finish { phong 0.7 \n'
					cyl = cyl + '  reflection {0.2 metallic} \n'
					cyl = cyl + '  diffuse 0.3\n'
					cyl = cyl + '  irid {0.75 thickness 0.5 turbulence 0.5}} \n'
					cyl = cyl + ' interior{ ior 1.5} \n'
					
				elif thisfinish == 'billiard':

					cyl = cyl + '  pigment { color rgb <%s,%s,%s> }\n' %(color2[0]/BBRR, color2[1]/BBRR, color2[2]/BBRR)
					cyl = cyl + '  finish { ambient 0.8 diffuse 1 specular 1 roughness 0.005 metallic 0.5} \n'
					
				else: 
					cyl = cyl + '  pigment { color rgb <%s,%s,%s> }\n' %(color2[0],color2[1],color2[2])
				cyl = cyl + '}\n'
				cylinder_body = cylinder_body + cyl 

		fid.write(cylinder_body)

	###### draw box section
	if draw_box:
		if verbose:
			print 'write_POV: Creating Box Cylinders and Corner Spheres...'
		if sys.box_mode == 'orthorhombic':
			sys.update_basis_and_origin_for_orthorhombic()
		rlist = []
		from math import sqrt
		for i in range(3):
			rlist.append( sys.dot_product_3D(sys.get_basis_vector(i), sys.get_basis_vector(i)) )
		r_box_cyl = sqrt(min(rlist))* 0.01 *box_scale
	
		cylinder_list = [
			[[0,0,0],[1,0,0]], # x direction
			[[0,0,1],[1,0,1]],
			[[0,1,0],[1,1,0]],
			[[0,1,1],[1,1,1]], 
			[[0,0,0],[0,1,0]], # y direction
			[[0,0,1],[0,1,1]],
			[[1,0,0],[1,1,0]],
			[[1,0,1],[1,1,1]],
			[[0,0,0],[0,0,1]], # Z direction
			[[0,1,0],[0,1,1]],
			[[1,0,0],[1,0,1]],
			[[1,1,0],[1,1,1]]]
		for i in range(12):
			for endpoint in range(2):
				point = cylinder_list[i][endpoint]
				cylinder_list[i][endpoint] =  sys.vector_vector_add_3D(sys.matrix_vector_product_3D(sys.basis, point) , sys.origin )
		for cylinder in cylinder_list:
			start = cylinder[0]	
			end = cylinder[1]
			cyl = ''
			cyl = cyl + '\ncylinder {\n'
			cyl = cyl + '<%s, %s, %s>, <%s, %s, %s>, %s\n' % (start[0], start[1], start[2], end[0], end[1], end[2], r_box_cyl )
			cyl = cyl + '\n'
			cyl = cyl + '  pigment { color rgb <%s,%s,%s> }\n' % (box_color[0], box_color[1], box_color[2])
			cyl = cyl + '}\n'
			
			fid.write(cyl)

		#nice end caps,grabs from cylinder_list
		for i in range(4):
			for j in range(2):
				center = cylinder_list[i][j]

				sph = '\nsphere {\n'
				sph = sph + ' <%s, %s, %s>, %s\n' % (center[0], center[1], center[2], r_box_cyl )
				sph = sph + '  pigment { color rgb <%s,%s,%s> }\n}\n' % (box_color[0], box_color[1], box_color[2])
				fid.write(sph)
	fid.close()

	###### this last section only maters if the render option is selected 
	if image_name == '':
		image_name = (file_name.split('.')[0]) + '.png'

	if verbose:
		print 'write_POV: POV-Ray file name : ', file_name
		print 'write_POV: PNG image file name : ', image_name

	if render == True:
		from os import system

		#i dont think we need lowered process priority
		#command = 'nice -n 5 povray Input_File_Name=%s Output_File_Name=%s +H%i +W%i' % (file_name, image_name, height, width)

		command = 'povray Input_File_Name=%s Output_File_Name=%s +H%i +W%i' % (file_name, image_name, height, width)

		if display: #Turns off render progress
			command = command + ' Display=on'
		else:
			command = command + ' Display=off'

		if transparent: #sets the background to be transparent, reflections of the background will still be the specified color.
			command = command + ' +ua'
		
		if antialias: #sets antialiasing on, it is adaptive and uses default values. It's a complex system in povray, read their documentation for more info.
			command = command + ' +A'

		### post processing in Imagemagick
		if rotate != 0:
			rotate_command = 'convert -rotate %i %s %s ' % (rotate, image_name, image_name)
			command = command + "&&" + rotate_command
		# Rotate command goes before comments, so that they are the right way up.
		if comment !='':
			comment_command = "convert %s  -fill white  -undercolor '#00000090'  -gravity Northwest -pointsize %i -annotate +5+5 ' %s ' %s" % (image_name,comment_size,comment,image_name)
			command = command + ' && ' + comment_command
			

		if verbose:
			div = '------------------------------------------'
			print 'write_POV: %s\nSystem command to be executed : %s\n%s' %(div,command,div)

		#### EXECUTES HERE ####
		system(command)

		if verbose:
			div = '------------------------------------------'
			print 'write_POV: %s\nThe system command that was just executed : %s\n%s' %(div,command,div)

		if gimbal_angle < 0.1:
			print "write_POV: WARNING!, POV-Ray has GIMBAL LOCK ISSUES if the look direction is purely in the Y direction!"


	return file_name
