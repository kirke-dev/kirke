# yo this is a mike function...

def write_POSCAR(sys, file_name = 'POSCAR', use_selective_dynamics = False, coordinate_mode = 'Direct' ,verbose = True):
	''' Writes POSCAR files used by VASP'''

		
	if sys.box_mode == 'orthorhombic' or sys.box_mode == 'triclinic':
		if sys.box_mode == 'orthorhombic':
			sys.update_basis_and_origin_for_orthorhombic()
		if len(sys.sx)!=sys.number_of_atoms:
			sys.fill_s_lists()
			sys.update_internal_from_external()
		if verbose:
			print 'write_POSCAR: This function sorts and then writes atoms in order of atomic number.'
		
		fid = open(file_name,'w')
		comment_line = sys.comment+'\n'
		fid.write(comment_line)
		
		scale_line = ' %0.10f\n' % (1.0000) # leaving scale factor as 1.0
		fid.write(scale_line)
		
		for i in range(3): # writting basis
			v = sys.get_basis_vector(i)
			for j in range(3):
				fid.write(' %.10f '%v[j])
			fid.write('\n')
			
		######################################
		number_of_each_type = []
		sorted_z_list = sorted(list(set(sys.atomic_number)))
		
		#writes down the elements
		from kirke.elements import element_symbol_list
		element_string = ''
		for z in sorted_z_list:
			element_string = element_string + ' %s'% element_symbol_list[z]
		fid.write(element_string + '\n')
		
		#writes down the number of each element
		for z in sorted_z_list:
			number_of_each_type.append(sys.atomic_number.count(z))
		for count in number_of_each_type:
			fid.write(' %i'%count)
		fid.write('\n')
		
		
		#must be in z order to match header
		sys.sort_by('atomic_number') # Magic happens here!
		
		################
		
		if use_selective_dynamics:
			if sys.number_of_atoms == len(sys.selective_dynamics):
				fid.write('Selective dynamics\n')
			else:
				print 'write_POSCAR: Your number of atoms and length of you selective dynamics list do not match!'
		
		
		trans_dict = {True : ' T', False : ' F' }
			
			
		##########
		
		if coordinate_mode == 'Direct':
			fid.write('Direct\n')	
			for i in range(sys.number_of_atoms):
				fid.write(' %.10f %.10f %.10f' %(sys.sx[i],sys.sy[i],sys.sz[i]))
				if use_selective_dynamics:
				
					dyn_string = ''
					for j in range(3):
						dyn_string = dyn_string + trans_dict[sys.selective_dynamics[i][j]]
					fid.write(dyn_string)
					
				fid.write('\n')
		else:
			fid.write('Cartesian\n')	
			for i in range(sys.number_of_atoms):
				fid.write(' %.10f %.10f %.10f' %(sys.x[i],sys.y[i],sys.z[i]))
				
				if use_selective_dynamics:
					dyn_string = ''
					for j in range(3):
						dyn_string = dyn_string + trans_dict[sys.selective_dynamics[i][j]]
					fid.write(dyn_string)
					
				fid.write('\n')
				
		
		fid.close()
		
	else:
		print 'write_POSCAR: You do not have a defined box, This function cannot operate without one.'
	
		
	
