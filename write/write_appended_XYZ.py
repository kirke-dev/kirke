#!/usr/bin/env python

def write_appended_XYZ(frames,file_name = '',with_charge = False, use_element = False): #Added use_element option
	''' Writes an appended XYZ file with multiple frames. '''
	if  file_name=='':
		file_name = frames[0].file_name + '.xyz'
	
	fid = open(file_name,'w')
	
	for i in range(len(frames)):
	
		sys = frames[i]
		if i==0:
			header = '%s\nAtoms' %  sys.number_of_atoms
		else:
			header = '\n%s\nAtoms' %  sys.number_of_atoms
		ws = ''

		for i in range(sys.number_of_atoms):
			if use_element ==True:
				line = '\n%s\t%s\t%s\t%s' % (sys.element[i],sys.x[i],sys.y[i],sys.z[i])
			else:
				line = '\n%s\t%s\t%s\t%s' % (sys.atomic_number[i],sys.x[i],sys.y[i],sys.z[i])

			if with_charge == True:
			
				line = line + '\t%s' % sys.q[i]
			ws = ws + line
		
		fid.write(header+ws)

	fid.close()

	return file_name
