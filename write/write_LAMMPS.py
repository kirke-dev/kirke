#!/usr/bin/env python

#Modified 11-13-12 to include atom_style support
#Modified 12 Feb 2013 to default to use of atomic_number
#Modified 18 March 13 to accomodate the atomistic attribute treatment of masses
#Modified 7 June 2013 for Kirke overhaul
def write_LAMMPS(sys,file_name='',element_to_number = {} ,box =  [[],[],[]],  padding = [0.0,0.0,0.0], use_element = False, atom_style = []):
	''' Writes a LAMMPS read_data file. Automatically fills box if it empty. '''
	from kirke.elements import element_symbol_list, element_mass_list
	from kirke.update_other import convert_element_to_number

	if file_name == '':
		file_name = sys.file_name + '.lammps'

	if len(sys.id) != sys.number_of_atoms:
		sys.fill_id_list()

	if len(sys.molecule) != sys.number_of_atoms:
		sys.fill_molecule_list()

	if sys.box == [[],[],[]] and sys.box_mode != 'triclinic':
		sys.box = sys.update_box_from_positions(padding = padding) 

	box_flag = False
	if sys.box_mode == 'triclinic' and sys.basis == []:
		print "write_LAMMPS: box will not be automatically written, information is missing. The rest of the file will continue as normal."
		box_flag = True
	
	if sys.box_mode == 'triclinic' and sys.basis != []:
		#Making sure everything is rotated to agree with the LAMMPS paralell piped descriptions
		ans = sys.compute_lengths_and_angles_from_basis(verbose = False)
		sys.update_basis_from_lengths_and_angles(ans[0][0],ans[0][1],ans[0][2], alpha = ans[1][0], beta = ans[1][1] , gamma = ans[1][2])
	if atom_style == []:
		atom_style = 'full'

	unique_element_list = list(set(sys.element))
	unique_atomic_number_list = list(set(sys.atomic_number))

	if use_element == True:
		if element_to_number == {}: # creates the handy dict to go from element to lammps 
			Natom_types = len(unique_element_list)
			i = 1
			for element in unique_element_list: 
				element_to_number.update({element:i})
				i += 1
		elif sys.pair_coeffs != []:
			Natom_types = 0
			for a in sys.pair_coeffs:
				Natom_types +=1
		else:
			Natom_types = len(element_to_number)
		sys.convert_element_to_number(element_to_number) #Uses the handy dictionary so we can just directly call atomic_number later

	else:
		if element_to_number != {}:
			Natom_types = len(element_to_number)
		elif sys.pair_coeffs != []:
			Natom_types = 0
			for a in sys.pair_coeffs:
				Natom_types +=1
		else:
			Natom_types = max(unique_atomic_number_list)#modified to assume consecutive number, even if unoccupied masses 18 March 13

	#------------ start of creating file
	write_line_list = []
	if sys.comment == '':
		comment = sys.file_name + ' was the source file for this automatic conversion from xyz to lammps input'
	elif '\n' in sys.comment:
		temp = sys.comment.split()
		comment = ''
		for t in temp:
			comment+=t+' '
	else:
		comment = sys.comment

	write_line_list.append(comment + '\n')

	#---------- numbers of things
	write_line_list.append('\n')
	write_line_list.append('%i atoms\n' % sys.number_of_atoms)
	if sys.bonds != []:
		write_line_list.append('%i bonds\n' % len(sys.bonds))

	if sys.angles != []:
		write_line_list.append('%i angles\n'% len(sys.angles))

	if sys.dihedrals != []:
		write_line_list.append('%i dihedrals\n' % len(sys.dihedrals))

	if sys.impropers != []:
		write_line_list.append('%i impropers\n'% len(sys.impropers))

	#---------- types of things
	write_line_list.append('\n')
	write_line_list.append('%i atom types\n' % Natom_types)
	if sys.bond_coeffs != []:
		write_line_list.append('%i bond types\n' % len(sys.bond_coeffs))

	if sys.angle_coeffs != []:
		write_line_list.append('%i angle types\n' % len(sys.angle_coeffs))

	if sys.dihedral_coeffs != []:
		write_line_list.append('%i dihedral types\n' % len(sys.dihedral_coeffs))

	if sys.improper_coeffs != []:
		write_line_list.append('%i improper types\n' % len(sys.improper_coeffs))

	#--------- Box

	if sys.box_mode =='orthorhombic' or sys.box_mode == '':
		write_line_list.append('\n')
		write_line_list.append('%f %f xlo xhi\n' % (sys.box[0][0], sys.box[0][1]) )
		write_line_list.append('%f %f ylo yhi\n' % (sys.box[1][0], sys.box[1][1]) )
		write_line_list.append('%f %f zlo zhi\n' % (sys.box[2][0], sys.box[2][1]) )

	elif sys.box_mode == 'triclinic' and box_flag == False:
		write_line_list.append('\n')
		write_line_list.append('%f %f xlo xhi\n' % (sys.origin[0], sys.basis[0][0]+sys.origin[0]) )
		write_line_list.append('%f %f ylo yhi\n' % (sys.origin[1], sys.basis[1][1]+sys.origin[1]) )
		write_line_list.append('%f %f zlo zhi\n' % (sys.origin[2], sys.basis[2][2]+sys.origin[2]) )
		write_line_list.append('%f %f %f xy xz yz\n' % (sys.basis[1][0], sys.basis[2][0], sys.basis[2][1]))

	#---------- Masses
	write_line_list.append('\n')
	write_line_list.append('Masses\n')
	write_line_list.append('\n')

	if use_element ==True or sys.masses == []:
		for element in unique_element_list:
			atomic_number = element_symbol_list.index(element)
			mass = element_mass_list[atomic_number]
			write_line_list.append('%i %g\n' % (element_to_number[element], mass))
	else: #MODIFIED 18 MARCH 2013
		if sys.type_masses == []:
			sys.update_type_masses()
		
		for massline in sys.type_masses:
			write_line_list.append('%i %g\n' % (massline[0], massline[1]))
	
	#---------- pair Coeffs
	if sys.pair_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('Pair Coeffs\n')
		write_line_list.append('\n')
		for a in sys.pair_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%f' % b
			write_line_list.append(line_str + '\n')

	#---------- Bond Coeffs
	if sys.bond_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('Bond Coeffs\n')
		write_line_list.append('\n')
		for a in sys.bond_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%f' % b
			write_line_list.append(line_str + '\n')

	#---------- angle Coeffs
	if sys.angle_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('Angle Coeffs\n')
		write_line_list.append('\n')
		for a in sys.angle_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%f' % b
			write_line_list.append(line_str + '\n')

	#---------- Dihedral Coeffs
	if sys.dihedral_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('Dihedral Coeffs\n')
		write_line_list.append('\n')
		for a in sys.dihedral_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')

	#---------- improper Coeffs
	if sys.improper_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('Improper Coeffs\n')
		write_line_list.append('\n')
		for a in sys.improper_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')
######### Begin Katie additions for COMPASS parameter accomodations 7/23/12

	#---------BondBond coeffs
	if sys.bondbond_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('BondBond Coeffs\n')
		write_line_list.append('\n')
		for a in sys.bondbond_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')

	#---------BondAngle coeffs
	if sys.bondangle_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('BondAngle Coeffs\n')
		write_line_list.append('\n')
		for a in sys.bondangle_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')

	#---------MiddleBondTorsion Coeffs
	if sys.middlebondtorsion_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('MiddleBondTorsion Coeffs\n')
		write_line_list.append('\n')
		for a in sys.middlebondtorsion_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')
	
	#---------EndBondTorsion Coeffs
	if sys.endbondtorsion_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('EndBondTorsion Coeffs\n')
		write_line_list.append('\n')
		for a in sys.endbondtorsion_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')
	#---------AngleTorsion Coeffs
	if sys.angletorsion_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('AngleTorsion Coeffs\n')
		write_line_list.append('\n')
		for a in sys.angletorsion_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')
	#---------AngleAngleTorsion Coeffs
	if sys.angleangletorsion_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('AngleAngleTorsion Coeffs\n')
		write_line_list.append('\n')
		for a in sys.angleangletorsion_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')
	#---------BondBond13Coeffs
	if sys.bondbond13_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('BondBond13 Coeffs\n')
		write_line_list.append('\n')
		for a in sys.bondbond13_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')

	#--------AngleAngle Coeffs
	if sys.angleangle_coeffs != []:
		write_line_list.append('\n')
		write_line_list.append('AngleAngle Coeffs\n')
		write_line_list.append('\n')
		for a in sys.angleangle_coeffs:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%s' % b
			write_line_list.append(line_str + '\n')

########End Katie additions 7/23/12
#Default to type: This is where I'm making the most changes, because it uses the damn element_to_number dictionary instead of atomic_number for every single atom_style. It should work for use_element = True:lammps expects atom_type to be from 1 to Ntype, but we've hopefully taken care of that with the automatic or manual translation dictionary above. 
	#---------- atoms  
	write_line_list.append('\n')
	write_line_list.append('Atoms\n')
	write_line_list.append('\n')
	nxyz = '\n'


	for i in range(sys.number_of_atoms):
		if atom_style == ([] or 'full'):
			atom_style_list = '%i\t %i\t %i\t %f\t %f\t %f\t %f' %(sys.id[i], sys.molecule[i], sys.atomic_number[i],sys.q[i], sys.x[i],sys.y[i], sys.z[i])
	
		elif atom_style == 'angle' or atom_style == 'bond' or atom_style == 'molecular':
			atom_style_list = '%i\t %i\t %i\t  %f\t %f\t %f' %(sys.id[i], sys.molecule[i], sys.atomic_number[i], sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'charge':
			atom_style_list = '%i\t  %i\t %f\t %f\t %f\t %f' %(sys.id[i], sys.atomic_number[i],sys.q[i], sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'atomic':
			atom_style_list = '%i\t %i\t  %f\t %f\t %f' %(sys.id[i],  sys.atomic_number[i],sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'dipole':
			atom_style_list = '%i\t  %i\t %f\t %f\t %f\t %f \t%f \t%f \t%f' %(sys.id[i], sys.atomic_number[i],sys.q[i], sys.x[i],sys.y[i], sys.z[i], sys.mux[i],sys.muy[i], sys.muz[i])

		elif atom_style == 'electron':
			atom_style_list = '%i\t  %i\t %f\t %f\t %f\t %f \t%f  \t%f' %(sys.id[i], sys.atomic_number[i],sys.q[i], sys.spin[i], sys.eradius[i], sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'ellipsoid':
			atom_style_list = '%i\t  %i\t %i\t  %f\t %f \t%f  \t%f' %(sys.id[i], sys.atomic_number[i], sys.ellipsoidflag[i], sys.density[i], sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'line':
			atom_style_list = '%i\t  %i\t %i\t %i\t %f\t %f \t%f  \t%f' %(sys.id[i],sys.molecule[i], sys.atomic_number[i],sys.lineflag[i], sys.density[i], sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'meso':
			atom_style_list = '%i\t  %i\t %f\t %f\t %f\t %f \t%f \t%f' %(sys.id[i], sys.atomic_number[i],sys.rho[i], sys.e[i], sys.cv[i], sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'peri':
			atom_style_list = '%i\t  %i\t %f\t %f\t %f \t%f  \t%f' %(sys.id[i], sys.atomic_number[i],sys.volume[i], sys.density[i],sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'sphere':
			atom_style_list = '%i\t  %i\t %f\t %f\t %f \t%f  \t%f' %(sys.id[i], sys.atomic_number[i],sys.diameter[i], sys.density[i],sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'tri':
			atom_style_list = '%i\t %i\t %i\t %i\t %f\t %f \t%f  \t%f' %(sys.id[i],sys.molecule[i], sys.atomic_number[i],sys.triangleflag[i], sys.density[i],sys.x[i],sys.y[i], sys.z[i])

		elif atom_style == 'wavepacket':
			atom_style_list = '%i \t%i \t%f \t%f \t%f \t%f \t%f \t%f \t%f \t%f \t%f' %(sys.id[i], sys.atomic_number[i], sys.q[i], sys.spin[i], sys.eradius[i], sys.etag[i], sys.cs_re[i], sys.cs_im[i], sys.x[i],sys.y[i], sys.z[i])

		if len(sys.nx) == sys.number_of_atoms:
			nxyz = '\t%i\t%i\t%i\n' % (sys.nx[i],sys.ny[i], sys.nz[i])
		write_line_list.append(atom_style_list + nxyz)

	#---------- velocities
	
	if len(sys.vx) == sys.number_of_atoms:
		write_line_list.append('\n')
		write_line_list.append('Velocities\n')
		write_line_list.append('\n')
		for i in range(sys.number_of_atoms):
			write_line_list.append('%i\t%f\t%f\t%f\n' % (sys.id[i], sys.vx[i],sys.vy[i], sys.vz[i]))

	#----------- Bonds
	if sys.bonds != []:
		write_line_list.append('\n')
		write_line_list.append('Bonds\n')
		write_line_list.append('\n')
		for a in sys.bonds:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%i' % b
			write_line_list.append(line_str + '\n')

	#----------- Angles
	if sys.angles != []:
		write_line_list.append('\n')
		write_line_list.append('Angles\n')
		write_line_list.append('\n')
		for a in sys.angles:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%i' % b
			write_line_list.append(line_str + '\n')

	#----------- Dihedrals
	if sys.dihedrals != []:
		write_line_list.append('\n')
		write_line_list.append('Dihedrals\n')
		write_line_list.append('\n')
		for a in sys.dihedrals:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t %i' %b
			write_line_list.append(line_str + '\n')

	#----------- Impropers
	if sys.impropers != []:
		write_line_list.append('\n')
		write_line_list.append('Impropers\n')
		write_line_list.append('\n')
		for a in sys.impropers:
			line_str = '%i' % a[0]
			for b in a[1:]:
				line_str = line_str + '\t%i' % b
			write_line_list.append(line_str + '\n')

	#----------- writting the file
	outputid = open(file_name,'w')
	for a in write_line_list:
		outputid.write(a)
	outputid.close()

	return file_name

