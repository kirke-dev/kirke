#!/usr/bin/env python

def write_TAPE2_sys(sys,file_name = ''):
	''' Writes tape2 files from a GAS class object '''
	print 'write_TAPE2_sys: Please check that your units for velocity and box are in nanometers, not angstroms.'
	if file_name=='':
		file_name = sys.file_name + '.tape2'

	fid=open(file_name,'w')

	#Generating the wrapbody from the GAS object
	sys.update_number_of_atoms()
	sys.update_internal_from_external()
	if sys.wrapbody:
		#Only updating positions and charges
		lines = sys.number_of_atoms*sys.lines_per_atom
		if sys.lines_per_atom == 4:
			print "write_TAPE2_sys: Sorry, haven't gotten this done yet"
		elif sys.lines_per_atom == 5:
			a = 0
			for i in range(0,lines,5):
				sys.wrapbody[i] = [sys.sx[a],sys.sy[a],sys.sz[a],sys.vx[a]]
				sys.wrapbody[i+4][3] = sys.q[a]
				a+=1
	else:
		for i in range(sys.number_of_atoms):
			sys.wrapbody.append([sys.sx[i],sys.sy[i],sys.sz[i],sys.vx[i]])
			sys.wrapbody.append([sys.vy[i],sys.vz[i],sys.ax[i],sys.ay[i]])
			sys.wrapbody.append([sys.az[i],0.0,0.0,0.0])
			sys.wrapbody.append([0.0,0.0,0.0,0.0])
			sys.wrapbody.append([0.0,0.0,sys.q[i]])

	#Generating the tail
	if sys.tail == []:
		sys.tail.append(['0.00000'],['300.0000'],['0.0000'],['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'])
		if sys.box_mode == 'orthorhombic':
			sys.update_box_mode_to_triclinic_from_orthorhombic()
			sys.tail.append(sys.box)
		elif sys.box_mode == 'triclinic':
			sys.tail.append(sys.box)
		elif sys.box_mode == '':
			print 'write_TAPE2_sys: Please define your box mode. The file will autogenerate a box assuming orthorhombic from min and max external coordinates.'
			if sys.box == [[],[],[]]:
				sys.box_mode = 'orthorhombic'
				sys.update_box_from_positions
				sys.update_box_mode_to_triclinic_from_orthorhombic()
				sys.tail.append(sys.box)
			else:
				sys.box_mode == 'orthorhombic'
				sys.update_box_mode_to_triclinic_from_orthorhombic()
				sys.tail.append(sys.box)
		sys.tail.append(['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'],['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'],['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'],['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'], ['0.000000', '0.000000', '0.000000'],['1.000000','0.000000', '0.000000','0.000000'],['0.000000'])
	else:
		if sys.box_mode == 'orthorhombic':
			sys.update_box_mode_to_triclinic_from_orthorhombic()
			sys.tail[6:9]=sys.box
		elif sys.box_mode == 'triclinic':
			sys.tail[6:9]=sys.box
		elif sys.box_mode == '':
			print 'write_TAPE2_sys: Please define your box mode. The file will autogenerate a box assuming orthorhombic from min and max external coordinates.'
			if sys.box == [[],[],[]]:
				sys.box_mode = 'orthorhombic'
				sys.update_box_from_positions
				sys.update_box_mode_to_triclinic_from_orthorhombic()
				sys.tail[6:9]=sys.box
			else:
				sys.box_mode = 'orthorhombic'
				sys.update_box_mode_to_triclinic_from_orthorhombic()
				sys.tail[6:9]=sys.box


	#writes the atoms using the wrapbody, if you modifiy the body make sure to update the wrapbody
	for a in sys.wrapbody:

		ws = ''
		for b in a: 
			ws+= '%2.10e' % b + ' '
		ws=ws+'\n'
		fid.write(ws)

	#starts tail
	for a in sys.unparced_tail[0:6]:
		fid.write(a)

	#stops tail for the box
	for a in sys.box:
	
		ws = ''
		for b in a: 
			ws+= '%2.10e' % b + ' '
		ws=ws+'\n'
		fid.write(ws)
		
	#continues tail afterbox
	for a in sys.unparced_tail[9:]:
		fid.write(a)


	fid.close()


	return file_name

