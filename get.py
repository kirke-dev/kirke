#!/usr/bin/env python

def get_subset(self,id_list): # modified by mike on 3/28/2012 #Modified by Katie 8/24/12 to fetch box, lx #KS 9/25/12 to maintain periodicity  #Modified 18 March 13 to match use of masses as atomistic attributes
#Modified 3/19/2014 to use id_to_index
	''' Uses the given id_list of atomic IDs to retrieve a GAS object containing the indicated atoms. '''
	from kirke import GAS
	subset = GAS()

	for i in id_list:
		if self.id_to_index == []:
			self.build_index_dict()
		index = self.id_to_index[i]
		#print i, index, self.id[index]
		subset.id.append(self.id[index])
		subset.x.append(self.x[index])
		subset.y.append(self.y[index])
		subset.z.append(self.z[index])
		subset.atomic_number.append(self.atomic_number[index])
		subset.element.append(self.element[index])

		if len(self.q) == self.number_of_atoms:
			subset.q.append(self.q[index])

		if len(self.masses) == self.number_of_atoms:
			subset.masses.append(self.masses[index])

		if len(self.vx) == self.number_of_atoms:
			subset.vx.append(self.vx[index])
			subset.vy.append(self.vy[index])
			subset.vz.append(self.vz[index])

		if len(self.sx) == self.number_of_atoms:
			subset.sx.append(self.sx[index])
			subset.sy.append(self.sy[index])
			subset.sz.append(self.sz[index])

		if len(self.molecule) == self.number_of_atoms:
			subset.molecule.append(self.molecule[index])

		if len(self.nx) == self.number_of_atoms:
			subset.nx.append(self.nx[index])
			subset.ny.append(self.ny[index])
			subset.nz.append(self.nz[index])

		if len(self.selective_dynamics) == self.number_of_atoms:
			subset.selective_dynamics.append(self.selective_dynamics[index])
	subset.box = self.box
	subset.lx = self.lx
	subset.ly = self.ly
	subset.lz = self.lz
	subset.box_mode = self.box_mode
	subset.basis = self.basis
	subset.origin = self.origin
	subset.periodic = self.periodic
	subset.mass = sum(subset.masses)
	subset.update_number_of_atoms()
	return subset

##def get_subset_with_sort(self, id_list): ###Made redundant 3/19/2014
# Created by katie 10 December 2013 to use sort_by('id') instead of index
#	''' Uses the given id_list of atomic IDs to retrieve a GAS object containing the indicated atoms. Only faster for > 750 atom subsets.'''
#	from kirke import GAS
#	subset = GAS()
#	
#	self.sort_by('id')

#	for i in id_list:
#		index = i-1
#		subset.id.append(self.id[index])
#		subset.x.append(self.x[index])
#		subset.y.append(self.y[index])
#		subset.z.append(self.z[index])
#		subset.atomic_number.append(self.atomic_number[index])
#		subset.element.append(self.element[index])
#		subset.q.append(self.q[index])
#		
#		if len(self.masses) == self.number_of_atoms:
#			subset.masses.append(self.masses[index])

#		if len(self.vx) == self.number_of_atoms:
#			subset.vx.append(self.vx[index])
#			subset.vy.append(self.vy[index])
#			subset.vz.append(self.vz[index])

#		if len(self.sx) == self.number_of_atoms:
#			subset.sx.append(self.sx[index])
#			subset.sy.append(self.sy[index])
#			subset.sz.append(self.sz[index])

#		if len(self.molecule) == self.number_of_atoms:
#			subset.molecule.append(self.molecule[index])

#		if len(self.nx) == self.number_of_atoms:
#			subset.nx.append(self.nx[index])
#			subset.ny.append(self.ny[index])
#			subset.nz.append(self.nz[index])
#	subset.box = self.box
#	subset.lx = self.lx
#	subset.ly = self.ly
#	subset.lz = self.lz
#	subset.box_mode = self.box_mode
#	subset.basis = self.basis
#	subset.periodic = self.periodic
#	subset.mass = sum(subset.masses)
#	subset.update_number_of_atoms()
#	return subset


def get_cut(self,distance,dimension,above = True): # added by mike on 3/28/2012
	'''Generates a list of atomic IDs above or below the given distance in the indicated dimension, and retrieves the subset object by referencing get_subset. Call as GAS.get_cut(distance, dimension, above = True). Dimension expected as 'x', 'y', 'z' '''
	new_id_list = []
	if dimension == 'x':
		for i in range(self.number_of_atoms):
			if self.x[i] > distance and above == True:
				new_id_list.append(self.id[i])
			if self.x[i] <= distance and above == False:
				new_id_list.append(self.id[i])
	if dimension == 'y':
		for i in range(self.number_of_atoms):
			if self.y[i] > distance and above == True:
				new_id_list.append(self.id[i])
			if self.y[i] <= distance and above == False:
				new_id_list.append(self.id[i])
	if dimension == 'z':
		for i in range(self.number_of_atoms):
			if self.z[i] > distance and above == True:
				new_id_list.append(self.id[i])
			if self.z[i] <= distance and above == False:
				new_id_list.append(self.id[i])
	return self.get_subset(new_id_list) 
	
def get_molecule(self,molecule): # added by mike on 3/28/2012
	''' Retrieves to subset of atoms associated with the given molecule id.'''
	new_id_list = []
	for i in range(self.number_of_atoms):
		if self.molecule[i] == molecule:
			new_id_list.append(self.id[i])
	return self.get_subset(new_id_list) 

def get_molecule_bonds(self,molecule): #ADded by Katie 8/22/12 for building Gaussian structures
	'''Because Katie is lazy when building Gaussian inputs'''
	mol = self.get_molecule(molecule)
	mol_bonds = []
	for a in self.bonds:
		if (a[2] in mol.id) and (a[3] in mol.id):
			mol_bonds.append(a)
	return mol_bonds

def get_subset_bonds(self,id_list): #Added by Katie 10-9-12 
	''' Extracts bonds between atoms in a subset, defined by id_list (generated elsewhere)'''
	subset_bonds = []
	for a in self.bonds:
		if (a[2] in id_list) and(a[3] in id_list):
			subset_bonds.append(a)
	return subset_bonds

def get_subset_angles(self,id_list): #Added by katie 7 Feb 2013
	'''Extracts the angles wherein all three atoms belong to the subset defined by the given id_list, generated elsewhere. Angle and atom ids are not changed.'''
	subset_angles = []
	for a in self.angles:
		if (a[2] in id_list) and(a[3] in id_list) and (a[4] in id_list):
			subset_angles.append(a)
	return subset_angles

def get_subset_dihedrals(self, id_list): #Added by Katie 7 Feb 2013
	'''Extracts the dihedral angles wherein all atoms belong to the subset defined by the given id_list, generated elsewhere. Angle and atom ids are not changed.'''
	subset_dihedrals = []
	for a in self.dihedrals:
		if (a[2] in id_list) and(a[3] in id_list) and (a[4] in id_list) and (a[5] in id_list):
			subset_dihedrals.append(a)
	return subset_dihedrals

def get_subset_impropers(self, id_list): #Added by Eleanor 22 Nov 2013
	'''Extracts the improper angles wherein all atoms belong to the subset defined by the given id_list, generated elsewhere. Angle and atom ids are not changed.'''
	subset_impropers = []
	for a in self.impropers:
		if (a[2] in id_list) and(a[3] in id_list) and (a[4] in id_list) and (a[5] in id_list):
			subset_impropers.append(a)
	return subset_impropers

def get_slice(self,limits,dimension): # added by mike on 3/30/2012
	'''Retrieves the subset of atoms within the given limits in the indicated dimension. Limits expected as list. Dimension expected as 'x' 'y' or 'z'. '''
	new_id_list = []
	maxl = max(limits)
	minl = min(limits)

	if dimension == 'x':
		for i in range(self.number_of_atoms):
			if self.x[i] >= minl and self.x[i] < maxl :
				new_id_list.append(self.id[i])
	if dimension == 'y':
		for i in range(self.number_of_atoms):
			if self.y[i] >= minl and self.y[i] < maxl :
				new_id_list.append(self.id[i])
	if dimension == 'z':
		for i in range(self.number_of_atoms):
			if self.z[i] >= minl and self.z[i] < maxl :
				new_id_list.append(self.id[i])

	return self.get_subset(new_id_list) 

###############################################s
#Katie starting 27 March 2012
#Getting subset of indices according to types
#Updated 3/19/2014
####REDUNDANT!
def get_atom_typeid(self,typeid):
	''' Retrieves the subset of atoms of the indicated type.'''
	from kirke import GAS
	subset = GAS()

	for i in self.id:
		if self.id_to_index == []:
			self.build_index_dict()
		index = self.id_to_index[i]
		if self.atomic_number[index]==typeid :
			subset.id.append(self.id[index])
		elif self.element[index] == typeid:
			subset.id.append(self.id[index]) 
	return self.get_subset(subset.id)

#Added 15 August as a test function to avoid the use of the index function in get_atom_typeid
#Modified 17 June 2013 to handle 'all'
def get_atom_typeid_fast(self,typeid):
	''' Retrieves the subset of atoms of the indicated type.'''
	from kirke import GAS
	subset = GAS()
	if len(self.id)==0:
		self.fill_id_list(start = 1)

	for i in range(len(self.id)):
		if self.atomic_number[i]==typeid :
			subset.id.append(self.id[i])
		elif self.element[i] == typeid or typeid == 'all':
			subset.id.append(self.id[i]) 

	return self.get_subset(subset.id)

#Added 30 July 12 to get the entire bond/angle/dihedral/improper to minimize need to called indexing function, which is slow as balls.
#Modified 15 August 2012 to eliminate use of the sys.index function here too
def get_bonds_of_type(self,typeid):
	''' Retrieves the bonds of the indicated type '''
	from kirke import GAS
	subset = GAS()
	for ii in self.bonds:
		ty = ii[1]
		if ty == typeid:
			subset.bonds.append(ii)
	return subset.bonds

def get_angles_of_type(self,typeid):
	''' Retrieves angles of the indicated type'''
	from kirke import GAS
	subset = GAS()
	for ii in self.angles:
		ty = ii[1]
		if ty == typeid:
			subset.angles.append(ii)
	return subset.angles

def get_dihedrals_of_type(self,typeid):
	'''Retrieves dihedrals of the indicated type.'''
	from kirke import GAS
	subset = GAS()
	for ii in self.dihedrals:
		ty = ii[1]
		if ty == typeid:
			subset.dihedrals.append(ii)
	
	return subset.dihedrals

def get_impropers_of_type(self,typeid):
	'''Retrieves impropers of the indicated type.'''
	from kirke import GAS
	subset = GAS()

	for ii in self.impropers:
		ty = ii[1]
		if ty == typeid:
			subset.impropers.append(ii)

	return subset.impropers

############ End new additions/ modifications 15 Aug 2012
#Modified 3/19/2014 to be less stupid and avoid the .index function
def get_bonds_typeid(self,typeid):
	'''Retrieves bond ids of the indicated bond type. '''
	from kirke import GAS
	subset = GAS()
	#Note: returns list of bond IDs only
	for i in range(len(self.bonds)):
		ty = self.bonds[i][1]
		if ty == typeid:
			subset.bonds.append(self.bonds[i][0])
	return subset.bonds

def get_angles_typeid(self,typeid):
	''' Retrieves angle ids of the indicated angle type.'''
	from kirke import GAS
	subset = GAS()
	
	for i in range(len(self.angles)):
		ty = self.angles[i][1]
		if ty == typeid:
			subset.angles.append(self.angles[i][0])

	return subset.angles

def get_dihedrals_typeid(self,typeid):
	'''Retrieves dihedral ids of the indicate dihedral type'''
	from kirke import GAS
	subset = GAS()
	
	for i in range(len(self.dihedrals)):
		ty = self.dihedrals[i][1]
		if ty == typeid:
			subset.dihedrals.append(self.dihedrals[i][0])
	
	return subset.dihedrals

def get_impropers_typeid(self,typeid):
	'''Retrieves improper ids of te indicated improper type'''
	from kirke import GAS
	subset = GAS()
	
	for i in range(len(self.impropers)):
		ty = self.impropers[i][1]
		if ty == typeid:
			subset.impropers.append(self.impropers[i][0])

	return subset.impropers
