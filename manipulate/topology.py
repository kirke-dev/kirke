#!/usr/bin/env python

def generate_bonds_by_cutoff(sys, box = [[],[],[]] , periodic = [False, False , False], bond_rules =[[1, 1.0, 'all','all',6]], verbose=False):

	'''Uses a list of bond rules in the format [[type, cutoff, element1, element2],...] to generate new bonds according to a cutoff criterion.'''

	sys.build_index_dict()

	if sys.lx == 0.0:
		sys.update_lengths_from_box()
	
	bonds = []
	starting_bond_count = len(sys.bonds)
	for a in bond_rules:
		print('generate_bonds_by_cutoff: Current bonding rules: %s') %(a) 
		bond_type = a[0]
		cutoff = a[1]
		type1 = a[2]
		type2 = a[3]
		if len(a) == 5:
			max_bonds = a[4]
		else:
			max_bonds = (sys.number_of_atoms)**2		# assigns an absurdly large number because otherwise max_bonds is undefined 

		print('generate_bonds_by_cutoff: Generating subsets. Starting bonds: %s') %(starting_bond_count)

		if type1 != 'all':
			list1 = sys.get_atom_typeid_fast(type1)
		else:
			list1=sys

		if type2 !='all':
			list2 = sys.get_atom_typeid_fast(type2)
		else:
			list2=sys

		print ('generate_bonds_by_cutoff: Checking distances: lengths of lists are %s and %s') %(len(list1.id), len(list2.id))
		if type1!=type2:
			for i in range(len(list1.id)):
				iid = list1.id[i]
				index = sys.id_to_index[iid]
				tick = 0
				flag = False
				for j in range(len(list2.id)):
					if flag:
						break
					jid = list2.id[j]
					jndex = sys.id_to_index[jid]
					if jid!=iid:
						distance = sys.compute_minimum_image_distance_between_two_particles(index, jndex)
						if distance < cutoff:
							starting_bond_count+=1
							new_bond = [starting_bond_count, bond_type, iid, jid]
							bonds.append(new_bond)
							tick+=1
							if tick>=max_bonds:
								flag = True

							if distance < 0.9 and verbose == True:
								print 'generate_bonds_by_cutoff: WARNING! Potentially overlaping atoms detected!' , distance, iid, jid, index, jndex

		else:
			for i in range(len(list1.id)):
				iid = list1.id[i]
				index = sys.id_to_index[iid]
				tick = 0
				flag = False
				for j in range(i,len(list2.id)):
					if flag:
						break
					jid = list2.id[j]
					jndex = sys.id_to_index[jid]
					if jid!=iid:
						distance = sys.compute_minimum_image_distance_between_two_particles(index,jndex)
						if distance < cutoff:
							starting_bond_count+=1
							new_bond = [starting_bond_count, bond_type, iid, jid]
							bonds.append(new_bond)
							tick+=1
							if tick>=max_bonds:
								flag = True
							if distance < 0.9:
								print 'generate_bonds_by_cutoff: Danger Will Robinson! Overlap Imminent!', distance, iid, jid, index, jndex

	if verbose:
		print "generate_bonds_by_cutoff: ", starting_bond_count
	print('generate_bonds_by_cutoff: %s bonds generated') %(starting_bond_count)

	sys.update_number_of_bonds()

	return bonds


def generate_bonds_from_neighbors(sys, periodic = [False, False, False],bond_rules = [[0,1.0,'all','all']], verbose = False):
	'''Generates bonds following the given rules of [bond_type,cutoff, element, element], calling on the super-speedy neighbor_list function. These can then be added to the sys object using sys.add_bonds, which assumes the IDs have been corrected to avoid overlap with existing bonds '''

	from kirke.analyze.neighbors import neighbor_list
	from kirke.elements import element_symbol_list

	if sys.box == [[],[],[]]:
		sys.update_box_from_positions()
	if sys.lx == 0.0:
		sys.update_lengths_from_box()

	if sys.id_to_index == []:
		sys.build_index_dict()

	bonds = []
	bond_count = len(sys.bonds)

	for a in bond_rules:
		if verbose:
			print('generate_bonds_from_neighbors: Current bonding rules: %s') %(a)

		bond_type = a[0]
		cutoff = a[1]
		type1 = a[2]
		type2 = a[3]

		nghblst = neighbor_list(sys, cutoff, periodic ,element1 = type1, element2 = type2 ,verbose=False )

		#neighbor_list returns a list of all neighbors for a given center of element1. For two different, non-all elements, this results in no duplicates. Index in nghblst is the same as index for a given id: returns id values, not indexes. 
		if type1!=type2 and type1!='all' and type2!='all':
			#This is easy mode, where there are no duplicates possible, because neighbor_list is smart like that
			for n in range(len(nghblst)):
				if len(nghblst[n]) >1:
					for atom in nghblst[n][1:]:
						bond_count+=1
						bonds.append([bond_count,bond_type,sys.id[n],sys.id[sys.id_to_index[atom]]])
		elif type1=='all' and type2=='all':
			#Next easiest case: only add pairs if the second atom has a larger id than the first
			for n in range(len(nghblst)):
				if len(nghblst[n]) >1:
					for atom in nghblst[n][1:]:
						if atom > sys.id[n]:
							bond_count+=1
							bonds.append([bond_count,bond_type,sys.id[n],sys.id[sys.id_to_index[atom]]])
		else:
			for n in range(len(nghblst)):
				if len(nghblst[n]) >1:
					for atom in nghblst[n][1:]:
						bond_count+=1
						bonds.append([bond_count,bond_type,sys.id[n],sys.id[sys.id_to_index[atom]]])
						#Now we jump to atom in nghblst, and pop sys.id[n] from that list
						#Because we sorted by id, we don't have to index from the id list
						if sys.id[n] in nghblst[sys.id_to_index[atom]]:
							nghblst[sys.id_to_index[atom]].pop(nghblst[sys.id_to_index[atom]].index(sys.id[n]))
	sys.update_number_of_bonds()
	return bonds

def generate_angles_from_bonds(sys,angle_rules = [[0, 'all', 'all','all']]):
	'''Generates angles from sys.bonds following the given angle rules of [[angle_type, bond_type1, bond_type2, center_atom_type]]. These can then be added to the sys object using sys.add_angles, which assumes the IDs have been corrected to avoid overlap with existing angles May create duplicates?'''

	if sys.bonds == []:
		print('generate_angles_from_bonds: There are no bonds available')

	else:
		angles = []
		from kirke.elements import element_symbol_list
		#Sorting to avoid the index command
		sys.sort_by('id')

		if sys.angles != []:
			sys.update_number_of_angles()
			angle_id = sys.number_of_angles +1
		else:
			angle_id = 1

		for a in angle_rules:
			print('generate_angles_from_bonds: Current angle rules: %s' %(a))
			angle_type = a[0]
			bond_type1 = a[1]
			bond_type2 = a[2]
			center_type = a[3]

			if bond_type1 != 'all':
				bond_list1 = sys.get_bonds_of_type(bond_type1)
			else:
				bond_list1 = sys.bonds

			if bond_type2 != 'all':
				bond_list2 = sys.get_bonds_of_type(bond_type2)
			else:
				bond_list2 = sys.bonds


			if bond_type1!=bond_type2 and bond_type1!='all' and bond_type2!='all':
				for i in range(len(bond_list1)):
					bond_i = bond_list1[i]
					for j in range(len(bond_list2)):
						bond_j = bond_list2[j]
						if bond_i[2] == bond_j[2] and bond_i[3]!=bond_j[3]:
							center_element = sys.element[bond_i[2]-1]
							center_atomic_number = sys.atomic_number[bond_i[2]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[3], bond_i[2],bond_j[3]])
								angle_id+=1
						elif bond_i[2] == bond_j[3] and bond_i[3]!=bond_j[2]:
							center_element = sys.element[bond_i[2]-1]
							center_atomic_number = sys.atomic_number[bond_i[2]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[3], bond_i[2],bond_j[2]])
								angle_id+=1
						elif bond_i[3] == bond_j[2] and bond_i[2]!=bond_j[3]:
							center_element = sys.element[bond_i[3]-1]
							center_atomic_number = sys.atomic_number[bond_i[3]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[2], bond_i[3],bond_j[3]])
								angle_id+=1
						elif bond_i[3] == bond_j[3] and bond_i[2] != bond_j[2]:
							center_element = sys.element[bond_i[3]-1]
							center_atomic_number = sys.atomic_number[bond_i[3]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[2], bond_i[3],bond_j[2]])
								angle_id+=1
			else:
				for i in range(len(bond_list1)):
					bond_i = bond_list1[i]
					for j in range(i+1,len(bond_list2)):
						bond_j = bond_list2[j]
						if bond_i[2] == bond_j[2] and bond_i[3]!=bond_j[3]:
							center_element = sys.element[bond_i[2]-1]
							center_atomic_number = sys.atomic_number[bond_i[2]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[3], bond_i[2],bond_j[3]])
								angle_id+=1
						elif bond_i[2] == bond_j[3] and bond_i[3]!=bond_j[2]:
							center_element = sys.element[bond_i[2]-1]
							center_atomic_number = sys.atomic_number[bond_i[2]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[3], bond_i[2],bond_j[2]])
								angle_id+=1
						elif bond_i[3] == bond_j[2] and bond_i[2]!=bond_j[3]:
							center_element = sys.element[bond_i[3]-1]
							center_atomic_number = sys.atomic_number[bond_i[3]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[2], bond_i[3],bond_j[3]])
								angle_id+=1
						elif bond_i[3] == bond_j[3] and bond_i[2] != bond_j[2]:
							center_element = sys.element[bond_i[3]-1]
							center_atomic_number = sys.atomic_number[bond_i[3]-1]
							if center_element == center_type or center_atomic_number == center_type:
								angles.append([angle_id,angle_type, bond_i[2], bond_i[3],bond_j[2]])
								angle_id+=1

			print('generate_angles_from_bonds: %s angles have been generated' %(len(angles)))

	sys.update_number_of_angles()

	return angles

def generate_dihedrals_from_angles(sys,dihedral_rules = [[0, 'all', 'all']]):
	'''Generates dihedrals from sys.angles following the given angle rules of [[dihedral_type, angle_type1, angle_type2]]. These can then be added to the sys object using sys.add_dihedrals, which assumes the IDs have been corrected to avoid overlap with existing angles '''

	if sys.angles == []:
		print('generate_dihedrals_from_angles: There are no angles available. Try generate_angles_from_bonds first')
	else:
		dihedrals = []

		if sys.dihedrals !=[]:
			sys.update_number_of_dihedrals()
			dihed_id = sys.number_of_dihedrals+1
		else:
			dihed_id = 1

		for a in dihedral_rules:
			print('generate_dihedrals_from_angles: Current rules: %s'%a)
			dihed_type = a[0]
			angle_type1 = a[1]
			angle_type2 = a[2]

			if angle_type1 != 'all':
				angle_list1 = sys.get_angles_of_type(angle_type1)
			else:
				angle_list1 = sys.angles
			if angle_type2 != 'all':
				angle_list2 = sys.get_angles_of_type(angle_type2)
			else:
				angle_list2 = sys.angles
			if angle_type1!=angle_type2:
				for i in range(len(angle_list1)):
					angle_i = angle_list1[i]
					for j in range(len(angle_list2)):
						angle_j = angle_list2[j]
						if angle_i!=angle_j:
							if angle_i[2]==angle_j[3] and angle_i[3]==angle_j[2]:
								dihedrals.append([dihed_id,dihed_type,angle_j[4],angle_i[2], angle_i[3], angle_i[4]])
								dihed_id+=1
							elif angle_i[2]==angle_j[3] and angle_i[3]==angle_j[4]:
								dihedrals.append([dihed_id,dihed_type,angle_j[2],angle_i[2], angle_i[3], angle_i[4]])
								dihed_id+=1
							elif angle_i[3]==angle_j[2] and angle_i[4]==angle_j[3]:
								dihedrals.append([dihed_id,dihed_type,angle_j[4],angle_i[4], angle_i[3], angle_i[2]])
								dihed_id+=1
							elif angle_i[3]==angle_j[4] and angle_i[4]==angle_j[3]:
								dihedrals.append([dihed_id,dihed_type,angle_j[2],angle_i[4], angle_i[3], angle_i[2]])
								dihed_id+=1
			else:
				for i in range(len(angle_list1)):
					angle_i = angle_list1[i]
					for j in range(len(angle_list2)):
						angle_j = angle_list2[j]
						if angle_i!=angle_j:
							if angle_i[2]==angle_j[3] and angle_i[3]==angle_j[2]:
								dihedrals.append([dihed_id,dihed_type,angle_j[4],angle_i[2], angle_i[3], angle_i[4]])
								dihed_id+=1
							elif angle_i[2]==angle_j[3] and angle_i[3]==angle_j[4]:
								dihedrals.append([dihed_id,dihed_type,angle_j[2],angle_i[2], angle_i[3], angle_i[4]])
								dihed_id+=1
							elif angle_i[3]==angle_j[2] and angle_i[4]==angle_j[3]:
								dihedrals.append([dihed_id,dihed_type,angle_j[4],angle_i[4], angle_i[3], angle_i[2]])
								dihed_id+=1
							elif angle_i[3]==angle_j[4] and angle_i[4]==angle_j[3]:
								dihedrals.append([dihed_id,dihed_type,angle_j[2],angle_i[4], angle_i[3], angle_i[2]])
								dihed_id+=1

			print('generate_dihedrals_from_angles: %s dihedrals have been generated' %len(dihedrals) )

	sys.update_number_of_dihedrals()

	return dihedrals


def generate_molecule_from_bonds(sys,bond_list):
	'''This function will generate a molecule from bonds. Will automatically populate sys.molecule. This is a work in progress, meant to be used with unwrapping. May be useful for other things later.'''
	from kirke.analyze.neighbors import neighbor_list_from_bonds
	from kirke.analyze.neighbors import updated_cluster_list

	if bond_list == []:
		print ("generate_molecule_from_bonds : You don't have any bonds! Use generate_bonds_from_cutoff or generate_bonds_from_neighbors!")
	
	if sys.id_to_index == []:
		sys.build_index_dict()
	if sys.molecule == []:
		sys.fill_molecule_list()
	neighbor_list = neighbor_list_from_bonds(sys)
	
	cluster_list = updated_cluster_list( neighbor_list)
	#print cluster_list
	i = 1
	for cluster in cluster_list:
		molecule_id = i
		for atom in range(len(cluster)):
			eye = sys.id_to_index[cluster[atom]]
			sys.molecule[eye] = i
		i += 1
		
	return sys.molecule	



