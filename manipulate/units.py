#!/usr/bin/env python


def seconds_to_human_time(stuff):
	'''Converts seconds to units of time your brain can actually comprehend.'''
	sec = int(stuff)
	factors = [1, 60, 60, 24, 7, 52, 10, 10, 10]
	names = ['second', 'minute', 'hour', 'day', 'week' , 'year', 'decade', 'century', 'millennium']
	plural_names = ['seconds', 'minutes', 'hours', 'days', 'weeks' , 'years', 'decades', 'centuries', 'millenia']
	
	factors.reverse()
	names.reverse()
	plural_names.reverse()
	
	outstring = ''
	
	factor = 1
	for a in factors:
		factor = factor*a
	nfactors = len(factors)
	
	for i in range(nfactors):
		time = sec/factor
		sec = sec - time*factor
		factor = factor/factors[i]
		if time != 0:
			outstring = outstring + str(time)+' '  
			if time > 1: 
				outstring = outstring + plural_names[i]
			else:
				outstring = outstring + names[i]
			outstring = outstring + ' '
	return outstring


def lammps_unit_conversions(sys, start_units = 'metal', final_units = 'si'):
	'''Converts system units between different lammps units styles (except lj, which will have it's own function: Does not currently handle potential coefficients to avoid style issues '''
		#Updated to also modify type_masses
	if start_units == final_units:
		print 'lammps_unit_conversions: No unit conversion indicated'
		 
	elif start_units == 'metal' and final_units == 'real':
		if sys.vx !=[]:
			print 'lammps_unit_conversions: Converting velocity: other system attribute units unchanged'
			for a in range(len(sys.vx)):
				sys.vx[a]= sys.vx[a]*1000
				sys.vy[a] = sys.vy[a]*1000
				sys.vz[a] = sys/xz[a]*1000
	elif start_units == 'real' and final_units == 'metal':
		if sys.vx !=[]:
			print 'lammps_unit_conversions: Converting velocity: other system attribute units unchanged'
			for a in range(len(sys.vx)):
				sys.vx[a]= sys.vx[a]/1000
				sys.vy[a] = sys.vy[a]/1000
				sys.vz[a] = sys/xz[a]/1000
	elif start_units == 'metal' and final_units == 'si':
		print 'lammps_unit_conversions: Converting mass, length, velocity and charge values as present in the system'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]/1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]/1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e-10
				sys.y[a] = sys.y[a]*1e-10
				sys.z[a] = sys.z[a]*1e-10
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*100
				sys.vy[a] = sys.vy[a]*100
				sys.vz[a] = sys.vz[a]*100	
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]*1.602176487e-19
	elif start_units == 'si' and final_units == 'metal':
		print 'lammps_unit_conversions: Converting mass, length, velocity and charge values as present in the system'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]*1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]*1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e10
				sys.y[a] = sys.y[a]*1e10
				sys.z[a] = sys.z[a]*1e10
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1e8
				sys.vy[a] = sys.vy[a]*1e8
				sys.vz[a] = sys.vz[a]*1e8
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]/1.602176487e-19
	elif start_units == 'metal' and final_units == 'cgs':
		print 'lammps_unit_conversions: Converting length and charge values as present in the system'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e-8
				sys.y[a] = sys.y[a]*1e-8
				sys.z[a] = sys.z[a]*1e-8

		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]*4.8032e-10
	elif start_units == 'cgs' and final_units == 'metal':
		print 'lammps_unit_conversions: Converting length and charge values as present in the system'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e8
				sys.y[a] = sys.y[a]*1e8
				sys.z[a] = sys.z[a]*1e8

		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]/4.8032e-10

	elif start_units == 'metal' and final_units == 'electron':
		print 'lammps_unit_conversions: Converting length and velocity'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1.8897161646320724
				sys.y[a] = sys.y[a]*1.8897161646320724
				sys.z[a] = sys.z[a]*1.8897161646320724
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1.952e-3
				sys.vy[a] = sys.vy[a]*1.952e-3
				sys.vz[a] = sys.vz[a]*1.952e-3

	elif start_units == 'electron' and final_units == 'metal':
		print 'lammps_unit_conversions: Converting length and velocity'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]/1.8897161646320724
				sys.y[a] = sys.y[a]/1.8897161646320724
				sys.z[a] = sys.z[a]/1.8897161646320724
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]/1.952e-3
				sys.vy[a] = sys.vy[a]/1.952e-3
				sys.vz[a] = sys.vz[a]/1.952e-3
 ## All metal convrsion done, now for "real"

	elif start_units == 'real' and final_units == 'si':
		print 'lammps_unit_conversions: Converting mass, length, velocity and charge values as present in the system'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]/1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]/1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e-10
				sys.y[a] = sys.y[a]*1e-10
				sys.z[a] = sys.z[a]*1e-10
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1e5
				sys.vy[a] = sys.vy[a]*1e5
				sys.vz[a] = sys.vz[a]*1e5	
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]*1.602176487e-19
	elif start_units == 'si' and final_units == 'real':
		print 'lammps_unit_conversions: Converting mass, length, velocity and charge values as present in the system'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]*1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]*1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e10
				sys.y[a] = sys.y[a]*1e10
				sys.z[a] = sys.z[a]*1e10
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1e-5
				sys.vy[a] = sys.vy[a]*1e-5
				sys.vz[a] = sys.vz[a]*1e-5
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]/1.602176487e-19

	elif start_units == 'real' and final_units == 'cgs':
		print 'lammps_unit_conversions: Converting length, velocity and charge values as present in the system'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e-8
				sys.y[a] = sys.y[a]*1e-8
				sys.z[a] = sys.z[a]*1e-8
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1e7
				sys.vy[a] = sys.vy[a]*1e7
				sys.vz[a] = sys.vz[a]*1e7
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]*4.8032e-10

	elif start_units == 'cgs' and final_units == 'real':
		print 'lammps_unit_conversions: Converting length, velocity and charge values as present in the system'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e8
				sys.y[a] = sys.y[a]*1e8
				sys.z[a] = sys.z[a]*1e8
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1e-7
				sys.vy[a] = sys.vy[a]*1e-7
				sys.vz[a] = sys.vz[a]*1e-7
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]/4.8032e-10

	elif start_units == 'real' and final_units == 'electron':
		print 'lammps_unit_conversions: Converting length and velocity'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1.8897161646320724
				sys.y[a] = sys.y[a]*1.8897161646320724
				sys.z[a] = sys.z[a]*1.8897161646320724
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1.952
				sys.vy[a] = sys.vy[a]*1.952
				sys.vz[a] = sys.vz[a]*1.952

	elif start_units == 'electron' and final_units == 'real':
		print 'lammps_unit_conversions: Converting length and velocity'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]/1.8897161646320724
				sys.y[a] = sys.y[a]/1.8897161646320724
				sys.z[a] = sys.z[a]/1.8897161646320724
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]/1.952
				sys.vy[a] = sys.vy[a]/1.952
				sys.vz[a] = sys.vz[a]/1.952

##Remaining si conversions
	elif start_units == 'si' and final_units == 'cgs':
		print 'Converting mass length, velocity and charge values as present in the system'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]/1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]/1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1e2
				sys.y[a] = sys.y[a]*1e2
				sys.z[a] = sys.z[a]*1e2
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1e2
				sys.vy[a] = sys.vy[a]*1e2
				sys.vz[a] = sys.vz[a]*1e2
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]/3.0e-9

	elif start_units == 'cgs' and final_units == 'si':
		print 'Converting length, velocity and charge values as present in the system'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]*1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]*1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*.01
				sys.y[a] = sys.y[a]*0.01
				sys.z[a] = sys.z[a]*0.01
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*0.01
				sys.vy[a] = sys.vy[a]*0.01
				sys.vz[a] = sys.vz[a]*0.01
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]*3.0e-9

	elif start_units == 'si' and final_units == 'electron':
		print 'Converting mass, length and velocity'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]/1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]/1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1.8897161646320724e10
				sys.y[a] = sys.y[a]*1.8897161646320724e10
				sys.z[a] = sys.z[a]*1.8897161646320724e10
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1.952
				sys.vy[a] = sys.vy[a]*1.952
				sys.vz[a] = sys.vz[a]*1.952
#CHARGE
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]/1.602176487e-19

	elif start_units == 'electron' and final_units == 'si':
		print 'Converting mass, length and velocity'
		if sys.masses !=[]:
			for a in range(len(sys.masses)):
				sys.masses[a]=sys.masses[a]*1000
		if sys.type_masses !=[]:
			for a in range(len(sys.type_masses)):
				sys.type_masses[a][1]=sys.type_masses[a][1]*1000
		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]/1.8897161646320724e10
				sys.y[a] = sys.y[a]/1.8897161646320724e10
				sys.z[a] = sys.z[a]/1.8897161646320724e10
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]/1.952
				sys.vy[a] = sys.vy[a]/1.952
				sys.vz[a] = sys.vz[a]/1.952
#CHARGE
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]*1.602176487e-19

	elif start_units == 'cgs' and final_units == 'electron':
		print 'Converting charge, length and velocity'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]*1.8897161646320724e8
				sys.y[a] = sys.y[a]*1.8897161646320724e8
				sys.z[a] = sys.z[a]*1.8897161646320724e8
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]*1.952e-2
				sys.vy[a] = sys.vy[a]*1.952e-2
				sys.vz[a] = sys.vz[a]*1.952e-2
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]/4.8032e-10

	elif start_units == 'electron' and final_units == 'cgs':
		print 'Converting charge, length and velocity'

		if sys.x !=[]:
			for a in range(len(sys.x)):
				sys.x[a] =sys.x[a]/1.8897161646320724e8
				sys.y[a] = sys.y[a]/1.8897161646320724e8
				sys.z[a] = sys.z[a]/1.8897161646320724e8
		if sys.vx !=[]:
			for a in range(len(sys.vx)):
				sys.vx[a] =sys.vx[a]/1.952e-2
				sys.vy[a] = sys.vy[a]/1.952e-2
				sys.vz[a] = sys.vz[a]/1.952e-2
		if sys.q !=[]:
			for a in range(len(sys.q)):
				sys.q[a] =sys.q[a]*4.8032e-10
	return sys

