#!/usr/bin/env python

def unwrap_coordinates(sys,use_nx = False, center_image = True):
	'''Unwraps system coordinates using n lists if available, or distance to previous atom in periodic directions only. Assumes orthorhombic! Best applied to molecule subsets. Only works if your atoms are in order according to position! Works fairly well for single molecules, but not full systems. '''
#Assumes orthorhombic
#Still a little messy, but unwrap from previous index is a strange criteria
	from kirke import GAS
	from kirke.manipulate.translate import center_box_image
	from kirke.analyze.commonsubfunctions import periodic_delta

	#print "WARNING: This method can produce wonky results. If possible, define bonds and use unwrap_from_bond_list instead"

	if sys.box == []:
		sys.get_box()
	if sys.lx == 0.0:
		sys.update_lengths_from_box()

	if sys.box_mode!='orthorhombic':
		print 'unwrap_coordinates: Check your box mode: this can only handle orthorhmbic for the time being'
	if sys.periodic == [False,False,False]:
		print 'unwrap_coordinates: Non-periodic system. No unwrapping to do here.'
	if sys.nx == [] or use_nx==False:
	#Correcting periodic distances according the previous atoms positions
		for i in range(1,len(sys.id)):
			if sys.periodic == []:
				periodic = [True, True, True]

			if sys.periodic[0] == True:
				dx = sys.x[i]-sys.x[i-1]
				sys.x[i] = sys.x[i]-sys.lx*round(dx/sys.lx)
			if sys.periodic[1] == True:
				dy = sys.y[i]-sys.y[i-1]
				sys.y[i] = sys.y[i]-sys.ly*round(dy/sys.ly)
			if sys.periodic[2]== True:
				dz = sys.z[i]-sys.z[i-1]
				sys.z[i] = sys.z[i]-sys.lz*round(dz/sys.lz)

	else:
		if center_image == True:	# because LAMMPS can be wonky occasionally
				center_box_image(sys)

		for i in range(len(sys.id)):
			sys.x[i]+=sys.nx[i]*sys.lx
			sys.y[i]+=sys.ny[i]*sys.ly
			sys.z[i]+=sys.nz[i]*sys.lz

	return sys

def unwrap_from_bond_list(sys, change_box=False):
	'Unwraps system coordinates using the adjacency list generated from bonds and a breadth-first search algorithm. Also checks all bond lengths at the end to look for loops. If there are multiple molecules, they will all be unwrapped, but the net center of mass will not be conserved at this time.'''

	from kirke.analyze import  neighbor_list_from_bonds, bond_length_distribution
	from math import floor
	from copy import copy

	adjacency_list = neighbor_list_from_bonds(sys, element1 = 'all', element2 = 'all', verbose = False, use_index = False)

	unfound = copy(sys.id)
	discovered = []
	checked = []

	#The first atom selected inevitably defines the center box image for that molecule
	if sys.id_to_index == []:
		sys.build_index_dict()

	if sys.bonds == []:
		print 'unwrap_from_bond_list_using_internal: This function requires a bond list'
	else:	
		if sys.box == []:
			sys.get_box()
		if sys.lx == 0.0 and sys.box_mode == 'orthorhombic':
			sys.update_lengths_from_box()
		if sys.sx == []:
			sys.fill_s_lists()
			sys.update_internal_from_external()


		if sys.periodic == []:
			periodic = [True, True, True]

		start = adjacency_list[0][0]

		checked.append(start)

		a1 = sys.id_to_index[start]
		if len(adjacency_list[0])>1:
			for a in adjacency_list[0][1:]:
				a2 = sys.id_to_index[a]
				if sys.periodic[0] == True:
					dx = sys.sx[a1]-sys.sx[a2]
					sys.sx[a2] = sys.sx[a2]+round(dx)
					sys.nx[a2]=floor(sys.sx[a2])

				if sys.periodic[1] == True:
					dy = sys.sy[a1]-sys.sy[a2]
					sys.sy[a2] = sys.sy[a2]+round(dy)
					sys.ny[a2]=floor(sys.sy[a2])

				if sys.periodic[2]== True:
					dz = sys.sz[a1]-sys.sz[a2]
					sys.sz[a2] = sys.sz[a2]+round(dz)
					sys.nz[a2]=floor(sys.sz[a2])

				print a

				discovered.append(a)
				unfound.remove(a)
		

		else:
			print 'I need to fix this else'

		while discovered !=[]:
			a1 = sys.id_to_index[discovered[0]]
			if len(adjacency_list[a1])>1:
				for a in adjacency_list[a1][1:]:
					a2 = sys.id_to_index[a]
					if a in checked:
						print 'Check!', a

					if a not in checked and a not in discovered:
						if sys.periodic[0] == True:
							dx = sys.sx[a1]-sys.sx[a2]
							sys.sx[a2] = sys.sx[a2]+round(dx)
							sys.nx[a2]=floor(sys.sx[a2])
						if sys.periodic[1] == True:
							dy = sys.sy[a1]-sys.sy[a2]
							sys.sy[a2] = sys.sy[a2]+round(dy)
							sys.ny[a2]=floor(sys.sy[a2])
						if sys.periodic[2]== True:
							dz = sys.sz[a1]-sys.sz[a2]
							sys.sz[a2] = sys.sz[a2]+round(dz)
							sys.nz[a2]=floor(sys.sz[a2])

						discovered.append(a)
						unfound.remove(a)		
			
			checked.append(discovered[0])
			discovered.remove(discovered[0])
			print len(checked), len(discovered), len(unfound), len(sys.id)

			#Pop goes the weasel.. I mean atom
			
			if discovered ==[] and unfound!= []:
				discovered.append(unfound[0])
				unfound.pop(0)

		if len(checked) != len(sys.id):
			print 'unwrap_from_bond_list: You seem to be missing things. Not all atoms were successfully checked'	
			print len(checked), len(sys.id), sys.number_of_atoms
			print sum(checked)-sum(sys.id)
		sys.update_external_from_internal()

		[length_list, bins, counts] = bond_length_distribution(sys, bond_type = 'all', periodic = [False, False, False], bins = 20, max_radius = 5.0, fix_bin_size = False, bin_size = 0.5, verbose=False, return_list = True)

		for l in length_list:
			if l> sys.lx/2 or l> sys.ly/2 or l>sys.lz>2:
				print 'There is a periodic loop in your system. Complete unwrapping is not actually possible'
		if change_box == True:
			sys.update_box_from_positions()
			sys.update_lengths_from_box()

	return sys


def unwrap_coordinates_from_center_of_mass(sys):
 	from kirke import GAS
        from kirke.analyze import center_of_molecule_mass
	from kirke.manipulate.topology import generate_molecule_from_bonds
	from kirke.elements import element_mass_list, element_symbol_list
        if sys.id_to_index == []:
                sys.build_index_dict()
	
	if sys.molecule == []:
		sys.molecule = generate_molecule_from_bonds()
	num_molecules = max(sys.molecule)

        if sys.bonds == []:
                print 'unwrap_from_bond_list_using_internal: This function requires a bond list'
       
	if sys.masses == []:
		sys.update_masses_from_element()

	pre_unwrap_center_of_masses = []
	for i in range(num_molecules):
		pre_unwrap_center_of_masses.append(center_of_molecule_mass(sys,i+1,internal = True, wrapped = True)[0:3])
		
	else:
                if sys.box == []:
                        sys.get_box()
                if sys.lx == 0.0 and sys.box_mode == 'orthorhombic':
                        sys.update_lengths_from_box()
                if sys.sx == []:
                        sys.fill_s_lists()
                        sys.update_internal_from_external()

                sys.sort_by('id')
                for i in sys.bonds:
                        a1 = sys.id_to_index[i[2]]
                        a2 = sys.id_to_index[i[3]]

                        if sys.periodic == []:
                                periodic = [True, True, True]
                        if sys.periodic[0] == True:
                                dx = sys.sx[a1]-sys.sx[a2]
                                sys.sx[a2] = sys.sx[a2]+round(dx)
                        if sys.periodic[1] == True:
                                dy = sys.sy[a1]-sys.sy[a2]
                                sys.sy[a2] = sys.sy[a2]+round(dy)
                        if sys.periodic[2]== True:
                                dz = sys.sz[a1]-sys.sz[a2]
                                sys.sz[a2] = sys.sz[a2]+round(dz)

                sys.update_external_from_internal()

        return sys


