#/usr/bin/python

def place_new_molecule(sys, btick, box_dims):
	''' Generates a randomly located molecule. '''
	from random import random
	from kirke.manipulate import move, rotate_coordinates
	from numpy import cross

	# Randomizing new orthogonal basis axes
	a = [random(), random(), random()]
	if btick == 1:
		b = [random(), random(), 0]
		b[2]= -1*(a[0]*b[0]+a[1]*b[1])/a[2]
		btick = 2
	elif btick == 2:
		b = [random(), 0,random()]
		b[1]= -1*(a[0]*b[0]+a[2]*b[2])/a[1]
		btick = 3
	elif btick == 3:
		b = [0,random(),random()]
		b[0]= -1*(a[1]*b[1]+a[2]*b[2])/a[0]
		btick = 1
	c = cross(a,b)
	rotate_coordinates(sys, [a,b,c])

	# Randomizing insertion location
	insert_point = [random()*box_dims[0],random()*box_dims[1],random()*box_dims[2]]
	move(sys, insert_point)

	return sys, btick

def amorphous_builder(out_file = 'file.out', monomer_list = [], count = [], itermax=100000, rmin=3.5, density=0.2, periodic=[True,True,True], aspect_ratio=1, fix_xy_dimensions=False, xy_box_length=0.0, verbose=False):
	''' Builds amorphous boxes of monomers and/or short-chain polymers from a list of GAS objects. Can build cubic or rectangular boxes, and can fix the x and y dimensions to a specific length. The x and y dimensions will always be equal, but the z dimension can be varied. Telling this function to fix the x,y coordinates will override the aspect ratio input. '''

#	# WARNING, do not call the same molecule twice, because it messes up the automated molecule numbering!!! We should probably modify the code to check the monomer_list input.

	from math import floor
	from kirke.analyze import periodic_delta
	from copy import deepcopy

	if len(monomer_list) != len(count):
		print 'AmorphousBuilder: ERROR!! monomer_list and count are different lengths!!'

	# Iteration controls and id shifts
	i = 0			# number of monomer type added
	ii = 0			# indicates initial frame
	atom_id_shift = 0
	bond_id_shift = 0
	angles_id_shift = 0
	dihedrals_id_shift = 0
	impropers_id_shift = 0
	mol_id_shift = 0

	#### Make box ####
	sum_count_times_mass = 0
	for aa in range(len(monomer_list)):		# elements of monomer_list must be GAS objects!
		mass = sum(monomer_list[aa].masses)
		sum_count_times_mass += count[aa] * mass
	box_volume = sum_count_times_mass/(density*0.60221415)

	if fix_xy_dimensions==False:
		if aspect_ratio == 1:
			if verbose==True:
				print '\nAmorphousBuilder: Making cubic box!'
			box_length = pow(box_volume,1.0/3)
			half_box = box_length/2.0
			box = [[-half_box, half_box],[-half_box, half_box],[-half_box, half_box]]
			box_dims = [box_length, box_length, box_length]
                else:
			if verbose==True:
				print '\nAmorphousBuilder: Making rectangular box with an aspect ratio of {0}'.format(aspect_ratio)
			xy_box_length = pow(box_volume/float(aspect_ratio),1.0/3)
			z_box_length = xy_box_length*aspect_ratio
			box = [[-xy_box_length/2.0, xy_box_length/2.0],[-xy_box_length/2.0, xy_box_length/2.0],[-z_box_length/2.0, z_box_length/2.0]]
			box_dims = [xy_box_length, xy_box_length, z_box_length]
	else:
		if verbose==True:
			print '\nAmorphousBuilder: Making rectangular box with fixed x and y dimensions of {0}!'.format(xy_box_length)
		z_box_length = box_volume/(xy_box_length**2)
		box = [[-xy_box_length/2.0, xy_box_length/2.0],[-xy_box_length/2.0, xy_box_length/2.0],[-z_box_length/2.0, z_box_length/2.0]]
		box_dims = [xy_box_length, xy_box_length, z_box_length]

	if verbose==True:
		print 'AmorphousBuilder: Box lengths = {0}, {1}, {2}'.format(box_dims[0], box_dims[1], box_dims[2])

	#### Fill box ####
	for aa in range(len(monomer_list)):
		sys = monomer_list[aa]

#		if verbose==True:
		print '\namorphous_builder: inserting {0} of molecule {1}'.format(count[aa], monomer_list[aa].file_name)
		btick = 1
		iterations = 0
		i = 0

		while i < count[aa] and iterations < itermax:

			if ii == 0:
				# deepcopy required here: removing it results in the code failing to insert all remaining molecules from this particular system
				initial_sys = deepcopy(sys)
				new_sys, btick = place_new_molecule(initial_sys, btick, box_dims)

				if verbose==True:
					print '{0} {1} {2} {3} {4}'.format(atom_id_shift,bond_id_shift,angles_id_shift,dihedrals_id_shift, impropers_id_shift)
				flag = False
				ii+=1
				i+=1

				atom_id_shift = len(sys.id)
				bond_id_shift = len(sys.bonds)
				angles_id_shift = len(sys.angles)
				dihedrals_id_shift = len(sys.dihedrals)
				impropers_id_shift = len(sys.impropers)
				mol_id_shift = 1

			else:
				flag = False

				# code used to allow users to specify a number of orientations (num_orientations) to try before choosing a new location
				# this option is now disallowed because it's significantly faster to only use one orientation per insertion attempt!
				num_orientations = 1
				for j in range(num_orientations):
					sys, btick = place_new_molecule(sys, btick, box_dims)

					for a in range(len(new_sys.id)):
						for b in range(len(sys.id)):
							dx = periodic_delta(new_sys.x[a]-sys.x[b],box_dims[0])
							dy = periodic_delta(new_sys.y[a]-sys.y[b],box_dims[1])
							dz = periodic_delta(new_sys.z[a]-sys.z[b],box_dims[2])
							d = dx**2+dy**2+dz**2

							if d< rmin**2:
								flag = True
								break

						if flag == True:
							break

					if flag == True:
						iterations +=1
						break

				if flag == False:
					# Adding the molecule to the system
					# Need to shift indexing of sys to append to new_sys, making new_bond, new_angle and new_dihedral lists to simply add
					i+=1
					bond_id = []
					angle_id = []
					dihedral_id = []
					improper_id = []

					for a in range(len(sys.bonds)):
						bond_id.append(sys.bonds[a][0])
					for a in range(len(new_sys.angles)):
						angle_id.append(new_sys.angles[a][0])
					for a in range(len(sys.dihedrals)):
						dihedral_id.append(new_sys.dihedrals[a][0])
					for a in range(len(sys.impropers)):
						improper_id.append(new_sys.impropers[a][0])

					if verbose==True:
						print '{0} {1} {2} {3} {4}'.format(atom_id_shift,bond_id_shift,angles_id_shift,dihedrals_id_shift, impropers_id_shift)

					new_ids = []
					new_bonds = []
					new_angles = []
					new_dihedrals = []
					new_impropers = []

					for j in range(len(sys.id)):

						sys.id[j] += atom_id_shift
						sys.molecule[j] += mol_id_shift

#						if (j%50) == 0:
#							print 'after:', sys.id[j], sys.molecule[j], aa

					for j in range(len(sys.bonds)):
						sys.bonds[j][0] = sys.bonds[j][0]+bond_id_shift
						new_bonds.append([sys.bonds[j][0],sys.bonds[j][1],sys.bonds[j][2]+atom_id_shift,sys.bonds[j][3]+atom_id_shift])

					for j in range(len(sys.angles)):
						sys.angles[j][0] = sys.angles[j][0]+angles_id_shift
						new_angles.append([sys.angles[j][0],sys.angles[j][1],sys.angles[j][2]+atom_id_shift,sys.angles[j][3]+atom_id_shift,sys.angles[j][4]+atom_id_shift])

					for j in range(len(sys.dihedrals)):
						sys.dihedrals[j][0] = sys.dihedrals[j][0]+dihedrals_id_shift
						new_dihedrals.append([sys.dihedrals[j][0],sys.dihedrals[j][1],sys.dihedrals[j][2]+atom_id_shift,sys.dihedrals[j][3]+atom_id_shift,sys.dihedrals[j][4]+atom_id_shift,sys.dihedrals[j][5]+atom_id_shift])

					for j in range(len(sys.impropers)):
						sys.impropers[j][0]=sys.impropers[j][0]+impropers_id_shift
						new_impropers.append([sys.impropers[j][0],sys.impropers[j][1],sys.impropers[j][2]+atom_id_shift,sys.impropers[j][3]+atom_id_shift,sys.dihedrals[j][4]+atom_id_shift,sys.impropers[j][5]+atom_id_shift])

					new_sys.add_atoms_sys(sys)
					new_sys.add_bonds(new_bonds)
					new_sys.add_angles(new_angles)
					new_sys.add_dihedrals(new_dihedrals)
					new_sys.add_impropers(new_impropers)

					if i == count[aa]:
						atom_id_shift = len(new_sys.id)
						bond_id_shift = len(new_sys.bonds)
						angles_id_shift = len(new_sys.angles)
						dihedrals_id_shift = len(new_sys.angles)
						impropers_id_shift = len(new_sys.impropers)
						mol_id_shift = 0
						for j in range(aa+1):		# aa+1 because there are aa+1 molecules (alternate explanation: range(0)=[])
							mol_id_shift += count[j]

					else:
						atom_id_shift = len(sys.id)
						bond_id_shift = len(sys.bonds)
						angles_id_shift = len(sys.angles)
						dihedrals_id_shift = len(sys.dihedrals)
						impropers_id_shift = len(sys.impropers)
						mol_id_shift = 1

				if iterations >= itermax:
					print '\nAmorphousBuilder: Too many iterations!!!\nFailed to insert the {0}th of {1} desired {2} molecules\n'.format(i+1, count[aa], monomer_list[aa].file_name)
					break

		iterations = 0

	#### Update box ####
	new_sys.box = box

	if periodic[0] == True:
		new_sys.lx = box_dims[0]
	else:
		new_sys.lx = max(new_sys.x)-min(new_sys.x)

	if periodic[1] == True:
		new_sys.ly = box_dims[1]
	else:
		new_sys.ly = max(new_sys.y)-min(new_sys.y)

	if periodic[2] == True:
		new_sys.ly = box_dims[2]
	else:
		new_sys.ly = max(new_sys.y)-min(new_sys.y)

	return new_sys
