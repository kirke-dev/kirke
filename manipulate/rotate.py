#!/usr/bin/env python
def compute_rotation_matrix_from_basis_vectors(r1_prime, r2_prime, r3_prime, r1, r2, r3 ):
	'''the final basis vectors (prime) and initial are used to compute the rotation matrix to go from one to the other.'''
	from kirke import GAS
	math = GAS() #empty gas to use its functions
	# must take unit vectors to not have scalling
	r1_hat = math.unit_vector_3D(r1)
	r2_hat = math.unit_vector_3D(r2)
	r3_hat = math.unit_vector_3D(r3)
	r1_prime_hat = math.unit_vector_3D(r1_prime)
	r2_prime_hat = math.unit_vector_3D(r2_prime)
	r3_prime_hat = math.unit_vector_3D(r3_prime)
	
	bo = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	bprime = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	for i in range(3):
		bo[i][0] = r1_hat[i]
		bo[i][1] = r2_hat[i]
		bo[i][2] = r3_hat[i]
		bprime[i][0] = r1_prime_hat[i]
		bprime[i][1] = r2_prime_hat[i]
		bprime[i][2] = r3_prime_hat[i]
	
	boinv = math.matrix_inverse_3D(bo)
	rotation_matrix = math.matrix_matrix_product_3D(bprime,boinv)
	return rotation_matrix 

def rotate_about_point(sys, rotation_matrix, point):
	'''Rotates system about a point.'''
	from kirke import GAS
	math = GAS() #empty gas to use its functions
	
	for i in range(sys.number_of_atoms):
		r = [sys.x[i], sys.y[i], sys.z[i]]
		# rprime = rotate*(r-point)+point
		rprime = math.vector_vector_add_3D(
			math.matrix_vector_product_3D(rotation_matrix, math.vector_vector_subtract_3D(r,point)) ,
			point)
		sys.x[i] = rprime[0]
		sys.y[i] = rprime[1]
		sys.z[i] = rprime[2]
	

def rotate_about_z_axis_and_point(sys, angle, point):
	from kirke.manipulate import  compute_rotation_matrix_from_basis_vectors, rotate_about_point
	from math import sin, cos,pi
	theta = angle*pi/180
	rmat =  compute_rotation_matrix_from_basis_vectors([cos(theta),sin(theta),0], [-sin(theta),cos(theta),0], [0,0,1],
		[1,0,0], [0,1,0], [0,0,1] )
	rotate_about_point(sys, rmat, point)

def rotate_about_z_axis(sys,angle):
	point = [0,0,0] 
	rotate_about_z_axis_and_point(sys, angle, point)


def rotate_coordinates(sys, new_axes = [[],[],[]]):
	''' Rotates x y and z coordinates of a GAS object.'''
	#Note: if you want to rotate only a subset of atoms, use sys=GAS.get_subset
	from numpy import array, cross, dot, matrix, swapaxes
	from math import sqrt

	T =[[0,0,0],[0,0,0],[0,0,0]]
	old_axes = array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])
	if new_axes == [[],[],[]]: 
		new_axes=array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])
		print "rotate_coordinates: The system will not be rotated: no new axes given"

	else:
		norm_factor = []
		norm_factor.append(sqrt(new_axes[0][0]**2+new_axes[0][1]**2+new_axes[0][2]**2))
		norm_factor.append(sqrt(new_axes[1][0]**2+new_axes[1][1]**2+new_axes[1][2]**2))
		norm_factor.append(sqrt(new_axes[2][0]**2+new_axes[2][1]**2+new_axes[2][2]**2))
		new_axes[0] = [new_axes[0][0]/norm_factor[0],new_axes[0][1]/norm_factor[0],new_axes[0][2]/norm_factor[0]]
		new_axes[1] = [new_axes[1][0]/norm_factor[1],new_axes[1][1]/norm_factor[1],new_axes[1][2]/norm_factor[1]]
		new_axes[2] = [new_axes[2][0]/norm_factor[2],new_axes[2][1]/norm_factor[2],new_axes[2][2]/norm_factor[2]]
		T[0]=[dot(old_axes[0],new_axes[0]), dot(old_axes[1],new_axes[0]),dot(old_axes[2],new_axes[0])]
		T[1]=[dot(old_axes[0],new_axes[1]), dot(old_axes[1],new_axes[1]),dot(old_axes[2],new_axes[1])]
		T[2]=[dot(old_axes[0],new_axes[2]), dot(old_axes[1],new_axes[2]),dot(old_axes[2],new_axes[2])]
		T_matrix = matrix(T)

		for i in range(sys.number_of_atoms):
			xyz = matrix([sys.x[i], sys.y[i], sys.z[i]])
			xyz_vert = swapaxes(xyz,0,1)

			rotated_xyz = T_matrix*xyz_vert
			
			sys.x[i] = float(rotated_xyz[0][0])
			sys.y[i] = float(rotated_xyz[1][0])
			sys.z[i] = float(rotated_xyz[2][0])
	return sys

def rotate_fft_coordinates(coord, new_axes = [[],[],[]]):
	'''Rotates a collection of xyz coordinates.'''

	from numpy import array, dot, swapaxes
	from math import sqrt
	
	T =[[0,0,0],[0,0,0],[0,0,0]]
	old_axes = array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])

	if new_axes == [[],[],[]]: 
		new_axes=array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])
		print "rotate_fft_coordinates: The system will not be rotated: no new axes given"

	else:
		norm_factor = []
		norm_factor.append(sqrt(new_axes[0][0]**2+new_axes[0][1]**2+new_axes[0][2]**2))
		norm_factor.append(sqrt(new_axes[1][0]**2+new_axes[1][1]**2+new_axes[1][2]**2))
		norm_factor.append(sqrt(new_axes[2][0]**2+new_axes[2][1]**2+new_axes[2][2]**2))
		new_axes[0] = [new_axes[0][0]/norm_factor[0],new_axes[0][1]/norm_factor[0],new_axes[0][2]/norm_factor[0]]
		new_axes[1] = [new_axes[1][0]/norm_factor[1],new_axes[1][1]/norm_factor[1],new_axes[1][2]/norm_factor[1]]
		new_axes[2] = [new_axes[2][0]/norm_factor[2],new_axes[2][1]/norm_factor[2],new_axes[2][2]/norm_factor[2]]
		T[0]=[dot(old_axes[0],new_axes[0]), dot(old_axes[1],new_axes[0]),dot(old_axes[2],new_axes[0])]
		T[1]=[dot(old_axes[0],new_axes[1]), dot(old_axes[1],new_axes[1]),dot(old_axes[2],new_axes[1])]
		T[2]=[dot(old_axes[0],new_axes[2]), dot(old_axes[1],new_axes[2]),dot(old_axes[2],new_axes[2])]
		
		coord=array(coord)
		coord = swapaxes(coord,0,1)
		#print coord
		for i in range(len(coord)):

			xyz = [coord[i][0], coord[i][1], coord[i][2]]
			ans = [0.0,0.0,0.0]
			for a in range(3):
				for b in range(3):
					ans[a]+= T[a][b]*xyz[b]
			rotated_xyz = ans
			coord[i][0] = float(rotated_xyz[0])
			coord[i][1] = float(rotated_xyz[1])
			coord[i][2] = float(rotated_xyz[2])
		
		return coord


