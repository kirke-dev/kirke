#!/usr/bin/env python

def move(sys, direction = [0.0,0.0,0.0], with_box = True): # added by mike 3/27/2012
		'''Translates all x,y,z and box values by adding the given direction vector to all values. Default direction value is [0.0, 0.0, 0.0]'''
		for i in range(sys.number_of_atoms):
			sys.x[i] = sys.x[i] + direction[0]
			sys.y[i] = sys.y[i] + direction[1]
			sys.z[i] = sys.z[i] + direction[2]
		if with_box:
			if sys.box_mode == 'orthorhombic':
				sys.box[0] = [ sys.box[0][0] + direction[0], sys.box[0][1] + direction[0]]
				sys.box[1] = [ sys.box[1][0] + direction[1], sys.box[1][1] + direction[1]]
				sys.box[2] = [ sys.box[2][0] + direction[2], sys.box[2][1] + direction[2]]
			elif sys.box_mode == 'triclinic':
				sys.origin = sys.vector_vector_add_3D(sys.origin,direction)
			else:
				print 'move: Warning! The box has been selected to move the particle coordinates but the box_mode is not defined.'
		


def move_molecule(sys,molecule_id, direction= [0.0,0.0,0.0]):
	''' Moves a molecule in the specified direction.'''
	for i in range(sys.number_of_atoms):
		if sys.molecule[i] == molecule_id:
			sys.x[i] = sys.x[i] + direction[0]
                        sys.y[i] = sys.y[i] + direction[1]
                        sys.z[i] = sys.z[i] + direction[2] 

		
def  center_coordinates(sys):
	'''Moves system coordinates to be centered on 0,0,0'''
	new_origin = [
		sum(sys.x)/sys.number_of_atoms,
		sum(sys.y)/sys.number_of_atoms,
		sum(sys.z)/sys.number_of_atoms]

	for i in range(sys.number_of_atoms):
		sys.x[i] -= new_origin[0]
		sys.y[i] -= new_origin[1]
		sys.z[i] -= new_origin[2]

def center_box_image(sys):
	'''Moves the average box image to 0 0 0, because sometimes LAMMPS does not do this'''
	#Added because LAMMPS can be weird when writing restart files

	from math import floor
	if sys.nx == []:
		print 'center_box_image: This function cannot be used without sys.n* values'
	else:

		nx_center = floor(sum(sys.nx)/len(sys.nx))
		ny_center = floor(sum(sys.ny)/len(sys.ny))
		nz_center = floor(sum(sys.nz)/len(sys.nz))
	for a in range(len(sys.nx)):
		sys.nx[a]-=nx_center
		sys.ny[a]-=ny_center
		sys.nz[a]-=nz_center

