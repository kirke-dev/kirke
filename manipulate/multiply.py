#!/usr/bin/env python

# takes a system and super cells it in dimensions
def super_cell(sys,  dimensions = [1,1,1], lower_dimensions = [0,0,0]):
	''' Replicates a system in the indicated dimensions: only works on limited attributes.'''
	from kirke import GAS 
	
	if sys.box_mode == 'orthorhombic' or sys.box_mode == 'triclinic':
		output = GAS()
		output.box_mode = sys.box_mode # they should have the same symmetries 
		output.periodic = sys.periodic # they should have the same periodicities
		output.file_name = sys.file_name # and same file_name

		if sys.box_mode == 'orthorhombic':
			lx =  sys.box[0][1] - sys.box[0][0]
			ly =  sys.box[1][1] - sys.box[1][0]
			lz =  sys.box[2][1] - sys.box[2][0]
			for i in range(lower_dimensions[0], dimensions[0]):
				for j in range(lower_dimensions[1], dimensions[1]):
					for k in range(lower_dimensions[2], dimensions[2]):
						for a in range(sys.number_of_atoms):
							output.x.append(sys.x[a] + lx*i)
							output.y.append(sys.y[a] + ly*j)
							output.z.append(sys.z[a] + lz*k)
							output.atomic_number.append(sys.atomic_number[a])
							if len(sys.q) == sys.number_of_atoms:
								output.q.append(sys.q[a])
							if len(sys.selective_dynamics) == sys.number_of_atoms:
								output.selective_dynamics.append(sys.selective_dynamics[a])
			output.update_number_of_atoms()
			#now for the box
			output.box = [
				[sys.box[0][0]-lx*lower_dimensions[0],  sys.box[0][1]+ lx*(dimensions[0]-1)],
				[sys.box[1][0]-ly*lower_dimensions[1],  sys.box[1][1]+ ly*(dimensions[1]-1)],
				[sys.box[2][0]-lz*lower_dimensions[2],  sys.box[2][1]+ lz*(dimensions[2]-1)] ]
			#update the other coords
			output.fill_s_lists() # since we are using external coordinates
			output.update_internal_from_external() 


		elif sys.box_mode == 'triclinic':
			#stuffing all cells into range 0.0 to 1.0
			ncells = []
			for i in range(3):
				ncells.append(dimensions[i]-lower_dimensions[i])

			for i in range(ncells[0]):
				for j in range(ncells[1]):
					for k in range(ncells[2]):
						for atom in range(sys.number_of_atoms):
							output.sx.append(( sys.sx[atom]+i) /ncells[0] )
							output.sy.append(( sys.sy[atom]+j) /ncells[1] )
							output.sz.append(( sys.sz[atom]+k) /ncells[2] )
							output.atomic_number.append(sys.atomic_number[atom])
							if len(sys.q) == sys.number_of_atoms:
								output.q.append(sys.q[atom])
							if len(sys.selective_dynamics) == sys.number_of_atoms:
								output.selective_dynamics.append(sys.selective_dynamics[atom])
			output.update_number_of_atoms()
			#now for the box
			A=[]
			for vect in range(3):
				A.append(sys.get_basis_vector(vect))
				for j in range(3):
					A[vect][j]=A[vect][j]*(dimensions[vect]-lower_dimensions[vect])
			output.update_basis_from_vectors(a1 = A[0],a2 = A[1],a3 = A[2])
			output.origin = sys.vector_vector_add_3D(sys.origin, sys.matrix_vector_product_3D(sys.basis,lower_dimensions))
			#update the other coords
			output.fill_position_lists() # since we are using external coordinates
			output.update_external_from_internal()
		
		
		
		output.update_element_list()
		output.fill_id_list()
		return output

	else: 
		print "super_cell: box_mode is not 'triclinic' or 'orthorhombic' This function doesn't know what to do!\n"


def scale(sys, factor = [1.0,1.0,1.0]):
	'''Rescales x y and z values by the given factors'''
	from kirke import GAS 
	out = GAS()
	out.number_of_atoms = sys.number_of_atoms
	out.atomic_number = sys.atomic_number
	out.element = sys.element
	out.box = [[sys.box[0][0]*factor[0],sys.box[0][1]*factor[0]],[sys.box[1][0]*factor[1],sys.box[1][1]*factor[1]],[sys.box[2][0]*factor[2],sys.box[2][1]*factor[2]]]
	out.file_name = sys.file_name
	for i in range(sys.number_of_atoms):
		out.x.append( sys.x[i]*factor[0])
		out.y.append( sys.y[i]*factor[1])
		out.z.append( sys.z[i]*factor[2])

	return out


