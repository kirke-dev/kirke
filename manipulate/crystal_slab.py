#!/usr/bin/env python


def crystal_slab(sys, element_symbol, dimensions = [1,1,1], orientation = [1,0,0]):
	'''Work in progress'''
	from kirke import GAS
	from kirke.element import element_symbol_list, element_lattice_type, element_lattice_ratio
	

	element = element_symbol_list.index(element_symbol)
	lattice_type = element_lattice_type[element]

	if lattice_type == 'FCC':
		print 'crystal_slab: Face centered cubic lattice'
	elif lattice_type == 'BCC':
		print 'crystal_slab: Body centered cubic lattice'
	elif lattice_type == 'CUB':
		print 'crystal_slab: Basic cubic lattice'
	elif lattice_type == 'HEX':
		lattice_ratio = element_lattice_ratio[element]
		print 'crystal_slab: Hexagonal lattice with an a:c ratio of %s' %lattice_ratio
	elif lattice_type == 'DIA':
		print 'crystal_slab: Diamond cubic lattice'
	else:
		print 'crystal_slab: Im sorry, but lattice type %s is currently unsupported by the slab generator tool' %lattice_type

