#!/usr/bin/python

from kirke import * 
from kirke.read import LAMMPS_file
from kirke.manipulate import unwrap_from_bond_list
from kirke.write import write_LAMMPS, write_XYZ

#sys = LAMMPS_file('temp_2', number_to_element={1:'Car',2:'CT',3:'CT',4:'Har',5:'HC', 6: 'HC', 7: 'O', 8: 'OE', 9: 'CT', 10: 'CT', 11: 'CT', 12: 'HN', 13: 'HN', 14: 'HC', 15: 'N20', 16: 'N1', 17: 'N21', 18: 'N0', 19:'CTE', 20: 'CTR', 21: 'OH', 22: 'HO' })


sys = LAMMPS_file('polyethylene.data', number_to_element={1:'H', 2: 'CT', 3: 'CTR', 4:'Cu'}, atom_style = 'full')

print sys.number_of_bonds
print sys.bonds

unwrapped = unwrap_from_bond_list(sys)
