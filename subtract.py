#!/usr/bin/env python

def subtract_atoms(self,id_list):
	''' Using a list of atom IDs, removes atoms from all atomistic lists and calls update_number_of_atoms. Does not update the id list; use fill_id_list for that. '''
#The dict works, provided you create a list of indices, and then reverse sort it so that you always pop from the highest possible index, and therefore don't affect any other values you might want.
	if len(id_list) > 0:

		if self.id_to_index == []:
			self.build_index_dict()
		#Building the index list

		index_list = []
		for i in id_list:
			index_list.append(self.id_to_index[i])
		index_list.sort(reverse=True)

		for index in index_list:
			self.id.pop(index)
			self.x.pop(index)
			self.y.pop(index)
			self.z.pop(index)
			#3/19/14
			if len(self.masses) ==len(self.id)+1:#The plus one is because we just popped an id value
				self.masses.pop(index) 

			#8/13/12
			if self.vx != []:
				self.vx.pop(index)
				self.vy.pop(index)
				self.vz.pop(index)
			##
			self.atomic_number.pop(index)
			self.element.pop(index)
			if self.q !=[]:
				self.q.pop(index)
			#7/25/12
			if self.molecule!= []:
				self.molecule.pop(index)
			#8/13/12
			if self.nx != []:
				self.nx.pop(index)
				self.ny.pop(index)
				self.nz.pop(index)
			if self.sx != []:
				self.sx.pop(index)
				self.sy.pop(index)
				self.sz.pop(index)

		self.update_number_of_atoms() 
		self.build_index_dict()


######### K. Sebeck 27 March 2012 Additions
# Replaced broken code with new verion - ejcoyle 17 Feb 2014
def subtract_bonds(self,bond_id_list):
	'''Using the given list of bond ids, removes the items from the bonds list'''
	#Building a bondid_to_index dict because things aren't always in order and starting at 1
	new_dict = []
	for b in range(len(self.bonds)):
		new_dict.append([self.bonds[b][0],b])
	bond_to_index = dict(new_dict)

	bond_index_list = []
	for b in bond_id_list:
		bond_index_list.append(bond_to_index[b])
	#id_list is generated elsewhere
	bond_index_list.sort(reverse=True)
	for i in bond_index_list:
		self.bonds.pop(i)
	self.update_number_of_bonds()

def subtract_bonds_fast(self, bond_id_list):
	'''placeholder'''
	self.subtract_bonds(bond_id_list)
	print 'Please update to use subtract_bonds. This function will soon be exterminated.'

# Replaced broken code with new verion - ejcoyle 17 Feb 2014
def subtract_angles(self,angle_id_list):
	'''Using the given list of angle IDs, removes items from the angles list'''
	#building the dict for robustness
	new_dict = []
	for a in range(len(self.angles)):
		new_dict.append([self.angles[a][0],a])
	angle_to_index = dict(new_dict)

	angle_index_list = []
	for i in angle_id_list:
		angle_index_list.append(angle_to_index[i])
	#id_list is generated elsewhere
	angle_index_list.sort(reverse=True)
	for i in angle_index_list:
		self.angles.pop(i)
	self.update_number_of_angles()

def subtract_angles_fast(self,angles_id_list):
	'''placeholder'''
	self.subtract_angles(angles_id_list)
	print 'Please update to use subtract_angles. This function will soon be no more.'

# Replaced broken code with new verion - ejcoyle 17 Feb 2014
def subtract_dihedrals(self,dihedrals_id_list):
	'''Using the given list of angle IDs, removes items from the dihedrals list'''
	#dictionary for robustness
	new_dict = []
	for d in range(len(self.dihedrals)):
		new_dict.append([self.dihedrals[d][0],d])
	dihedral_to_index = dict(new_dict)

	dihedrals_index_list = []
	for d in dihedrals_id_list:
		dihedrals_index_list.append(dihedral_to_index[d])
	dihedrals_index_list.sort(reverse=True)

	for i in dihedrals_index_list:
		self.dihedrals.pop(i)
	self.update_number_of_dihedrals()

def subtract_dihedrals_fast(self,dihedrals_id_list):
	'''placeholder'''
	self.subtract_dihedrals(dihedrals_id_list)
	print "Please update to use subtract_dihedrals. I've done it for you this time, but this will be DELETED"

# Replaced broken code with new verion - ejcoyle 17 Feb 2014
def subtract_impropers(self,impropers_id_list):
	'''Using the given list of angle IDs, removes items from the impropers list'''
	#id_list is generated elsewhere
	#dictionary for robustness
	new_dict = []
	for i in range(len(self.impropers)):
		new_dict.append([self.impropers[i][0],i])
	improper_to_index = dict(new_dict)

	impropers_index_list = []
	for i in impropers_id_list:
		impropers_index_list.append(improper_to_index[i])

	impropers_index_list.sort(reverse=True)
	for i in impropers_index_list:
		self.impropers.pop(i)
	self.update_number_of_impropers()


def subtract_impropers_fast(self,impropers_id_list):
	'''placeholder'''
	self.subtract_impropers(impropers_id_list)
	print 'Camelot is a silly place. This bit of code is only a redirect. Use subtract_improper now.'

def subtract_molecules(self, molecule_id_list=[], verbose = False):
	''' Using the given list of molecule IDs, removes the molecules and their associated atoms, bonds, angles, dihedrals, and impropers. Updates atom IDs in remaining bonds, angles, dihedrals, and impropers. NOTE: Be careful if calling this function alongside the other subtract functions, e.g. in polymerization scripts. Doing so can lead to erroneous results. '''

	atoms_to_remove = []
	bonds_to_remove = []
	angles_to_remove = []
	dihedrals_to_remove = []
	impropers_to_remove = []
	self.number_of_molecules = max(self.molecule)

	if verbose == True:
		print 'subtract_molecules(verbose): System starts with {0} molecules:\n\t{1} atoms\n\t{2} bonds\n\t{3} angles\n\t{4} dihedrals\n\t{5} impropers\n\nSearching for things to delete, please wait ...'.format(self.number_of_molecules, len(self.id), len(self.bonds), len(self.angles), len(self.dihedrals), len(self.impropers))

	for i in range(len(self.molecule)):
		if self.molecule[i] in molecule_id_list:
			atoms_to_remove.append(self.id[i])

	for i in range(len(self.bonds)):
		if self.bonds[i][2] in atoms_to_remove or self.bonds[i][3] in atoms_to_remove:
			bonds_to_remove.append(self.bonds[i][0])

	for i in range(len(self.angles)):
		if self.angles[i][2] in atoms_to_remove or self.angles[i][3] in atoms_to_remove or self.angles[i][4] in atoms_to_remove:
			angles_to_remove.append(self.angles[i][0])

	for i in range(len(self.dihedrals)):
		if self.dihedrals[i][2] in atoms_to_remove or self.dihedrals[i][3] in atoms_to_remove or self.dihedrals[i][4] in atoms_to_remove or self.dihedrals[i][5] in atoms_to_remove:
			dihedrals_to_remove.append(self.dihedrals[i][0])

	for i in range(len(self.impropers)):
		if self.impropers[i][2] in atoms_to_remove or self.impropers[i][3] in atoms_to_remove or self.impropers[i][4] in atoms_to_remove or self.impropers[i][5] in atoms_to_remove:
			impropers_to_remove.append(self.impropers[i][0])

	if verbose == True:
		print 'subtract_molecules(verbose): Deleting {0} molecules containing:\n\t{1} atoms\n\t{2} bonds\n\t{3} angles\n\t{4} dihedrals\n\t{5} impropers\n\nSubtracting things ...'.format(len(molecule_id_list), len(atoms_to_remove), len(bonds_to_remove), len(angles_to_remove), len(dihedrals_to_remove), len(impropers_to_remove))

	atoms_to_remove.sort()
	bonds_to_remove.sort()
	angles_to_remove.sort()
	dihedrals_to_remove.sort()
	impropers_to_remove.sort()
	molecule_id_list.sort(reverse=True)

	self.subtract_atoms(atoms_to_remove)
	self.fill_id_list(start = 1)
	self.subtract_bonds(bonds_to_remove)
	self.subtract_angles(angles_to_remove)
	self.subtract_dihedrals(dihedrals_to_remove)
	self.subtract_impropers(impropers_to_remove)

	if verbose == True:
		print '\nsubtract_molecule(verbose): Updating molecule numbers and atom IDs in remaining topology. Please wait, it\'s kinda slow ...'

	# Fixed issue with atom ID updates (changed > to >=) 8 Sept 2014 ejcoyle
	atoms_to_remove.sort(reverse=True)
	for i in range(len(self.bonds)):			# bonds
		for j in range(len(atoms_to_remove)):
			if self.bonds[i][2] >= atoms_to_remove[j]:
				self.bonds[i][2] -= 1
			if self.bonds[i][3] >= atoms_to_remove[j]:
				self.bonds[i][3] -= 1

	for i in range(len(self.angles)):			# angles
		for j in range(len(atoms_to_remove)):
			if self.angles[i][2] >= atoms_to_remove[j]:
				self.angles[i][2] -= 1
			if self.angles[i][3] >= atoms_to_remove[j]:
				self.angles[i][3] -= 1
			if self.angles[i][4] >= atoms_to_remove[j]:
				self.angles[i][4] -= 1

	for i in range(len(self.dihedrals)):			# dihedrals
		for j in range(len(atoms_to_remove)):
			if self.dihedrals[i][2] >= atoms_to_remove[j]:
				self.dihedrals[i][2] -= 1
			if self.dihedrals[i][3] >= atoms_to_remove[j]:
				self.dihedrals[i][3] -= 1
			if self.dihedrals[i][4] >= atoms_to_remove[j]:
				self.dihedrals[i][4] -= 1
			if self.dihedrals[i][5] >= atoms_to_remove[j]:
				self.dihedrals[i][5] -= 1

	for i in range(len(self.impropers)):			# impropers
		for j in range(len(atoms_to_remove)):
			if self.impropers[i][2] >= atoms_to_remove[j]:
				self.impropers[i][2] -= 1
			if self.impropers[i][3] >= atoms_to_remove[j]:
				self.impropers[i][3] -= 1
			if self.impropers[i][4] >= atoms_to_remove[j]:
				self.impropers[i][4] -= 1
			if self.impropers[i][5] >= atoms_to_remove[j]:
				self.impropers[i][5] -= 1

	while len(molecule_id_list) > 0:			# molecule numbers
		for i in range(len(self.molecule)):
			if self.molecule[i] > molecule_id_list[0]:
				self.molecule[i] -= 1
		molecule_id_list.pop(0)

	self.update_number_of_molecules()

	# Added ID updates for the other things, too, in order to keep LAMMPS happy. 11 Sept 2014 ejcoyle
	for i in range(len(self.bonds)):
		self.bonds[i][0] = i+1

	for i in range(len(self.angles)):
		self.angles[i][0] = i+1

	for i in range(len(self.dihedrals)):
		self.dihedrals[i][0] = i+1

	for i in range(len(self.impropers)):
		self.impropers[i][0] = i+1

	if verbose == True:
		print 'subtract_molecule(verbose): System now has {0} molecules:\n\t{1} atoms\n\t{2} bonds\n\t{3} angles\n\t{4} dihedrals\n\t{5} impropers\n'.format(self.number_of_molecules, len(self.id), len(self.bonds), len(self.angles), len(self.dihedrals), len(self.impropers))

