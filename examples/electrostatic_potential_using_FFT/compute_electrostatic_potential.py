##### reading in electron density
from kirke.grid.read import CHGCAR_file
rho = CHGCAR_file()
total_number_of_electrons = rho.field.sum() * rho.compute_cell_volume()
rho.field = - rho.field # fix the sign for negatively charged electrons


##### reading in nuclear core charges
from kirke.read import POSCAR_file
#this is because not all electrons are used on each atom in the pseudo potentials
element_to_charge_dict = {	'H':1.0,
				'B':3.0,
				'C':4.0,
				'N':5.0,
				'Cl':7.0}
atoms = POSCAR_file('CONTCAR')
atoms.fill_q_list()
for i in range(atoms.number_of_atoms):
	atoms.q[i] = element_to_charge_dict[atoms.element[i]]


### these should be close for charge neutrality
print 'total number of electrons:', total_number_of_electrons 
print 'total_nuclear charge:',sum(atoms.q)


#### an nice way of creating an independent nuclear rho if you need to for some reason
#from kirke.grid import create_RGS_from_GAS
#nuclear_rho = create_RGS_from_GAS(electron_rho.field.shape, atoms) 

### we just add them together
from kirke.grid.interpolate import interpolate_scalar_to_mesh
interpolate_scalar_to_mesh(rho,  # this is a nice trick because the interpolate just adds the interpolated values to the supplied RGS
	sys = atoms, 
	point_value_list = atoms.q,  
	order = 7, 
	zero_centered = True )





from kirke.grid.analyze import compute_electrostatic_potential_using_FFT
phi = compute_electrostatic_potential_using_FFT(charge_density_rgs = rho)

from kirke.grid.write import write_XSF_DATAGRID

write_XSF_DATAGRID(phi,'electrostatic_potential.xsf')




