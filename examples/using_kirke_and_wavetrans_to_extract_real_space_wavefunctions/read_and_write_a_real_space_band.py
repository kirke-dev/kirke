from kirke.grid.read import  GCOEFF_file
# this parses the file and creates a text file with the locations of all the bands and kpoints within the GCOEFF file.
#it writes a look-up file to speed up things later so it will be very fast after the first pass.
my_wavecar_data = GCOEFF_file() 

######################## Part 1
######################## Basics of the data format


a_wave_function = my_wavecar_data.get_real_space_band(4) # returns the 5th band in the GCOEFF file

# basically just reading the wavefunction value from the first cell 
print a_wave_function.field[0,0,0]
#  .field is just a numpy array. 

#if i want the first basis vector
print a_wave_function.get_basis_vector(0)

# now we are projecting the wave function on it's self <psi|psi>
print (a_wave_function.field * a_wave_function.field.conjugate()).sum() *a_wave_function.compute_cell_volume() 
# this won't equal 1 because of the muffin tin componenets to the wavefunctions


######################## Part 2
######################## Using the sorted bands to the the HOMO/valence band


#the kpoints are in whatever order they are in the KPOINTS file,  
#the initialization of the GCOEFF class creates a list of all bands and k-points sorted by their energy


HOMO_index = my_wavecar_data.get_HOMO_index() #this gets the index of the HOMO or valence band edge in the energy sorted list.


band_number_for_HOMO = my_wavecar_data.sorted_band_list[HOMO_index][2] # the third value in that list is the band number which can be used for retrieveing the values.
# if i wanted the HOMO-3, i could simply pass the HOMO_index - 3 to this.


HOMO_density = my_wavecar_data.get_real_space_band_density(band_number_for_HOMO) # this function squares the values automatically to save that ugly step we saw before.

print HOMO_density.field[0,0,0]

print HOMO_density.field.sum()*HOMO_density.compute_cell_volume() 

# you can also use my_wavecar_data.get_LUMO_index() to do the same.




######################## Part 3
######################## Outputing to Xcrsyden in a nice way


# if you use xcrysden, You might want to out put a single band density plus the atoms to an XSF file
my_band = 4
my_band_density = my_wavecar_data.get_real_space_band_density(my_band)



from kirke.grid.write import write_XSF_DATAGRID # this formats the data grid into the XSF file format
write_XSF_DATAGRID(my_band_density, file_name =  'band_number_%i_density.xsf'% my_band)
		

## now we use the atomisitic componets of kirke to read in the atoms from the CONCAR file
from kirke.read import POSCAR_file
from kirke.write import write_XSF
my_atoms = POSCAR_file('CONTCAR')
my_atoms.comment = 'silicon UC'
write_XSF(my_atoms, file_name = 'my_atoms.xsf' ) # no different than file conversion		


		

# all you have to do is join them together, it's that lazy.
from os import system
system('cat my_atoms.xsf band_number_%i_density.xsf'% my_band +' > atoms_and_band_%i.xsf' % my_band)
