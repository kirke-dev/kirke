from kirke.read import CUBE_file

atoms = CUBE_file('LUMO.cube')
atoms.fill_id_list()

from kirke.write import write_POSCAR

write_POSCAR(atoms)
