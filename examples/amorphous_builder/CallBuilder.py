#!/usr/bin/python

from kirke.write import write_LAMMPS
from kirke.read import LAMMPS_file
from kirke.manipulate import amorphous_builder

''' Build an amorphous system of monomers '''

file1 = 'monomer1'		# input and output file info
file2 = 'monomer2'
file3 = 'monomer3'
out_file = 'box_of_monomers'

count = [20, 10, 20]		# number of each monomer
monomer_list = []		# monomers in the form of GAS objects; filled after reading files

itermax = 10000			# maximum number of tries to place a molecule
rmin = 2.2			# conflict cutoff
density = 0.25			# desired system density

number_to_element = {1:'H', 2:'C', 3:'F', 4:'H'}
element_to_number = {'H':1, 'C':2, 'F':3, 'H':4}

# importing monomers
print '\nReading data from {0} ...'.format(file1)
molecule1 = LAMMPS_file(file1, number_to_element)
molecule1.sort_by('id')

print 'Reading data from {0} ...'.format(file2)
molecule2 = LAMMPS_file(file2, number_to_element)
molecule2.sort_by('id')

print 'Reading data from {0} ...'.format(file3)
molecule3 = LAMMPS_file(file3, number_to_element)
molecule3.sort_by('id')

# amorphous_builder requires a list of GAS objects for the monomer input! 
monomer_list = [molecule1, molecule2, molecule3]

# building the system
sys = amorphous_builder(out_file, monomer_list, count, itermax, rmin, density, periodic=[True,True,True], aspect_ratio=1, fix_xy_dimensions=False, verbose=True)

print 'Writing {0} ...'.format(out_file)
write_LAMMPS(sys,out_file,element_to_number)
