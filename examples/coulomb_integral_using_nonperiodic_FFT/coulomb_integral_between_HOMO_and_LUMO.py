

#these two wave functions were calculated using the same grid, this is requirment of this example, in the future, we might have an interpolation system for going between grids
from kirke.grid.read import CUBE_file
HOMO = CUBE_file('HOMO.cube',square_values = True)
LUMO = CUBE_file('LUMO.cube',square_values = True)



from kirke.grid.analyze import compute_nonperiodic_electrostatic_potential_using_FFT
HOMO_phi = compute_nonperiodic_electrostatic_potential_using_FFT(HOMO) # i use the default coulomb constant of ~14.4 eV/(ang*e.u.^2)


# option for saving the data, incase you don't want to redo the calculation
if False:
	from kirke.grid.write import write_XSF_DATAGRID
	write_XSF_DATAGRID(phi,'phi_HOMO.xsf')

# with the potential field of the HOMO, all we need now is to compute the volume integral of the LUMO and the potential field of the HOMO
coulomb_integral = (LUMO.field * HOMO_phi.field).sum()  *  LUMO.compute_cell_volume()

#
print 'Coulomb Integral (eV):', coulomb_integral
