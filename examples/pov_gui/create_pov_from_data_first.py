#!/usr/bin/python


from kirke.read import LAMMPS_file
from kirke.write import write_POV

#This is a more complex POVray rendering, showing some of the more common options

input_file = 'LAMMPS_sample.data'

sys = LAMMPS_file(input_file, number_to_element={1:'C',2:'C',3:'C',4:'H',5:'H', 6: 'H', 7: 'O', 8: 'O', 9: 'C', 10: 'C', 11: 'C', 12: 'H', 13: 'H', 14: 'H', 15: 'N', 16: 'N', 17: 'N', 18: 'N', 19:'C', 20: 'C' , 21: 'O', 22: 'H'})


custom_radii = {1:0.60, 2:0.75, 3:0.75, 4:0.32,5:0.32, 6: 0.32, 7: 0.63, 8: 0.63, 9: 0.75, 10: 0.75, 11: 0.75, 12: 0.32, 13: 0.32, 14: 0.32, 15: 0.71, 16: 0.71, 17: 0.71, 18: 0.71, 19:0.75, 20: 0.75, 21: 0.63, 22: 0.32, 23: 1.25 }

# H [0.9, 0.9,0.9] 
#HO [0.8, 0.8, 1.0]
# Car: [0.4,0.4,0.4] 
#Reactive sites [0.2, 0.2, 0.8]
#Other Carbon [0.6,0.6,1.0] 
#O [1.0, 0.06, 0.06] 
#N [1.0,1.0,0.39] 
lookpoint = []
lookpoint.append(sum(sys.x)/sys.number_of_atoms)
lookpoint.append(sum(sys.y)/sys.number_of_atoms)
lookpoint.append(sum(sys.z)/sys.number_of_atoms)

custom_colors = {1:[0.6,0.6,1.0] ,2:[0.6,0.6,1.0],3:[0.6,0.6,1.0],4:[0.9, 0.9,0.9] ,5:[0.9, 0.9,0.9] , 6: [0.9, 0.9,0.9] , 7: [1.0, 0.06, 0.06] , 8: [1.0, 0.06, 0.06] , 9: [0.6,0.6,1.0], 10: [0.6,0.6,1.0], 11: [0.6,0.6,1.0], 12: [0.9, 0.9,0.9] , 13: [0.9, 0.9,0.9] , 14: [0.9, 0.9,0.9] , 15: [1.0,1.0,0.39] , 16: [1.0,1.0,0.39] , 17: [1.0,1.0,0.39] , 18: [1.0,1.0,0.39] , 19:[0.05, 0.05, 0.5], 20: [0.2, 0.2, 0.8], 21: [1.0, 0.06, 0.06] , 22: [0.8, 0.8,1.0], 23: [0.3,0.3,0.3]  }


write_POV(sys, file_name = 'customized_render.pov',image_name = 'customized_render.png' , render = False, coloring = custom_colors, height = 1200,width = 1600,
		       camera_style = 'perspective', # 'orthographic angle 30' will work in povray_gui but it does funny things with the translating in and out
		       camera_position = [-70, 55, 80], camera_look_at_point = lookpoint,
		       background_color = [1,1,1],
		       display = True, light_position = [-50,55,55], finish = {},radius_scale = 0.75, radius = custom_radii,
		       comment = 'Rendered with custom colors and bonds, orthographic camera style',comment_size = 32,show_charge = False, light_at_camera = True, rotate = 0, with_bonds = True, bond_scale = 0.75)




