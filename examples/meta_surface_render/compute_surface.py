from kirke.grid.read import CUBE_file

my_rgs = CUBE_file('HOMO.cube',square_values = True)
print 'Total Number of Cells :', my_rgs.number_of_cells

from kirke.grid.analyze import compute_cutoff_by_sum_percentile

cut_off = compute_cutoff_by_sum_percentile(my_rgs,80.0) # this works well for MOs
#cut_off = some value of interest like a mass density 


#### we only want to scan cells near the cutoff for speed reasons
safety_factor = 10.0  #increase this till you are happy with the images or it doesn't go up much.
cut_high = safety_factor*cut_off
cut_low  = (1.0/safety_factor)*cut_off

#lets keep them in bounds
cut_high = min(cut_high, my_rgs.field.max())
cut_low = max(cut_low, my_rgs.field.min())


from kirke.grid.analyze import get_cells_between_cutoffs
cell_list = get_cells_between_cutoffs(my_rgs,cut_low, cut_high)
print 'Cells Near Surface :' , len(cell_list) 



# we use cell_lists purely for speed rather than testing each cell
from kirke.grid.analyze import find_surface_from_cell_list
nsurface = find_surface_from_cell_list(my_rgs, cell_list, cut_off, file_name = 'surface_points.txt')
print 'Cells Projected to Surface :', nsurface
