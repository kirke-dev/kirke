from kirke.grid.read import CUBE_file

my_rgs = CUBE_file('HOMO.cube',square_values = True)

from kirke.grid.analyze import compute_cutoff_by_sum_percentile

cut_off = compute_cutoff_by_sum_percentile(my_rgs,80.0)

print 'Max:', my_rgs.field.max()
print '25%:', compute_cutoff_by_sum_percentile(my_rgs,25.0)
print '50%:', compute_cutoff_by_sum_percentile(my_rgs,50.0)
print '70%:', compute_cutoff_by_sum_percentile(my_rgs,70.0)
print '80%:', cut_off
print '90%:', compute_cutoff_by_sum_percentile(my_rgs,90.0)
print 'Min:', my_rgs.field.min() 

