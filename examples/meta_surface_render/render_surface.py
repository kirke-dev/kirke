


height = 2000
width = height
render = True
gui = True

name = 'HOMO'









image_name = name + '.png'
file_name = 'atoms_'+ name + '.pov'
sphere_file = 'spheres_' + name + '.pov'
combined_file = name + '.pov'

#####################


custom_colors= {1: [0.90, 0.90, 0.90],
		6: [0.35, 0.40, 0.40],
		7: [0.00, 0.25, 1.00],
#
		5: [0.85, 0.30, 0.48],
		13:[0.64, 0.64, 0.64],
		31:[0.00, 0.50, 0.07],
		49:[0.48, 0.01, 0.02],
		21:[0.41, 0.08, 0.83],
		39:[0.20, 0.67, 0.92],
#
		9: [0.90, 1.00, 0.00],
		17:[0.40, 1.00, 0.20],
		35:[0.80, 0.45, 0.00],
		53:[1.00, 0.75, 0.95]}



rNT = 2.7 #
rTX = 3.2 #

my_bond_rules= [[1, 1.7, 'C','C'],
		[2, 1.2, 'C','H'],
		[3, 1.6, 'C','N'],
#
		[4, rNT, 'N','B'],
		[5, rNT, 'N','Al'],
		[6, rNT, 'N','Ga'],
		[7, rNT, 'N','In'],
		[8, rNT, 'N','Sc'],
		[9, rNT, 'N','Y'],
#
		[10, rTX, 'B','F'],
		[11, rTX, 'B','Cl'],
		[12, rTX, 'B','Br'],
		[13, rTX, 'B','I']]
#



#######################


from kirke.read import  XYZ_file, CUBE_file
atoms =  CUBE_file(  name+'.cube')



######################3


from kirke.manipulate import generate_bonds_from_neighbors

atoms.fill_id_list(1)
new_bonds = generate_bonds_from_neighbors(atoms, periodic = [False, False, False],bond_rules = my_bond_rules , verbose = False)
atoms.add_bonds(new_bonds)


####################

#### better side view
look_at = atoms.convert_internal_position_to_external([0.5 ,0.5,   0.5])
campos = atoms.convert_internal_position_to_external([0.80 ,0.30, -0.6])
lit_pos = atoms.convert_internal_position_to_external([1.5, -2.0, -2])
up = [1.0,0,0]


### top down
#look_at = atoms.convert_internal_position_to_external([0.5 ,0.5,   0.5])
#campos = atoms.convert_internal_position_to_external([1.4 ,0.5,   0.5])
#lit_pos = atoms.convert_internal_position_to_external([0.5+3.5, 0.5 + 0.5, -5])
#up = [0,0.0,1.0]

from kirke.write import write_POV
atoms.number_of_atoms = 0 # lazy way of making it think it is empty
write_POV(atoms,
	file_name = file_name,
	image_name = image_name,
	height = height ,
	width = height ,
	render = False ,
	background_color = [1,1,1],
	camera_look_at_point = look_at,
	up_direction = up,
	camera_position = campos,
	radius_scale = 0.45, coloring = custom_colors,
	with_bonds = False,
	light_position = lit_pos, light_at_camera = True
	)




######## now for the spheres



from numpy import loadtxt, sqrt, array, dot
fake = loadtxt('surface_points.txt').T


#########################################################################################
##gonna have to make some ellipses here...

fid2 = open(sphere_file,'w')
####### header stuff ###########


cells = loadtxt('cell_info.txt')
############# Finding sphere sizes #############
thing = []
for i in range(3):
	vect = array(atoms.get_basis_vector(i))/cells[i]
	l = sqrt(dot(vect,vect))
	thing.append(l)

cell_length = max(thing)
sphere_radius = 0.5*sqrt(cell_length)

######################## writing spheres
surface_scale = 0.7 #make the balls touch if greater than 1, I like to fill in the gap with gooeyness 

threshold = 0.8 #basically stickyness

#using these formulas, means I can just vary two parameters (above) to get the same results.
strength = 1.0 # ignore this and be happy.
ap = sphere_radius*surface_scale
ra = ap/sqrt(1.0-sqrt(threshold/strength)) 


fid2.write('\n\nblob {\n')
fid2.write('threshold %f\n'% threshold)


material_string = ''

for i in range(fake[0].size):
		x = fake[0][i]
		y = fake[1][i]
		z = fake[2][i]

		sphere_to_add = [
			'sphere {',
			' <%f, %f, %f>, %f, %f' %(x,y,z, ra,strength),
			'pigment {  rgb <0.25, 0.25, 1.0> }', #'pigment {  rgbf <0.25, 0.25, 1.0, 0.95> }', # use this for glass
			material_string,
			'}']

		for thing in sphere_to_add:
			fid2.write(thing+'\n')

if False:
	fid2.write('finish{ phong 0.8  reflection 0.2} \n\ninterior { ior 1.5 }\n\n') # this turns it into a glass

fid2.write('}\n')

fid2.close()


##########
from os import system
system('cat %s %s > %s'% (file_name,sphere_file, combined_file ) ) # with MO's

#################
if gui:
	from kirke.visualize import povray_gui
	povray_gui(combined_file)


#################
command = 'povray Input_File_Name=%s Output_File_Name=%s +H%i +W%i +A +UA' % (combined_file, image_name, height, width)
if render:
	system(command)



	





