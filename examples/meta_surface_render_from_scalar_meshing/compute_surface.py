



meshing_order = 7

#################### 
from kirke.read import LAMMPS_dump_file
LD = LAMMPS_dump_file('dump.coord.lammpstrj')
my_system = LD.get_frame(LD.number_of_frames - 1) #grabs last frame (aka last GAS object)
mydict = {1:'H', 2:'C', 3:'N', 4:'B', 5:'Cl'} # this will make this exmaple much easier to understand
my_system.convert_number_to_element(mydict)
my_system.update_atomic_number_list()
my_system.periodic = [True, True, True]
my_system.update_masses_from_element()


##########
from kirke.analyze import compute_cells_to_fit_radius_inside
cells = compute_cells_to_fit_radius_inside(my_system,radius = 0.5) # for this purpose, set the radius equal to the desired cell length. 
print 'Cells used (x, y, z)', cells



#########3
# now we mesh 

from kirke.grid import create_RGS_from_GAS 
mass_density = create_RGS_from_GAS(cells, my_system) # this means the grid inherets the basis
mass_density.comment = 'Density in grams/cm3'
my_system.update_internal_from_external() # meshing only operates on internal coordinates


from kirke.grid.interpolate import interpolate_scalar_to_mesh
# this makes the density mesh, the units are always the point_value_list/(system volume units)
interpolate_scalar_to_mesh(mass_density,
	sys = my_system, 
	point_value_list = my_system.masses,  
	order = meshing_order, 
	zero_centered = True ) #
# doesn't return data, operates on mass_density, 

mass_density.field = mass_density.field*1.660468

print 'Total Number of Cells :', mass_density.number_of_cells






####################dumping the mesh
my_system.update_basis_and_origin_for_orthorhombic()
from kirke.grid.write import write_XSF_DATAGRID
write_XSF_DATAGRID(mass_density,  file_name = 'mass_density.xsf')

from kirke.write import write_XSF
write_XSF(my_system, 'atoms.xsf')

# this combines the files
from os import system
system('cat atoms.xsf mass_density.xsf > atoms_and_mass_density.xsf')

#########################


#############
# we create a cell list to speed up the calculation of the surface points

cut_off = 0.2
#i use these bounds to simplfiy later calculations, try chagning them to make sure you capture all the surface points
cut_high = 8.0
cut_low = 0.0001


from kirke.grid.analyze import get_cells_between_cutoffs
cell_list = get_cells_between_cutoffs(mass_density,cut_low, cut_high)
print 'Cells Near Surface :' , len(cell_list) 



# we use cell_lists purely for speed rather than testing each cell
from kirke.grid.analyze import find_surface_from_cell_list
nsurface = find_surface_from_cell_list(mass_density, cell_list, cut_off, file_name = 'surface_points.txt')
print 'Cells Projected to Surface :', nsurface


