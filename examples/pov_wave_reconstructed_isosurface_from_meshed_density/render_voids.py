height = 1000
width = 1000
render = True
gui = False

frame_total = 100




from kirke.read import LAMMPS_dump_file, LAMMPS_file
sysdump = LAMMPS_dump_file(file_name = 'out.mech', verbose = False)
mydict = {1:'C',2:'C',3:'C',4:'H',5:'H', 6: 'H', 7: 'O', 8: 'O', 9: 'C', 10: 'C', 11: 'C', 12: 'H', 13: 'H', 14: 'H', 15: 'N', 16: 'N', 17: 'N', 18: 'N', 19:'C', 20: 'C', 21: 'O', 22: 'H' }
bond_sys = LAMMPS_file('temp_mech',number_to_element={1:'Car',2:'CT',3:'CT',4:'Har',5:'HC', 6: 'HC', 7: 'O', 8: 'OE', 9: 'CT', 10: 'CT', 11: 'CT', 12: 'HN', 13: 'HN', 14: 'HC', 15: 'N20', 16: 'N1', 17: 'N21', 18: 'N0', 19:'CTE', 20: 'CTR', 21: 'OH', 22: 'HO' })

#for frame in range(0,frame_total):
#for frame in range(1):
for frame in [99]:
	sysdump.verbose = False
	atoms = sysdump.get_frame(frame)
	name = 'void_render'+str(frame)
	#cell_file = 'cell_info'+str(frame)+'.txt'
	#surface_file = 'surface_points'+str(frame)+'.txt'
	
	image_name = name + '.png'
	file_name = 'atoms_'+ name + '.pov'
	isosurface_file = 'isosurface_' + name + '.pov'
	combined_file = name + '.pov'



#	atoms.box_mode = 'orthorhombic'
#	atoms.update_box_from_positions()
	atoms.convert_number_to_element(mydict)
#	atoms.update_atomic_number_list()
	atoms.periodic = [True, True, True]
	atoms.update_basis_and_origin_for_orthorhombic()
	atoms.bonds = bond_sys.bonds

	tripod = [0,240,0]
	light_stand = [0, 100,30]
	model = [0,-50,0]
	up = [0,0,1]
	

	from kirke.write import write_POV
#	atoms.number_of_atoms = 0 #poof!

	custom_radii = {1:0.60, 2:0.75, 3:0.75, 4:0.32,5:0.32, 6: 0.32, 7: 0.63, 8: 0.63, 9: 0.75, 10: 0.75, 11: 0.75, 12: 0.32, 13: 0.32, 14: 0.32, 15: 0.71, 16: 0.71, 17: 0.71, 18: 0.71, 19:0.75, 20: 0.75, 21: 0.63, 22: 0.32, 23: 1.25, 24:1.75 }

	#custom_colors = {1:[0.4,0.4,1.0] ,2:[0.4,0.4,1.0],3:[0.4,0.4,1.0],4:[0.9, 0.9,0.9] ,5:[0.9, 0.9,0.9] , 6: [0.9, 0.9,0.9] , 7: [1.0, 0.06, 0.06] , 8: [1.0, 0.06, 0.06] , 9: [0.4,0.4,1.0], 10: [0.4,0.4,1.0], 11: [0.4,0.4,1.0], 12: [0.9, 0.9,0.9] , 13: [0.9, 0.9,0.9] , 14: [0.9, 0.9,0.9] , 15: [1.0,1.0,0.39] , 16: [1.0,1.0,0.39] , 17: [1.0,1.0,0.39] , 18: [1.0,1.0,0.39] , 19:[0.05, 0.75, 0.05], 20: [0.1, 0.5, 0.1], 21: [1.0, 0.06, 0.06] , 22: [0.8, 0.8,1.0], 23: [0.3,0.3,0.3], 24: [0.75, 0.75, 0.75]  }

	custom_colors = {1:[0.9, 0.1, 0.1],2:[0.9, 0.1, 0.1],3:[0.9, 0.1, 0.1],4:[0.9, 0.1, 0.1],5:[0.9, 0.1, 0.1],6:[0.9, 0.1, 0.1],7:[0.9, 0.1, 0.1],8:[0.9, 0.1, 0.1],9:[0.9, 0.1, 0.1],10:[0.9, 0.1, 0.1],11:[0.9, 0.1, 0.1],12:[0.9, 0.1, 0.1],13:[0.9, 0.1, 0.1],14:[0.9, 0.1, 0.1],15:[0.9, 0.1, 0.1],16:[0.9, 0.1, 0.1],17:[0.9, 0.1, 0.1],18:[0.9, 0.1, 0.1],19:[0.9, 0.1, 0.1],20:[0.9, 0.1, 0.1],21:[0.9, 0.1, 0.1],22:[0.9, 0.1, 0.1],23:[0.9, 0.1, 0.1],24:[0.9, 0.1, 0.1]}

	write_POV(atoms,
		file_name = file_name,
		image_name = file_name+'.png',
		height = height ,
		width = width ,
		render = False ,
		camera_style = 'orthographic angle 30',
		background_color = [1,1,1],
		camera_look_at_point = model,
		up_direction = up,
		camera_position = tripod,
		radius_scale = 0.25, coloring = custom_colors,
		with_bonds = True,
		light_position = light_stand, light_at_camera = True
		)

	##################################################################################### computing density data
	from kirke.analyze import compute_cells_to_fit_radius_inside
	cells = compute_cells_to_fit_radius_inside(atoms,radius = 1.5) # for this purpose, set the radius equal to the desired cell length.  #1.5
	print 'Cells used (x, y, z)', cells


	atoms.update_masses_from_element()

	average_density = sum(atoms.masses)/atoms.compute_box_volume()

	from kirke.grid import RGS, create_RGS_from_GAS
	from kirke.grid.interpolate import interpolate_scalar_to_mesh

	# the nitrogen atomic density of my system, I allocate now for speed.
	# (This always has to be done and the values will be overwriten by the meshing function)
	



	debug = False

	if debug: # this sections is for making sure the waves are going where they supposed to and visually checking coefficients 
		cells = (16,16,16)
		mass_density = create_RGS_from_GAS(cells, atoms)
		mass_density.zero_centered = True
		
		mass_density.field = mass_density.field + 1.0
		for i in range(cells[0]/2,cells[0]): # creates 1,1,1,1,0,0,0,0 on top half
			for j in range(cells[1]):
				for k in range(cells[2]/2 , cells[2]):
					mass_density.field[i][j][k] = 0.0
		
		for i in range(cells[0]):# creates linear gradient on bottom on half from 0.0 to 1.0 (in xdirection) # This means that the bottom should have a surface that ends half way in the x direction
			for j in range(cells[1]):
				for k in range(cells[2]/2):
					mass_density.field[i][j][k] =  float(i)/cells[0]

		cut_off = 0.5

	else:

		mass_density = create_RGS_from_GAS(cells, atoms)
		atoms.update_internal_from_external() # meshing only operates on internal coordinates
		# this makes the density mesh, the units are always the point_value_list/(system volume units)
		meshing_order = 7# 7
		interpolate_scalar_to_mesh(mass_density,
			sys = atoms, 
			point_value_list = atoms.masses, 
			order = meshing_order)
		# doesn't return data, operates on mass_density, 

		## this can be copied from another example an be demonstrated here
		#from plot_plane_wave_amplitude_distribution import plot_plane_wave_amplitude_distribution
		#plot_plane_wave_amplitude_distribution(mass_density)

		mass_density.field = mass_density.field*1.660468 # conversion from a.m.u./Ang^3 to grams/cc
		average_density = average_density *1.660468 
		print 'Average Density', average_density
		cut_off = average_density/20.0
		print 'Cut off', cut_off
	#####
	if True:
		from kirke.grid.write import write_XSF_DATAGRID
		write_XSF_DATAGRID(mass_density, file_name = name+'.xsf')
		
	




	###### rendering voids
	
	
	#cut_off = 0.5
	from kirke.grid.write import write_POV_isosurface_by_wave_reconstruction 
	

	for number_of_kpoints in [100,200,400,800]:
		for max_gradient in [4,8,16]:

			image_name = name + '_kps_%i_max_gradient_%f.png'%(number_of_kpoints, max_gradient)
			write_POV_isosurface_by_wave_reconstruction (	mass_density,
									cut_off = cut_off, #0.2
									file_name = isosurface_file,
									number_of_kpoints = number_of_kpoints,
									max_gradient = max_gradient,
									fill_greater_than_cut_off = False)


			##########
			from os import system
			system('cat %s %s > %s'% (file_name,isosurface_file, combined_file ) ) 

			#################
			if gui:
				from kirke.visualize import povray_gui
				povray_gui(combined_file,image_height = 400)


			#################
			command = 'povray Input_File_Name=%s Output_File_Name=%s +H%i +W%i +A +UA -D' % (combined_file, image_name, height, width)
			if render:
				system(command)


