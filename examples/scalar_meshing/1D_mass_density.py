from kirke.read import LAMMPS_dump_file


zcells = 50

meshing_order = 3


LD = LAMMPS_dump_file('dump.coord.lammpstrj')
my_system = LD.get_frame(LD.number_of_frames - 1) #grabs last frame (aka last GAS object)

mydict = {1:'H', 2:'C', 3:'N', 4:'B', 5:'Cl'} # this will make this exmaple much easier to understand

my_system.convert_number_to_element(mydict)
my_system.periodic = [True, True, True]


#############################33# up till now its just getting the GAS object from the file and filling in the important data.

my_system.update_masses_from_element()




from kirke.grid.interpolate import interpolate_scalar_to_mesh


cells = (1,1,zcells)
# the nitrogen atomic density of my system, I allocate now for speed.
# (This always has to be done and the values will be overwriten by the meshing function)
from kirke.grid import create_RGS_from_GAS
mass_density = create_RGS_from_GAS(cells, my_system) 
mass_density.comment = 'Density in grams/cm3'

my_system.update_internal_from_external() # meshing only operates on internal coordinates


# this makes the density mesh, the units are always the point_value_list/(system volume units)

interpolate_scalar_to_mesh(mass_density, sys = my_system, point_value_list = my_system.masses, order = meshing_order, zero_centered = False ) 

# doesn't return data, operates on mass_density, 

mass_density.field = mass_density.field*1.660468 # conversion from a.m.u./Ang^3 to grams/cc

# and we are done.





################ we will now plot this data to make it easier to understand ####################3
from pylab import arange,plot, xlabel, ylabel, show, axvline,axis, array

zlo = my_system.box[2][0]
zhi = my_system.box[2][1]
dz = (zhi-zlo)/zcells
print 'Delta Z cell width =',dz



#now to plot cell edges
cell_edges = arange(zlo,zhi + dz/2.0,dz) # mesh is not zero centered, the +dz/2.0 is to make sure it gets the last point
for edge in cell_edges:
	axvline(edge,color='#909090' ,linewidth = 0.75)


######## actual desnity data
mesh_z_points = arange(zlo + dz/2.0,zhi,dz) # mesh is not zero centered

plot(mesh_z_points,mass_density.field[0][0],'bo',markersize = 8,linewidth = 2.0) # remember that mass_density is a 3D array.





xlabel('Z Height ($\AA$)')
ylabel('Density ($grams/cm^{3}$)')



#### now to show the structure, this is just for demonstration, you don't learn anything usefull from this ############
ylo = my_system.box[1][0]
yhi = my_system.box[1][1]

 # i scale and translate the structure so it looks good on the same axes
fake_Y = array(my_system.y)/(yhi-ylo)*mass_density.field.max() + 1.2*mass_density.field.max()

plot(my_system.z, fake_Y, '.', color = '#101010',markersize = 6 )



maxy = max(fake_Y)


lz = zhi-zlo
axis([zlo - 0.1*lz, zhi + 0.1*lz, 0, maxy*1.2])
show()

