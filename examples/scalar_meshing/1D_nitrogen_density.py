from kirke.read import LAMMPS_dump_file


zcells = 60

meshing_order = 3 


LD = LAMMPS_dump_file('dump.coord.lammpstrj')
my_system = LD.get_frame(LD.number_of_frames - 1) #grabs last frame (aka last GAS object)

mydict = {1:'H', 2:'C', 3:'N', 4:'B', 5:'Cl'} # this will make this exmaple much easier to understand

my_system.convert_number_to_element(mydict)
my_system.periodic = [True, True, True]




nitrogen_list = [] # is 1 for nitrogen atoms, 0 for all other atoms
for element in my_system.element:
	if element == 'N':
		nitrogen_list.append(1)
	else:
		nitrogen_list.append(0)



#### and now for meshing! #################
from kirke.grid.interpolate import interpolate_scalar_to_mesh

cells = (1,1,zcells)
#creates grid and inherirts the basis and origin from the system
from kirke.grid import create_RGS_from_GAS 
rhoN = create_RGS_from_GAS(cells, my_system) 
rhoN.comment = 'Nitrogen atomic density'

my_system.update_internal_from_external()


# this makes the density mesh, the units are always the point_value_list/(system volume units)
interpolate_scalar_to_mesh(rhoN, sys = my_system, point_value_list = nitrogen_list, order = meshing_order, zero_centered = False ) # doesn't return data, operates on rhoN, 


################ we will now plot this data to make it easier to understand ####################3
from pylab import arange,plot, xlabel, ylabel, show, axvline,axis,array

zlo = my_system.box[2][0]
zhi = my_system.box[2][1]
dz = (zhi-zlo)/zcells
print 'Delta Z cell width =',dz



#now to plot cell edges
cell_edges = arange(zlo,zhi + dz/2.0,dz) # mesh is not zero centered, the +dz/2.0 is to make sure it gets the last point
for edge in cell_edges:
	axvline(edge,color='#909090' ,linewidth = 0.75)


######## actual desnity data
mesh_z_points = arange(zlo + dz/2.0,zhi,dz) # mesh is not zero centered

plot(mesh_z_points,rhoN.field[0][0],'bo',markersize = 10,linewidth = 2.0) # remember that rhoN is a 3D array.


xlabel('Z Height ($\AA$)')
ylabel('Nitrogen Atomic Density ($Atoms/\AA^{3}$)')

############ now to show the structure, you really don't need to do this##################3
ylo = my_system.box[1][0]
yhi = my_system.box[1][1]

plot(my_system.z, array(my_system.y)/(yhi-ylo)*rhoN.field.max() + 1.2*rhoN.field.max(), '.', color = '#101010',markersize = 6 ) # i scale and translate the structure so it looks good on the same axes


# i am plotting just the N's for pretty reasons
NY = []
NZ = []
for i in range(my_system.number_of_atoms):
	if my_system.element[i] == 'N':
		NY.append(my_system.y[i])
		NZ.append(my_system.z[i])
plot(NZ, array(NY)/(yhi-ylo)*rhoN.field.max() + 1.2*rhoN.field.max(), '.',color = 'b',markersize = 6 )
maxy = max(array(NY)/(yhi-ylo)*rhoN.field.max() + 1.2*rhoN.field.max())


lz = zhi-zlo
axis([zlo-0.1*(lz),zhi+0.1*lz,0,maxy*1.2])
show()

