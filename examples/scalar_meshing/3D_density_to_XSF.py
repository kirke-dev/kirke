from kirke.read import LAMMPS_dump_file


cells = (10,30,100)  #number of cells in each direction

meshing_order = 5

#################### 

LD = LAMMPS_dump_file('dump.coord.lammpstrj')
my_system = LD.get_frame(LD.number_of_frames - 1) #grabs last frame (aka last GAS object)

mydict = {1:'H', 2:'C', 3:'N', 4:'B', 5:'Cl'} # this will make this exmaple much easier to understand

my_system.convert_number_to_element(mydict)
my_system.update_atomic_number_list()
my_system.periodic = [True, True, True]





###################

# this part is just for furture reference

if False:# you can specify cells or you could do this to get them all roughly the same size in each direction, It tries to tightly fit four cells around a sphere. This is actually for cell list spatial decomposition
	from kirke.analyze import compute_cells_to_fit_radius_inside
	cells = compute_cells_to_fit_radius_inside(my_system,radius = 1.05) # for this purpose, set the radius equal to the desired cell length. 
	print 'Cells used (x, y, z)', cells


#############################33# up till now its just getting the GAS object from the file and filling in the important data.

my_system.update_masses_from_element()




from kirke.grid.interpolate import interpolate_scalar_to_mesh



# the mass density of my system, I create an RGS now.
# (This always has to be done and the values will be overwriten by the meshing function)
from kirke.grid import create_RGS_from_GAS 
mass_density = create_RGS_from_GAS(cells, my_system) 
mass_density.comment = 'Density in grams/cm3'

my_system.update_internal_from_external() # meshing only operates on internal coordinates


# this makes the density mesh, the units are always the point_value_list/(system volume units)
interpolate_scalar_to_mesh(mass_density,
	sys = my_system, 
	point_value_list = my_system.masses,  
	order = meshing_order, 
	zero_centered = True ) #
# doesn't return data, operates on mass_density, 

mass_density.field = mass_density.field*1.660468 # conversion from a.m.u./Ang^3 to grams/cc

# and we are done.


################################################################
my_system.update_basis_and_origin_for_orthorhombic()
from kirke.grid.write import write_XSF_DATAGRID
write_XSF_DATAGRID(mass_density,  file_name = 'mass_density.xsf')

from kirke.write import write_XSF
write_XSF(my_system, 'atoms.xsf')

# this combines the files
from os import system
system('cat atoms.xsf mass_density.xsf > atoms_and_mass_density.xsf')

