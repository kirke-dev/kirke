#!/usr/bin/python


from kirke.read import LAMMPS_file
from kirke.manipulate import center_coordinates
from kirke.write import write_basic_gauss_input

#This code imports a LAMMPS_file, extracts a single molecule from a structure, and converts it to a simple Gaussian file. 

input_file = 'LAMMPS_sample.data'

sys = LAMMPS_file(input_file, number_to_element={1:'C',2:'C',3:'C',4:'H',5:'H', 6: 'H', 7: 'O', 8: 'O', 9: 'C', 10: 'C', 11: 'C', 12: 'H', 13: 'H', 14: 'H', 15: 'N', 16: 'N', 17: 'N', 18: 'N', 19:'C', 20: 'C' , 21: 'O', 22: 'H'})

#Identifying the target molecule
mol_id =4

#Extracting atom and bond information for the target molecule form the system object
mol = sys.get_molecule(mol_id)
bonds = sys.get_molecule_bonds(mol_id)

#Recentering molecule position to the origin

center_coordinates(mol)


#Shifting id values to start at one. Because we want to track the bonds, we can't just use fill_id_list
tick = 1
for a in range(len(mol.id)):
	current_id = mol.id[a]
	mol.id[a] = tick
	for a in range(len(bonds)):
		if bonds[a][2] == current_id:
			bonds[a][2] = tick
		elif bonds[a][3] == current_id:
			bonds[a][3] = tick
	tick +=1

mol.add_bonds(bonds)

write_basic_gauss_input(mol,'extracted_molecule.in','%NProc=4 \n%mem=3800mb\n', '#HF/6-31G(d) Opt=ModRedundant','This system was extracted from '+input_file+' and converted in Kirke', charge = 0, spin_multiplicity = 1, bonds = True, angles = False)

