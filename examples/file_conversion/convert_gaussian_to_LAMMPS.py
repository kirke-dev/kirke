#!/usr/bin/python


from kirke.read import GAUSSIAN_file
from kirke.write import write_LAMMPS


input_file = 'gaussian_example.data'

sys = GAUSSIAN_file(input_file)

#Because Guassian contains no information about the system box, we must make some assumptions
sys.box_mode = 'orthorhombic'
sys.update_box_from_positions(padding = [2.0,2.0,2.0])


write_LAMMPS(sys, 'LAMMPS_out.data', use_element = True)

