
from kirke.read import PDB_file
from kirke.write import write_LAMMPS

sys = PDB_file('tryptophan.pdb')

#The PDB file contains no box information

sys.box_mode = 'orthorhombic'
sys.update_box_from_positions(padding = [2.0, 2.0, 2.0])

print sys.comment.index('\n')

write_LAMMPS(sys, 'tryptophan.data', use_element = True, atom_style ='molecular')
