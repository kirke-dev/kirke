#!/usr/bin/python


from kirke.read import POSCAR_file
from kirke.write import write_XYZ


input_file = 'POSCAR'

sys = POSCAR_file(input_file)


write_XYZ(sys, 'poscar.xyz')

