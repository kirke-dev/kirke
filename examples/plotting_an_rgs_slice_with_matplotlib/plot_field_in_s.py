#!/usr/bin/env python


def plot_field_in_s(field, cells, name = '',take_slice = False,layer_number = 0):
	'''plots a field in s, using regular cells'''
	from pylab import plot, figure, meshgrid, linspace, colorbar, zeros, pcolormesh, cm ,title
	
	cellwidth = zeros(3)
	for dim in range(3):
		cellwidth[dim] = 1.0/cells[dim]
	edges = []
	for dim in range(3):
		edges.append([])
		for i in range(cells[dim] + 1):
			edges[dim].append(i*cellwidth[dim])
	
	fig = figure()
	ax = fig.add_subplot(1,1,1)
	#makes cell lines
	ax.vlines(edges[0],[0.0], [1.0], 'k',linewidth = 0.5)
	ax.hlines(edges[1],[0.0], [1.0], 'k',linewidth = 0.5)
	# makes boundaries
	ax.vlines([0.0, 1.0],[0.0], [1.0], 'k',linewidth = 3.0)
	ax.hlines([0.0, 1.0],[0.0], [1.0], 'k',linewidth = 3.0)

	ax.set_xticks([0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0])
	ax.set_yticks([0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0])
	ax.set_xlabel('Internal Coordinate s1')
	ax.set_ylabel('Internal Coordinate s2')
	#ax.axis([0.0,1.0, 0.0, 1.0]) #does nothing
	ax.axis('equal')
	title(name) #title command

	map2D = zeros((cells[1],cells[0]))
	if take_slice:
		for i in range(cells[0]):
			for j in range(cells[1]):
				map2D[j][i] = field[i][j][layer_number]
	else:
		for i in range(cells[0]):
			for j in range(cells[1]):
				map2D[j][i] = field[i][j]
	
	mx = edges[0]
	my = edges[1]
	X, Y = meshgrid(mx,my)
	pcolormesh(X,Y,map2D,cmap=cm.RdBu, vmax=abs(field).max(), vmin=-abs(field).max())
	colormax = max([ abs(map2D.min()), abs(map2D.max())])
	tick_levels = 11
	tlevs = linspace(-colormax, colormax*1.00001, tick_levels)
	cbar = colorbar(ticks = tlevs)
	cbar.set_label(name)

