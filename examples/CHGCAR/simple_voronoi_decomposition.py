from kirke.grid.read import CHGCAR_file
from kirke.read import POSCAR_file

# this is not at all optimized for speed, it is optimized to simplicity and use of built in functions for clarity to users


rho = CHGCAR_file('CHGCAR')
atoms = POSCAR_file('CONTCAR')
atoms.fill_q_list() # we need somewhere to put these charges


def compute_closest_atom_to_point( point):
	mindist = rho.compute_minimum_image_distance_between_two_points([atoms.x[0], atoms.y[0], atoms.z[0]],point)
	minindex = 0
	for i in range(1,atoms.number_of_atoms):
		dist = rho.compute_minimum_image_distance_between_two_points([atoms.x[i], atoms.y[i], atoms.z[i]],point)
		if dist <= mindist:
			mindist = dist
			minindex = i
	return minindex
	
	
## in this POTCAR file
## Silicon has 4 electrons
## Oxygen has 6 electrons 
## we *must* add these nuclear charges to the charges we find!
for i in range(atoms.number_of_atoms):
	if atoms.element[i]=='Si':
		atoms.q[i] += 4
	elif atoms.element[i]=='O':
		atoms.q[i] += 6
	else:
		pass



for i in range(rho.field.shape[0]):
	for j in range(rho.field.shape[1]):
		for k in range(rho.field.shape[2]):
			charge = rho.field[i,j,k] * rho.compute_cell_volume()
			cell_center = rho.compute_cell_center_position([i,j,k])
			
			atom_index = compute_closest_atom_to_point(cell_center)
			atoms.q[atom_index] += -charge
	###### this is for tracking progress!
	for atom in range(atoms.number_of_atoms):
		print atoms.element[atom], atoms.q[atom]
	print '%i/%i'%(i+1, rho.field.shape[0]) # for tracking progress


# saving our hard work
from kirke.write import write_XYZ

write_XYZ(atoms,'Atoms_with_voronoi_charges.xyz', use_element = True, with_charge = True)
