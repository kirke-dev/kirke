from kirke.grid.read import CHGCAR_file


mah_grid = CHGCAR_file('CHGCAR')

print 'number of electrons :', mah_grid.field.sum()*mah_grid.compute_cell_volume()

from kirke.grid.write import write_XSF_DATAGRID

write_XSF_DATAGRID(mah_grid,'CHGCAR.xsf')

