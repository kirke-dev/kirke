


height = 1000
width = height
render = True
gui = False

name = 'electron_density'



kpoints = 500

max_gradient = 25.0




image_name = name + '_kpoints_%f.png'% kpoints
#image_name = name + '.png'
file_name = 'atoms_'+ name + '.pov'
surface_file = 'isosurface_' + name + '.pov'
combined_file = name + '.pov'

#####################


custom_colors= {1: [0.90, 0.90, 0.90],
		6: [0.35, 0.40, 0.40],
		7: [0.00, 0.25, 1.00],

#
		5: [0.85, 0.30, 0.48],
		8: [0.85, 0.30, 0.30],
		14:[0.70, 0.70, 0.70],
		13:[0.64, 0.64, 0.64],
		31:[0.00, 0.50, 0.07],
		49:[0.48, 0.01, 0.02],
		21:[0.41, 0.08, 0.83],
		39:[0.20, 0.67, 0.92],
#
		9: [0.90, 1.00, 0.00],
		17:[0.40, 1.00, 0.20],
		35:[0.80, 0.45, 0.00],
		53:[1.00, 0.75, 0.95]}



rNT = 2.7 #
rTX = 3.2 #

my_bond_rules= [[1, 1.7, 'C','C'],
		[2, 1.2, 'C','H'],
		[3, 1.6, 'C','N'],
		[33, 1.8, 'O','Si'],
#
		[4, rNT, 'N','B'],
		[5, rNT, 'N','Al'],
		[6, rNT, 'N','Ga'],
		[7, rNT, 'N','In'],
		[8, rNT, 'N','Sc'],
		[9, rNT, 'N','Y'],
#
		[10, rTX, 'B','F'],
		[11, rTX, 'B','Cl'],
		[12, rTX, 'B','Br'],
		[13, rTX, 'B','I']]
#



#######################


from kirke.read import LAMMPS_dump_file
from kirke.read import POSCAR_file
atoms = POSCAR_file('CONTCAR')



######################3


from kirke.manipulate import generate_bonds_from_neighbors

atoms.fill_id_list()
new_bonds = generate_bonds_from_neighbors(atoms,  bond_rules = my_bond_rules , verbose = False  ) 
atoms.add_bonds(new_bonds)


####################

#### better side view
#look_at = atoms.convert_internal_position_to_external([0.5 ,0.5,   0.5])
#campos = atoms.convert_internal_position_to_external([-1.6 ,0.5, 1.2])
#lit_pos = atoms.convert_internal_position_to_external([-0.5, 1.5, 1.8])
#up = [0.0,0,1.0]


if True: ### top down
	look_at = atoms.convert_internal_position_to_external([0.5, 0.5, 0.5 ])
	campos = atoms.convert_internal_position_to_external( [0.5, 0.5, 1.1 ])
	lit_pos = atoms.convert_internal_position_to_external([-2.0, 2.0, 5.0 ])
	campos[2] = campos[2] + max(atoms.basis[0][0],atoms.basis[1][1])
	up = [0,1,0]
	camera_style = 'orthographic angle 45'
	draw_box = True
	image_name = 'top_' + image_name

if False:#angle view
	look_at = atoms.convert_internal_position_to_external([0.5, 0.5, 0.5 ])
	campos = atoms.convert_internal_position_to_external( [0.5, 2.1, 1.6 ])
	lit_pos = atoms.convert_internal_position_to_external([-2.0, 2.0, 5.0 ])
	#campos[2] = campos[2] + max(atoms.basis[0][0],atoms.basis[1][1])
	up = [0,1,0]
	camera_style = 'orthographic angle 45'
	draw_box = True
	image_name = 'top_' + image_name

from kirke.write import write_POV
#atoms.number_of_atoms = 0 # lazy way of making it think it is empty
write_POV(atoms,
	file_name = file_name,
	image_name = image_name,
	height = height ,
	width = height ,
	render = False ,
	background_color = [1,1,1],
	camera_look_at_point = look_at,
	up_direction = up,
	camera_position = campos,
	radius_scale = 0.45, coloring = custom_colors,
	with_bonds = True,
	draw_box = True,
	light_position = lit_pos, light_at_camera = True
	)




######## now for the surface






#
##########################################################################################
###gonna have to make some ellipses here...
from kirke.grid.read import CHGCAR_file, CHG_file, XSF_DATAGRID_file
rho = CHGCAR_file()
#rho = XSF_DATAGRID_file('atoms_and_density_band_number_50.xsf' )
from kirke.grid.analyze import compute_cutoff_by_sum_percentile
cut_off = compute_cutoff_by_sum_percentile(rho,50.0)
print cut_off




image_name = name + '_kps_%i_max_gradient_%f.png'%(kpoints, max_gradient)

from kirke.grid.write import write_POV_isosurface_by_wave_reconstruction
write_POV_isosurface_by_wave_reconstruction (	rho,
						cut_off = cut_off, #0.2
						file_name = surface_file,
						number_of_kpoints = kpoints,
						max_gradient = max_gradient,
						fill_greater_than_cut_off = True )



##########
from os import system
system('cat %s %s > %s'% (file_name,surface_file, combined_file ) ) # with MO's

#################
if gui:
	from kirke.visualize import povray_gui
	povray_gui(combined_file,image_height = 400)


#################
command = 'povray Input_File_Name=%s Output_File_Name=%s +H%i +W%i +A +UA -D' % (combined_file, image_name, height, width)
if render:
	system(command)




