
def plot_plane_wave_amplitude_distribution(my_rgs):
	##########
	from numpy.fft import  fftn


	
	from kirke.grid import RGS
	kspace_rgs = RGS(my_rgs.field.shape,dtype=complex)
	kspace_rgs.field = fftn(my_rgs.field)
	
	####### demo mode stuff #########
	from numpy import absolute, sort, zeros	
	# in one line i take the abs of the flattened array, sort it and reverse it
	abs_kps = sort(absolute(kspace_rgs.field.flatten()))[::-1]
	#print abs_kps[0] ,abs_kps[-1]
	#print abs_kps.shape 

	cdf = zeros(abs_kps.size)
	for i in range(abs_kps.size):
		cdf[i] = cdf[i-1] + abs_kps[i]
	

	sum_abs = abs_kps.sum()
	
	#########################
	from pylab import figure, plot, gca, subplots_adjust, savefig, show
	planes = range(1,abs_kps.size+1)
	####
	figure(figsize = (4,3),dpi = 200)
	ax1 = gca()
	ax1.set_ylabel('Normalized Wave Amplitude')
	ax1.set_xlabel('Number of Plane Waves')
	ax1.xaxis.grid(True)
	ax1.set_xscale('log')
	

	plot(planes,abs_kps/sum_abs , linewidth = 2.0)
	#ax1.set_ylim((0.0,1.05*abs_kps[0]/sum_abs))
	#print 1.05*abs_kps[0]/sum_abs
	#########
	ax2 = gca().twinx()

	ax2.axvline(40000,color='r',linestyle = '--')
	ax2.text(40000, 0.5,'\n\nPOV-Ray Limit',color = 'r', ha = 'center', va = 'center', rotation = 90.0)
	
	ax2.plot(planes,cdf/sum_abs,'g',linewidth = 2.0)
	ax2.set_ylabel('Normalized CDF of\nPlane Wave Amplitudes')
	ax2.set_ylim((0.0,1.05))
	
	ax1.axvline(40000,color='r',linestyle = '--')
	ax1.text(40000, 0.5,'POV-Ray Limit', ha = 'center', va = 'top', rotation = 90.0)

	ax2.grid(True)
	#############
	ax1.set_xlim((1,abs_kps.size))
	subplots_adjust(bottom=0.17,top=0.95,left=0.20,right=0.80)
	
	savefig('amplitude_distribution.png',dpi = 300, transparent = True)
	show()


if __name__=='__main__':
	from kirke.grid.read import CHGCAR_file
	plot_plane_wave_amplitude_distribution( CHGCAR_file())
