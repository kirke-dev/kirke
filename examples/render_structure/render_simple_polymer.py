#!/usr/bin/python


from kirke.read import LAMMPS_file
from kirke.write import write_POV

#This is a more complex POVray rendering, showing some of the more common options

input_file = 'LAMMPS_sample.data'

sys = LAMMPS_file(input_file, number_to_element={1:'C',2:'C',3:'C',4:'H',5:'H', 6: 'H', 7: 'O', 8: 'O', 9: 'C', 10: 'C', 11: 'C', 12: 'H', 13: 'H', 14: 'H', 15: 'N', 16: 'N', 17: 'N', 18: 'N', 19:'C', 20: 'C' , 21: 'O', 22: 'H'})


write_POV(sys, file_name = 'simple.pov',image_name = 'simple.png' , render = True, coloring = {}, height = 1200,width = 1600,
		       camera_style = '', camera_position = [], camera_look_at_point = [], background_color = [1,1,1],
		       display = True, light_position = [], finish = {},radius_scale = 0.75, radius = {},
		       comment = 'Simple POVray rendering',comment_size = 32,show_charge = False, light_at_camera = True, rotate = 0, with_bonds = False, bond_scale = 0.75)




