#!/usr/bin/env python



def update_number_of_atoms(self):
	'''Checks length of x, y, and z lists and updates the number_of_atoms'''

	if len(self.x) > len(self.sx):
		if len(self.x) != len(self.y):
			print 'update_number_of_atoms: ERROR X and Y position lists are of unequal length, fix this first. This operation will not complete.'
			return
		elif len(self.z) != len(self.x):
			print 'update_number_of_atoms: ERROR X and Z position lists are of unequal length, fix this first. This operation will not complete.'
			return
		self.number_of_atoms = len(self.x)

	else:
		if len(self.sx) != len(self.sy):
			print 'update_number_of_atoms: ERROR sx and sy position lists are of unequal length, fix this first. This operation will not complete.'
			return
		elif len(self.sz) != len(self.sx):
			print 'update_number_of_atoms: ERROR sx and sz position lists are of unequal length, fix this first. This operation will not complete.'
			return
		self.number_of_atoms = len(self.sx)


################# K. Sebeck 27 March 2012 ##############
def update_number_of_bonds(self):
	'''Updates number_of_bonds from bonds'''
	self.number_of_bonds = len(self.bonds) 

def update_number_of_angles(self):
	'''Updates number_of_angles from angles'''
	self.number_of_angles = len(self.angles)

def update_number_of_dihedrals(self):
	'''Updates number_of_dihedrals from dihedrals'''
	self.number_of_dihedrals = len(self.dihedrals)

def update_number_of_impropers(self):
	'''Updates number_of_impropers from impropers'''
	self.number_of_impropers = len(self.impropers)		
#2 April 2012
def update_number_of_molecules(self):
	'''Updates number_of_molecules from the set of molecules'''
	self.number_of_molecules = len(set(self.molecule))
