#!/usr/bin/env python

def sort_by(self,sort_parameter):
	'''Sorts all atomic attributes by atom atomic number, can be reconfigured to do anything easily'''
	
	#sort_parameter = 'atomic_number'
	sort_key_dict = {'id':0,
			'x':1,
			'y':2,
			'z':3,
			'q':4,
			'element': 5, 
			'atomic_number':6,
			'vx':7,
			'vy':8,
			'vz':9,
			'molecule':10,
			'nx':11,
			'ny':12,
			'nz':13,
			'sx':14,
			'sy':15,
			'sz':16,
			'masses':17
			}
	sort_key = sort_key_dict[sort_parameter]
	#print sort_key
	tuple_list = []
	if len(self.q)!= self.number_of_atoms:
		self.fill_q_list()
		
	if len(self.vx)!= self.number_of_atoms:
		self.fill_v_lists()

	if len(self.molecule)!= self.number_of_atoms:
		self.fill_molecule_list()

	if len(self.nx)!= self.number_of_atoms:
		self.fill_n_lists()
	
	# can we fix this discrepency at the next merge?

	if len(self.masses)!= self.number_of_atoms:
		self.fill_masses_list()

	
	if len(self.sx)!= self.number_of_atoms:
		self.fill_s_lists()

	if len(self.selective_dynamics)!= self.number_of_atoms:
		self.fill_selective_dynamics_list()

	for i in range(self.number_of_atoms):
		tuple_list.append((  self.id[i],
				self.x[i],		self.y[i],		self.z[i],
				self.q[i],		self.element[i],	self.atomic_number[i],
				self.vx[i],		self.vy[i],		self.vz[i],
				self.molecule[i],
				self.nx[i],		self.nx[i],		self.nz[i],
				self.sx[i],		self.sy[i],		self.sz[i] ,
				self.masses[i],
				self.selective_dynamics[i]
				))
		
	from operator import itemgetter
	tuple_list.sort(key = itemgetter(sort_key))

	for i in range(self.number_of_atoms):
		self.id[i] = tuple_list[i][0]

		self.x[i] = tuple_list[i][1]
		self.y[i] = tuple_list[i][2]
		self.z[i] = tuple_list[i][3]
		
		self.q[i] = tuple_list[i][4]
		self.element[i] = tuple_list[i][5]
		self.atomic_number[i] = tuple_list[i][6]
			
		self.vx[i] = tuple_list[i][7]
		self.vy[i] = tuple_list[i][8]
		self.vz[i] = tuple_list[i][9]

		self.molecule[i] = tuple_list[i][10]
			
		self.nx[i] = tuple_list[i][11]
		self.ny[i] = tuple_list[i][12]
		self.nz[i] = tuple_list[i][13]

		self.sx[i] = tuple_list[i][14]
		self.sy[i] = tuple_list[i][15]
		self.sz[i] = tuple_list[i][16]
		
		self.masses[i] = tuple_list[i][17]

		self.selective_dynamics[i] = tuple_list[i][18]

def get_index_with_binary_search_from_id(self,id_in): # lol from wikipedia
	''' A much faster way to find index given a sorted list. '''
	imax = self.number_of_atoms-1
	imin = 0
	while imax >= imin:
		imid = imin + ((imax-imin)/2)
		
		if self.id[imid] < id_in:
			imin = imid + 1
		elif self.id[imid] > id_in:
			imax = imid - 1
		else:
			return imid 
        print "get_index_with_binary_search_from_id: ERROR ID NOT FOUND IN ID LIST"


def build_index_dict(self): #
	'''Generates the id_to_index list'''
	new_dict =[]
	for a in range(self.number_of_atoms):
		new_dict.append([self.id[a],a])
	self.id_to_index = dict(new_dict)

	return self.id_to_index # why does this return anything at all?
