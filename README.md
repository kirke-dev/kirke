# Kirke #

Kirke is a Python 2 library that functions as an atomistic and electronic simulation management environment. Kirke is a glue layer for managing various HPC simulations and building easy to manage workflows. In the future, we will release a Python 3 version if there is demand.

## Abilities ##

* File I/O, it can read atomic and regular grid data from some of the more popular codes. We are always extending it
* Manipulation and creation of simulated systems, there are tools for easy creation atomic structures and modification of existing structures
* Analysis, There are many analysis functions built in such as RDF or Kabsch analysis
* Rudimentary POV-Ray support, we have built a simple interactive POV-Ray viewer for setting up publication quality scenes
* Rolling release, development is fast right now 

## Installation ##

* Clone it.
* Append your Python path. Add 'export PYTHONPATH=$PYTHONPATH:~/folder-containing-kirke-directory/' to your .bashrc or .bash_profile.
### Dependencies ###
* [NumPy](http://www.numpy.org/), you can do most things without it and we have avoided adding it to the base classes, but many things need it such as density grids. 
### Optional Dependencies... and what breaks without them. ###
* [POV-Ray](http://www.povray.org/), if you want to start making pretty pictures with write_POV you will need this.
* [ImageMagick](http://imagemagick.org/) can be called to add captions at the end of write_POV.
* [Pillow](http://python-pillow.org/) + TkInter, with these two you can use the povgui.
* [PyEVTK](https://bitbucket.org/pauloh/pyevtk) is used for writing VTS files from RGS density meshes.
 
## Contribution Guidelines ##

* Try to follow the I/O functions as examples
* Don't break existing code unless everyone else is on board with the change.
* The GAS and RGS classes are fairly comprehensive, don't add things to them since the functionality may already exist
* Make examples for new functions in the examples folders 
* If you use Kirke in your simulations, please cite us!

## Citation Information ##

Michael J. Waters, Katherine Sebeck, Eleanor J. Coyle, Avi Bregman. Kirke. Kieffer Group, Ann Arbor, MI, 2012.

### Who do I talk to? ###

* Michael Waters, Katie Sebeck, Eleanor Coyle