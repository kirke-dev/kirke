

def POSCAR_file(file_name='POSCAR', use_selective_dynamics = False, verbose = True):
	''' Uses POSCAR file to initialize a GAS object.'''
	from kirke import GAS
	sys = GAS()
	sys.file_name=file_name
	fileid = open(file_name,'r')
	file_data = fileid.readlines()
	fileid.close()
	sys.comment = file_data[0].split('\n')[0]
	scale = float(file_data[1])
	
	base = file_data[2:5]
	a = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	for i in range(3):
		v2 = base[i].split()
		for j in range(3):
			a[i][j] = scale*float(v2[j])


	sys.update_basis_from_vectors(a1 =a[0] , a2 =a[1] , a3 =a[2] )
	sys.box_mode = 'triclinic'

	def is_number(s):
		try:
			float(s)
			return True
		except ValueError:
			return False
	
	# elements are not defined
	if is_number(file_data[5].split()[0]):
		if verbose: print "\nPOSCAR_file: No elements defined, only numbers.\n"
		number_of_each_type = []
		for a in file_data[5].split():
			number_of_each_type.append(int(a))
		with_element = False
		next_line = 6
	#elements are defined.
	else: 
		if verbose: print "\nPOSCAR_file: Elements are defined.\n"
		element_list = []
		for a in file_data[5].split():
			element_list.append(a)
		number_of_each_type = []
		for a in file_data[6].split():
			number_of_each_type.append(int(a))
		with_element = True
		next_line = 7
		#print element_list
		#print number_of_each_type

	if file_data[next_line][0] in 'sS': #passing the selective dynamics line
		next_line += 1
	
	if file_data[next_line][0] in 'cCkK': #cartensian test
		external_mode = True
	else:
		external_mode = False


	dyn_dict = {'T':True, 'F':False}

	type_number = 0
	findex = next_line + 1
	for count in number_of_each_type:
			type_number += 1
			for i in range(count):
				#print(findex)
				if with_element:
					sys.element.append(element_list[type_number-1])
				else:	
					sys.atomic_number.append(type_number)
				
				split_line = file_data[findex].split()
				
				if external_mode:
					sys.x.append(float(split_line[0]))
					sys.y.append(float(split_line[1]))
					sys.z.append(float(split_line[2]))

				else:
					sys.sx.append(float(split_line[0]))
					sys.sy.append(float(split_line[1]))
					sys.sz.append(float(split_line[2]))
					
				if use_selective_dynamics:
					bool_list = []
					for stri in range(3,6):
						bool_list.append( dyn_dict[ split_line[stri] ] )
					sys.selective_dynamics.append(bool_list)
					
					
				findex += 1
	#
	sys.update_number_of_atoms()
	sys.periodic =[True, True, True]
	#sys.fill_id_list()
	sys.origin = [0.0,0.0,0.0]
	if with_element: 
		sys.update_atomic_number_list()

	if external_mode:
		sys.fill_s_lists()
		sys.update_internal_from_external()
	else:
		sys.fill_position_lists()
		sys.update_external_from_internal()
	
	sys.fill_id_list()
	return sys


