#!/usr/bin/env python

class TAPE1:
	def __init__(self,file_name):
		'''Reads in the FLX tape1 file columns to lists'''
		self.file_name = file_name

		fileid=open(file_name,'r')

		file_data = fileid.readlines()
		
		if file_data == []:
			print 'TAPE1: ERROR tape1 is an empty file!'
		
		fileid.close()

		self.column_line = file_data[0]

		list_of_data_lines = file_data[1:]

		self.number_of_data_points = len(list_of_data_lines)
		self.time = []
		self.temperature = []
		self.energy = []
		self.pressure = []
		self.density = []

		self.pxx = []
		self.pyx = []
		self.pzx = []
		self.pxy = []
		self.pyy = []
		self.pzy = []
		self.pxz = []
		self.pyz = []
		self.pzz = []
		
		self.h11 = []
		self.h21 = []		
		self.h31 = []
		self.h12 = []
		self.h22 = []
		self.h32 = []
		self.h13 = []
		self.h23 = []
		self.h33 = []

		self.Rep = []
		self.b3 = []
		self.Cb = []
		self.kin = []
		self.fict = []
		self.total_volume = []

		self.x_disp = []
		self.y_disp = []
		self.z_disp = []

		for line in list_of_data_lines:
			sectioned_line = line.split('\t')	
			if '**********' in sectioned_line:
				print "TAPE1: This line contains an error: ", sectioned_line
			else:			
				self.time.append(float(sectioned_line[0]))
				self.temperature.append(float(sectioned_line[1]))
				self.energy.append(float(sectioned_line[2]))
				self.pressure.append(float(sectioned_line[3]))
				self.density.append(float(sectioned_line[4]))
	
				self.pxx.append(float(sectioned_line[5]))
				self.pyx.append(float(sectioned_line[6]))
				self.pzx.append(float(sectioned_line[7]))
				self.pxy.append(float(sectioned_line[8]))
				self.pyy.append(float(sectioned_line[9]))
				self.pzy.append(float(sectioned_line[10]))
				self.pxz.append(float(sectioned_line[11]))
				self.pyz.append(float(sectioned_line[12]))
				self.pzz.append(float(sectioned_line[13]))
		
				self.h11.append(float(sectioned_line[14]))
				self.h21.append(float(sectioned_line[15]))		
				self.h31.append(float(sectioned_line[16]))
				self.h12.append(float(sectioned_line[17]))
				self.h22.append(float(sectioned_line[18]))
				self.h32.append(float(sectioned_line[19]))
				self.h13.append(float(sectioned_line[20]))
				self.h23.append(float(sectioned_line[21]))
				self.h33.append(float(sectioned_line[22]))	

				self.Rep.append(float(sectioned_line[23]))
			
				self.b3.append(float(sectioned_line[24]))

				self.Cb.append(float(sectioned_line[25]))

				self.kin.append(float(sectioned_line[26]))

				self.fict.append(float(sectioned_line[27]))

				self.total_volume.append(float(sectioned_line[28]))

				self.x_disp.append(float(sectioned_line[29]))
				self.y_disp.append(float(sectioned_line[30]))
				self.z_disp.append(float(sectioned_line[31]))

