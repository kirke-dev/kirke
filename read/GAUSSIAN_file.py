#!/usr/bin/env python


def GAUSSIAN_file(file_name): 
	''' Reads in the Gaussian file type. Displays the sum of the partial charges given in the file and calculated as a running total. '''

	from kirke import GAS
	sys = GAS()
	sys.file_name = file_name
	fileid = open(file_name,'r')
	file_data = fileid.readlines()
	fileid.close()

	xyz_start = 0
	q_start = 0
	q_sum_start = 0
	q_sum = 0

	#print 'Reading {0} ...\n'.format(sys.file_name)

	for i in range(len(file_data)): # this only looks for the last set of data. keeps writing over values if it finds a new set
		line = file_data[i]
		split_line = line.split()

		if ('Standard orientation:' in line) or  ('Input orientation:' in line):
			xyz_start = i + 5 
			##### it looks something like this in the file:
			#                         Standard orientation:                         
			# ---------------------------------------------------------------------
			# Center     Atomic      Atomic             Coordinates (Angstroms)
			# Number     Number       Type             X           Y           Z
			# ---------------------------------------------------------------------
			#      1          8           0        7.516971    0.278441    1.180297
		if ('Mulliken atomic charges:' in line) or ('Mulliken charges:' in line) :
			q_start = i + 2
	
	i = q_start		
	while not('Sum of Mulliken atomic charges' in file_data[i]) and not('Sum of Mulliken charges' in file_data[i]): #needs to stop on the first one after q_start
		#print file_data[i]
		i+=1
	
	q_end = i
	#q_sum_from_file = split_line[-1] # why was this here?

	# finds the number of atoms
	num_atoms = q_end - q_start
	sys.number_of_atoms = num_atoms
	print sys.number_of_atoms
	#print '{0} atoms in system\n'.format(num_atoms)

	# reads atomic_number and x,y,z coordinates
	for i in range(xyz_start, xyz_start + num_atoms):
		line = file_data[i]
		#print line
		split_line = line.split()
		sys.id.append(int(split_line[0]))
		sys.atomic_number.append(int(split_line[1]))
		sys.x.append(float(split_line[3]))
		sys.y.append(float(split_line[4]))
		sys.z.append(float(split_line[5]))

	# reads in q and sums q values as it goes
	for i in range(q_start, q_start + num_atoms):
		line = file_data[i]
		split_line = line.split()
		sys.element.append(split_line[1])
		sys.q.append(float(split_line[2]))

	return sys
