#!/usr/bin/env python

################################################# handles large lammps XYZ files
#Modified Nov 2 1012 to deal with stupid modifications in new version of lammps from Atoms to Atoms.  Timestep
class Large_XYZ_file:
	def __init__(self,file_name,verbose = True, box_mode = 'orthorhombic'):
		'''Parses single frames of a large LAMMPS style appended xyz file. get_frame Must be used to populate the system. __init__ simply finds frame limits.'''
		self.file_name = file_name
		fid=open(self.file_name,'r')
		self.verbose = verbose
		self.start_line_list = []
		
		i = 0
		for a in fid:
			if (a == 'Atoms\n') or ('Atoms.' in a):
				self.start_line_list.append(i + 1) # the starting line of each block 
			i += 1
		fid.close()

		self.number_of_lines = i 
		self.number_of_frames = len(self.start_line_list)

		if self.verbose:
			print 'Large_XYZ_file: Found %i frame(s)' % self.number_of_frames


	def get_frame(self, frame_number, use_element = False, with_charge = False): # returns a system for the frame
		''' Returns one frame of an appended xyz file as a GAS object. '''
		lines = []
		fid = open(self.file_name)
		if self.verbose:
			print "Large_XYZ_file: Seeking..."
		i = 0 
		end_reached = False

		
		for a in fid: ### this loop cuts out the correct segment of atoms as a list of strings
			if end_reached == False:
				if i >= self.start_line_list[ frame_number ]:
					
					if (a == 'Atoms\n') or ('Atoms.' in a):
						end_reached = True
					else:
						lines.append(a)
			i += 1
		fid.close()
		#print lines
		if frame_number != (len(self.start_line_list)-1):
			lines.pop() # the last line is the next blocks number of atoms
		######################
		from kirke import GAS
		sys = GAS() ## creating an empty GAS class
		######### fixing a nice file name
		frame_file_name = self.file_name.split('.')
		suffix = frame_file_name.pop()
		for b in frame_file_name:
			sys.file_name = sys.file_name + b + '.'
		sys.file_name = sys.file_name + str(frame_number) + '.' + suffix

		#########################
		if self.verbose:
			print "Large_XYZ_file: Parsing " + sys.file_name + ' from ' + self.file_name + '...' 

		### just in case....
		 ##WTF LAMMPS WHY ARE THESE GARBAGE LINES RANDOMLY OCCURING!!??? 
		i = 0
		while i < len(lines):
			if lines[i]=='\r\n':
				print 'Large_XYZ_file: line %i was crap.' %i
				lines.pop(i)
			i+=1
		#seriously cleaning inputs..

		for a in lines: ###### this loop feeds the data into the GAS class
			sectioned_string = a.split()
	
			sys.x.append(float(sectioned_string[1]))
			sys.y.append(float(sectioned_string[2]))
			sys.z.append(float(sectioned_string[3]))
			####
			if with_charge == True:
				sys.q.append(float(sectioned_string[4]))
			else:
				sys.q.append(0.0)
			####
			if use_element:
				sys.element.append(sectioned_string[0])
			else:
				sys.atomic_number.append(int(sectioned_string[0]))

		if use_element:  # updating on the element vs atomic number lists
			sys.update_atomic_number_list()
		else:
			sys.update_element_list()
		sys.box = [[],[],[]]

		sys.update_number_of_atoms()
		
		return sys
#################################################################
