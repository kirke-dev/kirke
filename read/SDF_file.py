
def SDF_file(file_name = '', get_bonds = True, verbose=False):
	''' Reads PubChem sdf files into kirke. Automatically fills sys.id and sys.masses. If it reads the bond information, it automatically assigns bond type as 0. ''' # added 13 Mar 2014 by ejcoyle
	from kirke import GAS
	from kirke.elements import element_symbol_list,element_mass_list
	from operator import itemgetter
	from numpy import zeros

	sys = GAS()
	fileid = open(file_name,'r')
	file_data = fileid.readlines()
	fileid.close()

	CID = 0
	atoms_list_start = 0
	bonds_list_start = 0
	partial_charges_start = 0
	number_of_partial_charges = 0
	charge_fix = []
	partial_charges = []
	check_sys_info_start = 0
	bond_id = 1

	for i in range(len(file_data)):
		line = file_data[i]
		split_line = line.split()

		if i == 0:
			CID = int(split_line[0])
		if i == 3:
			sys.number_of_atoms = int(split_line[0])
			sys.number_of_bonds = int(split_line[1])
			atoms_list_start = i+1
			bonds_list_start = atoms_list_start + sys.number_of_atoms
			sys.id = range(1, sys.number_of_atoms + 1)
			if verbose == True:
				print 'SDF_file : {0} atoms, {1} bonds'.format(sys.number_of_atoms,sys.number_of_bonds)
		if 'PARTIAL_CHARGES' in line:
			partial_charges_start = i+2
		if i == partial_charges_start-1:
			number_of_partial_charges = int(split_line[0])
			if verbose == True:
				print 'SDF_file : {0} partial charges'.format(number_of_partial_charges)
		if 'PUBCHEM_COORDINATE_TYPE' in line:
			check_sys_info_start = i+1

	element_to_mass = dict(zip(element_symbol_list, element_mass_list))

	for i in range(atoms_list_start, atoms_list_start + sys.number_of_atoms):
		line = file_data[i]
		split_line = line.split()
		sys.id.append(int(i-atoms_list_start+1))
		sys.x.append(float(split_line[0]))
		sys.y.append(float(split_line[1]))
		sys.z.append(float(split_line[2]))
		sys.element.append(split_line[3])
		sys.masses.append(element_to_mass[split_line[3]])

	print '\nSDF_file : If atom subtypes (e.g. CA vs C) are needed, those must be adjusted manually.\n'

	for i in range(partial_charges_start, partial_charges_start + number_of_partial_charges):
		line = file_data[i]
		split_line = line.split()
		partial_charges.append([int(split_line[0]), float(split_line[1])])

	partial_charges.sort(key = itemgetter(0))

	if number_of_partial_charges != sys.number_of_atoms:
		charge_fix = zeros([sys.number_of_atoms,2], float)
		j = 0
		if verbose == True:
			print 'Number of partial charges missing: ',sys.number_of_atoms - number_of_partial_charges
		for i in range(sys.number_of_atoms):
			charge_fix[i][0] = i+1
			if i == partial_charges[j][0]-1:
				charge_fix[i][1] = partial_charges[j][1]
				j += 1
		number_of_partial_charges = sys.number_of_atoms
		if verbose == True:
			print 'Updated number of partial charges: ',number_of_partial_charges
		partial_charges = []
		for i in range(sys.number_of_atoms):
			partial_charges.append([int(charge_fix[i][0]), float(charge_fix[i][1])])

	for i in range(len(partial_charges)):
		sys.q.append(partial_charges[i][1])

	print 'SDF_file : WARNING!!! Net system charge = {0}\n'.format(sum(sys.q))

	if get_bonds == True:
		if verbose == True:
			print 'SDF_file : Fetching bond information from file! Automatically setting bond type to 0!!\nI should probably add bond rules to this at some point.\n'
		for i in range(bonds_list_start, bonds_list_start + sys.number_of_bonds):
			line = file_data[i]
			split_line = line.split()
			sys.bonds.append([bond_id, 0, int(split_line[0]), int(split_line[1])])
			bond_id += 1
	else:
		if verbose == True:
			print 'SDF_file : Not fetching bond info from SDF file\n'

	if verbose == True:
		print 'SDF_file: {0}\n\tCID {1}'.format(file_name,CID)
		for i in range(check_sys_info_start, check_sys_info_start + 3):
			line = file_data[i]
			split_line = line.split()
			if split_line[0] == '1':
				print '\t2-D system'
			if split_line[0] == '2':
				print '\t3-D system'
			if split_line[0] == '3':
				print '\tDepositor provided coordinates'
			if split_line[0] == '4':
				print '\tExperimentally determined coordinates'
			if split_line[0] == '5':
				print '\tCoordinates computed by PubChem'
			if split_line[0] == '10':
				print '\tCoordinates in Angstroms'
			if split_line[0] == '11':
				print '\tCoordinates in nanometers'
			if split_line[0] == '12':
				print '\tCoordinates in pixels'
			if split_line[0] == '13':
				print '\tCoordinates in points'
			if split_line[0] == '14':
				print '\tCoordinates in standard bond lengths'
			if split_line[0] == '255':
				print '\tCoordinate units are unknown or unspecified!'
		print ''
	return sys



