#!/usr/bin/env python

def XYZ_file(file_name,with_charge = False, use_element = False): #Default_to_type
		''' Uses .xyz file to initialize a GAS object.'''
		from kirke import GAS
		sys = GAS()
		sys.file_name=file_name
		fileid = open(file_name,'r')
		file_data = fileid.readlines()
		fileid.close()
		#sys.comment = file_data[1].split('\n')[0]
		list_of_data_lines = file_data[2:]

		sys.number_of_atoms = len(list_of_data_lines)

		if use_element == True:
			for a_string in list_of_data_lines:
				sectioned_string = a_string.split()
				sys.element.append(sectioned_string[0])
				sys.x.append(float(sectioned_string[1]))
				sys.y.append(float(sectioned_string[2]))
				sys.z.append(float(sectioned_string[3]))
				if with_charge == True:
					sys.q.append(float(sectioned_string[4]))

			sys.update_atomic_number_list()
		
		else: ########uses atomic number instead
			for a_string in list_of_data_lines:
				sectioned_string = a_string.split()
				#print sectioned_string
				sys.atomic_number.append(int(sectioned_string[0]))
				sys.x.append(float(sectioned_string[1]))
				sys.y.append(float(sectioned_string[2]))
				sys.z.append(float(sectioned_string[3]))
				if with_charge == True:
					sys.q.append(float(sectioned_string[4]))

			sys.update_element_list()

		#sys.box_mode = "orthorhombic"  # leave this disabled, there really is not box info in this file
		#sys.update_box_from_positions()
		#sys.fill_id_list()
		
#		if with_charge == False:
#			sys.fill_q_list()

		return sys

