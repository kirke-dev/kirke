#!/usr/bin/env python

class LAMMPS_XYZ_file:
	
	def __init__(self,file_name, use_element = False): #Default_to_type
		'''Reads in older version of the LAMMPS appended xyz files into a GAS object for each frame of the system. Cannot handle new format with Atoms. Timestep: ####'''
		self.file_name = file_name
		self.frames = []         
		from kirke import GAS
		fid = open(file_name,'r')
		lines=fid.readlines()	
		fid.close()

		 ##WTF LAMMPS WHY ARE THESE GARBAGE LINES RANDOMLY OCCURING!!???
		i = 0
		while i < len(lines):
			if lines[i]=='\r\n':
				lines.pop(i)
			i+=1
		#seriously cleaning inputs..

		offset = 0
		ln = 0
		while offset < len(lines):
			b = int(lines[offset].split()[0])
			offset += 2
			sys = GAS()
			if use_element == False:
				for a in range(b): 
					line = lines[ a + offset]
					#print [a+offset,line] 
					sectioned_string = line.split()
					#print line
					sys.atomic_number.append(int(sectioned_string[0]))
					sys.x.append(float(sectioned_string[1]))
					sys.y.append(float(sectioned_string[2]))
					sys.z.append(float(sectioned_string[3]))
					sys.element.append('')

			else:
				for a in range(b): 
					line = lines[ a + offset]
					#print [a+offset,line] 
					sectioned_string = line.split()
					#print line
					sys.element.append(sectioned_string[0])
					sys.x.append(float(sectioned_string[1]))
					sys.y.append(float(sectioned_string[2]))
					sys.z.append(float(sectioned_string[3]))
				
				sys.update_atomic_number_list()

			sys.update_number_of_atoms()
			#sys.fill_q_list()
			#sys.update_box_from_positions()
			#sys.fill_id_list()

			self.frames.append(sys)			
						
			ln+=1
			
			offset += b 

		self.number_of_frames = len(self.frames)
		
		######### inherits the file name for each GAS object
		for i in range(self.number_of_frames):
			self.frames[i].file_name = self.file_name

	def convert_number_to_atomic_number(self,translation_dict):
		for i in range(len(self.frames)):
			for j in range(len(self.frames[i].atomic_number)):
				self.frames[i].atomic_number[j] = translation_dict[ self.frames[i].atomic_number[j] ]

			self.frames[i].update_element_list()
				

	def convert_number_to_element(self,translation_dict):
		for i in range(len(self.frames)):
			#print i
			#print self.frames[i].number_of_atoms
			for j in range(len(self.frames[i].atomic_number)):
				#print j
				self.frames[i].element[j] = translation_dict[ self.frames[i].atomic_number[j] ]

			self.frames[i].update_atomic_number_list()	

