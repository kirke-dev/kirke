#!/usr/bin/env python

class FLX_sys:
#Modified by Katie 25 May 2012 to feed straight into a GAS object
#Requires both FLX fin type and tape2. This whole thing might be upgradeable with new GAS features -mike
	def __init__(self,fin_file_name,tape2_name,tape_lines_per_atom = 5):
		''' Uses the FLX tape2 and .fin files to generate a more complete GAS object than either file alone could be used for. '''
		from kirke import GAS
		sys = GAS()
		self.file_name = tape2_name
		self.fin_file_name = fin_file_name
				
		self.lines_per_atom = tape_lines_per_atom		

	def get_sys(self):
		from kirke import GAS
		sys = GAS()

		fid = open(self.file_name,'r')
		lines = fid.readlines()
		fid.close()
		splitlines = []

		sys.file_name = self.fin_file_name
		inputid = open(self.fin_file_name,'r')
		filedata = inputid.readlines()
		inputid.close()

		############ this is a good text parser for a block of numbers of different row lengths
		for i in range(len(lines)):
			splitlines.append(lines[i].split())			
		#############
				
		############ finds end position of atom lines
		endi = 0
		while 1!=len(splitlines[endi]):
			endi+=1

		############cuts up sections of file up accordingling

		self.wrapbody = splitlines[0:endi]
		self.unparced_tail = lines[endi:]
		self.tail = splitlines[endi:]
		
		############ converts strs to floats

		for i in range(len(self.wrapbody)):
			for j in range(len(self.wrapbody[i])):
				self.wrapbody[i][j] = float(self.wrapbody[i][j])
				
		########### creates a line version of the body so its easier to manipulate
		self.number_of_atoms = endi/self.lines_per_atom
		self.body = []
		
		for i in range(self.number_of_atoms):
			self.body.append([])
			
			wrapi = i*self.lines_per_atom
			
			for b in range(self.lines_per_atom):
				for a in self.wrapbody[ wrapi + b ]:
					self.body[i].append(a)
		for i in self.body:
			sys.sx.append(i[0])
			sys.sy.append(i[1])
			sys.sz.append(i[2])
			sys.vx.append(i[3])
			sys.vy.append(i[4])
			sys.vz.append(i[5])

			sys.q.append(i[18])
		
		########## parsing the box size
		self.box = self.tail[6:9]

		for i in range(len(self.box)):
			for j in range(len(self.box[i])):
				self.box[i][j] = float(self.box[i][j])*10
		#Sys box is being altered to be in bloody angstroms so it matches everything else!

		sys.box = self.box
		sys.box_mode = 'triclinic'
		sys.basis=sys.box
		sys.periodic = [True, True, True]

		stuff = sys.compute_lengths_and_angles_from_basis(verbose = False)
		sys.lx = stuff[0][0]
		sys.ly = stuff[0][1]
		sys.lz = stuff[0][2]


		#Using fin file to get external coordinates and types, tape2 for box and charges

		# finds the word 'ATOM' as start of body	
		s = ''	
		i = 0	
		while s != 'ATOM\n':
			s = filedata[i]

			i+=1
	
		data_lines = filedata[i:]
		from kirke.elements import element_symbol_list
		if len(data_lines) == self.number_of_atoms:
			atom_id = 0
			for line in data_lines:
				atom_id+=1
				splitline = line.split()
				sys.element.append(splitline[0])
				sys.atomic_number.append(element_symbol_list.index(splitline[0]))
				#sys.id.append(splitline[1])
				sys.id.append(atom_id)
				sys.x.append(float(splitline[2]))
				sys.y.append(float(splitline[3]))
				sys.z.append(float(splitline[4]))
				
			sys.update_atomic_number_list
			sys.update_number_of_atoms()
			#sys.fill_id_list()
			sys.update_masses_from_element() #The great masses modification
			#sys.update_box_from_positions()
			
			return sys
			
		else:
			print "FLX_sys: ERROR fin and tape2 don't match. Try again."

