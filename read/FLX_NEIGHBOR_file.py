#!/usr/bin/env python


class FLX_NEIGHBOR_file:
	def __init__(self,file_name = 'nghblst'):
		'''Uses the FLX nghblst file to populate the GAS neighbor_list. '''
		fid = open(file_name,'r')
		lines = fid.readlines()
		fid.close()
		self.neighbor_list = []
		self.number_of_neighbors = []
		self.center_atom = []
		for line in lines:
			self.center_atom.append(int(line.split()[0]))
			partbc = line.split('->')[1]
			partb = partbc.split(':')[0]
			neighbors = partb.split()
			
			i = 0
			max_possible_nghb = len(neighbors)
			while i < len(neighbors):
				if neighbors[i] == '0':
					neighbors.pop(i)
				
				else:
					neighbors[i] = int(neighbors[i])
					i+=1
			self.neighbor_list.append(neighbors)
			self.number_of_neighbors.append(len(neighbors))
		
		self.max_neighbors = max(self.number_of_neighbors)
		self.min_neighbors = min(self.number_of_neighbors)
		self.number_of_atoms = len(self.number_of_neighbors)

		self.neighbor_counts = []
		for i in range(max_possible_nghb+1):
			self.neighbor_counts.append(0)
		
		for a in self.number_of_neighbors:
			self.neighbor_counts[a] += 1
