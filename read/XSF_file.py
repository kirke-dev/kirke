


def XSF_file(file_name,position_set = 'PRIM'):

	''' Uses XSF file to initialize a GAS object.'''
	from kirke import GAS
	sys = GAS()
	sys.file_name=file_name
	fileid = open(file_name,'r')
	file_data = fileid.readlines()
	fileid.close()
	if position_set == 'PRIM':
		search_term = ['PRIMVEC', 'PRIMCOORD'] #later maybe
	else:
		search_term = ['CONVVEC', 'CONVCOORD']

	LN = 0
	while search_term[0] not in file_data[LN]: #great seek function
		LN+=1


	
	base = file_data[LN+1:LN+4]
	a = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	for i in range(3):
		v2 = base[i].split()
		for j in range(3):
			a[i][j] = float(v2[j])
	sys.update_basis_from_vectors(a1 =a[0] , a2 =a[1] , a3 =a[2] )
	sys.box_mode = 'triclinic'
	
	while search_term[1] not in file_data[LN]:
		LN+=1

	sys.number_of_atoms = int(file_data[LN+1].split()[0])

	for i in range(LN+2,LN+2 + sys.number_of_atoms):
		spl = file_data[i].split()
		sys.atomic_number.append(int(spl[0]))
		sys.x.append(float(spl[1]))
		sys.y.append(float(spl[2]))
		sys.z.append(float(spl[3]))

	
	sys.origin = [0.0,0.0,0.0]
	sys.update_element_list()
	sys.fill_s_lists()
	sys.update_internal_from_external()
	#sys.fill_id_list()
	return sys

