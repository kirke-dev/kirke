#!/usr/bin/env python

class TAPE2_sys:
	def __init__(self,file_name,lines_per_atom = 5, element_list = [], element_counts = []):
		'''Parses the tape2 file from FLX to create a GAS object. Does not retain 3rd and 4th deriviative information. Must be fed elemental information, or element will remain unpopulated. The element generator is very simple, so if you feed it things in the wrong order, it won't know. '''
		self.file_name = file_name

		self.lines_per_atom = lines_per_atom		

		fid = open(file_name,'r')

		lines = fid.readlines()

		fid.close()

		splitlines = []

		############ this is a good text parser for a block of numbers of different row lengths
		for i in range(len(lines)):
			splitlines.append(lines[i].split())			
		#############

		############ finds end position of atom lines
		endi = 0
		while 1!=len(splitlines[endi]):
			endi+=1

		############cuts up sections of file up accordingling

		self.wrapbody = splitlines[0:endi]
		self.unparced_tail = lines[endi:]
		self.tail = splitlines[endi:]
		
		############ converts strs to floats

		for i in range(len(self.wrapbody)):
			for j in range(len(self.wrapbody[i])):
				self.wrapbody[i][j] = float(self.wrapbody[i][j])
		
		########### creates a line version of the body so its easier to manipulate
		self.number_of_atoms = endi/self.lines_per_atom
		self.body = []

		for i in range(self.number_of_atoms):
			self.body.append([])
			
			wrapi = i*self.lines_per_atom
			
			for b in range(self.lines_per_atom):
				for a in self.wrapbody[ wrapi + b ]:
					self.body[i].append(a)

		########## parsing the box size
		self.box = self.tail[6:9]

		for i in range(len(self.box)):
			for j in range(len(self.box[i])):
				self.box[i][j] = float(self.box[i][j])

		######## Using the body to populate the GAS object

		from kirke import GAS
		sys = GAS()
		sys.box = self.box
		sys.box_mode = 'triclinic'

		sys.tail = self.tail

		for i in range(number_of_atoms):
			sys.sx.append(self.body[i][0])
			sys.sy.append(self.body[i][1])
			sys.sz.append(self.body[i][2])
			sys.vx.append(self.body[i][3])
			sys.vy.append(self.body[i][4])
			sys.ax.append(self.body[i][5])
			sys.ay.append(self.body[i][6])
			sys.q.append(self.body[i][13])

		if element_list != []:
			if len(element_list) == len(element_counts) and sum(element_counts) == number_of_atoms:
				start = 0
				for a in range(len(element_list)):
					end = start+element_counts[a]
					for b in range(start,end):
						sys.element[b]=element_list[a]
					start+=element_counts[a]+1

			else:
				print 'TAPE2_sys: Inconsistent number of elements: they will not be included in the sys object.'

		sys.update_external_from_internal()

		return sys

