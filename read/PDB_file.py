

def PDB_file(file_name='',verbose = True):
	''' Uses PDB file to initialize a GAS object. Uses ATOM and CONNECT to populate'''
	from kirke import GAS

	sys = GAS()
	sys.file_name=file_name
	fileid = open(file_name,'r')
	file_data = fileid.readlines()
	fileid.close()

	bond_id = 0

	for i in range(0,len(file_data)):
		line = file_data[i]
		split_line = line.split()
		if split_line != []:
			if split_line[0] == 'HEADER':
				sys.comment+=str(line)
			elif split_line[0] == 'COMPND':
				sys.comment+=str(line)
			elif split_line[0] == 'AUTHOR':
				sys.comment+=str(line)
			elif split_line[0] == 'ATOM':
				sys.id.append(int(split_line[1]))
				sys.element.append(split_line[2].capitalize())
				sys.molecule.append(int(split_line[4]))
				sys.x.append(float(split_line[5]))
				sys.y.append(float(split_line[6]))
				sys.z.append(float(split_line[7]))
				if len(split_line) == 15:
					sys.q.append(float(split_line[14]))

#Still needs help to deal with double bonds, types

			elif split_line[0] == 'CONECT':
				for b in range(2,len(split_line)):
					if split_line[b] > split_line[1]:
						bond_id+=1
						sys.bonds.append([bond_id,0,int(split_line[1]),int(split_line[b])])


	#Reprocessing the bonds list to update types
	element_set = list(set(sys.element))
	bond_type_list = [['null','null']]

	for a in range(len(element_set)):
		e = element_set[a]
		for b in range(a,len(element_set)):
			l = element_set[b]
			bond_type_list.append([e, l])

	sys.update_number_of_atoms()
	sys.update_number_of_bonds()
	sys.update_atomic_number_list()
	sys.build_index_dict()
	
	for b in range(sys.number_of_bonds):
		element_1 = sys.element[sys.id_to_index[sys.bonds[b][2]]]
		element_2 = sys.element[sys.id_to_index[sys.bonds[b][3]]]

		#Sorting elements according to their positions within element set

		index_element_1 = element_set.index(element_1)
		index_element_2 = element_set.index(element_2)

		if index_element_1 <= index_element_2:
			sys.bonds[b][1] = bond_type_list.index([element_1, element_2])
		else:
			sys.bonds[b][1] = bond_type_list.index([element_2, element_1])

	print "PDB file has been read in. Bond types have been automatically assigned according to the following rules:"
	print bond_type_list
	print "If there are any double bonds in your system, these will currently appear in the bonds list twice."


	return sys


