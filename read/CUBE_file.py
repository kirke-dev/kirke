def CUBE_file(file_name,verbose = False, convert_bohr_to_angstroms = True):
	fid = open(file_name,'r')
	from numpy import zeros,array, sqrt
	
	for i in range(2):# skipping stuff
		fid.readline()
	if convert_bohr_to_angstroms:
		bohr = 0.52917721092 # 2010 CODATA value
	else:
		bohr = 1.0
	# number of atoms and origin
	natoms_origin = fid.readline().split()
	natoms = abs(int(natoms_origin[0]))
	origin = [float(natoms_origin[1])*bohr  ,float(natoms_origin[2])*bohr, float(natoms_origin[3])*bohr]

	cells = [0,0,0]
	basis = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]

	for j in range(3):
		sline = fid.readline().split()
		cells[j] = int(sline[0])
		for i in range(3):
			basis[i][j] = float(sline[i+1]) * bohr * cells[j]
	if verbose:
		print 'Cells : ', cells
		print 'Basis : ', basis

	
	
	def is_int(a):
		try:
			int(a)
			return True
		except ValueError:
			return False
		


	from kirke import GAS
	atoms = GAS()
	
	sline = fid.readline().split() # if i do this first, it saves an if statement
	
	
	
	while is_int(sline[0]) and (is_int(sline[1])==False): # this part is atoms and stuff
		atoms.atomic_number.append(int(sline[0]))
		atoms.q.append(float(sline[1]))
		atoms.x.append(float(sline[2])*bohr)
		atoms.y.append(float(sline[3])*bohr)
		atoms.z.append(float(sline[4])*bohr)
		sline = fid.readline().split()
	
	fid.close()
	atoms.update_number_of_atoms()
	atoms.fill_id_list()
	atoms.update_element_list()
	atoms.box_mode = 'triclinic'

	atoms.basis = basis
	atoms.origin = origin
	atoms.update_internal_from_external()

	return atoms

