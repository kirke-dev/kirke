
class XDATCAR_file:

	def __init__(self,file_name = 'XDATCAR'):
	
	
		def is_number(s):
    			try:
			        float(s)
		        	return True
    			except ValueError:
        			return False
	
	
		def test_for_3_floats(line):
			sline = line.split()
			if len(sline) == 3:
				if is_number(sline[0]) and is_number(sline[1]) and is_number(sline[2]):
					return True
				else:
					return False
			else:
				return False
				
		def test_for_1_floats(line):
			sline = line.split()
			if len(sline) == 1:
				if is_number(sline[0]):
					return True
				else:
					return False
			else:
				return False
				
		def test_for_box(lines,ln):
			#first line is comment
			t1 = test_for_1_floats(lines[ln+1])
			t2 = test_for_3_floats(lines[ln+2])
			t3 = test_for_3_floats(lines[ln+3]) 
			t4 = test_for_3_floats(lines[ln+4])
			if t1 and t2 and t3 and t4:
				return True
			else:
				return False		
	
	
		self.file_name = file_name
		self.frames = []         
		from kirke import GAS
		fid = open(file_name,'r')
		lines = fid.readlines()	
		fid.close()
		
		
		
		
		line_number = 0
		while line_number < len(lines):
			sys = GAS()
		
			
			##########################
			
			if test_for_box(lines, line_number): 

				sys.comment = lines[line_number]  ### getting comment line out of the way
				line_number += 1
				
				scale = float(lines[line_number])	
				box_lines = lines[line_number+1:line_number+4]
				a = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
				for i in range(3):
					v2 = box_lines[i].split()
					for j in range(3):
						a[i][j] = scale*float(v2[j])
						
				sys.origin = [0.0,0.0,0.0]
				sys.update_basis_from_vectors( a1 =a[0] , a2 =a[1] , a3 =a[2] )
				sys.box_mode = 'triclinic'
			
				line_number += 4 #jumps over the scale factor and vectors (4 lines)
			
				element_list = []
				number_of_each_type = []
				for a in lines[line_number].split():
					element_list.append(a)
					number_of_each_type = []
				for a in lines[line_number +1].split():
					number_of_each_type.append(int(a))
				line_number += 2
				
			else:
				sys.origin = self.frames[0].origin
				sys.basis = self.frames[0].basis
				sys.box_mode = self.frames[0].box_mode
				sys.comment = self.frames[0].comment
				
			####################
			
			while 'Direct configuration=' not in lines[line_number]:  #seeks ahead toget past second boxes (bug in vasp?)
				line_number += 1
			#print lines[line_number]
			line_number += 1
			
			############
			more_left = True
			while line_number < len(lines) and more_left:
				if test_for_3_floats(lines[line_number]):
					sline = lines[line_number].split()
					sys.sx.append(float(sline[0]))
					sys.sy.append(float(sline[1]))
					sys.sz.append(float(sline[2]))
					line_number += 1
				else:
					more_left = False
					
			
			######## copied from the poscar reader
			with_element = True
			type_number = 0
			for count in number_of_each_type:
				type_number += 1
				for i in range(count):
					if with_element:	
						sys.element.append(element_list[type_number-1])
					else:
						sys.atomic_number.append(type_number)

			
			external_mode = False
			sys.update_number_of_atoms()
			sys.periodic = [True, True, True]
			if with_element: 
				sys.update_atomic_number_list()
		
			if external_mode:
				sys.fill_s_lists()
				sys.update_internal_from_external()
			else:
				sys.fill_position_lists()
				sys.update_external_from_internal()
			#############

			#print sys.number_of_atoms
			self.frames.append(sys)
			
		self.number_of_frames = len(self.frames)	
		
			
			
			
		
