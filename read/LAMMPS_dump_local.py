#!/usr/bin/env python

class LAMMPS_dump_local:
	#Variant of the LAMMPS dump file for compute */local commands
	#Added 25 May 2012
	#Has mutiple frames with headers: header identifies column types. 
	#Modified 2013 March 1 with new dump_local output structure from LAMMPS
	#Modified 2013 May 3 to handle Box Bounds
	def __init__(self,file_name, verbose = False):
		''' Parses LAMMPS dump file for compute */local commands. Header is used to automatically idenitify data types, which can then be retrieved with get('column_name'). '''
		self.file_name = file_name
		fid=open(self.file_name,'r')
		self.verbose = verbose
		self.start_line_list = []

		i = 0
		for a in fid:
			if a == 'ITEM: TIMESTEP\n' or ('Atoms. Timestep' in a):
				self.start_line_list.append(i+1) # the starting line of each block #blocks including the headers for now
			i += 1
		fid.close()

		self.number_of_lines = i 
		self.number_of_frames = len(self.start_line_list)

		if self.verbose:
			print 'LAMMPS_dump_local: Found %i frame(s)' % self.number_of_frames

	def get_frame(self, frame_number): # returns a system for the frame
		
		lines = []
		fid = open(self.file_name)
		if self.verbose:
			print "LAMMPS_dump_local.get_frame: Seeking..."
		i = 0 
		end_reached = False
		
		for a in fid: ### this loop cuts out the correct segment of atoms as a list of strings
			if end_reached == False:
				if i >= self.start_line_list[ frame_number ]:
					
					if a == 'ITEM: TIMESTEP\n' or ('Atoms. Timestep:' in a):
						end_reached = True
					else:
						lines.append(a)
			i += 1
		fid.close()
		
		#if frame_number != (len(self.start_line_list)-1):
		#	lines.pop() # the last line is the next blocks number of atoms
		######################
		######### fixing a nice file name
		#frame_file_name = self.file_name.split('.')
		#suffix = frame_file_name.pop()
		#for b in frame_file_name:
		#	self.file_name = self.file_name + b + '.'
		#self.file_name = self.file_name + str(frame_number) + '.' + suffix

		#########################
		if self.verbose:
			print "LAMMPS_dump_local: Parsing " + self.file_name + ' from ' + self.file_name + '...' 

		i = 0
		while i < len(lines):
			if lines[i]=='\r\n':
				print 'LAMMPS_dump_local: line %i was crap.' %i
				lines.pop(i)
			i+=1
		timestep = lines[0]
		entries = lines[2]		

		box = []
		for a in range(4,7):
			sectioned_string = lines[a].split()
			box.append([float(sectioned_string[0]), float(sectioned_string[1])])

		if self.verbose:
			print box

		headerline = lines[7].split()
		
		print headerline
		self.column_names = headerline[2:]
		print self.column_names
		self.columns = []
		for a in self.column_names:
			self.columns.append([]) #Makes an empty list for each column, regardless of GAS membership

		for a in range(8,len(lines)): ###### All data is fed into columns 

			sectioned_string = lines[a].split()

			for b in range(len(sectioned_string)): 

				self.columns[b].append(float(sectioned_string[b]))

		
		return self.column_names, self.columns, timestep, box

	def get(self,column_name): #this function returns the column with the input name
		'''Returns data from 'coulmn_name' '''
		return self.columns[ self.column_names.index(column_name) ]


