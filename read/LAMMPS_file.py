#!/usr/bin/env python

def clean_split_line(line):
	# Added 12 Dec 2014 to ignore comments in the LAMMPS file. -EJC
	''' Splits the line and removes any comments. '''
	clean_line = []
	split_line = line.split()
	for i in range(len(split_line)):
		if '#' in split_line[i]:
			break
		else:
			clean_line.append(split_line[i])
	return clean_line

def LAMMPS_file(file_name,number_to_element = {},periodic = [True,True,True], atom_style = 'full',): #mike added the periodic default because its more likely 10-8-2012 #Katie added atom_style option 11-13-12 because she needed it
 #Modified 18 March 13 to match use of masses as atomistic attributes
#Modified 6/7/2013 for kirke transition
#Modified 20 June 2013 to autofill type_masses
	'''Reads in the LAMMPS read_data file type, which can be generated from restart file using the restart2data tool supplied by LAMMPS. Assumes periodic [True, True, True].number_to_element dictionary is purely optional'''
	from kirke import GAS
	from time import time
	sys = GAS()
	sys.file_name = file_name
	fileid = open(file_name,'r')
	file_data = fileid.readlines()
	fileid.close()
	sys.comment = file_data[0].split('\n')[0]
	sys.periodic = periodic # added this as default, mike, 10-8-2012
	sys.atom_style = atom_style #added this 11-13-12 KS
	masses_start = 0
	pair_coeff_start = 0
	bond_coeff_start = 0
	angle_coeff_start = 0
	dihedral_coeff_start = 0
	improper_coeff_start = 0
#Katie additions 7/23/12
	bondbond_coeff_start = 0
	bondangle_coeff_start = 0
	middlebondtorsion_coeff_start = 0
	endbondtorsion_coeff_start = 0
	angletorsion_coeff_start = 0
	angleangletorsion_coeff_start = 0
	bondbond13_coeff_start = 0
	angleangle_coeff_start = 0
#End COMPASS additions
	
	atoms_start = 0
	velocities_start = 0
	bonds_start = 0
	angles_start = 0
	dihedrals_start = 0
	impropers_start = 0

	sys.box = [[0.0,0.0],[0.0,0.0],[0.0,0.0]]

	for i in range(1,len(file_data)):
		line = file_data[i]
		split_line = line.split()

#Making this less sensitive to stray spaces Feb 22 2013 -KS
#Rearranged and changed several if statements to elif to prevent problems assigning coefficients to the wrong category when using COMPASS - Dec 9 2013 EJC
		if 'Masses' in line :
			masses_start = i + 2
		if 'Pair Coeffs' in line :
			pair_coeff_start = i + 2
		# bond and angle coeffs are in elif statements with the COMPASS-specific bits
		if 'Dihedral Coeffs' in line:
			dihedral_coeff_start = i + 2
		if 'Improper Coeffs' in line :
			improper_coeff_start = i + 2
#Katie 7/23/12 COMPASS compatibility
		if 'BondBond Coeffs' in line:
			bondbond_coeff_start = i + 2
		elif 'Bond Coeffs' in line :		# bond coeffs
			bond_coeff_start = i + 2

		if 'AngleAngleTorsion Coeffs' in line:
			angleangletorsion_coeff_start = i + 2
		elif 'AngleTorsion Coeffs' in line:
			angletorsion_coeff_start = i + 2
		elif 'AngleAngle Coeffs' in line :
			angleangle_coeff_start = i + 2
		elif 'BondAngle Coeffs' in line:
			bondangle_coeff_start = i + 2
		elif 'Angle Coeffs' in line :		# angle coeffs
			angle_coeff_start = i + 2

		if 'MiddleBondTorsion Coeffs' in line:
			middlebondtorsion_coeff_start = i + 2
		if 'EndBondTorsion Coeffs' in line :
			endbondtorsion_coeff_start = i + 2
		if 'BondBond13 Coeffs' in line:
			bondbond13_coeff_start = i + 2
#End Katie 7/23/12, End Eleanor 12/9/2013

		if 'Atoms' in line:
			atoms_start = i + 2
		if 'Velocities' in line :
			velocities_start = i + 2
		if 'Bonds' in line :
			bonds_start = i + 2
		if 'Angles' in line :
			angles_start = i + 2
		if 'Dihedrals' in line:
			dihedrals_start = i + 2
		if 'Impropers' in line :
			impropers_start = i + 2

		if len(split_line) == 2:
			if split_line[1] == 'atoms':
				n_atoms = int(split_line[0])
			if split_line[1] == 'bonds':
				n_bonds = int(split_line[0])
			if split_line[1] == 'angles':
				n_angles = int(split_line[0])
			if split_line[1] == 'dihedrals':
				n_dihedrals = int(split_line[0])
			if split_line[1] == 'impropers':
				n_impropers = int(split_line[0])

		if len(split_line) == 3:
			if split_line[2] == 'types':
				if split_line[1] == 'atom':
					n_atom_t = int(split_line[0])
				if split_line[1] == 'bond':
					n_bond_t = int(split_line[0])
				if split_line[1] == 'angle':
					n_angle_t = int(split_line[0])
				if split_line[1] == 'dihedral':
					n_dihedral_t = int(split_line[0])
				if split_line[1] == 'improper':
					n_improper_t = int(split_line[0])

		if len(split_line) == 4:
			if split_line[2] == 'xlo' and split_line[3] == 'xhi':
				sys.box[0][0] = float(split_line[0])
				sys.box[0][1] = float(split_line[1])

			if split_line[2] == 'ylo' and split_line[3] == 'yhi':
				sys.box[1][0] = float(split_line[0])
				sys.box[1][1] = float(split_line[1])

			if split_line[2] == 'zlo' and split_line[3] == 'zhi':
				sys.box[2][0] = float(split_line[0])
				sys.box[2][1] = float(split_line[1])
			sys.box_mode = 'orthorhombic'
			sys.update_basis_and_origin_for_orthorhombic()


	#------------atoms are unavoidable
	#-----Modified 11-13-12 to include atom_style support. Defaults to full
	if atom_style == 'full':
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
#			split_line = line.split()
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.molecule.append(int(split_line[1]))
			sys.atomic_number.append(int(split_line[2]))
			sys.q.append(float(split_line[3]))
			sys.x.append(float(split_line[4]))
			sys.y.append(float(split_line[5]))
			sys.z.append(float(split_line[6]))
			sys.element.append('')
			if len(split_line) == 10: 
				sys.nx.append(int(split_line[7]))
				sys.ny.append(int(split_line[8]))
				sys.nz.append(int(split_line[9]))
	elif atom_style == 'charge':
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.q.append(float(split_line[2]))
			sys.x.append(float(split_line[3]))
			sys.y.append(float(split_line[4]))
			sys.z.append(float(split_line[5]))
			sys.element.append('')
			if len(split_line) == 9: 
				sys.nx.append(int(split_line[6]))
				sys.ny.append(int(split_line[7]))
				sys.nz.append(int(split_line[8]))
	elif atom_style == ('angle' or 'bond' or 'molecular'):
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.molecule.append(int(split_line[1]))
			sys.atomic_number.append(int(split_line[2]))
			sys.x.append(float(split_line[3]))
			sys.y.append(float(split_line[4]))
			sys.z.append(float(split_line[5]))
			sys.element.append('')
			if len(split_line) == 9: 
				sys.nx.append(int(split_line[6]))
				sys.ny.append(int(split_line[7]))
				sys.nz.append(int(split_line[8]))
	elif atom_style == 'atomic':
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.x.append(float(split_line[2]))
			sys.y.append(float(split_line[3]))
			sys.z.append(float(split_line[4]))
			sys.element.append('')
			if len(split_line) == 8: 
				sys.nx.append(int(split_line[5]))
				sys.ny.append(int(split_line[6]))
				sys.nz.append(int(split_line[7]))		
	elif atom_style == 'dipole':
		sys.mux = []
		sys.muy = []
		sys.muz = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.q.append(float(split_line[2]))
			sys.x.append(float(split_line[3]))
			sys.y.append(float(split_line[4]))
			sys.z.append(float(split_line[5]))
			sys.element.append('')
			sys.mux.append(float(split_line[6]))
			sys.muy.append(float(split_line[7]))
			sys.muz.append(float(split_line[8]))
			if len(split_line) == 12: 
				sys.nx.append(int(split_line[9]))
				sys.ny.append(int(split_line[10]))
				sys.nz.append(int(split_line[11]))
	elif atom_style == 'electron':
		sys.spin = []
		sys.eradius = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.q.append(float(split_line[2]))
			sys.spin.append(float(split_line[3]))
			sys.eradius.append(float(split_line[4]))
			sys.x.append(float(split_line[5]))
			sys.y.append(float(split_line[6]))
			sys.z.append(float(split_line[7]))
			sys.element.append('')
			if len(split_line) == 11: 
				sys.nx.append(int(split_line[8]))
				sys.ny.append(int(split_line[9]))
				sys.nz.append(int(split_line[10]))
	elif atom_style == 'ellipsoid':
		sys.ellipsoidflag = []
		sys.density = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.ellipsoidflag.append(int(split_line[2]))
			sys.density.append(float(split_line[3]))
			sys.x.append(float(split_line[4]))
			sys.y.append(float(split_line[5]))
			sys.z.append(float(split_line[6]))
			sys.element.append('')
			if len(split_line) == 10: 
				sys.nx.append(int(split_line[7]))
				sys.ny.append(int(split_line[8]))
				sys.nz.append(int(split_line[9]))		
	elif atom_style == 'line':
		sys.lineflag = []
		sys.density = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.molecule.append(int(split_line[1]))
			sys.atomic_number.append(int(split_line[2]))
			sys.lineflag.append(int(split_line[3]))
			sys.density.append(float(split_line[4]))
			sys.x.append(float(split_line[5]))
			sys.y.append(float(split_line[6]))
			sys.z.append(float(split_line[7]))
			sys.element.append('')
			if len(split_line) == 11: 
				sys.nx.append(int(split_line[8]))
				sys.ny.append(int(split_line[9]))
				sys.nz.append(int(split_line[10]))
	elif atom_style == 'meso':
		sys.rho = []
		sys.e = []
		sys.cv = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.rho.append(float(split_line[2]))
			sys.e.append(float(split_line[3]))
			sys.cv.append(float(split_line[4]))
			sys.x.append(float(split_line[5]))
			sys.y.append(float(split_line[6]))
			sys.z.append(float(split_line[7]))
			sys.element.append('')
			if len(split_line) == 11: 
				sys.nx.append(int(split_line[8]))
				sys.ny.append(int(split_line[9]))
				sys.nz.append(int(split_line[10]))

	elif atom_style == 'peri':
		sys.volume = []
		sys.density = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.volume.append(float(split_line[2]))
			sys.density.append(float(split_line[3]))
			sys.x.append(float(split_line[4]))
			sys.y.append(float(split_line[5]))
			sys.z.append(float(split_line[6]))
			sys.element.append('')
			if len(split_line) == 10: 
				sys.nx.append(int(split_line[7]))
				sys.ny.append(int(split_line[8]))
				sys.nz.append(int(split_line[9]))		

	elif atom_style == 'sphere':
		sys.diameter = []
		sys.density = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.diameter.append(float(split_line[2]))
			sys.density.append(float(split_line[3]))
			sys.x.append(float(split_line[4]))
			sys.y.append(float(split_line[5]))
			sys.z.append(float(split_line[6]))
			sys.element.append('')
			if len(split_line) == 10: 
				sys.nx.append(int(split_line[7]))
				sys.ny.append(int(split_line[8]))
				sys.nz.append(int(split_line[9]))

	elif atom_style == 'tri':
		sys.triangleflag = []
		sys.density = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.molecule.append(int(split_line[1]))
			sys.atomic_number.append(int(split_line[2]))
			sys.triangleflag.append(int(split_line[3]))
			sys.density.append(float(split_line[4]))
			sys.x.append(float(split_line[5]))
			sys.y.append(float(split_line[6]))
			sys.z.append(float(split_line[7]))
			sys.element.append('')
			if len(split_line) == 11: 
				sys.nx.append(int(split_line[8]))
				sys.ny.append(int(split_line[9]))
				sys.nz.append(int(split_line[10]))

	elif atom_style == 'wavepacket':
		sys.spin = []
		sys.eradius = []
		sys.etag = []
		sys.cs_re = []
		sys.cs_im = []
		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.atomic_number.append(int(split_line[1]))
			sys.q.append(float(split_line[2]))
			sys.spin.append(float(split_line[3]))
			sys.eradius.append(float(split_line[4]))
			sys.etag.append(float(split_line[5]))
			sys.cs_re.append(float(split_line[6]))
			sys.cs_im.append(float(split_line[7]))
			sys.x.append(float(split_line[8]))
			sys.y.append(float(split_line[9]))
			sys.z.append(float(split_line[10]))
			sys.element.append('')
			if len(split_line) == 14: 
				sys.nx.append(int(split_line[11]))
				sys.ny.append(int(split_line[12]))
				sys.nz.append(int(split_line[13]))

	else:
		print "LAMMPS_file: Check atom_style input. Hybrid is not currently supported" 

		for i in range(atoms_start, atoms_start + n_atoms):
			line = file_data[i]
			split_line = clean_split_line(line)
			sys.id.append(int(split_line[0]))
			sys.molecule.append(int(split_line[1]))
			sys.atomic_number.append(int(split_line[2]))
			sys.q.append(float(split_line[3]))
			sys.x.append(float(split_line[4]))
			sys.y.append(float(split_line[5]))
			sys.z.append(float(split_line[6]))
			sys.element.append('')
			if len(split_line) == 10: 
				sys.nx.append(int(split_line[7]))
				sys.ny.append(int(split_line[8]))
				sys.nz.append(int(split_line[9]))
	
	sys.update_number_of_atoms()
	#----------- masses

	if masses_start > 0:
		lammps_masses_types = []
		lammps_masses_values = []
		sys.type_masses=[]
		for i in range(masses_start, masses_start + n_atom_t):
			line = file_data[i]
			split_line = line.split()
			item = [int(split_line[0]),float(split_line[1])]
			lammps_masses_types.append(item[0])
			lammps_masses_values.append(item[1])
			sys.type_masses.append(item)
		#Properly filling the masses list as an atomistic attribute 18 March 2013
		for i in range(len(sys.atomic_number)):
			type_id = lammps_masses_types.index(sys.atomic_number[i])
			sys.masses.append(lammps_masses_values[type_id])

	#----------- pair coeffs

	if pair_coeff_start > 0:
		for i in range(pair_coeff_start, pair_coeff_start + n_atom_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append(float(a))
			sys.pair_coeffs.append(item)

	#----------- bond coeffs
	if bond_coeff_start > 0:
		for i in range(bond_coeff_start, bond_coeff_start + n_bond_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append(float(a))
			sys.bond_coeffs.append(item)

	#----------- angle coeffs
	if angle_coeff_start > 0:
		for i in range(angle_coeff_start, angle_coeff_start + n_angle_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append(float(a))
			sys.angle_coeffs.append(item)

	#----------- dihedral coeffs
	if dihedral_coeff_start > 0:
		for i in range(dihedral_coeff_start, dihedral_coeff_start + n_dihedral_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
				# dihedral coefficients are left as strings because some dihedral types require floats while others require ints
			sys.dihedral_coeffs.append(item)

	#----------- improper coeffs
	if improper_coeff_start > 0:
		for i in range(improper_coeff_start, improper_coeff_start + n_improper_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
				# improper coefficients are left as strings because some dihedral types require floats while others require ints
			sys.improper_coeffs.append(item)

######### Begin Katie additions for COMPASS parameter accomodations 7/23/12

	#---------BondBond coeffs
	if bondbond_coeff_start > 0:
		for i in range(bondbond_coeff_start, bondbond_coeff_start + n_angle_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.bondbond_coeffs.append(item)
	#---------BondAngle coeffs
	if bondangle_coeff_start > 0:
		for i in range(bondangle_coeff_start, bondangle_coeff_start + n_angle_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.bondangle_coeffs.append(item)
	#---------MiddleBondTorsion Coeffs
	if middlebondtorsion_coeff_start > 0:
		for i in range(middlebondtorsion_coeff_start, middlebondtorsion_coeff_start + n_dihedral_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.middlebondtorsion_coeffs.append(item)	
	#---------EndBondTorsion Coeffs
	if endbondtorsion_coeff_start > 0:
		for i in range(endbondtorsion_coeff_start, endbondtorsion_coeff_start + n_dihedral_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.endbondtorsion_coeffs.append(item)	
	#---------AngleTorsion Coeffs
	if angletorsion_coeff_start > 0:
		for i in range(angletorsion_coeff_start, angletorsion_coeff_start + n_dihedral_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.angletorsion_coeffs.append(item)	
	#---------AngleAngleTorsion Coeffs
	if angleangletorsion_coeff_start > 0:
		for i in range(angleangletorsion_coeff_start, angleangletorsion_coeff_start + n_dihedral_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.angleangletorsion_coeffs.append(item)	
	#---------BondBond13Coeffs
	if bondbond13_coeff_start > 0:
		for i in range(bondbond13_coeff_start, bondbond13_coeff_start + n_dihedral_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.bondbond13_coeffs.append(item)	
	#---------AngleAngle Coeffs
	if angleangle_coeff_start > 0:
		for i in range(angleangle_coeff_start, angleangle_coeff_start + n_impropers_t):
			line = file_data[i]
			split_line = clean_split_line(line)
			item = [int(split_line[0])]
			for a in split_line[1:]:
				item.append((a))
			sys.angleangle_coeffs.append(item)	

########End Katie additions 7/23/12

	#-------- atoms were done
	#--------- velocities
	if velocities_start > 0:

		sys.fill_v_lists()
		ii = 0
		for i in range(velocities_start, velocities_start + n_atoms):
			line = file_data[i]
			split_line = line.split()
			atom_id = int(split_line[0])
			if atom_id == sys.id[ii]:
				index = ii
			else:
				index = sys.id.index(atom_id)
			sys.vx[index] = float(split_line[1])
			sys.vy[index] = float(split_line[2])
			sys.vz[index] = float(split_line[3])
			ii+=1

	# --------- bonds
	if bonds_start > 0:
		for i in range(bonds_start, bonds_start + n_bonds):
			line = file_data[i]
			split_line = line.split()
			item = []
			for a in split_line[0:4]:
				item.append(int(a))
			sys.bonds.append(item)

	# --------- angles
	if angles_start > 0:
		for i in range(angles_start, angles_start + n_angles):
			line = file_data[i]
			split_line = line.split()
			item = []
			for a in split_line[0:5]:
				item.append(int(a))
			sys.angles.append(item)

	# --------- dihedrals
	if dihedrals_start > 0:
		for i in range(dihedrals_start, dihedrals_start + n_dihedrals):
			line = file_data[i]
			split_line = line.split()
			item = []
			for a in split_line[0:6]:
				item.append(int(a))
			sys.dihedrals.append(item)

	# --------- impropers
	if impropers_start > 0:
		for i in range(impropers_start, impropers_start + n_impropers):
			line = file_data[i]
			split_line = line.split()
			item = []
			for a in split_line[0:6]:
				item.append(int(a))
			sys.impropers.append(item)

	# --------- finished	
	if number_to_element != {}:	
		sys.convert_number_to_element(translation_dict = number_to_element)

	#Updating boxing behavior so that compute_minimum_image_vector works correctly
	if sys.box_mode == 'orthorhombic':
		sys.update_lengths_from_box()
		sys.update_internal_from_external()

	sys.update_number_of_atoms()
	sys.update_number_of_bonds()
	sys.update_number_of_angles()
	return sys

