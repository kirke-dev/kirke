#!/usr/bin/env python

################## UNIT CELL FILES (may not report x,y,z correctly should be reviewed) ##########
	
def UC_file(file_name, with_charge = False, with_origin = False):
	'''This file type is aimed at servicing unitcells and periodic boxes'''
	# updated to have and origin option 1/22/13
	from kirke import GAS
	sys = GAS()
	sys.file_name=file_name
	fileid = open(file_name,'r')
	file_data = fileid.readlines()
	fileid.close()
	#sys.comment = 
	if with_origin:
		list_of_atom_lines = file_data[3:]
	else:
		list_of_atom_lines = file_data[2:]

	sys.number_of_atoms = len(list_of_atom_lines)

	aline = file_data[0].split()
	bline = file_data[1].split()

	######################
	a = float(aline[0])
	b = float(aline[1])
	c = float(aline[2])
	######################
	alpha = float(bline[0])
	beta = float(bline[1])
	gamma = float(bline[2])
	###################
	originx = 0.0
	originy = 0.0
	originz = 0.0
	if with_origin:
		cline = file_data[2].split()
		originx = float(cline[0])
		originy = float(cline[1])
		originz = float(cline[2])

	for line in list_of_atom_lines:
		line_list=line.split()

		sys.element.append(line_list[0])
		sys.sx.append(float(line_list[1]))
		sys.sy.append(float(line_list[2]))
		sys.sz.append(float(line_list[3]))

		if with_charge:
			sys.q.append(float(line_list[4]))


	if alpha == 90.0 and beta == 90.0 and gamma == 90.0:
		sys.box_mode = 'orthorhombic'
		sys.box = [[originx, originx+a],[originy, originy+b],[originz, originz+c]]
	else:
		sys.box_mode = 'triclinic'
		sys.basis = sys.update_basis_from_lengths_and_angles(a,b,c,alpha,beta,gamma)
		sys.origin = [originx, originy, originz]

	sys.update_atomic_number_list()
	sys.fill_position_lists()
	sys.update_external_from_internal()
	
	#sys.fill_id_list()
	#if with_charge == False:
	#	sys.fill_q_list()

	return sys

