#!/usr/bin/env python

def XYZQV_file(file_name, use_element = False):#Default_to_type
		'''Uses a .xyzqv file to initialize a gas object.'''
		from kirke import GAS
		sys = GAS()
		sys.file_name=file_name
		fileid = open(file_name,'r')
		file_data = fileid.readlines()
		fileid.close()
		#sys.comment = file_data[1]
		list_of_data_lines = file_data[2:]

		if use_element == True:
			for a_string in list_of_data_lines:
				sectioned_string = a_string.split()
				sys.element.append(sectioned_string[0])
				sys.x.append(float(sectioned_string[1]))
				sys.y.append(float(sectioned_string[2]))
				sys.z.append(float(sectioned_string[3]))
				sys.q.append(float(sectioned_string[4]))
				sys.vx.append(float(sectioned_string[5]))
				sys.vy.append(float(sectioned_string[6]))
				sys.vz.append(float(sectioned_string[7]))
			
			sys.update_atomic_number_list()

		else: ########uses atomic number instead
			for a_string in list_of_data_lines:
				sectioned_string = a_string.split()
				#print sectioned_string
				sys.atomic_number.append(int(sectioned_string[0]))
				sys.x.append(float(sectioned_string[1]))
				sys.y.append(float(sectioned_string[2]))
				sys.z.append(float(sectioned_string[3]))
				sys.q.append(float(sectioned_string[4]))
				sys.vx.append(float(sectioned_string[5]))
				sys.vy.append(float(sectioned_string[6]))
				sys.vz.append(float(sectioned_string[7]))
				
			sys.update_element_list()

		sys.update_number_of_atoms()
		#sys.update_box_from_positions()
		#sys.fill_id_list()
		
		return sys

