#!/usr/bin/env python

class LAMMPS_dump_file:
	#A cross between the LAMMPS xyz and the log.lammps
	#Atomic dump files only
	#Added 17 May 2012
	#Modified 16 August 2012 for testing and debugging
		
	#Has mutiple frames with headers: header identifies column types. Including "if" clauses to populate the GAS object for specific column titles, but multi-frames may screw with mass.

	def __init__(self,file_name, verbose =True):
		''' Parses data from LAMMPS atomistic dump files. Automatically detects multiple frames. If included data is a GAS atomistic attribute, will automatically populate a GAS object. Multiple frames may cause errors in total system mass. Uses header information to identify columns. '''

		self.file_name = file_name
		fid=open(self.file_name,'r')
		self.verbose = verbose
		self.start_line_list = []

		i = 0
		for a in fid:
			
			if 'ITEM: TIMESTEP' in a:
				self.start_line_list.append(i+1) # the starting line of each block #blocks including the headers for now
			i += 1
		fid.close()
#		print self.start_line_list		# moved this to the verbose option on 24 Feb 2014 - Eleanor
		self.number_of_lines = i 
		self.number_of_frames = len(self.start_line_list)

		if self.verbose:
			print 'LAMMPS_dump_file: Found %i frame(s)' % self.number_of_frames

	def get_frame(self, frame_number, number_to_element = {}): # returns a system for the frame
		
		lines = []
		fid = open(self.file_name)
		if self.verbose:
			print "LAMMPS_dump_file.get_frame: Seeking..."
		i = 0 
		end_reached = False
		
		for a in fid: ### this loop cuts out the correct segment of atoms as a list of strings
			if end_reached == False:
				if i >= self.start_line_list[ frame_number ]:
					
					if 'ITEM: TIMESTEP'in a:
						end_reached = True
						
					else:
						lines.append(a)
			i += 1
		fid.close()

		######################
		from kirke import GAS
#		from kirke import *
		sys = GAS() ## creating an empty GAS class
		######### fixing a nice file name
		frame_file_name = self.file_name.split('.')
		suffix = frame_file_name.pop()
		for b in frame_file_name:
			sys.file_name = sys.file_name + b + '.'
		sys.file_name = sys.file_name + str(frame_number) + '.' + suffix

		#########################
		if self.verbose:
			print "LAMMPS_dump_file: Parsing " + sys.file_name + ' from ' + self.file_name + '...' 

		i = 0
		while i < len(lines):
			if lines[i]=='\r\n':
				print 'LAMMPS_dump_file: line %i was crap.' %i
				lines.pop(i)
			i+=1
		#Feeding in information from the header
		#Need to allow Triclinic bounds

		if 'xy' in lines[3]:
			xlines = lines[4].split()
			ylines = lines[5].split()
			zlines = lines[6].split()
			veca = [float(xlines[1])-float(xlines[0]),0.0,0.0]
			vecb = [float(xlines[2]), float(ylines[1])-float(ylines[0]),0.0]
			vecc = [float(ylines[2]), float(zlines[2]), float(zlines[1])-float(zlines[0])]
			sys.update_basis_from_vectors(veca,vecb,vecc)
			sys.box_mode = 'triclinic'
			
		else:
			xlines = lines[4].split()
			xbox = [float(xlines[0]), float(xlines[1])]
			ylines = lines[5].split()
			ybox = [float(ylines[0]),float(ylines[1])]
			zlines = lines[6].split()
			zbox = [float(zlines[0]),float(zlines[1])]
			sys.box_mode = 'orthorhombic'
			sys.box = [xbox,ybox,zbox]
			sys.update_lengths_from_box()
			sys.update_internal_from_external()		# added by EJC on 24 Feb 2014

		sys.number_of_atoms=lines[2]

		headerline = lines[7].split()
		
		print headerline
		self.column_names = headerline[2:]
		print self.column_names
		self.columns = []
		for a in self.column_names:
			
			self.columns.append([]) #Makes an empty list for each column, regardless of GAS membership

		for a in range(8,len(lines)): ###### this loop feeds the data into the GAS class. All data is fed into columns 
			#Holy if loops, Batman: compensating for very flexible file formats
			#All possible GAS categories that are in dump files
			#May want to add the following dump columns to gas: fx, fy, fz

			sectioned_string = lines[a].split()

			for b in range(len(sectioned_string)): 
				
				self.columns[b].append(float(sectioned_string[b]))

				if self.column_names[b] == 'id':
					sys.id.append(int(sectioned_string[b]))
				elif self.column_names[b] == 'x':
					sys.x.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'y':
					sys.y.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'z':
					sys.z.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'vx':
					sys.vx.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'vy':
					sys.vy.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'vz':
					sys.vz.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'q':
					sys.q.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'ix':
					sys.nx.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'iy':
					sys.ny.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'iz':
					sys.nz.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'mass':
					sys.mass += (float(sectioned_string[b]))
				elif self.column_names[b] == 'mol':
					sys.molecule.append(float(sectioned_string[b]))
				elif self.column_names[b] == 'type':
					sys.atomic_number.append(int(sectioned_string[b]))
				elif self.column_names[b] == 'element':
					sys.element.append(float(sectioned_string[b]))

		if number_to_element != {}:	
			sys.convert_number_to_element(translation_dict = number_to_element)


		sys.update_number_of_atoms()
		#sys.update_box_from_positions()
		#sys.fill_id_list()
		
		#print sys.number_of_atoms
		
		return sys

	def get(self,column_name,frame): #this function returns the column with the input name
		'''Returns data from 'column_name' '''
		self.get_frame(frame)

		return self.columns[ self.column_names.index(column_name) ]
