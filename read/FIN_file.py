#!/usr/bin/env python

####################### FIN FILES (aka. FLX's version of crystalmaker) #############
def FIN_file(file_name):
	'''Reads data from the FLX version of CrystalMaker files into a limited GAS object. '''
		#Updated to use update_atomice_number_list, rather than using an index
	from kirke import GAS
	from kirke.elements import element_symbol_list
	sys = GAS()
	sys.file_name = file_name
	inputid = open(file_name,'r')
	filedata = inputid.readlines()
	inputid.close()
	# finds the word 'ATOM' as start of body	
	s = ''	
	i = 0	
	while s != 'ATOM\n':
		s = filedata[i]
		i+=1
	
	data_lines = filedata[i:]

	for line in data_lines:
		splitline = line.split()
		sys.element.append(splitline[0])
		sys.x.append(float(splitline[2]))
		sys.y.append(float(splitline[3]))
		sys.z.append(float(splitline[4]))
		
		sys.atomic_number.append(element_symbol_list.index(splitline[0]))
	
	#sys.box_mode = 'orthorhombic'
	sys.update_number_of_atoms()
	#sys.fill_id_list()
	#sys.fill_q_list()
	#sys.update_box_from_positions()
	sys.update_atomic_number_list()
	sys.update_masses_from_element() #The great masses modification
	return sys

