#!/usr/bin/env python

class LAMMPS_LOG_file:

	def __init__(self,file_name = 'log.lammps',first_column_name = 'Step',verbose = True):
		''' Parses data from log.lammps files into columns which can then be called with get(column_name). Automatically detects and skips headers, footers. May have problems if the number of columns changes. '''
		fid = open(file_name,'r')
		lines = fid.readlines() 
		fid.close()

		# Changed start/end_data to self.start/end_data in case the user wants to extract a specific run from file - 26 Mar 2015 EJC
		self.start_data = []
		self.end_data = []

		i = 0
		while i < len(lines):  #this loop finds the start and end of the data
			line = lines[i]
			splitline = line.split()

			if splitline != []:
				if splitline[0] == first_column_name:
					self.column_names = splitline
					self.start_data.append( i+1)

				if splitline[0] == 'Loop' or splitline[0] == 'ERROR:' or splitline[0] == 'ERROR' or splitline[0] == 'mpirun:' or splitline[0] == 'mpirun': #Added to allow failed logs to be parsed
					self.end_data.append( i-1 )
				elif i==len(lines)-1 and len(splitline)<len(self.column_names):
					self.end_data.append(i-1)
			i+=1

		if len(self.start_data) != len(self.end_data):
			print "LAMMPS_LOG_file: Failed to parse data segments, error imminent."

		self.number_of_runs = len(self.start_data)
		print self.column_names
		print 'Data starts at line:',self.start_data
		print 'Data ends at line:',self.end_data

		self.columns = []
		for column_name in self.column_names: #this loops makes a list of empty lists, one for each column
			self.columns.append([])

		for m in range(len(self.start_data)):

			for i in range(self.start_data[m],self.end_data[m]+1): #this loop populates those empty lists with the data from each column
				line = lines[i]
				splitline = line.split()
				if splitline[1] != 'WARNING:':
					if 'help' in splitline:
						print 'LAMMPS_LOG_file: WARNING, skipped line: {0}'.format(splitline)
					else:
						for j in range(len(splitline)):
							self.columns[j].append( float(splitline[j]) )

		self.number_of_steps = len(self.columns[0])
		self.number_of_columns = len(self.column_names)
		self.header = lines[0:self.start_data[0]-1]
		#self.footer = lines[end_data[:]+1:] might need to make a list of footers in the future
		
		if verbose == True:

			print "LAMMPS_LOG_file: \n%i Steps in These %i Columns:\n %s " %(self.number_of_steps, self.number_of_columns, lines[self.start_data[0]-1] )

	def get(self,column_name): #this function returns the column with the input name
		'''Returns data from 'column_name' '''
		return self.columns[ self.column_names.index(column_name) ]

