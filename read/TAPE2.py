#!/usr/bin/env python

class TAPE2_file:
	def __init__(self,file_name,lines_per_atom = 5):
		'''Parses the tape2 file from FLX to be modified and rewritten. Does not create a GAS object at this point in time. '''
		self.file_name = file_name
				
		self.lines_per_atom = lines_per_atom		
				
		fid = open(file_name,'r')

		lines = fid.readlines()

		fid.close()

		splitlines = []

		############ this is a good text parser for a block of numbers of different row lengths
		for i in range(len(lines)):
			splitlines.append(lines[i].split())			
		#############

		############ finds end position of atom lines
		endi = 0
		while 1!=len(splitlines[endi]):
			endi+=1

		############cuts up sections of file up accordingling

		self.wrapbody = splitlines[0:endi]
		self.unparced_tail = lines[endi:]
		self.tail = splitlines[endi:]
		
		############ converts strs to floats

		for i in range(len(self.wrapbody)):
			for j in range(len(self.wrapbody[i])):
				self.wrapbody[i][j] = float(self.wrapbody[i][j])
		
		########### creates a line version of the body so its easier to manipulate
		self.number_of_atoms = endi/self.lines_per_atom
		self.body = []

		for i in range(self.number_of_atoms):
			self.body.append([])
			
			wrapi = i*self.lines_per_atom
			
			for b in range(self.lines_per_atom):
				for a in self.wrapbody[ wrapi + b ]:
					self.body[i].append(a)

		########## parsing the box size
		self.box = self.tail[6:9]

		for i in range(len(self.box)):
			for j in range(len(self.box[i])):
				self.box[i][j] = float(self.box[i][j])

	def update_number_of_atoms(self):
		self.number_of_atoms = len(self.body)	
		return self.number_of_atoms


	def update_wrapbody(self):
		########## this function updates the wrap body so that it can be written easier
		self.number_of_atoms = len(self.body)
		newwrap = []

		if self.lines_per_atom == 5:
			for i in range(self.number_of_atoms):
				newwrap.append(self.body[i][0:4])
				newwrap.append(self.body[i][4:8])
				newwrap.append(self.body[i][8:12])			
				newwrap.append(self.body[i][12:16])
				newwrap.append(self.body[i][16:19])

		if self.lines_per_atom == 4:
			for i in range(self.number_of_atoms):
				newwrap.append(self.body[i][0:5])
				newwrap.append(self.body[i][5:10])
				newwrap.append(self.body[i][10:15])			
				newwrap.append(self.body[i][15:19])

		self.wrapbody = newwrap
		self.number_of_atoms = len(self.body)


	def print_box(self):
		
		print str(self.box[0][0]) +'\t\t'+ str(self.box[0][1]) +'\t\t'+ str(self.box[0][2])
		print str(self.box[1][0]) +'\t\t'+ str(self.box[1][1]) +'\t\t'+ str(self.box[1][2])
		print str(self.box[2][0]) +'\t\t'+ str(self.box[2][1]) +'\t\t'+ str(self.box[2][2])


	def convert_body_to_external(self):
		for i in range(len(self.body)):
			self.body[i][0] = (self.body[i][0]+0.5)*self.box[0][0]
			self.body[i][1] = (self.body[i][1]+0.5)*self.box[1][1]
			self.body[i][2] = (self.body[i][2]+0.5)*self.box[2][2]

	def convert_body_to_internal(self):
		for i in range(len(self.body)):
			self.body[i][0] = (self.body[i][0] / self.box[0][0]) - 0.5
			self.body[i][1] = (self.body[i][1] / self.box[1][1]) - 0.5
			self.body[i][2] = (self.body[i][2] / self.box[2][2]) - 0.5

