#!/usr/bin/env python


def vector_vector_add_3D(self,v,u):
	'''Adds three-D vectors without calling numpy'''
	c = [0.0,0.0,0.0]
	c[0] = v[0] + u[0]
	c[1] = v[1] + u[1]
	c[2] = v[2] + u[2]
	return c

def vector_vector_angle_3D(self,v,u):
	''' Computes vector-vector angles in 3D without calling numpy'''
	from math import acos,sqrt
	theta = acos(self.dot_product_3D(v,u) / sqrt( self.dot_product_3D(v,v) * self.dot_product_3D(u,u) ))
	return theta

def vector_vector_subtract_3D(self,v,u):
	''' Computes vector-vector subtraction in 3D without calling numpy'''
	c = [0.0,0.0,0.0]
	c[0] = v[0] - u[0]
	c[1] = v[1] - u[1]
	c[2] = v[2] - u[2]
	return c

def matrix_determinant_3D(self,M): #mike 11/27/2012
	ans = M[0][0]*M[1][1]*M[2][2] + M[0][1]*M[1][2]*M[2][0] + M[0][2]*M[1][0]*M[2][1]
	ans = ans - (M[0][2]*M[1][1]*M[2][0] + M[0][1]*M[1][0]*M[2][2] + M[2][1]*M[1][2]*M[0][0])
	return ans

def matrix_inverse_3D(self,M): #mike 11/27/2012
	det_inv = 1.0/self.matrix_determinant_3D(M)
	c0 = [ M[0][0], M[1][0], M[2][0] ]
	c1 = [ M[0][1], M[1][1], M[2][1] ]
	c2 = [ M[0][2], M[1][2], M[2][2] ]
	M_inv = []
	M_inv.append(self.cross_product_3D(c1,c2))
	M_inv.append(self.cross_product_3D(c2,c0))
	M_inv.append(self.cross_product_3D(c0,c1))
	for i in range(3):
		for j in range(3):
			M_inv[i][j] = M_inv[i][j]*det_inv
	return M_inv

def cross_product_3D(self,v,u):
	''' Computes 3D cross product without numpy, cross_product_3D(V,U)=VxU '''
	c = [0.0,0.0,0.0]
	c[0] = v[1]*u[2] - v[2]*u[1]
	c[1] = v[2]*u[0] - v[0]*u[2]
	c[2] = v[0]*u[1] - v[1]*u[0]
	return c

def dot_product_3D(self,v,u):
	'''Computes 3D dot product without numpy'''
	return v[0]*u[0] + v[1]*u[1] + v[2]*u[2]

def unit_vector_3D(self,v):
	c = [0.0,0.0,0.0]
	from math import sqrt
	inv_mag = 1.0/sqrt(self.dot_product_3D(v,v))
	c[0] = v[0]*inv_mag
	c[1] = v[1]*inv_mag
	c[2] = v[2]*inv_mag
	return c
	
def vector_projection_3D(self,v,u):
	'''Computes the vector projection of v on to u'''
	proj = [0.0,0.0,0.0]
	unit_u = self.unit_vector_3D(u)
	mag = self.dot_product_3D(v,unit_u)
	for i in range(3):
		proj[i] = mag*unit_u[i]
	return proj
	
def vector_rejection_3D(self,v,u):
	'''Computes the vector rejection of v on to u'''
	rej = self.vector_vector_subtract_3D(v, self.vector_projection_3D(v,u))
	return rej

def matrix_vector_product_3D(self,M,V): 
	'''Computes the matrix vector product without using numpy. Assumes 3x3 matrix list and 1x3 vector list.'''
	ans = [0.0,0.0,0.0]
	for i in range(3):
		for j in range(3):
			ans[i]+= M[i][j]*V[j]
	return ans

def matrix_matrix_product_3D(self,M1,M2): 
	'''Computes the matrix matrix product without using numpy. Assumes tw0 3x3 matrices. M1*M2 '''
	ans = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	for i in range(3):

		for j in range(3):
			for k in range(3):
				ans[i][j]+= M1[i][k]*M2[k][j]
	return ans

def convert_internal_position_to_external(self,V): 
	'''Give it an internal position and it will give the external. Don't use this to convert the atom positions, it would be slow.'''
	return self.vector_vector_add_3D( self.matrix_vector_product_3D(self.basis,V), self.origin)

def convert_external_position_to_internal(self,V): 
	'''Give it an external position and it will give the internal. Don't use this to convert the atom positions, it would be slow.'''
	return self.matrix_vector_product_3D( self.matrix_inverse_3D(self.basis), self.vector_vector_subtract_3D(V, self.origin))


def internal_wrap(self,s):
	'''Forces the atom into the box'''
	return s-self.floor(s)

def vector_magnitude(self,a):
	'''Compute vector Magnitude'''
	mag = sqrt((a[0]*a[0])+(a[1]*a[1])+(a[2]*a[2]))
	return mag
