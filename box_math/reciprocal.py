#!/usr/bin/env python

def compute_reciprocal_basis(self): # mike 11/27/2012
	from math import pi
	h_inv = self.compute_basis_inverse()
	if self.box_mode == 'triclinic':
		reciprocal_basis = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
		for i in range(3):
			for j in range(3):
				reciprocal_basis[i][j] = 2*pi* h_inv[j][i]
	elif self.box_mode == 'orthorhombic':
		reciprocal_basis = [[2*pi/self.lx, 0.0, 0.0],
					[0.0, 2*pi/self.ly, 0.0]
					[0.0 ,0.0, 2*pi/self.lz]]
	else:
		print "compute_reciprocal_basis: Warning, No box_mode is set. This is will fail."
	return reciprocal_basis
	
def compute_reciprocal_basis_vector(self,i):  # mike 11/26/2012 
	''' Returns reciprocal vector desribing the basis set in the given dimension, 0,1 or 2.'''
	
	from math import pi
	if self.box_mode == 'orthorhombic':
		self.update_lengths_from_box()
		if i == 0:
			return [2*pi/self.lx,	0.0,		0.0]
		elif i == 1:
			return [0.0,		2*pi/self.ly,	0.0]
		elif i == 2:
			return [0.0		,0.0,		2*pi/self.lz]
		else:
			print "compute_reciprocal_basis_vector: Error: Improper Axes Number"

	elif self.box_mode == 'triclinic':
		vect_out = [0.0, 0.0, 0.0]
		reciprocal_basis = self.compute_reciprocal_basis()
		for j in range(3):
			vect_out[j]=reciprocal_basis[j][i]
		return vect_out
	else:
		print "compute_reciprocal_basis_vector: Error: No box mode set!"
