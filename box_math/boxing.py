#!/usr/bin/env python

def update_basis_from_lengths_and_angles(self,a,b,c, alpha = 90.0, beta = 90.0 , gamma = 90.0):
	''' Updates the basis set from the given lattice lengths and angles (in degrees). The default value for alpha, beta and gamma is 90.0 degrees. '''
	from math import sqrt, cos, sin, pi, acos
	alpha = alpha*pi/180
	beta = beta*pi/180
	gamma = gamma*pi/180
	
	A1 = [a , 0.0 ,0.0]  # formulas from lammps manual 

	A2 = [b*cos(gamma) , b*sin(gamma) ,0.0] 

	A3 = [c*cos(beta), 0.0 ,0.0 ]
	A3[1] = ( c*b*cos(alpha) - A2[0]*A3[0] )/A2[1]
	A3[2] = sqrt(c**2.0 - A3[0]**2.0 - A3[1]**2.0)

	# check section
	dalpha = abs(acos(self.dot_product_3D(A2,A3) / (b*c)) - alpha)*180/pi
	dbeta  = abs(acos(self.dot_product_3D(A1,A3) / (a*c)) - beta )*180/pi
	dgamma = abs(acos(self.dot_product_3D(A1,A2) / (a*b)) - gamma)*180/pi

	maxerr = 0.01 #deg
	if dalpha > maxerr or dbeta > maxerr or dgamma > maxerr:
		print 'update_basis_from_lengths_and_angles: ERROR Something is wrong with the basis update, check your lengths and angles.'

	h = self.update_basis_from_vectors(A1,A2,A3)
	return h

def compute_lengths_and_angles_from_basis(self,verbose = True):
	'''Uses the basis set to compute the a,b, and c lengths and alpha, beta and gamma angles'''
	A1 = self.get_basis_vector(0)
	A2 = self.get_basis_vector(1)
	A3 = self.get_basis_vector(2)
	from math import sqrt,pi
	ans = [[],[]]
	ans[0].append( sqrt(self.dot_product_3D(A1,A1)) )
	ans[0].append( sqrt(self.dot_product_3D(A2,A2)) )
	ans[0].append( sqrt(self.dot_product_3D(A3,A3)) )

	ans[1].append(self.vector_vector_angle_3D(A2,A3)*180/pi)
	ans[1].append(self.vector_vector_angle_3D(A1,A3)*180/pi)
	ans[1].append(self.vector_vector_angle_3D(A1,A2)*180/pi)

	if verbose:
		print 'compute_lengths_and_angles_from_basis(verbose): Box parameters as lattice parameters:'
		print 'a = %f, b = %f, c = %f' % (ans[0][0], ans[0][1] ,ans[0][2])
		print 'alpha = %f, beta = %f, gamma = %f' % (ans[1][0], ans[1][1] ,ans[1][2])
	return ans

def get_basis_vector(self,j):
	''' Returns vector desribing the basis set in the given dimension, 0,1 or 2.'''
	a = [0.0,0.0,0.0]
	for i in range(3):
		a[i] = self.basis[i][j]
	return a

def compute_box_volume(self):
	''' Computes box volume from the basis vectors or lx, ly, lz, depending on box mode.'''
	if self.box_mode == 'orthorhombic' or self.box_mode == 'triclinic' :
		if  self.box_mode == 'orthorhombic': # I just removed the self.lx, self.ly, self.lz, and update_box_from_lengths -mike 6/14/2015
			lx = self.box[0][1] - self.box[0][0]
			ly = self.box[1][1] - self.box[1][0]
			lz = self.box[2][1] - self.box[2][0]
			v = lx * ly * lz
		else:
			v = self.matrix_determinant_3D( self.basis )
		return v
	else:
		print 'compute_box_volume: Error, No good box_mode set!'

def compute_maximum_radius_sphere_in_box(self):
	''' What is the largest radius sphere that will fit in the box? This answers that, this is also the longest range an interaction can have without invoking PBCs'''
	if self.box_mode == 'orthorhombic' or self.box_mode == 'triclinic' :
		if  self.box_mode == 'orthorhombic': # 
			lx = self.box[0][1] - self.box[0][0]
			ly = self.box[1][1] - self.box[1][0]
			lz = self.box[2][1] - self.box[2][0]
			radius = min(lx , ly , lz)/2.0
		else:
			spacing = [0.0,0.0,0.0]
			from math import pi,sqrt,floor
			for i in range(3):
				B = self.compute_reciprocal_basis_vector(i)
				spacing[i] = (2.0*pi) / sqrt(B[0]**2.0 + B[1]**2.0 + B[2]**2.0)	
			radius = min(spacing)/2.0	
			
		return radius
	else:
		print 'compute_maximum_radius_sphere_in_box: Error, No good box_mode set!'


def compute_basis_inverse(self):
	'''Computes the inverse of the basis set for use in reciprocal space.'''
	return self.matrix_inverse_3D(self.basis) #shortened to use the new functions


def update_basis_from_vectors(self,a1,a2,a3):
	''' Updates the basis set, h, from the given vectors'''
	h = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	for i in range(3):
		h[i][0] = a1[i]
		h[i][1] = a2[i]
		h[i][2] = a3[i]
	self.basis = h
	return h


def realign_basis(self):
	'''This function rotates the basis (box vectors) so that the first vector lies in the x-direction and the second vector lies in the x-y plane. Just makes an upper triangular matrix of the basis matrix via a rotation.'''

	a = self.get_basis_vector(0)
	b = self.get_basis_vector(1)
	c = self.get_basis_vector(2)
	
	aprime = [0,0,0]
	bprime = [0,0,0]
	cprime = [0,0,0]
	
	def magnitude(a):
		from math import sqrt
		return sqrt(self.dot_product_3D(a,a))
	
	b_rej_a = self.vector_rejection_3D(b,a)
	c_rej_a = self.vector_rejection_3D(c,a)
	
	aprime[0] = magnitude(a)
	
	bprime[0] = self.dot_product_3D(b, self.unit_vector_3D(a))
	bprime[1] = magnitude(b_rej_a)
	
	cprime[0] = self.dot_product_3D(c, self.unit_vector_3D(a))
	cprime[1] = self.dot_product_3D(c_rej_a, self.unit_vector_3D(b_rej_a))
	cprime[2] = magnitude(self.vector_rejection_3D(c_rej_a, b_rej_a))

	self.update_basis_from_vectors(aprime, bprime, cprime)
