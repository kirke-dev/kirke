#!/usr/bin/env python

# howdy

def add_atoms_sys(self,new_atoms):
	'''Appends atoms in an existing GAS object to the current object. Currently assumes that atom IDs have been corrected to account for the destination system size.'''
#Modified 20 June 2013 to correctly handle masses: previous typo fixed
#Modified 20 March 2014 to include internals when possible
	if new_atoms.number_of_atoms > 0:
		for index in range(new_atoms.number_of_atoms):

			self.x.append(new_atoms.x[index])
			self.y.append(new_atoms.y[index])
			self.z.append(new_atoms.z[index])
			#self.q.append(new_atoms.q[index])
			self.element.append(new_atoms.element[index])
			self.atomic_number.append(new_atoms.atomic_number[index])
			self.id.append(new_atoms.id[index])

			if len(new_atoms.q) == new_atoms.number_of_atoms:
				self.q.append(new_atoms.q[index])

			if len(new_atoms.masses) == new_atoms.number_of_atoms:
				self.masses.append(new_atoms.masses[index])

			if len(new_atoms.vx) == new_atoms.number_of_atoms:
				self.vx.append(new_atoms.vx[index])
				self.vy.append(new_atoms.vy[index])
				self.vz.append(new_atoms.vz[index])

			if len(new_atoms.nx) == new_atoms.number_of_atoms:
				self.nx.append(new_atoms.nx[index])
				self.ny.append(new_atoms.ny[index])
				self.nz.append(new_atoms.nz[index])

			if len(new_atoms.sx) == new_atoms.number_of_atoms:
				self.sx.append(new_atoms.sx[index])
				self.sy.append(new_atoms.sy[index])
				self.sz.append(new_atoms.sz[index])

			if len(new_atoms.molecule) == new_atoms.number_of_atoms:
				self.molecule.append(new_atoms.molecule[index])

			if len(new_atoms.selective_dynamics) == new_atoms.number_of_atoms:
				self.selective_dynamics.append(new_atoms.selective_dynamics[index])
		self.update_number_of_atoms()

def add_atoms_simple(self, new_atoms):
	'''New atoms are expected in the format: ID, type, molecule, Q, X, Y, Z. Currently assumes IDs are already corrected to account for system size'''
	if len(new_atoms) > 0:
		for i in range(len(new_atoms)):
			self.x.append(new_atoms[i][4])
			self.y.append(new_atoms[i][5])
			self.z.append(new_atoms[i][6])
			self.q.append(new_atoms[i][3])
			self.id.append(new_atoms[i][0])
			self.atomic_number.append(new_atoms[i][2])
			self.molecule.append(new_atoms[i][1])
			#Creating 0.0s to hold open spaces for other atomic objects
			self.vx.append(0.0)
			self.vy.append(0.0)
			self.vz.append(0.0)
			self.nx.append(0.0)
			self.ny.append(0.0)
			self.nz.append(0.0)
			self.masses.append(0.0) #KS 18Mar13
			
		self.update_atomic_number_list()
		self.update_element_list()
		self.update_number_of_atoms()
		

def add_bonds(self,new_bonds):
	'''Adds the given bonds to the existing system. Expects the format: ID type atom1-ID atom2-ID. Assumes ID has been corrected to be non-overlapping with existing bonds.'''
	if len(new_bonds) > 0:
		for i in range(len(new_bonds)):
			self.bonds.append(new_bonds[i])
		self.update_number_of_bonds()

def add_angles(self,new_angles):
	'''Adds the given angles to the existing system. Expects the format: ID type atom1-ID atom2-ID atom3-ID. Assumes ID has been corrected to be non-overlapping with existing angles.'''
	if len(new_angles) > 0:
		for i in range(len(new_angles)):
			self.angles.append(new_angles[i])
		self.update_number_of_angles()

def add_dihedrals(self,new_dihedrals):
	'''Adds the given dihedrals to the existing system. Expects the format: ID type atom1-ID atom2-ID atom3-ID atom4-ID. Assumes ID has been corrected to be non-overlapping with existing dihedrals.'''
	if len(new_dihedrals) > 0:
		for i in range(len(new_dihedrals)):
			self.dihedrals.append(new_dihedrals[i])
		self.update_number_of_dihedrals()

def add_impropers(self,new_impropers):
	'''Adds the given impropers to the existing system. Expects the format: ID type atom1-ID atom2-ID atom3-ID atom4-ID. Assumes ID has been corrected to be non-overlapping with existing impropers.'''
	if len(new_impropers) > 0:
		for i in new_impropers:
			self.impropers.append(new_impropers[i])
		self.update_number_of_impropers()

