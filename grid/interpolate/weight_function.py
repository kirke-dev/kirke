




def weight_function(delta,segment,order): #these are weight functions derived from convolutions of characteristic functions (AKA top hat functions), If you have half a wit, you won't mess with these. They work well.
	x = delta
	P = order
	if P == 1:
		if segment == 0:
			W = 1.0

	elif P == 2:
		if segment == 0:
			W = (1.0/2)*(1 - 2*x)
		elif segment == 1:
			W = (1.0/2)*(1 + 2*x)

	elif P == 3:
		if segment == 0:
			W = (1.0/8)*(1 - 4*x + 4*x**2)
		elif segment == 1:
			W = (1.0/4)*(3       - 4*x**2)
		elif segment == 2:
			W = (1.0/8)*(1 + 4*x + 4*x**2)

	elif P == 4:
		if segment == 0:
			W = (1.0/48)*(1  - 6*x  + 12*x**2 - 8*x**3)
		elif segment == 1:
			W = (1.0/48)*(23 - 30*x - 12*x**2 + 24*x**3)
		elif segment == 2:
			W = (1.0/48)*(23 + 30*x - 12*x**2 - 24*x**3)
		elif segment == 3:
			W = (1.0/48)*(1  + 6*x  + 12*x**2 + 8*x**3)

	elif P == 5:
		if segment == 0:
			W = (1.0/384)*(1   - 8*x   + 24*x**2  - 32*x**3 + 16*x**4)
		elif segment == 1:
			W = (1.0/96 )*(19  - 44*x  + 24*x**2  + 16*x**3 - 16*x**4)
		elif segment == 2:
			W = (1.0/192)*(115         - 120*x**2           + 48*x**4)
		elif segment == 3:
			W = (1.0/96 )*(19  + 44*x  + 24*x**2  - 16*x**3 - 16*x**4)
		elif segment == 4:
			W = (1.0/384)*(1   + 8*x   + 24*x**2  + 32*x**3 + 16*x**4)


	elif P == 6:
		if segment == 0:
			W = (1.0/3840)*(1   - 10*x  + 40*x**2  - 80*x**3  + 80*x**4  - 32*x**5 )
		elif segment == 1:
			W = (1.0/3840)*(237 - 750*x + 840*x**2 - 240*x**3 - 240*x**4 + 160*x**5)
		elif segment == 2:
			W = (1.0/1920)*(841 - 770*x - 440*x**2 + 560*x**3 + 80*x**4  - 160*x**5)
		elif segment == 3:
			W = (1.0/1920)*(841 + 770*x - 440*x**2 - 560*x**3 + 80*x**4  + 160*x**5)
		elif segment == 4:
			W = (1.0/3840)*(237 + 750*x + 840*x**2 + 240*x**3 - 240*x**4 - 160*x**5)
		elif segment == 5:
			W = (1.0/3840)*(1   + 10*x  + 40*x**2  + 80*x**3  + 80*x**4  + 32*x**5 )


	elif P == 7:
		if segment == 0:
			W = (1.0/46080)*(1     - 12*x    + 60*x**2   - 160*x**3  + 240*x**4  - 192*x**5 + 64*x**6 )
		elif segment == 1:
			W = (1.0/23040)*(361   - 1416*x  + 2220*x**2 - 1600*x**3 + 240*x**4  + 384*x**5 - 192*x**6)
		elif segment == 2:
			W = (1.0/46080)*(10543 - 17340*x + 4740*x**2 + 6880*x**3 - 4080*x**4 - 960*x**5 + 960*x**6)
		elif segment == 3:
			W = (1.0/11520)*(5887            - 4620*x**2             + 1680*x**4            - 320*x**6)
		elif segment == 4:
			W = (1.0/46080)*(10543 + 17340*x + 4740*x**2 - 6880*x**3 - 4080*x**4 + 960*x**5 + 960*x**6)
		elif segment == 5:
			W = (1.0/23040)*(361   + 1416*x  + 2220*x**2 + 1600*x**3 + 240*x**4  - 384*x**5 - 192*x**6)
		elif segment == 6:
			W = (1.0/46080)*(1     + 12*x    + 60*x**2   + 160*x**3  + 240*x**4  + 192*x**5 + 64*x**6 )
	else:
		W = 0.0

	return W


