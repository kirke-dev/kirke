#!/usr/bin/env python

def interpolate_scalar_to_mesh(rho, sys, point_value_list, order):
	'''Requires internal coordinates to be populated, It doesn't return data just operates the nd.array given. Zero_centered means that there will be a cell with its center at <0,0,0> in internal coordinates. maybe i should make an auxilary function for returning cell centers'''
	dimensionality = 3

	cells = rho.field.shape

	zero_centered = rho.zero_centered

	bump = 0.5   # the bump centers the odd order polynomials on the cell center
	even = False # this might be needed later... 
	if order % 2 == 0:
		even = True
		bump = 0.0


	from math import floor
	from kirke.grid.interpolate import weight_function #Might need to uncomment this line later
	from numpy import zeros


	cellf = [0.0,0.0,0.0]
	atomcellindex = [0,0,0]
	smalldelta = [0,0,0]    # fractional coordinate in a cell.
	firstcellindex = [0,0,0]
	WF_pre_list_0 = zeros(order)
	WF_pre_list_1 = zeros(order)
	WF_pre_list_2 = zeros(order)

	inverse_cell_volume = (cells[0]*cells[1]*cells[2])/sys.compute_box_volume()
	cell_volume = 1.0/inverse_cell_volume
	### end of prep


	for atom in range(sys.number_of_atoms):
		S = (sys.sx[atom], sys.sy[atom], sys.sz[atom])
		for dim in range(dimensionality):
			cellf[dim] = S[dim]*cells[dim]
			if zero_centered:
				cellf[dim] += 0.5 # bumps in the NEGATIVE direction the cell wall
			atomcellindex[dim] = int(floor(cellf[dim]))
			smalldelta[dim] = cellf[dim] - atomcellindex[dim]
			firstcellindex[dim] = atomcellindex[dim] - order/2  #integer math, wont matter if outside bounds, will be modulo'ed later
		for i in range(order):
			WF_pre_list_0[i] = weight_function(smalldelta[0] - bump,i,order)

		for j in range(order):
			WF_pre_list_1[j] = weight_function(smalldelta[1] - bump,j,order)

		for k in range(order):
			WF_pre_list_2[k] = weight_function(smalldelta[2] - bump,k,order)

	
		# puting it on the mesh

		for i in range(order):
			for j in range(order):
				for k in range(order):
					weight =  WF_pre_list_0[i] * WF_pre_list_1[j] * WF_pre_list_2[k]
					rho.field[(firstcellindex[0]+i)%cells[0]][(firstcellindex[1]+j)%cells[1]][(firstcellindex[2]+k)%cells[2]]+= point_value_list[atom]* weight*inverse_cell_volume






	

