
def compute_electrostatic_potential_using_FFT(charge_density_rgs, coulomb_constant = 14.399645353):
	'''Computes the electrostatic potential of an RGS containing a charge density. Requires charge neutrality due to the periodic boundary conditions. The coulomb_constant should be in units of [energy]*[length]/[charge]**2. The default value is for [eV]*[Ang]/[e.u.]**2'''

	####for reference, this calculates the coulumb_constant used as the default from 2014 CODATA values
	#from numpy import pi
	#eu = 1.6021766208 * 10**-19 #electron charge (SI)
	#epsilon0 = (8.854187817 * 10**-12) # in SI
	#epsilon0 = epsilon0 * (1.0/eu)*(1.0/10.0**10) # conversion
	#coulomb_constant  = 1.0/(4.0*pi*epsilon0) # about 14.4 eV*Ang usually used in atomistics





	from numpy.fft import fftn, ifftn, fftshift, ifftshift
	from numpy import zeros,array,dot, pi

	b1 = array(charge_density_rgs.compute_reciprocal_basis_vector(0))
	b2 = array(charge_density_rgs.compute_reciprocal_basis_vector(1))
	b3 = array(charge_density_rgs.compute_reciprocal_basis_vector(2))

	kspace_rho = fftshift(fftn(charge_density_rgs.field))*charge_density_rgs.compute_cell_volume() ## the cell volume isn't stickly needed but it makes the units correct in kspace.



	# with FFT shifting:
	# evens look like this [-2,-1,0,1]
	# odds look like this [-2,-1,0,1,2]
	# so the center x index is simply shape[0]/2

	########### Coulomb greens function
	# real space -> k-space
	#1/r -> 4*pi/k**2
	#so coulomb_constant/r -> coulomb_constant*4*pi * 1/k**2


	kspace_g = zeros(kspace_rho.shape)

	# Filling out greens function 
	for i in range(charge_density_rgs.field.shape[0]):
		ki = i - charge_density_rgs.field.shape[0]/2

		for j in range(charge_density_rgs.field.shape[1]):
			kj = j - charge_density_rgs.field.shape[1]/2 
	
			for k in range(charge_density_rgs.field.shape[2]):
				kk = k - charge_density_rgs.field.shape[2]/2
			
				kvector = b1*ki + b2*kj + b3*kk
				ksqr = dot(kvector,kvector)

				#if ksqr!=0.0:
				#	kspace_g[i,j,k] = (CC*4.0*pi)/ksqr
				#else:
				#	kspace_g[i,j,k] = 0.0
				#	print 'zero kpoint (%i,%i,%i) replaced with 0.0'%(i,j,k)
				kspace_g[i,j,k] = (coulomb_constant*4.0*pi)/ksqr

	# Fixing the divide by zeros warning 
	kspace_g[
		charge_density_rgs.field.shape[0]/2,
		charge_density_rgs.field.shape[1]/2,
		charge_density_rgs.field.shape[2]/2] = 0.0 #gotta kill the center kpoint because its 1/0 but charge neutrality implies that we multiply it by zero. A stronger zero... if you will.




	from kirke.grid import RGS

	phi = RGS(charge_density_rgs.field.shape)
	phi.basis = charge_density_rgs.basis
	phi.origin = charge_density_rgs.origin
	phi.zero_centered = charge_density_rgs.zero_centered

	phi.field = ifftn(ifftshift(kspace_g*kspace_rho)).real / charge_density_rgs.compute_cell_volume() # again the cell volume could be taken out above and here, but it would make the units incorrect in kspace.
	#the imaginary part should be near zero and not matter.

	return phi
