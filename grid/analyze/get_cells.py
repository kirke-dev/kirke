
def get_cells_between_cutoffs(rho, cutoff_low, cutoff_high):
	'''Uses absolute values to allow for complex numbers'''
	cell_list = []
	from numpy import absolute
	
	for i in range(rho.field.shape[0]):
		for j in range(rho.field.shape[1]):
			for k in range(rho.field.shape[2]):
				if absolute(rho.field[i][j][k]) >= cutoff_low:
					if absolute(rho.field[i][j][k]) <= cutoff_high:
						cell_list.append([i,j,k])

	return cell_list





def get_cells_above_cutoff(rho,cutoff):
	'''Uses absolute values to allow for complex numbers'''
	cell_list = []
	from numpy import absolute
	for i in range(rho.field.shape[0]):
		for j in range(rho.field.shape[1]):
			for k in range(rho.field.shape[2]):
				if absolute(rho.field[i][j][k]) >= cutoff:
					cell_list.append([i,j,k])

	return cell_list
	
	
	
def get_cells_below_cutoff(rho,cutoff):
	'''Uses absolute values to allow for complex numbers'''
	cell_list = []
	from numpy import absolute
	for i in range(rho.field.shape[0]):
		for j in range(rho.field.shape[1]):
			for k in range(rho.field.shape[2]):
				if absolute(rho.field[i][j][k]) <= cutoff:
					cell_list.append([i,j,k])

	return cell_list
