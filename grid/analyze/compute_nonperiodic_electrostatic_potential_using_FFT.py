
def compute_nonperiodic_electrostatic_potential_using_FFT(charge_density_rgs, coulomb_constant = 14.399645353):
	'''Computes the electrostatic potential of an RGS containing a charge density. The coulomb_constant should be in units of [energy]*[length]/[charge]**2. The default value is for [eV]*[Ang]/[e.u.]**2. This exploits a Coulomb kernel that is truncated to the longest dimension in the box and then analyticaly transformed to kspace. The box is also expanded so that the periodic images do not interact even with the truncated coulomb interaction. The box expansion makes this 8 to 10 times slower than using a normal periodic calculation.'''

	####for reference, this calculates the coulumb_constant used as the default from 2014 CODATA values
	#from numpy import pi
	#eu = 1.6021766208 * 10**-19 #electron charge (SI)
	#epsilon0 = (8.854187817 * 10**-12) # in SI
	#epsilon0 = epsilon0 * (1.0/eu)*(1.0/10.0**10) # conversion
	#coulomb_constant  = 1.0/(4.0*pi*epsilon0) # about 14.4 eV*Ang usually used in atomistics


	from numpy.fft import fftn, ifftn, fftshift, ifftshift
	from numpy import zeros,array,dot, pi,sqrt, ceil, cos

	##### new shape calculation ####
	a1 = array( charge_density_rgs.get_basis_vector(0) )
	a2 = array( charge_density_rgs.get_basis_vector(1) )
	a3 = array( charge_density_rgs.get_basis_vector(2) )
	rc_sqr = dot(a1,a1) + dot(a2,a2) + dot(a3,a3) + 2*( abs(dot(a1,a2)) + abs(dot(a2,a3)) + abs(dot(a1,a3)) )

	rc = sqrt(rc_sqr)

	#print rc

	n1 = charge_density_rgs.field.shape[0]
	n2 = charge_density_rgs.field.shape[1]
	n3 = charge_density_rgs.field.shape[2]
	#print n1

	d1 = sqrt(dot(a1,a1))/n1
	d2 = sqrt(dot(a2,a2))/n2
	d3 = sqrt(dot(a3,a3))/n3

	new_n1 = int(ceil(rc/d1))+n1
	new_n2 = int(ceil(rc/d2))+n2
	new_n3 = int(ceil(rc/d3))+n3



	from kirke.grid import RGS
	expanded_grid = RGS((new_n1, new_n2, new_n3))
	#print 'New shape'
	#print expanded_grid.field.shape

	expanded_grid.field = zeros((new_n1, new_n2, new_n3), dtype = complex)
	expanded_grid.update_basis_from_vectors(new_n1*(a1/n1),  new_n2*(a2/n2), new_n3*(a3/n3))

	#print 'filling new grid...'
	for i in range(charge_density_rgs.field.shape[0]):
		for j in range(charge_density_rgs.field.shape[1]):
			for k in range(charge_density_rgs.field.shape[2]):
				expanded_grid.field[i][j][k] = charge_density_rgs.field[i][j][k] 




	##### Kspace below

	b1 = array(expanded_grid.compute_reciprocal_basis_vector(0))
	b2 = array(expanded_grid.compute_reciprocal_basis_vector(1))
	b3 = array(expanded_grid.compute_reciprocal_basis_vector(2))

	kspace_rho = fftshift(fftn(expanded_grid.field))*expanded_grid.compute_cell_volume() ## the cell volume isn't stickly needed but it makes the units correct in kspace.



	# with FFT shifting:
	# evens look like this [-2,-1,0,1]
	# odds look like this [-2,-1,0,1,2]
	# so the center x index is simply shape[0]/2

	########### Coulomb greens function
	# real space -> k-space
	#1/r -> 4*pi/k**2
	#so coulomb_constant/r -> coulomb_constant*4*pi * 1/k**2
	
	#with the truncated greens functions
	#4*pi/k**2 *(1-cos(mag(k)*rc)

	kspace_g = zeros(kspace_rho.shape)

	# Filling out greens function 
	for i in range(expanded_grid.field.shape[0]):
		ki = i - expanded_grid.field.shape[0]/2

		for j in range(expanded_grid.field.shape[1]):
			kj = j - expanded_grid.field.shape[1]/2 
	
			for k in range(expanded_grid.field.shape[2]):
				kk = k - expanded_grid.field.shape[2]/2
			
				kvector = b1*ki + b2*kj + b3*kk
				ksqr = dot(kvector,kvector)
				ksqrt = sqrt(ksqr)
				#if ksqr!=0.0:
				#	kspace_g[i,j,k] = (CC*4.0*pi)/ksqr
				#else:
				#	kspace_g[i,j,k] = 0.0
				#	print 'zero kpoint (%i,%i,%i) replaced with 0.0'%(i,j,k)
				kspace_g[i,j,k] = (coulomb_constant*4.0*pi)* (1.0 - cos(ksqrt*rc))/ksqr 

	# Fixing the divide by zeros warning 
	kspace_g[
		expanded_grid.field.shape[0]/2,
		expanded_grid.field.shape[1]/2,
		expanded_grid.field.shape[2]/2] = coulomb_constant*2.0*pi*rc_sqr #gotta kill the center kpoint because of the 1/0 






	expanded_phi = RGS(expanded_grid.field.shape)
	expanded_phi.basis = expanded_grid.basis
	expanded_phi.origin = expanded_grid.origin

	expanded_phi.field = ifftn(ifftshift(kspace_g*kspace_rho)).real / expanded_grid.compute_cell_volume() # again the cell volume could be taken out above and here, but it would make the units incorrect in kspace.
	#the imaginary part should be near zero and not matter.


	#print 'filling new phi grid...'
	phi = RGS(charge_density_rgs.field.shape)
	phi.basis = charge_density_rgs.basis
	phi.origin = charge_density_rgs.origin
	phi.zero_centered = charge_density_rgs.zero_centered
	for i in range(phi.field.shape[0]):
		for j in range(phi.field.shape[1]):
			for k in range(phi.field.shape[2]):
				phi.field[i][j][k] = expanded_phi.field[i][j][k]
		

	return phi
