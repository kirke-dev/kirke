def find_surface_from_cell_list(rho, cell_list, cut_off, file_name = 'surface_points.txt',cell_info_file = 'cell_info.txt' , verbose = False): 
	#the cell info is here till i find a better method
	#################
	#this is here till I find a better method 
	cellfid = open(cell_info_file,'w')
	cellfid.write('%i\t%i\t%i\n'%rho.field.shape)
	cellfid.close()
	
	###########
	
	def in_cell(s_sphere,s_center,delta):
		'''Tests if in cell or not'''
		inside = False
		if  (s_center[0] - 0.5*delta[0]) <= s_sphere[0] and s_sphere[0] <= (s_center[0] + 0.5*delta[0]):
			if  (s_center[1] - 0.5*delta[1]) <= s_sphere[1] and s_sphere[1] <= (s_center[1] + 0.5*delta[1]):
				if  (s_center[2] - 0.5*delta[2]) <= s_sphere[2] and s_sphere[2] <= (s_center[2] + 0.5*delta[2]):
					inside = True
		return inside

	#################
	from numpy import zeros, array, dot, matrix


	cells = rho.field.shape
	delta = zeros(3)
	for i in range(3):
		delta[i] = 1.0/cells[i]

	###
	fid = open(file_name,'w')
	###

	number_of_surface_points = 0
	from numpy.linalg import solve
	for cell in cell_list:  # dear mike, WTF is all this? I need to double check it for compatibility with nonzero-centered cells 
		i = cell[0]
		j = cell[1]
		k = cell[2]

		smat = zeros((4,27))
		rhomat = zeros((27,1))
		#betamat = zeros((3,1)) # more for reference...
		mat_index = 0
		for ii in [-1,0,1]:
			for jj in [-1,0,1]:
				for kk in [-1,0,1]:
					if rho.zero_centered:
						s = delta*array([i+ii,j+jj,k+kk]) # i am pretty sure this is the cell center. 
					else: 
						s = delta*array([i+ii,j+jj,k+kk]) + delta*0.5
					smat[0][mat_index] = s[0]
					smat[1][mat_index] = s[1]
					smat[2][mat_index] = s[2]
					smat[3][mat_index] = 1.0
					rhomat[mat_index] = rho.field[(i+ii)%cells[0]][(j+jj)%cells[1]][(k+kk)%cells[2]]
					#A*beta=B

					mat_index += 1
	
		A_mat = matrix(smat)*matrix(smat.T)
		B_mat = matrix(smat)*matrix(rhomat)
		betamat = solve(A_mat,B_mat).T  # transpose makes it easier
		#### now reorganizing
		beta = array(betamat)[0]
		D = beta[3]
		beta = beta[0:3]
		#### where is the local equality ?
		if rho.zero_centered:
			s_center = delta*array([i,j,k])
		else:
			s_center = delta*array([i,j,k]) + delta*0.5
		
		lamda = (cut_off - dot(beta,s_center) - D)/dot(beta,beta)
		s_sphere = s_center + beta*lamda
		#### test if in cell
		if in_cell(s_sphere,s_center,delta):

			xyz_sphere = rho.convert_internal_position_to_external(s_sphere)
			fid.write('%f\t%f\t%f\n'%tuple(xyz_sphere))
			number_of_surface_points +=1
			#######
	if verbose:
		print 'find_surface_from_cell_list: %i Surface Points Found.' % number_of_surface_points
	fid.close()
	return number_of_surface_points


########################################


def find_wave_surface_from_cell_list(wave, origin, dr, cell_list, cutoff, file_name = 'surface_spheres_in_internal_coordinates.xyz', smooth = True):
	
	def in_cell(s_sphere,s_center,delta):
		'''Tests if in cell or not'''
		inside = False
		if  (s_center[0] - 0.5*delta[0]) <= s_sphere[0] and s_sphere[0] <= (s_center[0] + 0.5*delta[0]):
			if  (s_center[1] - 0.5*delta[1]) <= s_sphere[1] and s_sphere[1] <= (s_center[1] + 0.5*delta[1]):
				if  (s_center[2] - 0.5*delta[2]) <= s_sphere[2] and s_sphere[2] <= (s_center[2] + 0.5*delta[2]):
					inside = True
		return inside


	#################
	from numpy import zeros, array, dot, matrix

	cells = wave.shape
	delta = zeros(3)
	for i in range(3):
		delta[i] = 1.0/cells[i]

	###
	from kirke import GAS
	from kirke.write import write_XYZ
	fake = GAS()
	fake.box_mode = 'orthorhombic'
	fake.box = 	[
			[origin[0], origin[0]+dr[0]*cells[0]],
			[origin[1], origin[1]+dr[1]*cells[1]],
			[origin[2], origin[2]+dr[2]*cells[2]]
			]
	fake.update_basis_and_origin_for_orthorhombic()
	###

	from numpy.linalg import solve
	for cell in cell_list:
		i = cell[0]
		j = cell[1]
		k = cell[2]

		smat = zeros((4,27))
		rhomat = zeros((27,1))
		#betamat = zeros((3,1)) # more for reference...
		mat_index = 0
		for ii in [-1,0,1]:
			for jj in [-1,0,1]:
				for kk in [-1,0,1]:
					s = delta*array([i+ii,j+jj,k+kk])
					smat[0][mat_index] = s[0]
					smat[1][mat_index] = s[1]
					smat[2][mat_index] = s[2]
					smat[3][mat_index] = 1.0
					rhomat[mat_index] = wave[(i+ii)%cells[0]][(j+jj)%cells[1]][(k+kk)%cells[2]]**2.0
					#A*beta=B

					mat_index += 1
	
		A_mat = matrix(smat)*matrix(smat.T)
		B_mat = matrix(smat)*matrix(rhomat)
		betamat = solve(A_mat,B_mat).T  # transpose makes it easier
		#### now reorganizing
		beta = array(betamat)[0]
		D = beta[3]
		beta = beta[0:3]
		#### where is the local equality ?
		s_center = delta*array([i,j,k])
		lamda = (cutoff - dot(beta,s_center) - D)/dot(beta,beta)
		s_sphere = s_center + beta*lamda
		#### test if in cell
		if in_cell(s_sphere,s_center,delta):
			#sphere_list.append(s_sphere)
			####3
			if smooth:
				fake.sx.append(s_sphere[0])
				fake.sy.append(s_sphere[1])
				fake.sz.append(s_sphere[2])
			else:
				fake.sx.append(s_center[0])
				fake.sy.append(s_center[1])
				fake.sz.append(s_center[2])

			if wave[i,j,k] >= 0.0:
				fake.element.append('H')
			#	print '+'
			else: 
				fake.element.append('He')
			#	print '-'
			fake.update_number_of_atoms()
			#print '%i Spheres Found.' % fake.number_of_atoms
			#######
	print '%i Spheres Found.' % fake.number_of_atoms
	fake.update_external_from_internal()
	write_XYZ(fake, file_name = file_name ,use_element = True)

