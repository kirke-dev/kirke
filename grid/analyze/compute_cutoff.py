


def compute_cutoff_by_sum_percentile(rho,percent = 95):
	'''This only with positive scalar fields.'''
	from numpy import sort
	
	threshold = percent*0.01*rho.field.sum()
	sorted_values = sort(rho.field,axis=None)

	sum_inside = 0.0
	i = sorted_values.size-1 #counts down
	while sum_inside<=threshold and i >= 0:
		#print sum_inside, threshold, i
		sum_inside += sorted_values[i]
		i -= 1
	cutoff_found = sorted_values[i+1]

	return cutoff_found


def compute_cutoffs_for_cumulative_percentiles(rho, percents = [25,50,75,90] ):
	'''This only with positive scalar fields.'''
	from numpy import sort, array
	
	sorted_percents = sort(percents,axis=None) #the while loops assumes these are sorted (ascending order)
	
	thresholds = array(percents)*0.01*rho.field.sum() 
	
	sorted_values = sort(rho.field,axis=None)

	sum_inside = 0.0
	i = sorted_values.size-1 #counts down
	cutoffs_found = []
	
	threshold_index = 0
	
	while sum_inside <= thresholds[-1] and i >= 0:
		#print sum_inside, threshold, i
		sum_inside += sorted_values[i]
		
		if sum_inside > thresholds[threshold_index] :
			cutoffs_found.append ( sorted_values[i+1])
			threshold_index += 1
		i -= 1

	return cutoffs_found, sorted_percents



def compute_cutoff_for_abs_value_percentile(rho, percent  = 10):
	'''Finds the absolute value in the field where the X percent is higher in absolue value'''
	from numpy import sort, array, absolute, floor
	
	
	sorted_values = sort(absolute(rho.field), axis=None)
	
	index = int(floor( (1.0-percent*0.01)*(rho.field.size-1) )) # the -1 should prevent out of bounds errors
	cut_off = absolute(sorted_values[index])
	
	return cut_off
