#!/usr/bin/env python



def compute_laplacian(input_rgs, laplacian_of_rgs):
	
	'''Computes the laplacian of a scalar field RGS, The first input is the RGS to perform the Laplacian on and the second input is the RGS to store it in. Warning untested...'''
	
	
	hessianS = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]

	h_bar_inv = input_rgs.matrix_inverse_3D(basis)
	h_bar_inv_T = input_rgs.matrix_transpose_3D(h_bar_inv)

	#1/(length cell 0) = cells[0] this relationship is handy below
	nx = input_rgs.shape[0] # really in the s1 direction, but this is easier to think about
	ny = input_rgs.shape[1] 
	nz = input_rgs.shape[2]	
	cells = input_rgs.shape
	for c1 in range(cells[0]):
		for c2 in range(cells[1]):
			for c3 in range(cells[2]):
				# each cell starts here
				#xx really means partial s1 partial s1
				#xy really means partial s1 partial s2
				# its just easier to use them like this 
				input_rgs_xx = input_rgs[(c1+1)%cells[0]][c2][c3] + phi[(c1-1)%cells[0]][c2][c3] -2.0*input_rgs[c1][c2][c3]
				input_rgs_yy = input_rgs[c1][(c2+1)%cells[1]][c3] + input_rgs[c1][(c2-1)%cells[1]][c3] -2.0*input_rgs[c1][c2][c3]
				input_rgs_zz = input_rgs[c1][c2][(c3+1)%cells[2]] + input_rgs[c1][c2][(c3-1)%cells[2]] -2.0*input_rgs[c1][c2][c3]
				
				input_rgs_xy = (input_rgs[(c1+1)%cells[0]][(c2+1)%cells[1]][c3] +
						input_rgs[(c1-1)%cells[0]][(c2-1)%cells[1]][c3] -
						input_rgs[(c1+1)%cells[0]][(c2-1)%cells[1]][c3] -
						input_rgs[(c1-1)%cells[0]][(c2+1)%cells[1]][c3]   ) * 0.25
				
				input_rgs_xz = (input_rgs[(c1+1)%cells[0]][c2][(c3+1)%cells[2]] +
						input_rgs[(c1-1)%cells[0]][c2][(c3-1)%cells[2]] -
						input_rgs[(c1+1)%cells[0]][c2][(c3-1)%cells[2]] -
						input_rgs[(c1-1)%cells[0]][c2][(c3+1)%cells[2]]   ) * 0.25 

				input_rgs_yz = (input_rgs[c1][(c2+1)%cells[1]][(c3+1)%cells[2]] +
						input_rgs[c1][(c2-1)%cells[1]][(c3-1)%cells[2]] -
						input_rgs[c1][(c2+1)%cells[1]][(c3-1)%cells[2]] -
						input_rgs[c1][(c2-1)%cells[1]][(c3+1)%cells[2]]   ) * 0.25
							
				input_rgs_xx = input_rgs_xx*nx*nx
				input_rgs_yy = input_rgs_yy*ny*ny
				input_rgs_zz = input_rgs_zz*nz*nz
				
				input_rgs_xy = input_rgs_xy*nx*ny
				input_rgs_xz = input_rgs_xz*nx*nz
				input_rgs_yz = input_rgs_yz*ny*nz
				
				hessianS[0][0] = input_rgs_xx
				hessianS[0][1] = input_rgs_xy
				hessianS[0][2] = input_rgs_xz
				
				hessianS[1][0] = input_rgs_xy
				hessianS[1][1] = input_rgs_yy
				hessianS[1][2] = input_rgs_yz
				
				hessianS[2][0] = input_rgs_xz
				hessianS[2][1] = input_rgs_yz
				hessianS[2][2] = input_rgs_zz
				
				hessianR =  input_rgs.matrix_matrix_product_3D( 
					input_rgs.matrix_matrix_product_3D(h_bar_inv,hessianS),h_bar_inv_T)

				#hessianR = h_bar_inv_T*matrix(hessianS) *h_bar_inv
				
				laplacian_of_rgs.field[c1][c2][c3] = hessianR[0][0] + hessianR[1][1] + hessianR[2][2]
