
def create_RGS_from_GAS(cells, sys):
	my_rgs = RGS(cells)
	if sys.box_mode != 'orthorhombic' and sys.box_mode != 'triclinic':
		print "create_RGS_from_GAS: You don't have a box, this won't work right!"
		
	if sys.box_mode == 'orthorhombic':
		sys.update_basis_and_origin_for_orthorhombic()
	
	my_rgs.basis = sys.basis
	my_rgs.origin = sys.origin
	
	return my_rgs
	

class RGS():

	from grid_math import (compute_cell_volume,
		update_number_of_cells,
		compute_cell_center_position,
		compute_minimum_image_vector_between_two_points,
		compute_minimum_image_distance_between_two_points)

	from get import (get_subgrid)

	from kirke.box_math import (vector_vector_add_3D,
		vector_vector_angle_3D,
		vector_vector_subtract_3D,
		matrix_determinant_3D,
		matrix_inverse_3D,
		cross_product_3D,
		dot_product_3D,
		unit_vector_3D,
		matrix_vector_product_3D,
		matrix_matrix_product_3D,
		convert_internal_position_to_external,
		convert_external_position_to_internal,
		internal_wrap)
		
	from kirke.box_math import (update_basis_from_vectors,
		update_basis_from_lengths_and_angles,
		compute_lengths_and_angles_from_basis,
		get_basis_vector,
		compute_box_volume,
		compute_basis_inverse)

	from kirke.box_math import (compute_reciprocal_basis,
		compute_reciprocal_basis_vector)


	def __init__(self,cells, dtype='float64'):
		from numpy import ndarray, zeros
		self.field =  zeros(cells, dtype = dtype)
		self.number_of_cells = 0

		self.update_number_of_cells()
		
		self.origin  = [0.0, 0.0, 0.0]
		self.basis = [[0.0, 0.0, 0.0],[0.0, 0.0, 0.0],[0.0, 0.0, 0.0]]
		self.box_mode = 'triclinic' # this is always true. grids don't do ortho because it makes them so very messy.
		self.zero_centered = True # not sure about this one, but for now it is true. 

		self.comment = ''
		from math import floor # just in case I need it. 
		self.floor = floor
