

def CHGCAR_file(file_name = 'CHGCAR'):
	
	fid = open(file_name,'r')
	line = fid.readline()
	
	scale_factor = float(fid.readline())
	
	sline = fid.readline().split()
	a1 = [float(sline[0]), float(sline[1]), float(sline[2])]
	sline = fid.readline().split()
	a2 = [float(sline[0]), float(sline[1]), float(sline[2])]
	sline = fid.readline().split()
	a3 = [float(sline[0]), float(sline[1]), float(sline[2])]
	#####################

	while len(line.split()) != 0:
		line = fid.readline()
		
	cell_line = fid.readline()
	
	cells = [cell_line.split()[0], cell_line.split()[1], cell_line.split()[2]] 
	cells = [int(cells[0]), int(cells[1]), int(cells[2])]
	
	from kirke.grid import RGS

	chg_rgs = RGS(cells)
	chg_rgs.update_basis_from_vectors(a1,a2,a3) 
	
	chg_rgs.zero_centered = True
	
	
	num = 0
	while num < (chg_rgs.number_of_cells - 1) :
		sline = line.split()
		for thing in sline:
			rho = float(thing)
			i = num  % cells[0]
			j = (num / (cells[0])) % cells[1]
			k = (num / (cells[0]*cells[1])) % cells[2]
			
			chg_rgs.field[i][j][k] = rho # yeah its just the amount of electron per cell
			num += 1
		
		line = fid.readline()
		
	### debug stuff to make sure its reading right	
	#(a, b, c) = chg_rgs.field.shape
	#print chg_rgs.field[0,0,0]  #first item in file
	#print chg_rgs.field[a-1,b-1,c-1] #last item in file
	
	
	cell_volume = chg_rgs.compute_cell_volume()
	chg_rgs.field = chg_rgs.field /(cell_volume * chg_rgs.number_of_cells)
	
	return chg_rgs
