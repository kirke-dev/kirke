

class GCOEFF_file:
	def __init__(self,file_name = 'GCOEFF.txt', write_band_energies = True, try_reading_band_energies = True, band_energies_file_name = 'bands_and_energies.txt'):
		self.file_name = file_name
		############## read vectors
		fid = open(file_name,'r')
		fid.readline()
		fid.readline()
		fid.readline()
		a1 = fid.readline().split()
		a2 = fid.readline().split()
		a3 = fid.readline().split()
		line_num = 5
		#a1[1] = '2.0'
		self.basis = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
		self.origin = [0.0,0.0,0.0]
		for j in range(3):
			self.basis[j][0] = float(a1[j])
			self.basis[j][1] = float(a2[j])
			self.basis[j][2] = float(a3[j])

		from numpy import matrix, linalg, array
		self.box_volume = linalg.det(matrix(self.basis))
		print 'Box Volume', self.box_volume
		################################ now we populate lookups
		from operator import itemgetter
		
		
		import os.path
		 
		if try_reading_band_energies and os.path.isfile(band_energies_file_name):
			
			fid.close() # close the GCOEFF file
			fid2 = open(band_energies_file_name,'r')
			
			self.sorted_band_list = []
			
			for line in fid2:
				sline = line.split()
				if len(sline)!=3:
					self.sorted_band_list.append( ( float(sline[0]), float(sline[1]), int(sline[2]), [float(sline[3]), float(sline[4]), float(sline[5])], int(sline[6]), int(sline[7]) ) )
				else:
					self.cells = ( int(sline[0]), int(sline[1]), int(sline[2]))

			self.number_of_bands = len(self.sorted_band_list)
			
			############ sorting by index just for populating the lists
			self.sorted_band_list = sorted(self.sorted_band_list, key = itemgetter(2))

			self.energy_list = []
			self.occupancy_list = []
			self.kpoint_list = []
			self.start_pos_list = []
			self.end_pos_list = []
			warn = False
			for i in range(self.number_of_bands):
				self.energy_list.append(self.sorted_band_list[i][0])
				self.occupancy_list.append(self.sorted_band_list[i][1])
				if i!= self.sorted_band_list[i][2]:
					warn = True
				self.kpoint_list.append(self.sorted_band_list[i][3])
				
				self.start_pos_list.append(self.sorted_band_list[i][4])
				self.end_pos_list.append(self.sorted_band_list[i][5])
			if warn:
				print "GCOEFF_file: Warning! On reading the bands and energies, the indices did not match the file perfectly."
			# now sorting for the real sorted energies			
			self.sorted_band_list = sorted(self.sorted_band_list, key = itemgetter(0))
			
		else:  #####################
		
		
			pos = 0  # IMMPPPORRTAANNT this it he file position of the line that was just read, fid.tell() is one line ahead.
			# this is simpler than setting up a queue
			pos_1 = 0 # minus 1 #previous position in file
			pos_2 = 0 # minus 2 # the previous previous position in file
			pos_3 = 0 # minus 3 # the previousx3 position in file
		
			for i in range(3):
				line_num+=1
				line = fid.readline() #skip the header
				#do stuff here
				#then update the pos
				pos_3 = pos_2
				pos_2 = pos_1
				pos_1 = pos
				pos   = fid.tell()
				#print line_num, line

			self.start_line_list = []
			self.end_line_list = []

			self.start_pos_list = []
			self.end_pos_list = []

			self.energy_list = []
			self.occupancy_list = []
			self.kpoint_list = []
		
			self.gmax = [0,0,0]
			
			while line != '':
			#for line in fid:#dont do this 
			

				if line[0] == '(':
					self.energy_list.append( float(line.split()[1]))
					self.occupancy_list.append(float(line.split()[5]))
					self.start_line_list.append(line_num+1)
					self.start_pos_list.append(fid.tell()) #remember .tell is ahead by one line
					#
					if len(self.start_line_list)>1:# once the start line list has a second value, we can start adding end line values
						if current_kpoint ==  self.kpoint_list[-1]: # checks to see if it is still on the same kpoint
							self.end_line_list.append(line_num-2)
							self.end_pos_list.append(pos_2)
						else:
							self.end_line_list.append(line_num-3)
							self.end_pos_list.append(pos_3)

					self.kpoint_list.append(current_kpoint)
					nbands = len(self.energy_list)
					if nbands % 10 == 0: # progress vebosity
						print nbands, 'Bands Found, Current Energy', self.energy_list[nbands-1],
						print 'and Occupancy',  self.occupancy_list[nbands-1]
					
				elif '(' not in line:
					sline = line.split()
					if len(sline) == 3 and line_num > 8:#make sure the get past the header
						current_kpoint =[ float(sline[0]), float(sline[1]),float(sline[2]) ]
						print 'Found new K-point', current_kpoint

				elif len(self.energy_list)<2:  # no need to keep checking past the first band i think
					sline = line.split()
					if len(sline) == 8:
						xint = int(sline[0])
						yint = int(sline[1])
						zint = int(sline[2])
						if xint > self.gmax[0]: 
							self.gmax[0] = xint
						if yint > self.gmax[1]: 
							self.gmax[1] = yint
						if zint > self.gmax[2]:
							self.gmax[2] = zint
							#print gmax
				pos_3 = pos_2 # too small for needing a queue
				pos_2 = pos_1
				pos_1 = pos
				pos   = fid.tell()
				line = fid.readline()
				line_num += 1
		
			self.end_line_list.append(line_num-1) # otherwise these dont get filled out
			self.end_pos_list.append(pos_1)
			
			self.cells = tuple(2*array(self.gmax)+1)
			self.number_of_bands = len(self.energy_list)
			print 'Total Number of Bands', self.number_of_bands
			fid.close()
			######
		
		
			########### now I make a look up table

			self.sorted_band_list = []
			for i in range(self.number_of_bands):	#0			#1		  #2	#3 [0][1][2]		#4			#5
				self.sorted_band_list.append((self.energy_list[i], self.occupancy_list[i], i, self.kpoint_list[i], self.start_pos_list[i], self.end_pos_list[i]))
		
			if write_band_energies: #write it before sorting and after
				self.write_band_energies(file_name = 'bands_and_energies.txt')
		
		
			self.sorted_band_list = sorted(self.sorted_band_list, key = itemgetter(0))

			if write_band_energies:  #write it before sorting and after
				self.write_band_energies(file_name = 'sorted_bands_and_energies.txt')
		
			print 'GCOEFF_file: Remember, GCOEFF files do not contain k-point multiplicites, you will have to check the IBZKPT file. The kpoints will be in the same order thankfully.'
		########### box details
		
		#number_of_cells = self.cells[0]*self.cells[1]*self.cells[2] # i think these can be deprecated
		#self.cell_volume = self.box_volume/number_of_cells # i think these can be deprecated



###################
	def write_band_energies(self, file_name = 'bands_and_energies.txt'):
		fid=open(file_name , 'w')
		fid.write('%i\t%i\t%i\n' % self.cells)
		for thing in self.sorted_band_list:
			fid.write('%.10f\t%.10f\t%i\t' % (thing[0], thing[1], thing[2]))
			fid.write('%.10f\t%.10f\t%.10f\t' % (thing[3][0], thing[3][1], thing[3][2]))
			fid.write('%i\t%i\n' % (thing[4], thing[5]))
			
		fid.close()

########################## We need a get_kspace_band at some point
	def get_kspace_band(self,band_number, oversample = 1.0, desired_cells = (), verbose = False):
		
		debug = False
		
		###########################
		if oversample > 1.0:
			if verbose:
				print 'GCOEFF_file: Warning you have selected to oversample. Your realspace cell'
				print 'GCOEFF_file: volumes will change. Double check the accordingly.'
			OS = oversample #makes the next line easier to read
			local_cells = ( int(self.cells[0]*OS), int(self.cells[1]*OS), int(self.cells[2]*OS) )
			
		elif oversample < 1.0:
			print 'GCOEFF_file: Warning you have selected to undersample. This function will not comply.'
			print 'GCOEFF_file: You will have to modify the code to do this, this message will lead you to the relevent section.'
			local_cells = (self.cells[0], self.cells[1], self.cells[2])
		else:
			local_cells = (self.cells[0], self.cells[1], self.cells[2])
		##################	
		if len(desired_cells) == 3:
			if verbose:
				print 'GCOEFF_file: You have selected to set your k-space limits by hand.'	
				print "GCOEFF_file: If you don't understand under and oversampling,  don't do this"
			local_cells =( int(desired_cells[0]), int(desired_cells[1]), int(desired_cells[2]) )
		#else:
		#	print 'GCOEFF_file: Your desired_cells is incorrect.'
		
		number_of_cells = local_cells[0]*local_cells[1]*local_cells[2]
		if verbose:
			print 'Number of cells', number_of_cells,', in this shape (x, y, z)', local_cells, '[in file',self.cells ,']'
			print 'Cell Volume', self.cell_volume
		
		from numpy import sqrt, zeros
		from kirke.grid import RGS
		my_rgs = RGS(tuple(local_cells), dtype = complex)
		#my_rgs.field = zeros(local_cells, dtype = complex)

		start_pos =  self.start_pos_list[band_number] 
		end_pos   =  self.end_pos_list[band_number] 
		
		if debug:
			start = self.start_line_list[band_number] 
			end = self.end_line_list[band_number]  

		#print start, end
		fid = open(self.file_name,'r')
		#################
		if verbose: 
			print 'Seeking...'
			
		fid.seek(start_pos)

		if verbose: 
			print 'Parsing...'
		if debug:
			first = fid.readline()
			fid.seek(-len(first),1)
			print start, first
		while fid.tell() <= end_pos:  # this reads it
			#line = getline(self.file_name, line_num)
			line = fid.readline()
			sline = line.split()
		 
			cell = ( int(sline[0]), int(sline[1]), int(sline[2]))

			a = float(sline[4])
			b = float(sline[6])
			z = a + b*1j
			#print cell, z
			# kspace Discrete FT are periodic so i am wrapping this one up.
			my_rgs.field[ cell[0] % local_cells[0] ][ cell[1] % local_cells[1]  ][ cell[2] % local_cells[2]  ] = z 
				#break
		
			#line_num += 1 # next line
		if debug: print end, line
		fid.close()
		
		
		coeff = number_of_cells*sqrt(1.0/self.box_volume)
		my_rgs.field = my_rgs.field * coeff
		my_rgs.origin = self.origin
		my_rgs.basis = self.basis
		
		return my_rgs
		
		
##########################		
	def get_real_space_band(self,band_number, oversample = 1.0, desired_cells = (), verbose = False):

		
		kspace = self.get_kspace_band(band_number = band_number, oversample = oversample, desired_cells = desired_cells, verbose = verbose)
		from numpy import fft, zeros
		from kirke.grid import RGS
		realspace = RGS(kspace.field.shape, dtype = complex)
		#realspace.field = zeros(kspace.field.shape, dtype = complex)
		
		realspace.field = fft.ifftn(kspace.field) 
		realspace.origin = self.origin
		realspace.basis = self.basis
		
		return realspace


	def get_real_space_band_density(self,band_number, oversample = 1.0, desired_cells = (), verbose = False):
		from numpy import conjugate
		realspace = self.get_real_space_band(band_number, oversample = oversample, desired_cells = desired_cells, verbose = verbose)

		realspace.field = (realspace.field * conjugate(realspace.field)).real # in units of electrons per ang^3
		ncells = realspace.field.size
		if verbose:
			number_of_electrons =  realspace.field.sum()*(realspace.compute_cell_volume()/ncells)
			print "Number of Electrons in Band Number %i:"%band_number , number_of_electrons 

		return realspace

	def get_HOMO_index(self):
		index = 0
		oc = self.sorted_band_list[index][1]
		while oc > 0.5:
			index += 1
			oc = self.sorted_band_list[index][1]
		index -= 1
		return index

	def get_LUMO_index(self):
		index = 0
		oc = self.sorted_band_list[index][1]
		while oc > 0.5:
			index += 1
			oc = self.sorted_band_list[index][1]
		return index
