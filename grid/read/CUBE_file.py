def CUBE_file(file_name, square_values = False, convert_bohr_to_angstroms = True, verbose = False):
	'''Reads in a cube file as an RGS, if you want the atoms, use the cube file reader in the regular part of kirke'''

	fid = open(file_name,'r')
	from numpy import zeros,array, sqrt, conjugate
	
	for i in range(2):# skipping stuff
		fid.readline()
	if convert_bohr_to_angstroms: # normally we are unit agnostic, but guassian takes in Ang and spits out Bohr and thats just not cool
		bohr = 0.52917721092 # 2010 CODATA value
	else:
		bohr = 1.0
	# number of atoms and origin
	natoms_origin = fid.readline().split()
	#natoms = abs(int(natoms_origin[0]))
	origin = [float(natoms_origin[1])*bohr  ,float(natoms_origin[2])*bohr, float(natoms_origin[3])*bohr]

	cells = [0,0,0]
	basis = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]

	for j in range(3):
		sline = fid.readline().split()
		cells[j] = int(sline[0])
		for i in range(3):
			basis[i][j] = float(sline[i+1]) * bohr * cells[j]
	if verbose:
		print 'Cells : ', cells
		print 'Basis : ', basis

	
	
	def is_int(a):
		try:
			int(a)
			return True
		except ValueError:
			return False
		


	#Updated by Avi 8/10/15:Save Atom info in case you want to write a new cube file
	#Atom_Info = []	
	sline = fid.readline().split() # if i do this first, it saves an if statement
	while is_int(sline[0]) and (is_int(sline[1])==False): # this part is atoms and stuff
		sline = fid.readline().split()
	#	Atom_Info.append(sline)
	#print Atom_Info

	
	while is_int(sline[0]) and is_int(sline[1]): # skips past read info
		sline = fid.readline().split()
		#print sline
	
	from kirke.grid import RGS
	psi = RGS(tuple(cells))
	psi.origin = origin
	psi.basis = basis
	psi.zero_centered = False

	ncells = cells[0]*cells[1]*cells[2]
	cn = 0
	#now we read the cells
	

	while len(sline)!=0:
		for val in sline: # already contains cell info
			xindex = (cn/(cells[1]*cells[2]))%cells[0] #integer math, the last part is iffy? do we want it looping back if it should have reached end of file?
			yindex = (cn/cells[2])%cells[1] #integer math
			zindex = (cn)%cells[2] 
			#print cn, xindex,yindex,zindex
			psi.field[xindex][yindex][zindex] = float(val)
			cn += 1
		sline = fid.readline().split()
	fid.close()

	volume_factor =sqrt(1.0/(bohr**3 ))
	psi.field = psi.field * volume_factor
	
	if ncells!= cn:
		print 'There was a parsing error, the number of cells does not match the number read in.'
	
	
	if square_values:
		psi.field = (conjugate(psi.field)*psi.field).real 
	#Updated by Avi 8/10/15: Will return Atom_Info if you wat to Save it
	#if atom_info:
	#	return (psi,Atom_Info)
	#else:
	return psi


