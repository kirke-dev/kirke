def XSF_DATAGRID_file(file_name):

	def int_line(string):
		int_list = []
		for thing in string.split():
			int_list.append(int(thing))
		return int_list

	def float_line(string):
		float_list = []
		for thing in string.split():
			float_list.append(float(thing))
		return float_list

	from numpy import zeros
	fid = open(file_name,'r')


	#finding the first line
	line = fid.readline()
	while 'BEGIN_DATAGRID_3D' not in line:
		line = fid.readline()

	#now for the data header
	cells = tuple(int_line(fid.readline()))

	origin = float_line( fid.readline())
	a1 = float_line( fid.readline())
	a2 = float_line( fid.readline())
	a3 = float_line( fid.readline())
	basis = [[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]]
	for j in range(3):
		basis[j][0] = float(a1[j])
		basis[j][1] = float(a2[j])
		basis[j][2] = float(a3[j])

	from kirke.grid import RGS
	my_grid = RGS(cells)  
	my_grid.zero_centered = True # XSF files are!
	my_grid.basis = basis
	my_grid.origin = origin

	for k in range(cells[2]):
		for j in range(cells[1]):
			flist = float_line(fid.readline())
			for i in range(cells[0]):
				my_grid.field[i][j][k]= flist[i]
		blank_line = fid.readline()
	
	if 'END_DATAGRID_3D' not in blank_line: #the last line of the data block
		print 'XSF_DATAGRID_file: Warning parsing the DATAGRID may have failed!'
	return my_grid
