
def write_XSF_DATAGRID(my_rgs, file_name):
	'''This writes scalar fields to XSF files. These can be appended to atomistic XSF files for great isosurfaces in XCrySDen.'''
	# Later we might add block number
	# block number will be useful for multiple fields in same block in the XSF file. For now just leave it as 1''' 
	cells = my_rgs.field.shape

	if my_rgs.zero_centered == False:
		print "write_XSF_DATAGRID: Warning, you have a non-zero centered data grid. The XSF format doesn't allow for this, but will show you everything shifted by half a cell."

	fid = open(file_name,'w')
	
	fid.write('\nBEGIN_BLOCK_DATAGRID_3D\n')
	if my_rgs.comment == '':
		fid.write('  %s\n' % 'No Comment'  )
	else:
		fid.write('  %s\n' % my_rgs.comment  )
	fid.write('  BEGIN_DATAGRID_3D_this_is_3Dgrid#1\n')
	fid.write('    %i %i %i\n'% tuple(cells))
	fid.write('    %f %f %f\n' % tuple(my_rgs.origin) )
	fid.write('    %f %f %f\n' % (my_rgs.basis[0][0],  my_rgs.basis[1][0], my_rgs.basis[2][0])   )
	fid.write('    %f %f %f\n' % (my_rgs.basis[0][1],  my_rgs.basis[1][1], my_rgs.basis[2][1])   )
	fid.write('    %f %f %f\n' % (my_rgs.basis[0][2],  my_rgs.basis[1][2], my_rgs.basis[2][2])   )
	
	for k in range(cells[2]):
		for j in range(cells[1]):
			fid.write('    ')
			for i in range(cells[0]):
				fid.write('  %.10f'% my_rgs.field[i][j][k] )
			fid.write('\n')
		if k < cells[2]-1:
			fid.write('\n')

	fid.write('  END_DATAGRID_3D\n')
	fid.write('END_BLOCK_DATAGRID_3D\n')
	fid.close()
