from kirke import *
from kirke.grid.read import CUBE_file
from numpy import sqrt

def Write_CUBE(my_rgs, Atom_Info,file_name):
	cube_list = []
	bohr = 0.52917721092
	foobar = open(file_name,'w')
	## write the header
	foobar.write("Writing a Manual Cube File\n")
	foobar.write("Add Title describing what this is\n")

	foobar.write('  -22')
	i = 0
	while i < len(my_rgs.origin):
		my_rgs.origin[i] /= bohr
		i += 1
	foobar.write("  %s   %s   %s\n" %(my_rgs.origin[0],my_rgs.origin[1],my_rgs.origin[2])) 

	lengths = [len(my_rgs.field), len(my_rgs.field[0]),len(my_rgs.field[0][0])]
	i = 0
	while i < len(my_rgs.basis[0]):
		my_rgs.basis[0][i] /= (lengths[0]*bohr)
		i += 1
	i = 0
	while i < len(my_rgs.basis[1]):
                my_rgs.basis[1][i] /= (lengths[1]*bohr)
		i += 1
	i = 0
	while i < len(my_rgs.basis[2]):
                my_rgs.basis[2][i] /= (lengths[2]*bohr)
		i +=1
	basis = my_rgs.basis
	foobar.write("  %s    %0.6f    %0.6f    %0.6f\n" %(lengths[0],basis[0][0],basis[0][1],basis[0][2]))
	foobar.write("   %s    %0.6f    %0.6f    %0.6f\n" %(lengths[1],basis[1][0],basis[1][1],basis[1][2]))
	foobar.write("   %s    %0.6f    %0.6f    %0.6f\n" %(lengths[2],basis[2][0],basis[2][1],basis[2][2]))
	for line in Atom_Info[:-1]:
		if len(line[0]) == 1:
			foobar.write("    %s    %s" %(line[0],line[1]))
		else:
			foobar.write("   %s   %s" %(line[0],line[1]))
		j = 2
		while j <= 4:
			if line[j][0] == '-' and len(line[j]) == 10:
                                foobar.write("  %s" %(line[j]))
				
			elif len(line[j]) == 9 and line[j][0] != '-':
				foobar.write("   %s" %(line[j]))
			
			elif line[j][0] == '-':
				foobar.write("   %s" %(line[j]))


			else:
				foobar.write("    %s" %(line[j]))
			j +=1
		foobar.write("\n")
			
		
		
	foobar.write("    %s    %s\n" %(Atom_Info[-1][0], Atom_Info[-1][1]))
	
	volume_factor =  1 / sqrt(1.0/(0.52917721092**3))	
	my_rgs.field = my_rgs.field * volume_factor
	cells = my_rgs.field.shape
	i = 0
	count = 0
	while i < len(my_rgs.field):
		j = 0
		while j < len(my_rgs.field[i]):
			k = 0
			while k < len(my_rgs.field[i][j]):
				count += 1
				if str(my_rgs.field[i][j][k])[0] == "-":
					foobar.write(' %1.5E' %(my_rgs.field[i][j][k]))
				else:
					foobar.write('  %1.5E' %(my_rgs.field[i][j][k]))
				if count == 6:
					count = 0
					foobar.write("\n")						 	
			
				k += 1
			j += 1
		i += 1

	foobar.close()


my_rgs = CUBE_file("T1.cube")
Write_CUBE(my_rgs[0],my_rgs[1], "test.cube")
