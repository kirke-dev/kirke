def write_VTK(my_rgs, file_name, field_name = 'Cell_data'):
	'''This writes a vts binary file from an RGS. This function relies upon PyEVTK being installed. Warning, Not tested yet.'''	
	from numpy import arange, zeros
	from evtk.hl import gridToVTK 
	# Dimensions 

	

	nx, ny, nz = my_rgs.field.shape[0], my_rgs.field.shape[1], my_rgs.field.shape[2]
	basis = my_rgs.basis
	
	dx, dy, dz = 1.0/nx, 1.0/ny, 1.0/nz # in internal coordinates space

	sx = arange(0, 1.0 + 0.1*dx, dx, dtype='float64') 
	sy = arange(0, 1.0 + 0.1*dy, dy, dtype='float64') 
	sz = arange(0, 1.0 + 0.1*dz, dz, dtype='float64') 

	X = zeros((nx + 1, ny + 1, nz + 1)) 
	Y = zeros((nx + 1, ny + 1, nz + 1)) 
	Z = zeros((nx + 1, ny + 1, nz + 1)) 
	for k in range(nz + 1):
		for j in range(ny + 1):
			for i in range(nx + 1): # Ri=HijSi #this should be double checked agaist accidental transposition
				X[i,j,k] = basis[0][0]*sx[i] + basis[0][1]*sy[j] + basis[0][2]*sz[k]
				Y[i,j,k] = basis[1][0]*sx[i] + basis[1][1]*sy[j] + basis[1][2]*sz[k]
				Z[i,j,k] = basis[2][0]*sx[i] + basis[2][1]*sy[j] + basis[2][2]*sz[k]
	# Variables 
	gridToVTK(file_name, X, Y, Z, cellData = {field_name : my_rgs.field})
