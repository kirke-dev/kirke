
def write_POV_isosurface_by_wave_reconstruction(my_rgs, cut_off, file_name = 'isosurface.pov', number_of_kpoints = 500, max_gradient = 1.1, accuracy = 0.01, fill_greater_than_cut_off = True , texture = [], verbose = True, mask = [[0.0, 1.0], [0.0, 1.0], [0.0, 1.0]] ):
	''' Keep the kpoints under about 40K idk what the exact limit is.  max_gradient should be about 30 times the cut_off value, if there are clipping errors increase this value. Lowering max_gradient is a good idea since it significantly slows render speed. The mask can be used to only evaluate the isosurface in a selected volume, if you surface is small compared to the total box size, this saves signifcant amounts of time. '''

	##########
	from numpy.fft import  fftn
	from numpy import floor, pi, sqrt, arctan2, absolute, angle, dot,array

	
	from kirke.grid import RGS
	kspace_rgs = RGS(my_rgs.field.shape,dtype=complex)
	if fill_greater_than_cut_off:
		kspace_rgs.field = fftn(-my_rgs.field) # the negative sign is because povray does other side of the iso surface, the less dense side
	else:
		kspace_rgs.field = fftn(my_rgs.field) 
	

	if my_rgs.zero_centered == False:
		delta = 1.0/array(my_rgs.field.shape)


	#here we created a prioritized cell list by sorting by kpoint strength
	from kirke.grid.analyze import compute_cutoff_for_abs_value_percentile, get_cells_above_cutoff
	percent = (number_of_kpoints*100.0)/kspace_rgs.field.size
	abs_kspace_cut_off = compute_cutoff_for_abs_value_percentile(kspace_rgs, percent = percent) # we are sloppy here since the inversion symmetry operation means the exact amount used is not predictable anyway. I could make this more exact later anyway. Also the sign doesn't matter till later..
	
	kcell_list = get_cells_above_cutoff(kspace_rgs,abs_kspace_cut_off)
#	print kspace_rgs.field.size, len(kcell_list)
#	

#	
#	print kspace_rgs.field[-1,1,1], kspace_rgs.field[0,1,1], kspace_rgs.field[1,1,1], kspace_rgs.field[2,1,1]
#	print kspace_rgs.field[-1,0,1], kspace_rgs.field[0,0,1], kspace_rgs.field[1,0,1], kspace_rgs.field[2,0,1]
#	print kspace_rgs.field[-1,-1,1], kspace_rgs.field[0,-1,1], kspace_rgs.field[1,-1,1], kspace_rgs.field[2,-1,1]
#	print ' '
#	print kspace_rgs.field[-1,1,0], kspace_rgs.field[0,1,0], kspace_rgs.field[1,1,0], kspace_rgs.field[2,1,0]
#	print kspace_rgs.field[-1,0,0], kspace_rgs.field[0,0,0], kspace_rgs.field[1,0,0], kspace_rgs.field[2,0,0]
#	print kspace_rgs.field[-1,-1,0], kspace_rgs.field[0,-1,0], kspace_rgs.field[1,-1,0], kspace_rgs.field[2,-1,0]
#	print ' '
#	print kspace_rgs.field[-1,1,-1], kspace_rgs.field[0,1,-1], kspace_rgs.field[1,1,-1], kspace_rgs.field[2,1,-1]
#	print kspace_rgs.field[-1,0,-1], kspace_rgs.field[0,0,-1], kspace_rgs.field[1,0,-1], kspace_rgs.field[2,0,-1]
#	print kspace_rgs.field[-1,-1,-1], kspace_rgs.field[0,-1,-1], kspace_rgs.field[1,-1,-1], kspace_rgs.field[2,-1,-1]
#	print ' '
#	print kspace_rgs.field[-6,-2,-1], kspace_rgs.field[6,2,1]
#	print kspace_rgs.field[-3,-4,-5], kspace_rgs.field[3,4,5]




	######## Isosurface stuff ###################
	fid2 = open(file_name,'w')
	fid2.writelines(['\nisosurface {',
			'\n\tfunction { 0 '])	

	
	counter = 0
	for kcell in kcell_list:
		
		kshifted = [0.0, 0.0, 0.0]
		for i in range(3):
			if kcell[i] > (kspace_rgs.field.shape[i]/2): # the greater than sign means that on even cells the last on doesnt get wrapped 
				kshifted[i] = kcell[i]-kspace_rgs.field.shape[i] 
			else:
				kshifted[i] =  kcell[i] 




		kpoint = (kspace_rgs.field[ kcell[0], kcell[1], kcell[2] ]) / kspace_rgs.field.size
		
		amplitude = absolute(kpoint)
		phase_shift = angle(kpoint)
		#print kpoint, amplitude, phase_shift

		if kshifted[0] >= 0: # using the hermition symmetry to reduce the number of plane waves, this trick only works for real valued fields 
			if kshifted[0] > 0: amplitude = 2.0*amplitude
				
			# i really don't know why the phase shift is positive here. doesn't make sense with the math, but it works in practice.
			if my_rgs.zero_centered == False: phase_shift = -pi*dot(kshifted,delta) + phase_shift 
			
			fid2.write( '+ %e * cos((%i*x + %i*y + %i*z)*%e + %e)' %(amplitude, kshifted[0], kshifted[1], kshifted[2] , 2.0*pi, phase_shift ) )
			counter+=1
				



	
	fid2.write('\n\t}') #closes functions definition
	if verbose: print 'Wrote %i waves.'%counter
	
	################### terminating properties, how the isosurface is bounded
	v1 = tuple(my_rgs.get_basis_vector(0)) 
	v2 = tuple(my_rgs.get_basis_vector(1))
	v3 = tuple(my_rgs.get_basis_vector(2))
	
	if fill_greater_than_cut_off:#again because povray only lets you go one way..
		threshold = -cut_off
	else:
		threshold = cut_off
	terminating = [ '\n\tthreshold %f' %  threshold,
		'\n\tmax_gradient %f' % max_gradient,  # raise this number if there are clipping problems
		'\n\taccuracy %f'% accuracy,
		'\n\tcontained_by { box { <%f,%f,%f> , <%f,%f,%f> }}'%(mask[0][0],mask[1][0], mask[2][0],
								 mask[0][1],mask[1][1], mask[2][1]),
		'\n\tmatrix < %f, %f, %f,' % v1,
		'\n\t\t%f, %f, %f,' % v2,
		'\n\t\t%f, %f, %f,' % v3,
		'\n\t\t%f, %f, %f>' % tuple(my_rgs.origin),
		#'\n\t}'
		]
		
	fid2.writelines(terminating)	
	

	######texture definitions
	#texture=['\n\tpigment {  rgb <0.25, 0.25, 0.80> transmit 0.66 }'], # for transperency
	if texture == []:
		texture=['\n\tpigment {  rgb <0.25, 0.25, 0.80> }'] # for solid surface
		

	fid2.writelines(texture)

	#Heres a crazy texture example that looks like purple pink jelly
	texture=['\n\ttexture{ ',
		'\n\t\tpigment {  rgbft  <0.63, 0.05, 0.7, 0.1, 0.60> }',
		'\n\t\tfinish{ diffuse 0.85 ambient 0.99 brilliance 3 specular 0.5 roughness 0.001',
		'\n\t\treflection { .05,.98 fresnel on exponent 1.5 }',
		'\n\t\tconserve_energy}',
		'\n\t}',
		'\n\t\tinterior { ior 1.3 }',
		'\n\tphotons {',
		'\n\t\ttarget',
		'\n\t\trefraction on',
		'\n\t\treflection on',
		'\n\t\tcollect on',
		'\n\t}']



	############
	fid2.write('\n}\n') # closes the isosurface object
	fid2.close()

	return counter

