
def update_number_of_cells(self):
	self.number_of_cells = self.field.shape[0]*self.field.shape[1]*self.field.shape[2]

def compute_cell_volume(self):
	return self.compute_box_volume()/self.number_of_cells	

def compute_cell_center_position(self,cell):
	from numpy import array
	s1 = cell[0]/float(self.field.shape[0])
	s2 = cell[1]/float(self.field.shape[1])
	s3 = cell[2]/float(self.field.shape[2])
	if self.zero_centered == False:
		s1 += 1.0/(2.0*float(self.field.shape[0]))
		s2 += 1.0/(2.0*float(self.field.shape[1]))
		s3 += 1.0/(2.0*float(self.field.shape[2]))
	return self.convert_internal_position_to_external([s1,s2,s3])

def compute_minimum_image_vector_between_two_points(self,point1,point2): # mike 16-8-2012
	'''Vector pointing from particle 1 to particle 2'''

	if self.basis == []:
		print 'RGS.compute_minimum_image_vector_between_two_points: ERROR This needs a basis to function. Go fix that.'

	[sx1, sy1, sz1] = self.convert_external_position_to_internal(point1)
	[sx2, sy2, sz2] = self.convert_external_position_to_internal(point2)

	ds1 = sx2 - sx1
	ds2 = sy2 - sy1
	ds3 = sz2 - sz1
	from math import copysign
	if abs(ds1) > 0.5:
		ds1 = 1.0 - (ds1 * copysign(1.0, ds1))
	if abs(ds2) > 0.5:
		ds2 = 1.0 - (ds2 * copysign(1.0, ds2))
	if abs(ds3) > 0.5:
		ds3 = 1.0 - (ds3 * copysign(1.0, ds3))		
	ds = [ds1, ds2, ds3]
	
	dr = self.matrix_vector_product_3D(self.basis, ds)

	return dr

def compute_minimum_image_distance_between_two_points(self,point1,point2): 
	from math import sqrt
	dr = self.compute_minimum_image_vector_between_two_points(point1,point2)
	absdr = sqrt(self.dot_product_3D(dr,dr))
	return absdr
