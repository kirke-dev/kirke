# changing this line to see whats up with git
#Overhaul to default to calling atomic_number instead of element
#Modifying mass to be an atomistic attribute, adding new object, type_masses in order to handle the way LAMMPS data files work
######################## GAS class (The unifying thing for this software) ####################
class GAS:
	def __init__(self):

		self.file_name = ''
		self.x = []
		self.y = []
		self.z = []
		self.q = []
		self.element = []
		self.atomic_number = []
		self.masses = []
		self.number_of_atoms = 0
		self.box = [[],[],[]]
		self.lx = 0.0
		self.ly = 0.0
		self.lz = 0.0
		self.id = []
		self.comment = ''
		self.neighbor_list = []
		self.periodic = [False, False, False] # xyz periodicities
		self.vx = []
		self.vy = []
		self.vz = []
		self.mass = 0.0 # overall system mass, not often used, but handy
		# these are because of lammps
		self.molecule = []
		self.nx = []
		self.ny = []
		self.nz = []
		self.atom_style =[]
		self.id_to_index = []

		self.bonds = []
		self.angles = []
		self.dihedrals = []
		self.impropers = []

		self.pair_coeffs = []
		self.bond_coeffs = []
		self.angle_coeffs = []
		self.dihedral_coeffs = []
		self.improper_coeffs = []
		self.type_masses = []

###Katie 7/23/12: COMPASS additions
		self.bondbond_coeffs = []
		self.bondangle_coeffs = []
		self.middlebondtorsion_coeffs = []
		self.endbondtorsion_coeffs = []
		self.angletorsion_coeffs = []
		self.angleangletorsion_coeffs = []
		self.endbondtorsion_coeffs = []
		self.bondbond13_coeffs = []
		self.angleangle_coeffs = []

		# stuff for triclinics
		self.box_mode = '' # orthorhombic,  triclinic
		self.origin = [] #this used to be called box_origin
		self.basis = [] # this is h in the R-P formulation
		self.sx = []
		self.sy = []
		self.sz = []
		from math import floor
		self.floor = floor

		#acceleration in x y and z
		#Stuff for tape2 sys support# 
		self.ax = []
		self.ay = []
		self.az = []
		self.tail = [] #Saves tail data so you can write to file later
		
		# this allows you to specify which atoms are allowed to move in which directions	
		self.selective_dynamics = [] 

		#####New from Katie#####
		self.bonds_id = []
		self.angles_id = []
		self.dihedrals_id = [] 
		self.impropers_id = []
		self.number_of_bonds = 0
		self.number_of_angles = 0
		self.number_of_dihedrals = 0
		self.number_of_impropers = 0
		self.number_of_molecules = 0

	from add import (add_atoms_sys,
		add_atoms_simple,
		add_bonds,
		add_angles,
		add_dihedrals,
		add_impropers)

	from compute import (compute_velocities_from_previous,
		compute_mass,
		compute_displacements_from_previous,
		compute_system_velocity,
		compute_density,
		compute_minimum_image_vector_between_two_particles,
		compute_minimum_image_distance_between_two_particles,
		compute_minimum_image_angle,
		compute_minimum_image_dihedral)

	from fill_lists import (fill_q_list,
		fill_molecule_list,
		fill_v_lists,
		fill_s_lists,
		fill_position_lists,
		fill_n_lists,
		fill_masses_list,
		fill_id_list,
		fill_selective_dynamics_list)

	from get import (get_subset,
		get_cut,
		get_molecule,
		get_molecule_bonds,
		get_subset_bonds,
		get_subset_angles,
		get_subset_dihedrals,
		get_slice,
		get_atom_typeid,
		get_atom_typeid_fast,
		get_bonds_of_type,
		get_angles_of_type,
		get_dihedrals_of_type,
		get_impropers_of_type,
		get_bonds_typeid,
		get_angles_typeid,
		get_dihedrals_typeid,
		get_impropers_typeid)

	from sort_search import (sort_by,
		get_index_with_binary_search_from_id,
		build_index_dict)

	from subtract import (subtract_atoms,
		subtract_bonds,
		subtract_bonds_fast,
		subtract_angles,
		subtract_angles_fast,
		subtract_dihedrals,
		subtract_dihedrals_fast,
		subtract_impropers,
		subtract_impropers_fast,
		subtract_molecules)

	from update_number import (update_number_of_atoms,
		update_number_of_bonds,
		update_number_of_angles,
		update_number_of_dihedrals,
		update_number_of_impropers,
		update_number_of_molecules)

	from update_other import (update_element_list,
		update_atomic_number_list,
		update_masses_from_element,
		update_type_masses,
		convert_number_to_atomic_number,
		convert_number_to_element,
		convert_element_to_number,
		update_box_from_positions,
		update_lengths_from_box,
		rewrap_internal, 
		update_external_from_internal,
		update_internal_from_external,
		update_basis_and_origin_for_orthorhombic, 
		update_box_mode_to_triclinic_from_orthorhombic ) 


#### unified box math for the RGS and GAS
	from box_math import (vector_vector_add_3D,
		vector_vector_angle_3D,
		vector_vector_subtract_3D,
		matrix_determinant_3D,
		matrix_inverse_3D,
		cross_product_3D,
		dot_product_3D,
		unit_vector_3D,
		vector_projection_3D,
		vector_rejection_3D,
		matrix_vector_product_3D,
		matrix_matrix_product_3D,
		convert_internal_position_to_external,
		convert_external_position_to_internal,
		internal_wrap)
		
	from box_math import (update_basis_from_vectors,
		update_basis_from_lengths_and_angles,
		compute_lengths_and_angles_from_basis,
		get_basis_vector,
		compute_box_volume,
		compute_maximum_radius_sphere_in_box,
		compute_basis_inverse,
		realign_basis)

	from box_math import (compute_reciprocal_basis,
		compute_reciprocal_basis_vector)
