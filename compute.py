#!/usr/bin/env python


def compute_displacements_from_previous(self,previous):
	''' dr = dbasis*s + basis*ds + dorigin '''
	if previous.number_of_atoms == self.number_of_atoms:
		x_list = []
		y_list = []
		z_list = []
		
		
		if self.box_mode == 'orthorhombic' or self.box_mode == 'triclinic':
			if self.box_mode == 'orthorhombic':
				self.update_basis_and_origin_for_orthorhombic()
				previous.update_basis_and_origin_for_orthorhombic()
			dbasis = [[0.0, 0.0, 0.0],[0.0, 0.0, 0.0],[0.0, 0.0, 0.0]]
			dorigin = [0.0, 0.0, 0.0]
			for i in range(3):
				dorigin[i] = self.origin[i] - previous.origin[i]
				for j in range(3):
					dbasis[i][j] = self.basis[i][j] - previous.basis[i][j]
				
		from math import copysign
		for i in range(self.number_of_atoms):
			if self.box_mode == 'orthorhombic' or self.box_mode == 'triclinic':

				if self.sx == []:
					self.update_internal_from_external()	


				ds1 = self.sx[i] - previous.sx[i] 
				ds2 = self.sy[i] - previous.sy[i] 
				ds3 = self.sz[i] - previous.sz[i] 
				s = [self.sx[i], self.sy[i], self.sz[i]]
				
				if self.periodic[0] == True and abs(ds1) > 0.5:
					ds1 = 1 - (ds1 * copysign(1, ds1))

				if self.periodic[1] == True and abs(ds2) > 0.5:
					ds2 = 1 - (ds2 * copysign(1, ds2))
	
				if self.periodic[2] == True and abs(ds3) > 0.5:
					ds3 = 1 - (ds3 * copysign(1, ds3))		
				ds = [ds1, ds2, ds3]
				

				dr_part_1 = self.matrix_vector_product_3D(dbasis, s)	
				dr_part_2 = self.matrix_vector_product_3D(self.basis, ds) # in most systems, this is the largest contributor
				dr_part_3 = dorigin
				dr = self.vector_vector_add_3D(dr_part_1, self.vector_vector_add_3D(dr_part_2,dr_part_3))
				
				dx = dr[0]
				dy = dr[1]
				dz = dr[2]
				
			else:
				dx = self.x[i] - previous.x[i]
				dy = self.y[i] - previous.y[i]
				dz = self.z[i] - previous.z[i]
		
			x_list.append(dx)
			y_list.append(dy)
			z_list.append(dz)

		return x_list, y_list, z_list
	
	else:
		print 'compute_displacments_from_previous: Different numbers of atoms, cannot compute velocities.'
	

def compute_velocities_from_previous(self,previous, dt):  # renamed for clarity by mike 3/28/2012
	'''Uses the current GAS object, a previous GAS and given time step, dt, to compute particle velocities. Corrects for periodic boundary conditions. Assumes atom order is unchanged between systems. '''
	delta_x, delta_y, delta_z = self.compute_displacements_from_previous(self, previous)
	self.vx = delta_x/dt
	self.vy = delta_y/dt
	self.vz = delta_z/dt


#	def get_box(self,padding = [0.0,0.0,0.0]):
#		'''Retrives values for box according to maximum and minimum values, plus padding to the maximum values. Default value for padding is [0.0, 0.0, 0.0]'''
#		if self.box == [[],[],[]]:
#			new_box = [[min(self.x),max(self.x) + padding[0]],
#				[min(self.y),max(self.y) + padding[1]],
#				[min(self.z),max(self.z) + padding[2]]]
#			return new_box
#		else:
#			return self.box

def compute_mass(self): #Modified 18 March 13 to match use of masses as atomistic attributes
	''' Computes total system mass from self.masses. If needed, self.masses is updated from self.element by referencing element_symbol_list and elemenet_mass_list'''
	if self.masses == []:
		self.update_masses_from_element() 

	self.mass = sum(self.masses)

	return self.mass

def compute_density(self,conversion_factor = ''): 
	''' Uses the compute_mass and compute_box_volume to give the system density. It uses those GAS.masses and GAS.box/GAS.basis units.'''
	m = self.compute_mass()
	v = self.compute_box_volume()
	if conversion_factor == 'metal':
		m = m*1.66053892
	return m/v

def compute_system_velocity(self):  #Modified 18 March 13 to match use of masses as atomistic attributes
	'''Computes velocity of the system center of mass from element, vx, vy and xz by referencing element_symbol_list and element_mass_list. '''
	vx = 0.0
	vy = 0.0
	vz = 0.0
	self.compute_mass()
	for i in range(self.number_of_atoms):
		vx += self.masses[i]*self.vx[i]
		vy += self.masses[i]*self.vy[i]
		vz += self.masses[i]*self.vz[i]
	return [vx/self.mass, vy/self.mass, vz/self.mass]
							


def compute_minimum_image_vector_between_two_particles(self,index1,index2): # mike 16-8-2012
	'''Vector pointing from particle 1 to particle 2'''
	if self.box_mode == 'orthorhombic' or self.box_mode == 'triclinic':

		if self.basis == []:
			print 'compute_minimum_image_vector_between_two_particles: ERROR This needs a basis to function. Go fix that.'

		if self.sx == []:
			self.update_internal_from_external()

		from math import copysign, floor
		ds1 = self.sx[index2] - floor(self.sx[index2]) - self.sx[index1] + floor(self.sx[index1])
		ds2 = self.sy[index2] - floor(self.sy[index2]) - self.sy[index1] + floor(self.sy[index1])
		ds3 = self.sz[index2] - floor(self.sz[index2]) - self.sz[index1] + floor(self.sz[index1])
		
		if self.periodic[0] == True and abs(ds1) > 0.5:
			ds1 = ds1 - copysign(1, ds1)
		
		if self.periodic[1] == True and abs(ds2) > 0.5:
			ds2 = ds2 - copysign(1, ds2)

		if self.periodic[2] == True and abs(ds3) > 0.5:
			ds3 = ds3 - copysign(1, ds3)
				
		ds = [ds1, ds2, ds3]

		dr = self.matrix_vector_product_3D(self.basis, ds)
	else:
		dr = [self.x[index2] - self.x[index1],
			self.y[index2] - self.y[index1],
			self.z[index2] - self.z[index1]]
	return dr

def compute_minimum_image_distance_between_two_particles(self,index1,index2): # mike 16-8-2012
	from math import sqrt
	dr = self.compute_minimum_image_vector_between_two_particles(index1,index2)
	absdr = sqrt(self.dot_product_3D(dr,dr))
	return absdr
	
def compute_minimum_image_angle(self,center_index,index1,index2): 
	'''Computes the angle of 3 particles form in radians'''
	from math import acos
	j = center_index
	i, k = index1, index2
	ij = self.compute_minimum_image_distance_between_two_particles(i,j)
	jk = self.compute_minimum_image_distance_between_two_particles(j,k)
	ik = self.compute_minimum_image_distance_between_two_particles(i,k)
	angle = acos( (jk**2 + ij**2 - ik**2) / (2 * jk * ij) )	# law of cosines
	return angle
	
def compute_minimum_image_dihedral(self, index1, index2, index3, index4): 
	'''Computes the dihedral angle of 4 particles form in radians'''
	from math import acos, sqrt
	v1 = self.compute_minimum_image_vector_between_two_particles(index2, index1)
	v2 = self.compute_minimum_image_vector_between_two_particles(index3, index2)
	v3 = self.compute_minimum_image_vector_between_two_particles(index3, index4)

	n1 = self.cross_product_3D(v1, v2)
	n2 = self.cross_product_3D(v3, v2)

	top = self.dot_product_3D(n1, n2)
	bottom =  sqrt(self.dot_product_3D(n1, n1) * self.dot_product_3D(n2, n2))

	angle = acos(top/bottom)
	return angle
	
